#include "BasicUtilities.h"
#include "Rectangle.h"

#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>

Rectangle::Rectangle(){};

Rectangle::Rectangle(double _xMin, double _xMax, double _yMin, double _yMax, std::string _id)
    : xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax}, id{_id} {};

std::string Rectangle::toString(){
    std::string str = "id: " + id + " | x: [" + std::to_string(xMin) + ", " + std::to_string(xMax) + "] | y: [" + std::to_string(yMin) + ", " + std::to_string(yMax) + "]";
    return str;
}

// bool containsPoint(const Point &pt){
//     if(pt.x >= xMin & pt.x <= xMax & pt.y >= yMin & pt.y <= yMax){
//         return true;
//     }
//     return false;
// }

std::string Rectangle::isPointInRectangles(const Point &pt, const std::vector<Rectangle> &rects){
    std::string rectId{""};
    for(size_t i = 0; i < rects.size(); ++i){
        if(pt.x >= rects[i].xMin && pt.x <= rects[i].xMax && 
           pt.y >= rects[i].yMin && pt.y <= rects[i].yMax){
            rectId = rects[i].id;
            break;
        }
    }

    return rectId;
}

std::vector<Rectangle> Rectangle::readRectanglesFromCSV(std::string filePath){
    std::cout << "Reading file: " << filePath << "\n";
    auto start = std::chrono::system_clock::now();
    
    std::fstream fin;
    fin.open(filePath, std::fstream::in);

    if(!fin.is_open()) throw std::runtime_error("Could not open file (" + filePath + ")");

    std::string line, colName; //initialize variables that will be used to store input
    std::vector<std::string> colNames{"xmin", "xmax", "ymin", "ymax", "id"}; //these are the required column names
    std::map<int, std::string> colMap; //map the index to the column name
    std::vector<Rectangle> rects;
    if(fin.good()){
        std::getline(fin, line); //get the first row
        std::istringstream ss(line);

        
        std::vector<std::string> colNamesFound; //keeps track of which columns we've found
        //check column names, make sure they're valid
        int colNumber{0}; //keeps track of which column we're on
        
        std::cout << "checking format\n";
        while(std::getline(ss,colName, ',')){
            BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
            BasicUtilities::removeSubstrs(colName, "\"");
            BasicUtilities::removeSubstrs(colName, "\'");
            if(std::find(colNames.begin(), colNames.end(), colName) != colNames.end()){ //if this string is in the 'colNames' vector, add it to out 'colNamesFound' vector
                colNamesFound.push_back(colName);
            }
            //std::cout << "readAgents8\n";
            colMap.insert(std::pair<int, std::string>(colNumber,colName)); //add an entry to our map
            colNumber++;
        }
        //std::cout << "readAgents18\n";
        if(colNamesFound.size() != colNames.size()){
            std::string colsString = BasicUtilities::combineStrings(colNames);
            std::string colsFoundString = BasicUtilities::combineStrings(colNamesFound);
            throw std::runtime_error("Error in Rectangle::readRectanglesFromCSV() while reading " + filePath + ": not all required columns included. Required columns are: (" + colsString + "). Of these names, these were found: (" + colsFoundString + ").");
        }

        std::cout << "begin reading in data lines\n";
        while(std::getline(fin, line)){
            //In a CSV I edited in Excel there were a bunch of empty lines (just a bunch of commas), and that was breaking this function. So this part removes all commas and spaces and sees if there's anything left. If not, then this row is empty, and we'll skip it.
            std::string lineCopy = line;
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ','), lineCopy.end()); // remove all commas
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ' '), lineCopy.end()); // remove all spaces
            if(lineCopy.length() > 0){

                Rectangle rect;
                std::string val;
                std::stringstream ss(line);
                int colIdx{0};
                
                while(std::getline(ss,val, ',')){
                    
                    BasicUtilities::cleanString(val);
                    
                    std::string column = colMap[colIdx];
                    if(val.length() > 0){ //skip this entry if there's no value;
                        std::smatch numMatch;
                        
                        if(column == "id"){ rect.id = val; }
                        else if (column == "xmin") { rect.xMin = std::stod(val); }
                        else if (column == "xmax") { rect.xMax = std::stod(val); }
                        else if (column == "ymin") { rect.yMin = std::stod(val); }
                        else if (column == "ymax") { rect.yMax = std::stod(val); }
                    } else {
                        std::cout << "WARNING: Empty value found in column '" << column << "'\n";
                    }
                    colIdx++;
                }
                rects.push_back(rect);
            }

        }
    }
    fin.close();
    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
    std::cout << "Reading in file took: " << duration.count() << " ms \n"; 
    return rects;
}