#include "Agent.h"
#include "AgentPoint.h"
#include "Point.h"
#include <string>

AgentPoint::AgentPoint() : Point{0, 0}{}

AgentPoint::AgentPoint(double _x, double _y) : Point{_x, _y}{}

//AgentPoint::AgentPoint(double _x, double _y, int _id) : Point{_x, _y}, id{_id}{}

std::string AgentPoint::toString() const{
    std::string str = "x: " + std::to_string(x) + " | y: " + std::to_string(y);
    return(str);
}


std::shared_ptr<AgentPoint> makeAgentPoint(double x, double y){
    AgentPoint* agentPoint = new AgentPoint(x, y);
    agentPoint->ptr = std::shared_ptr<AgentPoint>(agentPoint);
    // agentPoint->agent = _agent;
    // agentPoint->pointCell = _pointCell;
    return agentPoint->ptr;
}

// std::shared_ptr<AgentPoint> makeAgentPoint(double x, double y, std::shared_ptr<Agent> _agent, std::shared_ptr<PointCell> _pointCell){
//     AgentPoint* agentPoint = new AgentPoint(x, y);
//     agentPoint->ptr = std::shared_ptr<AgentPoint>(agentPoint);
//     agentPoint->agent = _agent;
//     agentPoint->pointCell = _pointCell;
//     return agentPoint->ptr;
// }