#include "Matrix.h"
#include <iostream>
#include <vector>
#include <cassert>
#include <string>
#include <cmath>

Matrix::Matrix(double val, int _nrow, int _ncol) : nrow{_nrow}, ncol{_ncol}
{
    vec = std::vector<double>(nrow*ncol, val);
}

Matrix::Matrix(std::vector<double> _vec, int _nrow, int _ncol) 
    : vec(_vec), nrow{_nrow}, ncol{_ncol} 
{
    assert(vec.size() == (size_t)nrow*ncol);
}
int Matrix::nRow() const{ return nrow; }
int Matrix::nCol() const{ return ncol; }
int Matrix::size() const{ return vec.size(); }

std::vector<double> Matrix::asVector() const{ return vec; }

Matrix Matrix::getRow(const int index) const{
    std::vector<double> slice(ncol);
    for(int i = 0; i < ncol; i++){
        slice[i] = vec[index * ncol + i];
    }
    return Matrix(slice, 1, ncol);
}

Matrix Matrix::getCol(const int index) const{
    std::vector<double> slice(nrow);
    for(int i = 0; i < nrow; i++){
        slice[i] = vec[index + ncol*i];
    }
    return Matrix(slice, nrow, 1);
}

Matrix Matrix::subset(int rMin, int rMax, int cMin, int cMax) const{
    int nRow = rMax - rMin + 1;
    int nCol = cMax - cMin + 1;
    std::vector<double> sub(nRow*nCol);
    int counter{0};
    for(int i = rMin; i <=rMax; i++){
        std::vector<double> row_i = getRow(i).asVector();
        for(int j = cMin; j <= cMax; j++){
            sub[counter] = row_i[j];
            counter++;
        }
    }
    Matrix subset = Matrix(sub, nRow, nCol);
    return subset;
}

Matrix Matrix::flipRows() const{
    std::vector<double> newVec(vec.size());
    int counter{0};
    for(int i = nrow-1; i >= 0; i--){
        std::vector<double> sub = getRow(i).asVector();
        for(int j = 0; j < ncol; j++){
            newVec[counter] = sub[j];
            counter++;
        }
    }
    Matrix newMat = Matrix(newVec, nrow, ncol);
    return newMat;
}

double Matrix::getByIndex(const int index) const{
    return vec[index];
}

double Matrix::mean() const{
    double sum = 0;
    for(size_t i = 0; i < vec.size(); i++){
        sum += vec[i];
    }
    return sum/vec.size();
}

double Matrix::min() const{
    double min = vec[0];
    for(size_t i = 1; i < vec.size(); i++){
        if(vec[i] < min){
            min = vec[i];
        }
    }
    return min;
}

double Matrix::max() const{
    double max = vec[0];
    for(size_t i = 1; i < vec.size(); i++){
        if(vec[i] > max){
            max = vec[i];
        }
    }
    return max;
}

int Matrix::countNans() const{
    int count = 0;
    for(size_t i = 0; i < vec.size(); i++){
        if(std::isnan(vec[i])){
            count++;
        }
    }
    return count;
}

std::string Matrix::toString() const{
    std::string str = "";
    for(int i = 0; i < nrow; i++){
        for(int j = 0; j < ncol; j++){
            str = str + std::to_string(getByIndex(i*ncol + j)) + " ";
        }
        str = str + "\n";
    }
    return(str);
}
