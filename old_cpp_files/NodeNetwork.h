#ifndef NODENETWORK_H
#define NODENETWORK_H


#include "Node.h"
#include "Quadtree.h"
#include "Matrix.h"
#include "Point.h"
#include <vector>
#include <map>
#include <memory>


class NodeNetwork{
    public:

        Matrix adjMat;

        //having these two is redundant, but I wanted to be able to look up both ways - look up the network ID of a node, and get a node from the network ID. This is a sloppy solution, and it introduces the possibility of there being a discrepancy between the two, so it's not ideal
        std::map<int, int> nodeIDToNetID; //first int is the ID of the original node (i.e. node->id) and the second int is the ID used in this network (corresponds with the adjacency matrix)
        //std::map<int, std::shared_ptr<Node>> netIDtoNode;
        std::vector<std::shared_ptr<Node>> nodes; //these will be ordered based on net ID - i.e. the node associated with network vertex 4 will be store at index 4

        NodeNetwork(Quadtree &qt);
        NodeNetwork(Quadtree &qt, double xMin, double xMax, double yMin, double yMax);

        void createNetwork(Quadtree& qt, double xMin, double xMax, double yMin, double yMax);
        //void getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>>> &networkNodes);
        void getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::shared_ptr<Node>> &networkNodes);
        std::vector<std::shared_ptr<Node>> getShortestPath(int nodeID1, int nodeID2);
};








#endif