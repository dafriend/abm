#include "LcpFinder.h"
#include "MathUtilities.h"
#include "NodeNetwork.h"

NodeNetwork::NodeNetwork(Quadtree &qt){
    nodes = std::vector<std::shared_ptr<Node>>();
    createNetwork(qt, qt.root->xMin, qt.root->xMax, qt.root->yMin, qt.root->yMax);
}

NodeNetwork::NodeNetwork(Quadtree& qt, double xMin, double xMax, double yMin, double yMax){
    nodes = std::vector<std::shared_ptr<Node>>();
    createNetwork(qt, xMin, xMax, yMin, yMax);
}

void NodeNetwork::createNetwork(Quadtree& qt, double xMin, double xMax, double yMin, double yMax){
    if(xMin < qt.root->xMin) xMin = qt.root->xMin;
    if(xMax > qt.root->xMax) xMax = qt.root->xMax;
    if(yMin < qt.root->yMin) yMin = qt.root->yMin;
    if(yMax > qt.root->yMax) yMax = qt.root->yMax;

    //this might be the craziest nested object I've ever made
    // std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>>> networkNodes;
    //std::vector<std::shared_ptr<Node>> networkNodes;
    //nodes = std::vector<std::shared_ptr<Node>>();
    std::shared_ptr<Node> startNode = qt.getNode((xMin+xMax)/2, (yMin+yMax)/2);
    std::cout << "getConnectedNodes\n";
    getConnectedNodes(startNode, xMin, xMax, yMin, yMax, nodes);

    //make a map so that we can link the node ID's to the IDs of the network
    //nodes = std::vector<std::shared_ptr<Node>>(networkNodes.size());
    //nodes = std::vector<std::shared_ptr<Node>>(nodes.size());
    for(size_t i = 0; i < nodes.size(); ++i){
        nodeIDToNetID[nodes.at(i)->id] = i; 
        //nodes.at(i) = networkNodes.at(i);
    }

    std::cout << "get distances\n";
    //Loop through 'networkNodes', and use the neighbors and distances we calculated to populate 'adjMat'
    adjMat = Matrix(NAN,nodes.size(), nodes.size());
    for(size_t i = 0; i < nodes.size(); ++i){
        if(i%100 == 0){
            std::cout << "i: " << i << " of " << nodes.size() << "\n";
        }
        //std::vector<std::pair<std::shared_ptr<Node>,double>> &nbs = nodes.at(i).second; //for code readability
        Point nodePoint = Point((nodes.at(i)->xMax + nodes.at(i)->xMin)/2, (nodes.at(i)->yMax + nodes.at(i)->yMin)/2); //get the point representing the center of 'node'
        int nodeNetID = nodeIDToNetID[nodes.at(i)->id];
        for(size_t j = 0; j < nodes.at(i)->neighbors.size(); ++j){
            std::shared_ptr<Node> nb = nodes.at(i)->neighbors.at(j);
            //int nbNodeID = nodes.at(i)->neighbors.at(j)->id;
            if(nodeIDToNetID.find(nb->id) != nodeIDToNetID.end()){
                int nbNetID = nodeIDToNetID[nb->id];
                if(std::isnan(adjMat.getValue(nodeNetID,nbNetID))){
                    Point nbPoint = Point((nb->xMax + nb->xMin)/2, (nb->yMax + nb->yMin)/2); //get the point representing the center of 'node'
                    //Point nbPoint = Point((nodes.at(i)->neighbors.at(j)->xMax + nodes.at(i)->neighbors.at(j)->xMin)/2, (nodes.at(i)->neighbors.at(j)->yMax + nodes.at(i)->neighbors.at(j)->yMin)/2); //get the point representing the center of 'node'
                    double dist = MathUtilities::distBtwPoints(nodePoint, nbPoint);
                    adjMat.setValue(dist,nodeNetID, nbNetID);
                    adjMat.setValue(dist, nbNetID, nodeNetID);
                }
            }
        }
    }
}

//void NodeNetwork::getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>>> &networkNodes){
void NodeNetwork::getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::shared_ptr<Node>> &networkNodes){
    if(networkNodes.size()%100 == 0){
        std::cout << "networkNodes.size(): " << networkNodes.size() << "\n";
    }
    //std::vector<std::pair<std::shared_ptr<Node>,double>> nodeDistVec;
    networkNodes.push_back(node);
    //networkNodes.emplace_back(std::make_pair(node, nodeDistVec));
    //int index = networkNodes.size()-1;
    if(node->neighbors.size() > 0){
        //std::vector<std::pair<std::shared_ptr<Node>,double>> nodeDists;
        Point nodePoint = Point((node->xMax + node->xMin)/2, (node->yMax + node->yMin)/2); //get the point representing the center of 'node'
        for(size_t i = 0; i < node->neighbors.size(); ++i){
            std::shared_ptr<Node> nb = node->neighbors.at(i); //this is to make the code more readable
            bool isAlreadyIncluded = false;
            //for(size_t j = 0; j < networkNodes.size(); ++j){ //check to see if we've already added this node
            for(size_t j = 0; j < networkNodes.size(); ++j){ //check to see if we've already added this node
                //if(networkNodes.at(j).first->id == nb->id){
                if(networkNodes.at(j)->id == nb->id){
                    isAlreadyIncluded = true;
                    break;
                }
            }
            //if(std::find(networkNodes.begin(), networkNodes.end(), nb) == networkNodes.end()){ //check if this node is already included
            if(!isAlreadyIncluded){ //check if this node is already included
                bool isXValid = !(xMax < nb->xMin || xMin > nb->xMax);
                bool isYValid = !(yMax < nb->yMin || yMin > nb->yMax);
                if(isXValid && isYValid){
                    //Point nbPoint = Point((nb->xMax + nb->xMin)/2, (nb->yMax + nb->yMin)/2); //get the point representing the center of 'node'
                    //double distBtwCells = MathUtilities::distBtwPoints(nodePoint, nbPoint); //get the distance btw the nodes
                    //std::pair<std::shared_ptr<Node>,double> nodeDistPair(nb,distBtwCells);
                    //networkNodes.emplace_back(node->neighbors.at(i));
                    //networkNodes.emplace_back(std::make_pair(nb))
                    //nodeDists.emplace_back(std::make_pair(nb,distBtwCells)); //add the distance to the 
                    //networkNodes.at(index).second.emplace_back(std::make_pair(nb,distBtwCells)); //add the distance to the 
                    // networkNodes.emplace_back(node->id);
                    getConnectedNodes(node->neighbors.at(i), xMin, xMax, yMin, yMax, networkNodes);
                }
            }
        }
        //networkNodes.emplace_back(std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>*>(node,&nodeDists));
    }
}   

std::vector<std::shared_ptr<Node>> NodeNetwork::getShortestPath(int nodeID1, int nodeID2){
    LcpFinder spf(adjMat, nodeIDToNetID[nodeID1]);
    std::cout << "get shortest path\n";
    std::vector<int> spNetIDs = spf.getShortestPath(nodeIDToNetID[nodeID2]);
    std::vector<std::shared_ptr<Node>> spNodes(spNetIDs.size());
    std::cout << "return nodes\n";
    for(size_t i = 0; i < spNodes.size(); ++i){
        spNodes.at(i) = nodes.at(spNetIDs.at(i));
    }
    return spNodes;
}