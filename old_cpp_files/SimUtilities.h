#ifndef SIMUTILITIES_H
#define SIMUTILITIES_H

#include "Agent.h"
#include <vector>
#include <list>
#include <random>
#include <memory>

class Point;

class Quadtree;

namespace SimUtilities{

    //MOVEMENT
    // Point getStraightDistEndPoint(const Point& firstPoint, const Point &penultimatePoint, const Point& lastPoint, double maxDistance);
    // Point getTotalDistEndPoint(const Point &point1, const Point& point2, double distance);
    // Point getNextPoint(const Quadtree& qt, const Point& currentPt, const Point& prevPt, const Point& attrPt, int nPoints, double dist, double valExp, double attrExp, double corExp, std::mt19937& randomGenerator, bool hasPrev, bool debug);
    // std::list<Point> moveAgent(const Quadtree &qt, const Point& startPoint, const Point& attractPoint, int nCheckPoints, double stepSize, double maxTotalDistance, double maxStraightLineDistance, double maxTotalDistanceSubStep, double qualityExp1, double attractExp1, double directionExp1, double qualityExp2, double attractExp2, double directionExp2, std::mt19937& randomGenerator, bool debug); // i 2/19/2021

    //REPRODUCTION
    // void mutate(std::vector<std::vector<int>> &alleles, std::mt19937 &randomGenerator);
    std::vector<std::vector<int>> breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, double mutateProb, std::mt19937 &randomGenerator);
    // std::vector<std::vector<int>> breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, std::mt19937 &randomGenerator);
    //std::shared_ptr<Agent> makeNewAgent(std::shared_ptr<Agent> &agent1, std::shared_ptr<Agent> &agent2);
}

#endif