#ifndef AGENTPOINT_H
#define AGENTPOINT_H

//#include "PointCell.h"
//#include "Agent.h"
#include "Point.h"
#include <memory>
#include <string>

class Agent;

class PointCell;

class PointGrid;

class AgentPoint : public Point {
public:
  //int id;
  std::shared_ptr<AgentPoint> ptr; //pointer to itself
  std::shared_ptr<Agent> agent; //pointer to its agent
  std::shared_ptr<PointCell> pointCell; //pointer to the pointCell it's in
  

  AgentPoint();
  AgentPoint(double _x, double _y);
  //AgentPoint(double _x, double _y, int _id);
  
  virtual std::string toString() const;
};

std::shared_ptr<AgentPoint> makeAgentPoint(double x, double y);
//std::shared_ptr<AgentPoint> makeAgentPoint(double x, double y, std::shared_ptr<Agent> _agent, std::shared_ptr<PointCell> _pointCell);

#endif