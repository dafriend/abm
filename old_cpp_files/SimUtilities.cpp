#include "Point.h"
#include "Quadtree.h"
#include "Node.h"
#include "MathUtilities.h"
#include "PointUtilities.h"
#include "SimUtilities.h"

#include <vector>
#include <list>
#include <memory>
#include <random>
#include <cmath>
#include <iostream>
#include <math.h>
#include <cassert>

// //for use in the movement algorithm. This is for the situation in which we want to 
// //guarantee that the distance travelled is exactly some straight line distance away
// //from the starting point. That point will almost certainly occur between steps. So
// //this function is designed to be used when, after a step, we've exceeded the 
// //user-defined maximum distance. Based on the starting point, the second to last point
// //(which is within the maximum distance of the starting point), and the last point 
// //(which is more than maximum distance away from the starting point), this function
// //finds the point between the second to last point and the last point where the 
// //distance from the starting point is exactly 'maximum disance'. 
// Point SimUtilities::getStraightDistEndPoint(const Point& firstPoint, const Point &penultimatePoint, const Point& lastPoint, double maxDistance){
//     //notation: note that 'x' in variable names refers to the unknown point location I'm trying to find
//     //get distances between all points
//     double dist_fp = PointUtilities::distBtwPoints(firstPoint, penultimatePoint); // 'dist_f_p' is for 'distance from First point to Penultimate point. Same pattern is used for other distance variables
//     double dist_fl = PointUtilities::distBtwPoints(firstPoint, lastPoint);
//     double dist_pl = PointUtilities::distBtwPoints(penultimatePoint, lastPoint);



//     // if(dist_fp + dist_pl == dist_fl){ //this is for the edge case where all three points fall on a line, in which case the trigonometry approach doesn't work. But in this case it's actually pretty easy to find the right point.
//     //     std::cout << "ALL ON A LINE!!!\n";
//     //     double angle_fl = std::atan2((lastPoint.y - firstPoint.y), (lastPoint.x - firstPoint.x)); //get angle between firstPoint and lastPoint, relative to the x axis
//     //     Point newPoint = Point(firstPoint.x + maxDistance*std::cos(angle_fl), firstPoint.y + maxDistance*std::sin(angle_fl)); //use the angle and the max distance to get the coordinates of the new point
//     //     return newPoint;
//     // } else {
//         // std::cout << "NORMAL!!!\n";
//         // std::cout << "dist_fp: " << dist_fp << "\n";
//         // std::cout << "dist_pl: " << dist_pl << "\n";
//         // std::cout << "dist_fl: " << dist_fl << "\n";
//         // std::cout << "dist_fp + dist_pl: " << dist_fp + dist_pl << "\n";
//         // std::cout << "dist_fp + dist_pl == dist_fl: " << (dist_fp + dist_pl == dist_fl) << "\n";
//         double angle_fpl = std::acos((std::pow(dist_fp,2) + std::pow(dist_pl,2) - std::pow(dist_fl,2))/(2*dist_fp*dist_pl)); //use the law of cosines to find the angle formed by (firstPoint-penultimatePoint-lastPoint)
//         // std::cout << "angle_fpl: " << angle_fpl << "\n";
//         //If 'angle_fpl' is NaN then we'll assume this means that the three points are on a straight line (I think this is a valid assumption - the only problem I see is that it doesn't tell us anything about what order the points are in. For this to work as planned, it assumes the order of the points on the line is firstPoint-penultimatePoint-lastPoint. if the order was instead firstPoint-lastPoint-penultimatePoint then this would break. BUT - this function is only called when the distance between firstPoint and lastPoint exceeds the max straight line distance. Because of this it's not possible for this function to be called when lastPoint is closer than firstPoint, because in the previous iteration this function would have been called (since in this case if lastPoint is more than the max distance away from firstPoint then penultimatePoint also has to be more than the max distance away from firstPoint, meaning that this function would be called in the previous iteration) and the movement process would have ended.
//         //I've also noticed that there are times when the points fall on a straight line but angle_fpl ISN'T NaN - there are cases where dist_fp + dist_pl == dist_fl == TRUE (which is only possible when the points fall on a straight line) but angle_fpl ISN'T NaN. Seems weird to me that the math would come out perfect in one case but not the other. I'm not sure if I need to add this second condition (dist_fp + dist_pl == dist_fl), since if the error is big enough for the trigonometry to work then we should get the right answer anyway (or at least very very very close, which is good enough for me). But better safe than sorry - I just hope this doesn't introduce other errors in certain edge cases that I haven't thought of yet.
//         if(isnan(angle_fpl) || dist_fp + dist_pl == dist_fl){
//             // std::cout << "ALL ON A LINE!!!\n";
//             double angle_fl = std::atan2((lastPoint.y - firstPoint.y), (lastPoint.x - firstPoint.x)); //get angle between firstPoint and lastPoint, relative to the x axis
//             Point newPoint = Point(firstPoint.x + maxDistance*std::cos(angle_fl), firstPoint.y + maxDistance*std::sin(angle_fl)); //use the angle and the max distance to get the coordinates of the new point
//             return newPoint;
//         }
//         double angle_fxp = std::asin((std::sin(angle_fpl)*dist_fp)/maxDistance); //use the law of sines to find the angle formed by (firstPoint-x-penultimatePoint) - this triangle is ASS so there COULD be two solutions - but I think the properties of this triangle mean there will always be one solution. see: https://mathbitsnotebook.com/Geometry/TrigApps/TAUsingLawSines.html - in our situation I think the angle will always be acute and S2 will always be longer than S1. I've tested this out (originally I tried both possible angles) and the one I'm using now was always the correct one, and the other angle was always wrong. Also I tested this function with 100000 random points (where pt1 was always less than max_dist away from first_pt, and pt2 was always more than max_dist away from first_pt) and there were no errors. So I'm fairly confident this is correct.
//         double angle_pfx = M_PI - angle_fpl - angle_fxp; //get the angle formed by (penultimatePoint-firstPoint-x)

//         double dist_px = (std::sin(angle_pfx)*maxDistance)/std::sin(angle_fpl); //get the distance between penultimatePoint and x
//         double angle_pl = std::atan2((lastPoint.y - penultimatePoint.y), (lastPoint.x - penultimatePoint.x)); //get angle between penultimatePoint and lastPoint, relative to the x axis
//         Point newPoint = Point(penultimatePoint.x + dist_px*std::cos(angle_pl), penultimatePoint.y + dist_px*std::sin(angle_pl)); //use the angle and the distance to get the coordinates of the new point
        
        
//         // std::cout << "angle_fxp: " << angle_fxp << "\n";
//         // std::cout << "angle_pfx: " << angle_pfx << "\n";
//         // std::cout << "dist_px: " << dist_px << "\n";
//         // std::cout << "angle_pl: " << angle_pl << "\n";
//         // std::cout << newPoint.toString() << "\n";
//         return newPoint;
//     // }
// }

// //finds the point on the segment between point1 and point2 that is 'distance' away 
// //from point1 (assumption is that 'distance' is less than the distance between
// //point1 and point2)
// Point SimUtilities::getTotalDistEndPoint(const Point &point1, const Point& point2, double distance){
//     double angle_12 = std::atan2((point2.y - point1.y), (point2.x - point1.x)); //get angle between penultimatePoint and lastPoint, relative to the x axis
    
//     Point newPoint(point1.x + distance*std::cos(angle_12), point1.y + distance*std::sin(angle_12)); //use the angle and the distance to get the coordinates of the new point
//     return newPoint;
// }

// Point SimUtilities::getNextPoint(const Quadtree& qt, const Point& currentPt, const Point& prevPt, const Point& attrPt, int nPoints, double dist, double valExp, double attrExp, double corExp, std::mt19937& randomGenerator, bool hasPrev, bool debug=false){
    
//     //DEBUGGING {
//     if(debug){
//         std::cout << "---------------------\n";
//         std::cout << "getNextPoint()\n";
//         std::cout << "currentPt: " << currentPt.toString() << "\n";
//         std::cout << "prevPt: " << prevPt.toString() << "\n";
//         std::cout << "attrPt: " << attrPt.toString() << "\n";
//         std::cout << "nPoints: " << nPoints << "\n";
//         std::cout << "dist: " << dist << "\n";
//         std::cout << "valExp: " << valExp << "\n";
//         std::cout << "attrExp: " << attrExp << "\n";
//         std::cout << "corExp: " << corExp << "\n";
//     }
//     //DEBUGGING }

//     std::vector<Point> circlePoints = PointUtilities::getPointsAroundPoint(currentPt, nPoints, dist); //get 'nPoints' around 'currentPt' with radius = 'dist'
    
//     //DEBUGGING {
//     if(debug){    
//         std::cout << "   circlePoints:\n";
//         for(size_t i = 0; i < circlePoints.size(); ++i){
            
//             std::cout << "      " << i << ": " << circlePoints[i].x << ", " << circlePoints[i].y << "\n";
//         }
//     }
//     //DEBUGGING }

//     std::vector<std::tuple<double, double, double, Point>> pointVals(circlePoints.size()); //create a vector where we'll store the info on each point - the three doubles store the resistance value, angle value, and correlation value, respectively
//     int nNotNan{0}; //there's a good possibility that we'll be checking values outside of the quadtree's extent, in which case we'll get NaNs. When we get an NaN we don't add anything to the vector, so we'll increment this when the value isn't NaN, and this will tell us the length of resulting vector
//     for(size_t i = 0; i < circlePoints.size(); ++i){ 
//         double val = 1-qt.getValue(circlePoints[i]); //the value of the quadtree at this point - subtract one becaue the value is the resistance - and in this case we want higher values to represent better values
//         if(!std::isnan(val)){ //if it's not NaN, then add it to our vector of tuples
//             double angleVal = PointUtilities::getAngleOffsetVal(currentPt, PointUtilities::getAngle(currentPt, attrPt), circlePoints[i]); //get the angle value (based on the start pt and the attraction point - basically returns a value where the closer the point is to being directly on the line between the two, the closer the value is 1)
//             //double corVal{std::numeric_limits<double>::quiet_NaN()}; //initialize our correlation as NaN, as if this is the first point there won't be a previous point
//             double corVal{1}; //this fixes the problem I was having with points near the left and bottom jumping to 0,0... but why?
//             if(!hasPrev){ //if there is a previous point, get the direction correlation value
//                 //Rcout << "has prev" << std::endl;
                
//                 corVal = PointUtilities::getAngleOffsetVal(currentPt, PointUtilities::getAngle(prevPt, currentPt), circlePoints[i]); //get the correlation value

//             }
//             pointVals[nNotNan] = std::make_tuple(val, angleVal, corVal, circlePoints[i]); //make a tuple for the values and the point
//             ++nNotNan; //keep track of how many 'not NaNs' we have.
//         }
//     }

//     //if ALL of the points are NA, then just return the current point.
//     if(nNotNan == 0){
//         return currentPt;
//     } else {
//         //DEBUGGING {
//         if(debug){
//             std::cout << "   Component probs\n";
//             for(int i = 0; i < nNotNan; ++i){
//                 std::cout << "      " << i << ": " << std::get<3>(pointVals[i]).x << ", " << std::get<3>(pointVals[i]).y << " | resist prob: " << std::get<0>(pointVals[i]) << " | attract prob: " << std::get<1>(pointVals[i]) << " | direction prob: " << std::get<2>(pointVals[i]) << "\n";
//             }
//         }
//         //DEBUGGING }

//         std::tuple<double, double, double> sums(std::make_tuple(0,0,0)); //get the sums of the values we calculated above
//         for(int i = 0; i < nNotNan; ++i){
//             std::get<0>(sums) = std::get<0>(sums) + std::get<0>(pointVals[i]);
//             std::get<1>(sums) = std::get<1>(sums) + std::get<1>(pointVals[i]);
//             std::get<2>(sums) = std::get<2>(sums) + std::get<2>(pointVals[i]);
//         }

//         //DEBUGGING {
//         if(debug){
//             std::cout << "   Summed probabilities\n";
//             std::cout << "      resist sum: " << std::get<0>(sums) << " | attract sum: " << std::get<1>(sums) << " | direction sum: " << std::get<2>(sums) << "\n";
//         }
//         //DEBUGGING }

//         std::vector<double> probs(nNotNan);
//         double probSum{0};
//         for(int i = 0; i < nNotNan; ++i){
//             double prob = pow(std::get<0>(pointVals[i])/std::get<0>(sums), valExp) * 
//                         pow(std::get<1>(pointVals[i])/std::get<1>(sums), attrExp) *
//                         pow(std::get<2>(pointVals[i])/std::get<2>(sums), corExp); ;
            
//             probs[i] = prob;
//             probSum += prob;
//         }
//         for(size_t i = 0; i < probs.size(); ++i){
//             probs[i] = probs[i]/probSum;
//         }

//         //DEBUGGING {
//         if(debug){
//             std::cout << "   Final probs\n";
//             for(int i = 0; i < nNotNan; ++i){
//                 std::cout << "      " << i << ": " << std::get<3>(pointVals[i]).x << ", " << std::get<3>(pointVals[i]).y << " | " << probs[i] << "\n";
//             }
//         }
//         //DEBUGGING }
        
//         //<old method>
//         // std::random_device rd;  //Will be used to obtain a seed for the random number engine
//         // std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//         // std::discrete_distribution<int> distribution(probs.begin(), probs.end());
//         // int number = distribution(gen);
//         //</old method>

//         //<new method>
//         std::discrete_distribution<int> distribution(probs.begin(), probs.end());
//         int number = distribution(randomGenerator);
//         //</new method>

//         //DEBUGGING {
//         if(debug){
//             std::cout << "   Chosen point index\n";
//             std::cout << "      " << number << "\n"; 
//         }
//         //DEBUGGING }

//         //std::cout << number << std::endl;
//         return std::get<3>(pointVals[number]);
//     }
// }



// //one way to optimize would be to just return the final point rather than the
// //entire movement path. This would avoid having to use "push_back()" on every
// //loop.
// // std::vector<Point> SimUtilities::moveAgent(const Quadtree &qt, // 2/19/2021
// std::list<Point> SimUtilities::moveAgent(const Quadtree &qt, // 2/19/2021
//                     const Point& startPoint,
//                     const Point& attractPoint,
//                     int nCheckPoints,
//                     double stepSize,
//                     double maxTotalDistance,
//                     double maxStraightLineDistance,
//                     double maxTotalDistanceSubStep,
//                     //int maxSteps,
//                     //int maxSubSteps,
//                     double qualityExp1,
//                     double attractExp1,
//                     double directionExp1,
//                     double qualityExp2,
//                     double attractExp2,
//                     double directionExp2,
//                     std::mt19937& randomGenerator,
//                     bool debug=false){

//     //DEBUGGING {
//     if(debug){
//         std::cout << "=======================\n";
//         std::cout << "moveAgent\n";
//         std::cout << "=======================\n";
//     }
//     //DEBUGGING }

//     double sqMaxStraightLineDistance = std::pow(maxStraightLineDistance, 2);
    
//     Point currentPoint = startPoint;
//     std::shared_ptr<Node> currentNode = qt.getNode(startPoint);
//     Point prevPoint1; //initialize the two previous point variables to empty since we have no previous points yet
//     Point prevPoint2;
//     double totalDistance = 0; //create vars for keeping track of how far we've come
//     double sqStraightLineDistance = 0; //squared distance, because 'sqrt()' is compute intensive and we just as easily compare the squared values
    
//     //std::vector<Point> moveHistory(maxTotalDistance);
//     //std::vector<Point> moveHistory(1); // 2/19/2021
//     std::list<Point> moveHistory;// 2/19/2021
    
//     //std::vector<Point> moveHistory(maxSteps*maxSubSteps+1); //this is NOT memory efficient but avoids having to do 'push_back' all the time, which is expensive | add 1 because we're sticking the starting in the vector as well
//     //moveHistory.at(0) = currentPoint; // 2/19/2021
//     moveHistory.push_back(currentPoint); // 2/19/2021

//     bool doFirstLoop = true;
//     double nSteps = 0;
//     //start the top-level movement process
//     //for(int i = 0; i < maxSteps; ++i){
    
//     //DEBUGGING {
//     if(debug){
//         std::cout << "current point: " << currentPoint.x << ", " << currentPoint.y << "\n";
//         std::cout << "-----------------------------------\n";
//         std::cout << "STARTING MOVEMENT PROCESS\n";
//         std::cout << "-----------------------------------\n";
//     }
//     //DEBUGGING }
    
//     while(doFirstLoop){
        
//         //DEBUGGING {
//         if(debug){
//             std::cout << "1. top of FIRST loop\n";
//         }
//         //DEBUGGING }

//         //get the next point that we'll try to get to
//         Point nextPoint1 = getNextPoint(qt, currentPoint, prevPoint1, attractPoint, nCheckPoints, stepSize, qualityExp1, attractExp1, directionExp1, randomGenerator, debug);
//         std::shared_ptr<Node> nextNode1 = qt.getNode(nextPoint1);

//         //DEBUGGING {
//         if(debug){
//             std::cout << "1. first loop chosen point: " << nextPoint1.x << ", " << nextPoint1.y <<"\n";
//         }
//         //DEBUGGING }


//         bool doSecondLoop = true;
//         if(nextNode1->id != currentNode->id){ //if the next point is not in the same cell
//             for(size_t j = 0; j < currentNode->neighbors.size(); j++){ //check if the point chosen falls in a neighbor
//                 if(nextNode1->id == currentNode->neighbors[j].lock()->id){ //compare id's to see if the next point falls in this cell
//                     doSecondLoop = false;
//                     break;
//                 }
//             }
//         } else { //if the next point we chose is in the current cell, we can move directly there
//             doSecondLoop = false;
//         }

//         //DEBUGGING {
//         if(debug){
//             if(!doSecondLoop){
//                 std::cout << "1. No need for second loop\n";
//             }
//         }
//         //DEBUGGING }

//         if(doSecondLoop){ //begin lower-level movement process

//             //DEBUGGING {
//             if(debug){
//                 std::cout << "2. top of SECOND loop\n";
//             }
//             //DEBUGGING }

//             //for(int j = 0; j < maxSubSteps; ++j){
//             double totalDistanceSubStep = 0; //keep track of how far we've travelled in this substep
//             while(doSecondLoop){
//                 //double nearestNbDist = getNearestNeighborDist_2(currentPoint, currentNode); //get the closest distance from this point to the centroids of the neighboring cells
//                 double nearestNbDist = MathUtilities::getNearestNeighborDistance(currentNode, currentPoint); //get the closest distance from this point to the centroids of the neighboring cells
//                 if(nearestNbDist > stepSize){ //if this distance is greater than our step size then make this distance equal the step size
//                     nearestNbDist = stepSize;
//                 }
                
//                 //using the step size we just determined, get our next point
//                 Point nextPoint2 = getNextPoint(qt, currentPoint, prevPoint2, nextPoint1, nCheckPoints, nearestNbDist, qualityExp2, attractExp2, directionExp2, randomGenerator, debug);
                
//                 //update prevPoint2, currentPoint and currentNode
//                 prevPoint2 = currentPoint;
//                 currentPoint = nextPoint2;
//                 currentNode = qt.getNode(currentPoint); //future optimization - get next Node by looking through 'currentNodes' neighbors rather than going through the quadtree

//                 // totalDistance += nearestNbDist; //add the distance traveled to our total distance
//                 // totalDistanceSubStep += nearestNbDist; //add the distance traveled to our total distance
//                 // straightLineDistance = distanceBetweenPoints_2(startPoint, currentPoint);

//                 if(currentNode->id == nextNode1->id){ // check if we're in the same cell as the point chosen in the high level movement process
//                     //////NOTE
//                     //what I'm doing here (automatically making the point chosen in step 1 the current point if we're in the same cell as the point from step 1) might cause problems. I think there's probably an edge case where the distance between 'currentPoint' and 'nextPoint1' is greater than 'stepSize' even though they're in the same cell.
//                     //totalDistance += sqrt(sqDistBtwPoints_2(currentPoint, nextPoint1)); //add the additional distance to 'totalDistance'
//                     currentPoint = nextPoint1; //set the current point to be the point chosen in the top-level movement process
//                     currentNode = qt.getNode(currentPoint); 
//                     totalDistance += sqrt(PointUtilities::sqDistBtwPoints(prevPoint2, currentPoint));
//                     doSecondLoop = false;

//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "2. reached the target cell, ending second loop\n";
//                     }
//                     //DEBUGGING }

//                 } else {
//                     totalDistance += nearestNbDist; //add the distance traveled to our total distance
//                 }
                
//                 totalDistanceSubStep += nearestNbDist; //add the distance traveled to our total distance
//                 //straightLineDistance = distanceBetweenPoints_2(startPoint, currentPoint);
//                 sqStraightLineDistance = PointUtilities::sqDistBtwPoints(startPoint, currentPoint);
                
//                 //DEBUGGING {
//                 if(debug){
//                     std::cout << "2. totalDistance: " << totalDistance << "\n";
//                     std::cout << "2. totalDistanceSubStep: " << totalDistanceSubStep << "\n";
//                     std::cout << "2. sqStraightLineDistance: " << sqStraightLineDistance << "\n";
//                 }
//                 //DEBUGGING }

//                 //if(straightLineDistance >= maxStraightLineDistance){ // check if we've exceeded the maximum straight-line distance - if we have, we're done
//                 if(sqStraightLineDistance >= sqMaxStraightLineDistance){ // check if we've exceeded the maximum straight-line distance - if we have, we're done
//                     currentPoint = getStraightDistEndPoint(startPoint, prevPoint2, currentPoint, maxStraightLineDistance); // adjust the end point so that we're exactly 'maxStraightLineDistance' away from the starting point
//                     currentNode = qt.getNode(currentPoint); 
//                     doFirstLoop = false;
//                     doSecondLoop = false;

//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "2. Max straight-line distance exceeded, ending second loop\n";
//                     }
//                     //DEBUGGING }

//                 } else if(totalDistance >= maxTotalDistance){
//                     currentPoint = getTotalDistEndPoint(currentPoint, prevPoint2, totalDistance-maxTotalDistance); // adjust the end point so that we're exactly 'maxTotalDistance' away from the starting point
//                     currentNode = qt.getNode(currentPoint); 
//                     doFirstLoop = false;
//                     doSecondLoop = false;

//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "2. Max total distance exceeded, ending second loop\n";
//                     }
//                     //DEBUGGING }
//                 } else if(totalDistanceSubStep >= maxTotalDistanceSubStep){
//                     doSecondLoop = false; //should I add code here to adjust the last location so that 'totalDistanceSubStep == maxTotalDistanceSubStep'?

//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "2. Max total substep distance exceeded, ending second loop\n";
//                     }
//                     //DEBUGGING }
//                 }
//                 moveHistory.push_back(currentPoint);
//                 nSteps++;
//                 //moveHistory[++nSteps] = currentPoint;
//             }
//         } else {
//             prevPoint2 = currentPoint;//Need to reset the previous point of the second level movement process. Say that we hit a stretch where we don't go into the second loop for a while. If 'prevPoint2' never gets updated, when we do end up going into the second loop prevPoint2 is from a long time ago - and that'll influence the 'direction' component of the movement process
//             prevPoint1 = currentPoint;
//             currentPoint = nextPoint1;
//             currentNode = nextNode1;
//             totalDistance += stepSize;
//             //straightLineDistance = distanceBetweenPoints_2(startPoint, currentPoint);
//             sqStraightLineDistance = PointUtilities::sqDistBtwPoints(startPoint, currentPoint);
//             //if(straightLineDistance >= maxStraightLineDistance){ // check if we've exceeded the maximum straight-line distance - if we have, we're done
//             if(sqStraightLineDistance >= sqMaxStraightLineDistance){ // check if we've exceeded the maximum straight-line distance - if we have, we're done
                
//                 //DEBUGGING {
//                 if(debug){
//                     std::cout << "1. Max straight-line distance exceeded, ending first loop\n";
//                     std::cout << "      currentPoint, before: " << currentPoint.toString() << "\n";
//                 }
//                 //DEBUGGING }
                
//                 if(nSteps == 0){ //if this is the first step, 'getMaxDistEndPoint()' doesn't work because startPoint and prevPoint1 are the same. Not sure exactly why that is, but it's failing when it reaches maxStraighLineDistance in the first iteration. So instead use getTotDistEndPoint().
                    
//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "      using 'getTotalDistEndPoint()'\n";
//                     }
//                     //DEBUGGING }

//                     currentPoint = getTotalDistEndPoint(prevPoint1, currentPoint, maxStraightLineDistance);
//                 } else {
                    
//                     //DEBUGGING {
//                     if(debug){
//                         std::cout << "      using 'getStraightDistEndPoint()'\n";
//                     }
//                     //DEBUGGING }

//                     currentPoint = getStraightDistEndPoint(startPoint, prevPoint1, currentPoint, maxStraightLineDistance); // adjust the end point so that we're exactly 'maxDistance' away from the starting point
//                 }
//                 doFirstLoop = false;

//                 //DEBUGGING {
//                 if(debug){
//                     std::cout << "      currentPoint, after: " << currentPoint.toString() << "\n";
//                 }
//                 //DEBUGGING }

//             } else if(totalDistance >= maxTotalDistance){
//                 currentPoint = getTotalDistEndPoint(currentPoint, prevPoint1, totalDistance-maxTotalDistance); // adjust the end point so that we're exactly 'maxTotalDistance' away from the starting point
//                 currentNode = qt.getNode(currentPoint);
//                 doFirstLoop = false;

//                 //DEBUGGING {
//                 if(debug){
//                     std::cout << "1. Max total distance exceeded, ending first loop\n";
//                 }
//                 //DEBUGGING }

//             }
//             moveHistory.push_back(currentPoint);
//             nSteps++;
//             //moveHistory[++nSteps] = currentPoint;
//         }
//     }
//     //return moveHistory(Range(0,nSteps),_);
//     //moveHistory.resize(nSteps+1);//+1 because we included the starting point

//     //DEBUGGING {
//     if(debug){
//         std::cout << "finalPoint: " << currentPoint.x << ", " << currentPoint.y << "\n";
//         std::cout << "moveHistory:\n";
//         // 2/19/2021 {
//         for ( auto &iPoint : moveHistory){
//             std::cout << iPoint.x << ", " << iPoint.y << "\n";        
//         }
//         // 2/19/2021 }

//         // 2/19/2021 {
//         // for(size_t i = 0; i < moveHistory.size(); ++i){
//         //     std::cout << moveHistory[i].x << ", " << moveHistory[i].y << "\n";    
//         // }
//         // 2/19/2021 }
//     }
//     //DEBUGGING }
    
//     return moveHistory;
// }


// void SimUtilities::mutate(std::vector<std::vector<int>> &alleles, std::mt19937 &randomGenerator){
//     std::uniform_int_distribution<> locusDstr(0,alleles.size()-1);
//     int lociIndex = locusDstr(randomGenerator);
    
//     std::uniform_int_distribution<> positionDstr(0,alleles.at(lociIndex).size()-1);
//     int positionIndex = positionDstr(randomGenerator);
    
//     std::uniform_int_distribution<> mutTypeDstr(0,2);
//     int mutType = mutTypeDstr(randomGenerator);

//     if(mutType == 0){
//         alleles.at(lociIndex).at(positionIndex) += 1; 
//     } else if((mutType == 1) && (alleles.at(lociIndex).at(positionIndex) > 0)){
//         alleles.at(lociIndex).at(positionIndex) -= 1;
//     } else if(mutType == 2){
//         std::uniform_int_distribution<> newValueDstr(0,alleles.at(lociIndex).at(positionIndex));
//         alleles.at(lociIndex).at(positionIndex) = newValueDstr(randomGenerator);
//     }

// }

std::vector<std::vector<int>> SimUtilities::breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, double mutateProb, std::mt19937 &randomGenerator){
// std::vector<std::vector<int>> SimUtilities::breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, std::mt19937 &randomGenerator){
    if(alleles1.size() != alleles2.size()) throw std::runtime_error("Error in 'SimUtilities::breed() - 'alleles1' and 'alleles2' have different lengths");

    std::uniform_int_distribution<> indexDist(0,1); 
    std::vector<std::vector<int>> newAlleles(alleles1.size());

    for(size_t i = 0; i < alleles1.size(); ++i){
        if((alleles1[i].size() != 2) || (alleles2[i].size() != 2)) { //throw std::runtime_error("Error in 'SimUtilities::breed() - 'alleles1.at(" + std::to_string(i) + ")' and 'alleles2.at(" + std::to_string(i) + ")' have different lengths");
            throw std::runtime_error("Error in 'SimUtilities::breed() -  'alleles1.at(" + std::to_string(i) + ")' had size " + std::to_string(alleles1[i].size()) + " and 'alleles2.at(" + std::to_string(i) + ")' had size " + std::to_string(alleles2[i].size()) + ". Both must have size 2.");
        }
        newAlleles[i] = std::vector<int>(2);
        newAlleles[i][0] = (indexDist(randomGenerator) == 0) ? alleles1[i][0] : alleles1[i][1];
        newAlleles[i][1] = (indexDist(randomGenerator) == 0) ? alleles2[i][0] : alleles2[i][1];

        // newAlleles[i] = std::vector<int>(alleles1[i].size());    
        // for(size_t j = 0; j < alleles1[i].size(); ++j){
        //     newAlleles[i][j] = (indexDist(randomGenerator) == 0) ? alleles1[i][j] : alleles2[i][j];
        // }
    }
    if(mutateProb > 0){
        std::uniform_real_distribution<> mutateProbDist(0.0,1.0);
        std::uniform_int_distribution<> mutateTypeDist(0,1); 
        for(size_t i = 0; i < newAlleles.size(); ++i){
            for(size_t j = 0; j < 2; ++j){
                if(mutateProbDist(randomGenerator) < mutateProb){
                    newAlleles[i][j] = (mutateTypeDist(randomGenerator) == 0) ? newAlleles[i][j] - 1 : newAlleles[i][j] + 1;
                }
            }
        }
    }
    return newAlleles;
}

// std::shared_ptr<Agent> SimUtilities::makeNewAgent(std::shared_ptr<Agent> &agent1, std::shared_ptr<Agent> &agent2){

// }