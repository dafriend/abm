#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <vector>
#include <string>

//zero-based indexing
class Matrix{
    private:
        std::vector<double> vec;
        int nrow{0};
        int ncol{0};
    public:
        Matrix(double val, int _nrow, int _ncol);
        Matrix(std::vector<double> _vec, int _nrow, int _ncol);
        
        int nRow() const;
        int nCol() const;
        int size() const;
        std::vector<double> asVector() const;

        Matrix getRow(const int index) const;
        Matrix getCol(const int index) const;
        Matrix subset(int rMin, int rMax, int cMin, int cMax) const;
        Matrix flipRows() const;
        //Matrix flipCols();

        double mean() const;
        double min() const;
        double max() const;
        int countNans() const;
        double getByIndex(const int index) const;
        std::string toString() const;
};
#endif