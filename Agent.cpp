#include "Point.h"
#include "Agent.h"
//#include "AgentPoint.h"

#include <memory>
#include <vector>

// Agent::Agent() : Point{}, id{-1}, age{-1}, isMale{-1}, isAlive{-1}, yearDied{-1}, deathRisk{0}, dadPtr{std::weak_ptr<Agent>()}, momPtr{std::weak_ptr<Agent>()} {
//     //point = Point(); //can this go in the initialization list?
// }

Agent::Agent() : Point{}, id{-1}, age{-1}, isMale{-1}, isAlive{-1}, yearDied{-1}, deathRisk{0}, dadPtr{std::shared_ptr<Agent>()}, momPtr{std::shared_ptr<Agent>()} {
    //point = Point(); //can this go in the initialization list?
}
    // double habitatRisk;
    // double densityRisk;
    // double ageRisk;
    // double deathRisk;
    // double resistance;
    // double dist;
    // double dir;
Agent::Agent(int _id, int _age, int _isMale, int _isAlive, double _x, double _y, std::shared_ptr<Agent> _dadPtr, std::shared_ptr<Agent> _momPtr, std::vector<std::vector<int>> _loci) 
    : Point{_x, _y}, id{_id}, age{_age}, isMale{_isMale}, isAlive{_isAlive}, yearDied{-1}, deathRisk{0}, loci{_loci}, dadPtr{_dadPtr}, momPtr{_momPtr} {}

// Agent::Agent(double x, double y, int _id) : id{_id}{
//     point = makePoint(x,y, _id);
// }

// void Agent::setCoords(double _x, double _y){
//     point.setCoords(_x, _y);
// }

std::string Agent::toString() const{
    std::string string = "id: " + std::to_string(id) + " | age: " + std::to_string(age) + " | isMale: " + std::to_string(isMale) + " | isAlive: " + std::to_string(isAlive) + " | x: " + std::to_string(x)  + " | y: " + std::to_string(y);
    return string;
}

// std::shared_ptr<Agent> makeAgent(int id, int age, int isMale, int isAlive, double x, double y, std::shared_ptr<Agent> dadPtr, std::shared_ptr<Agent> momPtr, std::vector<std::vector<int>> loci){
//     Agent* agent = new Agent(id, age, isMale, isAlive, x, y, dadPtr, momPtr, loci);
//     agent->ptr = std::shared_ptr<Agent>(agent);
//     //agent->ptr = std::make_shared<Agent>(id, age, isMale, isAlive, x, y, dadPtr, momPtr, loci);
//     return agent->ptr;
// }