#ifndef SIMULATION_H
#define SIMULATION_H

// #include "Agent.h"
// #include "PointGrid.h"
// #include "Quadtree.h"
#include "ProbabilityTable.h"
#include "Agent.h"
#include "Rectangle.h"
#include <memory>
#include <vector>
#include <list>
#include <random>
#include <tuple>

struct SimulationParameters{
    //-----------------------------------------
    //OVERALL SIMULATION PARAMETERS
    //-----------------------------------------
    // NOTE: several parameters are for specifying file paths. When the parameter has the
    // word "Path" in its name, that means it needs a FULL file path. When the paramter has
    // the word "Folder" in its name (without "Path"), that means it needs ONLY the folder name - the specified
    // folder will be created inside the relevant parent folder (specified by one of the 'Path' 
    // variables). When a parameter has the word "FilePath" in its name, it must be a full path
    // to a file.
    //
    // None of the parameters need a trailing slash.
    //
    // output file structure is as follows (note that not all these files/folders will be present 
    // depending on the parameter selections):
    //
    // simParentFolderPath/
    // └── simFolder/
    //     ├── backup/
    //     │   └── agents05.csv
    //     │   └── iterInfo.csv
    //     ├── info/
    //     │   ├── allMovt.csv
    //     │   ├── iterInfo.csv
    //     │   ├── moveDistanceProbs.csv
    //     │   ├── params.csv
    //     │   ├── quadtree.qtree
    //     │   ├── rectangles.csv
    //     │   └── runInfo.csv
    //     ├── output/
    //     │   ├── agents01.csv
    //     │   ├── agents02.csv
    //     │   └── ...
    //     └── output_all/
    //         ├── allAgents10.csv
    //         ├── allAgents20.csv
    //         └── ...
    int simNIterations{0}; // number of iterations to run the simulation
    double simPointGridCellSize{-1}; // the cell size of the pointGrid data structure
    std::string simQuadtreeFilePath{""}; // file path to the quadtree
    std::string simParentFolderPath{""}; // the folder containing the results will be created within this folder
    std::string simFolder{""}; // folder where files will be saved - will be created within 'simOuputParentPath'
    int simRandomSeed{-1}; //random seed to use
    int simSaveFrequency{1}; // the frequency to save outputs (i.e. 1 means every iteration, 10 means every 10 iterations, etc.)
    std::string simSaveRectanglesFilePath{""}; // optional - path to a CSV that specifies rectangles - only agents that fall in these rectangles will be saved. CSV must have 5 columns: id, xmin, xmax, ymin, and ymax
    int simSaveAllFrequency{-1}; // only relevant if 'simSaveRectanglesPath' is specified. This is the frequency at which to save all the agents. For example, you could output the "rectangle" output every year but then also output all agents every 100 years
    int simSaveBackupFrequency{-1}; // optional - if specified, all agents will be outputted at the given frequency. However, only the most recent CSV will be kept - the old one will be deleted when a new one is written. This is for backup purposes - in case the simulation is stopped unexpectedly, it could be restarted using these agents as the starting agents
    int simVerbose{1};
    std::string simCulvertRectanglesFilePath{""}; // file path to a CSV with the same format as 'saveRectanglesFilePath' - these specify "culverts" - only used for the 'moveCulverts()' and 'reproduceCulverts()', so if you're not using either of those two this doesn't need to be specified

    // THESE ARE NOT SET BY THE USER (i.e. don't need to be included in the parameters CSV)
    std::string simSaveFolder{"output"}; // folder in which output CSVs will be saved
    std::string simSaveAllFolder{"output_all"}; // folder in which to save all agents (only relevant if simSaveAllFrequency > 0.) - doesn't need to specified. Folder will be automatically created if not provided.
    std::string simSaveInfoFolder{"info"}; // folder in which sim info files will be saved - this includes copies of input parameters as well as info about the run
    std::string simSaveBackupFolder{"backup"}; // folder in which to save the backup agents. Only relevant if simSaveBackupFrequency > 0. Will be created within 'simOutputFolder'
    
    // these will be created when read in - they contain the FULL paths constructed from the above parameters
    std::string simPath{""};
    std::string simSavePath{""};
    std::string simSaveInfoPath{""};
    std::string simSaveBackupPath{""};
    std::string simSaveAllPath{""};

    //-----------------------------------------
    //INITIALIZATION PARAMETERS
    //-----------------------------------------
    std::string initAgentFilePath{""}; // file path to a CSV file with the agents to be used for initialization. Otherwise they are randomly generated using the other 'init' parameters
    //The following parameters are only used if 'initAgentFilePath' is not provided
    // int initNAgents{-1}; //number of agents to begin the simulation with
    // int initAlleleMin{-1}; //minimum value of the alleles
    // int initAlleleMax{-1}; //maximum value for the alleles
    int initNLoci{-1}; //number of loci that each agent will have
    // int initNPerLocus{2}; //number of alleles per loci
    
    //-----------------------------------------
    //MOVEMENT PROCESS PARAMETERS
    //-----------------------------------------
    std::string movtMethod{""}; //specifies the method to use for movement. Must match one of the "move*()" methods - see definition of 'Simulation' below for the possible methods
    std::string movtDistanceProbsFilePath{""};
    // int movtNPoints{-1}; //number of "evaluation points" used at each step of the movement algorithm
    // double movtStepSize{-1}; //distance used for the top-level step; i.e. how the evaluation points are from the current point
    // //double movtMaxStraightDist; //the maximum straight line distance that an agent can travel
    // double movtMaxTotalDist{-1}; //the maximum total distance that an agent can travel
    // double movtMaxTotalDistSubstep{-1}; //the maximum distance the agent can move in a single step of the second-level movement process
    // double movtAttractDist{-1}; //how far away the chosen attraction point is from the current point
    // //std::vector<double> movtSampleDists; //numbers that represent a distribution of movement distances - movement distances will be drawn from these numbers
    // //the following 6 parameters are the weights (or exponents) for the movement function
    // double movtResistWeight1{-1}; //exponent for the "resistance" component for the top level process
    // double movtAttractWeight1{-1}; //exponent for the "attraction point" component for the top level process
    // double movtDirectionWeight1{-1}; //exponent for the "direction correlation" component for the top level process
    // double movtResistWeight2{-1};  //exponent for the "resistance" component for the lower level process
    // double movtAttractWeight2{-1}; //exponent for the "attraction point" component for the lower level process
    // double movtDirectionWeight2{-1}; //exponent for the "direction correlation" component for the lower level process

    // double movtBarrierThreshold{-1}; // any cells with values above this will be assumed to be absolute barriers that the agents can't pass through
    bool movtSaveAllMovt{false}; // if true, ALL movement is saved (i.e. every single point in a path, not just start and end points) - don't use this for big sims - this file will get MASSIVE! I use this for debugging.
    double movtSaveThresholdDist{-1}; // only used if 'movtSaveAllMovt' is true. Only paths where the drawn distance is greater than this value will be saved
    int movtNPerCulvertPerYear{-1};
    //-----------------------------------------
    //MATING/REPRODUCTION PROCESS PARAMETERS
    //-----------------------------------------
    std::string mateMethod{""};
    int mateAge{-1}; // tortoises AT or above this age are allowed to mate
    int mateNumBabiesPois{-1};
    // double mateProb; // probability of a single female reproducing in a single year
    double mateMaxDist{-1}; // tortoises can mate if they're closer than 'maxMatingDist' to each other
    // int mateClutchSize;
    double mateMutateProb{-1}; // the probability of mutation for each locus
    // std::string mateNumEggsProbsFilePath{""};
    
    
    //-----------------------------------------
    //DEATH PROCESS PARAMETERS
    //-----------------------------------------
    std::string deathMethod{""};
    int deathMaxAge{-1}; //oldest age an agent can reach
    // double deathDist{-1}; //distance used for the cell size of the quadrat count
    // double deathPercentage; //percentage of tortoises that will die each year
    // std::string deathAgeScoresFilePath{""};
    // std::string deathCarryingCapacityFilePath{""};
    
    double deathBaseMortality{-1}; //base mortality rate. Between 0 and 100

    double deathAgeAdultMortality{-1};
    double deathAgeAdultSlope{-1};
    double deathAgeAdultThresh{-1};
    
    double deathAgeJuvMortality{-1};
    double deathAgeJuvSlope{-1};
    double deathAgeJuvThresh{-1};

    double deathDensityBandwidth{-1};
    double deathDensityMortality{-1};
    double deathDensitySlope{-1};
    double deathDensityThresh{-1};
    
    double deathHabMortality{-1};
    double deathHabSlope{-1};
    double deathHabThresh{-1};



    //-----------------------------------------
    //PARAMETERS THAT DON'T NEED TO BE SET BY THE USER - WILL BE AUTOMATICALLY POPULATED USING THE FILE PATHS PROVIDED
    //-----------------------------------------
    ProbabilityTable<int> mateNumEggsProbs; //probability table for how many eggs a female produces in a single year 
    ProbabilityTable<double> movtDistanceProbs; //probability table for the distance travelled in a single year
    ProbabilityTable<int> deathAgeScores; //probability table for death scores based on age
    ProbabilityTable<int> deathCarryingCapacity; //carrying capacity by resistance value. Note that resistance is multiplied by 10 to make it an integer
    
};

//this struct is for storing info on the simulation run - we'll write this out to a file once the simulation is done
struct RunInfo{
    std::string startTime;
    std::string endTime;
    double totalTimeSec;
};

struct IterationInfo{
    int iteration{0};
    int nAgents{0};
    int nBorn{0};
    int nDied{0};
};

class Point;

class Agent;

class PointGrid;

class Quadtree;

class Node;

class Simulation{
    int nextId;
    int getNextId(){
        nextId++;
        return(nextId-1); //I'm pretty sure I could just do 'return(nextId++)'
    }
public: 
    int counter; //keeps track of which iteration we're on

    std::list<std::shared_ptr<Agent> > agents;
    // std::list<std::shared_ptr<Agent> > deadAgents;
    std::vector<std::tuple<int,int,int,int,Point>> movtHist; //iteration,step,agent ID, LCP flag, point - NOTE - LCP flag is 0 or 1. 1 means that the agent actually travelled to that spot. 0 means that it was part of the original LCP but the path was cut off before this point, so the agent never actually travelled to this spot
    //maybe make these into unique_ptrs? That's how I had it at first but then I changed it shared_ptr just because I found it easier to code... but might not necessarily be the best way to go about it
    //std::shared_ptr<PointGrid> pointGrid; 
    std::shared_ptr<Quadtree> quadtree;
    // std::shared_ptr<Quadtree> zoneQuadtree;
    //std::shared_ptr<PointGrid> pointGrid;
    std::vector<Rectangle> saveRectangles;
    std::vector<Rectangle> culvertRectangles;
    std::mt19937 randomGenerator;
    SimulationParameters params;
    RunInfo runInfo;
    IterationInfo thisIter;
    std::vector<IterationInfo> iterInfo;

    //Simulation(PointGrid &_pointGrid, Quadtree& _quadtree);
    //Simulation(std::vector<std::shared_ptr<Agent>> _agents, std::unique_ptr<PointGrid> _pointGrid, std::unique_ptr<Quadtree> _quadtree);
    //Simulation();
    // Simulation(SimulationParameters _params, std::shared_ptr<Quadtree> _quadtree, int randomSeed);
    Simulation(SimulationParameters _params);
    Simulation(SimulationParameters _params, std::string agentsPath, std::string iterInfoPath); //this one is for restarting a simulation
    //Simulation(SimulationParameters _params, bool restart);
    Simulation(std::string paramsFilePath);
    
    //Simulation(SimulationParameters _params, std::shared_ptr<PointGrid> _pointGrid, std::shared_ptr<Quadtree> _quadtree, int randomSeed);
    //Simulation(std::vector<Point> points, std::unique_ptr<PointGrid> _pointGrid, std::unique_ptr<Quadtree> _quadtree, int randomSeed);
    //void getNextPoint(std::shared_ptr<Agent> agent);
    
    //initialization functions
    //void initializePointGrid(); //creates an empty PointGrid
    void initializeFromParams();
    void initializeFromParamsRestart(std::string agentsPath, std::string iterInfoPath);
    // void initializeAgents();
    void initializeRandomGenerator(int seed);

    //void reconstructPointGrid(); //assigns the agents in "agents" to the pointGrid

    std::shared_ptr<PointGrid> makePointGrid(); //gets the pointGrid - refreshes it if it needs to be
    std::shared_ptr<PointGrid> makePointGridAdultMales(); //gets the pointGrid - refreshes it if it needs to be

    void beginIteration();
    //Simulation functions
    // void move(bool debug=false);
    void moveLCP();
    static Point moveAgentToNeighbor(const Point &pt, const std::shared_ptr<Node> &node, const Point &ptNb, const std::shared_ptr<Node> &nodeNb, double maxCost);
    void moveLCP2();
    // void moveLCPTest();
    void moveLCPConstrained();
    void moveSimple();
    void moveCulverts();
    // void moveSimple2();
    // void moveSimple3();
    // void moveSimple4();
    // void moveSimple5();
    // void moveLCP2();
    // void reproduce();
    //void mutate(std::shared_ptr<Agent> agent);
    static std::vector<std::vector<int>> breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, double mutateProb, std::mt19937 &randomGenerator);
    void reproducePointGrid();
    void reproducePointGridNoLCP();
    // void reproducePointGridNoLCP2();
    void reproducePointGridCulverts();
    void reproduceSimple();
    void dieRiskScores();
    void dieRiskScoresQuadrat();
    void dieRiskScoresCulverts();
    // void die2();
    // void die3();
    void dieSimple();

    void doMoveMethod();
    void doReproduceMethod();
    void doDieMethod();

    //apparently passing a shared_ptr as a parameter can slow things down: https://herbsutter.com/2013/06/05/gotw-91-solution-smart-pointer-parameters/
    void killAgent(std::list<std::shared_ptr<Agent>>::iterator &i);

    double riskScore(double val, double slope, double thresh);
    // double riskScoreHabitat(double habitat, double a=15, double mid=.2);
    // double riskScoreDensity(double density, double a=.2, double mid=50);
    // double riskScoreAge(int age, double a1=2, double mid1=10, double a2=.2, double mid2=60, double inflectionAge=17, double juvMortality=16, double adultMortality=6, double baselineMortality=4);

    
    void age();
    void advanceCounter();
    // void endIteration();
    void addIterationInfo();
    void clearDeadAgents();

    //void runSim(int outputFreq = 1, bool writeDeadAgents = false);
    void runSim(bool isRestart = false);
    //void runSimKen(int outputFreq = 1);

    std::vector<std::vector<int>> breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2);
    std::shared_ptr<Agent> makeNewAgent(std::shared_ptr<Agent> &agent1, std::shared_ptr<Agent> &agent2);

    
    // void killTort(int index);//removes the tortoise at 'index' in agents and sticks in deadAgents
    // void writeAgents(std::list<std::shared_ptr<Agent> > agentVec, std::string filePath);
    void writeAgents(std::string folder, bool useSaveRects);
    //void writeAgentsKen(std::list<std::shared_ptr<Agent> > agentVec, std::string filePath);
    // void writeAgents();
    // void writeLiveAgents();
    // void writeLiveAgentsKen();
    // void writeDeadAgents();
    void writeAllMovtHist(std::string filePath);
    void writeRunInfo(std::string filePath);
    void writeIterInfo(std::string filePath);
    void readIterInfo(std::string filePath);

    static SimulationParameters readParams(std::string paramsFilePath);
    static void writeParams(std::string filePath, SimulationParameters pars);

    // void readAgents(std::string filePath);
    void readAgentsKen(std::string filePath, bool loadDeadAgents = false);
};

#endif