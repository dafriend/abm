#ifndef MATHUTILITIES_H
#define MATHUTILITIES_H

#include "Agent.h"
#include "Raster.h"
#include "Quadtree.h"
#include "Node.h"
#include "Matrix.h"
#include <vector>
#include <random>
#include <list>
#include <set>
#include <map>
#include <complex>
#include <cmath>

class Point;

namespace MathUtilities{
    // double sqDistBtwPoints(const Point& point1, const Point& point2);
    // double distBtwPoints(const Point& point1, const Point& point2);
    // double getAngle(const Point& point1, const Point& point2);
    // std::vector<Point> getPointsAroundPoint(const Point& point, int n, double radius);
    // double getAngleOffsetVal(const Point& point, double angle, const Point& optionPoint);
    // Point getRandomPointOnCircle(const Point& center, double radius, std::mt19937& randomGenerator);
    // Point getStraightDistEndPoint(const Point& firstPoint, const Point &penultimatePoint, const Point& lastPoint, double maxDistance);
    // Point getTotDistEndPoint(const Point &point1, const Point& point2, double distance);

    double gaussDens(double x, double mean, double sd);
    //std::vector<int> nPointsWithin(std::shared_ptr<Agent> &points, double distance);
    std::vector<int> nPointsWithin(std::list<std::shared_ptr<Agent>> &points, double distance);
    
    // Raster pointDensity(std::vector<std::shared_ptr<Agent>> &points, double bandwidth, int nX, int nY);


    double scaleNum(double num, double oldMin, double oldMax, double newMin, double newMax);
    std::vector<double> scaleNums(std::vector<double> nums, double oldMin, double oldMax, double newMin, double newMax);

    double max(std::vector<double> nums);
    double min(std::vector<double> nums);
    
    bool isReachable(Quadtree& qt, double x1, double y1, double x2, double y2, double distance);
    bool doesPathExist(std::shared_ptr<Node> node, int endNodeId, double xMin, double xMax, double yMin, double yMax, std::list<int> &prevIds);
    


    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //EVENTUALLY DELETE THESE TWO FUNCTIONS. I should write it so that it works for a template. But I'm in a hurry and don't feel like writing it, so I'm just making a separate one for ints
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    int max(std::vector<int> nums);
    int min(std::vector<int> nums);



    //================================
    // FFT functions
    //================================
    //https://cp-algorithms.com/algebra/fft.html
    // IMPORTANT NOTE - from comparison with the 'fft' function in R, it looks this function only
    // produces correct results when the length of 'a' is a power of 2
    void fft(std::vector<std::complex<double>> & a, bool invert);
    std::vector<std::vector<std::complex<double>>> rotateVector(std::vector<std::vector<std::complex<double>>> &a);
    void fft2d(std::vector<std::vector<std::complex<double>>> &vals, bool invert);
    std::vector<std::vector<std::complex<double>>> doubleVecToComplex(std::vector<std::vector<double>> vals);



    //-------------------------
    // getNearestNeighborDistance
    //-------------------------
    //get the distance to the nearest neighbor (using centroids)
    double getNearestNeighborDistance(const std::shared_ptr<Node> node);

    //find the minimum distance from a point to the centroids of the neighbors of this node
    double getNearestNeighborDistance(const std::shared_ptr<Node> node, const Point& point);










    // //class for finding the shortest path(s) in a network
    // class ShortestPathFinder{
    //     public:
    //         Matrix* adjMat;
    //         std::vector<std::pair<int,double>> nodeCosts;
    //         int nodeCostsSize;
    //         Matrix pathMat;
    //         bool fullySearched;
    //         int previouslyAddedNode;
    //         int startIndex;

    //         ShortestPathFinder(Matrix &_adjMat, int _startIndex); 
    //         Matrix getShortestPathNetwork();
    //         std::vector<int> getShortestPath(int endIndex);
    //         int addNextEdge();
    // };

    // std::vector<std::shared_ptr<Node>> getShortestPathQuadtree(Quadtree &qt, Point startPoint, Point endPoint, double xMin, double xMax, double yMin, double yMax);
    // std::vector<std::shared_ptr<Node>> getShortestPathQuadtree(Quadtree &qt, Point startPoint, Point endPoint);


    // class ShortestPathFinder2{

    //     struct NodeEdge{
    //         int id;
    //         std::shared_ptr<Node> node;
    //         std::shared_ptr<NodeEdge> parent;
    //         //std::shared_ptr<NodeEdge> self;
    //         double cost;
    //         int nNodesFromOrigin;
    //         //std::vector<std::shared_ptr<NodeEdge>> possibleEdges;
    //     };

    //     //comparator function for the multiset
    //     struct cmp {
    //         bool operator() (std::tuple<int,int,double> a, std::tuple<int,int,double> b) const {
    //             return std::get<2>(a) < std::get<2>(b);
    //         }
    //     };

    //     void init(int startNodeID);
    //     std::vector<std::shared_ptr<Node>> findShortestPath(int endNodeID);
    //     //bool cmp(std::tuple<int,int,double> a, std::tuple<int,int,double> b);
    //     public:
            
    //         std::shared_ptr<Quadtree> quadtree;
    //         Point startPoint;
    //         std::shared_ptr<Node> startNode;
    //         std::vector<std::shared_ptr<NodeEdge>> nodeEdges;
    //         std::map<int, int> dict; //dictionary with Node ID's as the key and the index of the corresponding 'NodeEdge' in 'nodeEdges'
    //         //std::multiset<std::tuple<int,int,double>, decltype(cmp)> possibleEdges(cmp);
    //         //auto cmp = [](std::tuple<int,int,double> a, std::tuple<int,int,double> b) { return std::get<2>(a) < std::get<2>(b); };
    //         //std::multiset<std::tuple<int,int,double>, decltype(cmp)*> possibleEdges;
    //         std::multiset<std::tuple<int,int,double>, cmp> possibleEdges;
    //         // std::multiset<std::tuple<int,int,double>> possibleEdges;

    //         double xMin;
    //         double xMax;
    //         double yMin;
    //         double yMax;

    //         ShortestPathFinder2(std::shared_ptr<Quadtree> _quadtree, int startNodeID);
    //         ShortestPathFinder2(std::shared_ptr<Quadtree> _quadtree, Point startPoint);
    //         ShortestPathFinder2(std::shared_ptr<Quadtree> _quadtree, int startNodeID, double _xMin, double _xMax, double _yMin, double _yMax);
    //         ShortestPathFinder2(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);

            
    //         void makeShortestPathNetwork();
    //         std::vector<std::shared_ptr<Node>> getShortestPath(int endNodeID);
    //         std::vector<std::shared_ptr<Node>> getShortestPath(Point endPoint);

    //         int doNextIteration();
    // };
    //std::vector<std::shared_ptr<Node>> getShortestPathQuadtree(std::shared_ptr<Node> startNode, std::shared_ptr<Node> endNode, double xMin, double xMax, double yMin, double yMax);
    //void 
    // Matrix getAdjMatFromQuadtree(Quadtree& qt, double xMin, double xMax, double yMin, double yMax);
    // void getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>*>> &networkNodes);
}

#endif