#include "Simulation.h"
#include "Agent.h"
#include "Point.h"
#include "Raster.h"
//#include "AgentPoint.h"
#include "PointCell.h"
#include "PointGrid.h"
#include "Quadtree.h"
#include "Node.h"
#include "MathUtilities.h"
// #include "SimUtilities.h"
#include "BasicUtilities.h"
#include "PointUtilities.h"
#include "AgentUtilities.h"
#include "LcpFinder.h"
//#include "ProbabilityTable.h"
#include <date/date.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <random>
#include <regex>
#include <vector>



//just initialize pointGrid and quadtree, leave points/agents empty to be filled later
// Simulation::Simulation(PointGrid &_pointGrid, Quadtree& _quadtree){
    
// }

//Simulation::Simulation(SimulationParameters _params, std::shared_ptr<Quadtree> _quadtree, int randomSeed)

Simulation::Simulation(std::string paramsFilePath)
    : nextId{0}, counter{0}{
    
    std::cout << "\nreading parameters from: " << paramsFilePath << "\n";
    params = readParams(paramsFilePath);
    initializeFromParams();
    //read in ProbabilityTables
    // params.mateNumEggsProbs = ProbabilityTable<int>(params.mateNumEggsProbsFilePath);
    // params.movtDistanceProbs = ProbabilityTable<double>(params.movtDistanceProbsFilePath);
    // params.deathAgeScores = ProbabilityTable<int>(params.deathAgeScoresFilePath);
    // params.deathCarryingCapacity = ProbabilityTable<int>(params.deathCarryingCapacityFilePath);

    // //read in quadtree
    // quadtree = Quadtree::readQuadtree(params.simQuadtreeFilePath);

    // //initializePointGrid(); //initialize pointGrid (using the extent of 'quadtree')
    // movtHist = std::vector<std::tuple<int,int,int,Point>>(0);
    // if(params.initAgentFilePath.size() == 0){
    //     std::cout << "no agent file provided, creating agents\n";
    //     initializeAgents();
    // } else {
    //     std::cout << "reading in agents from" << params.initAgentFilePath << "\n";
    //     readAgents(params.initAgentFilePath);
    // }
    // initializeRandomGenerator(params.simRandomSeed);
}

Simulation::Simulation(SimulationParameters _params)
    : nextId{0}, counter{0}, params{_params}{
    initializeFromParams();
    // //read in ProbabilityTables
    // params.mateNumEggsProbs = ProbabilityTable<int>(params.mateNumEggsProbsFilePath);
    // params.movtDistanceProbs = ProbabilityTable<double>(params.movtDistanceProbsFilePath);
    // params.deathAgeScores = ProbabilityTable<int>(params.deathAgeScoresFilePath);
    // params.deathCarryingCapacity = ProbabilityTable<int>(params.deathCarryingCapacityFilePath);

    // //read in quadtree
    // quadtree = Quadtree::readQuadtree(params.simQuadtreeFilePath);

    // //initializePointGrid(); //initialize pointGrid (using the extent of 'quadtree')
    // movtHist = std::vector<std::tuple<int,int,int,Point>>(0);
    // initializeAgents();
    // initializeRandomGenerator(params.simRandomSeed);
}

// restart a simulation
Simulation::Simulation(SimulationParameters _params, std::string agentsPath, std::string iterInfoPath)
    : params{_params}{
    
    initializeFromParamsRestart(agentsPath, iterInfoPath);
}
// Simulation::Simulation(SimulationParameters _params, std::shared_ptr<PointGrid> _pointGrid, std::shared_ptr<Quadtree> _quadtree, int randomSeed) 
//     : params{_params}, pointGrid{_pointGrid}, quadtree{_quadtree}{}

// Simulation::Simulation(std::vector<Point> points, std::unique_ptr<PointGrid> _pointGrid, std::unique_ptr<Quadtree> _quadtree, int randomSeed){
    
// }

void Simulation::initializeFromParams(){
    // params.mateNumEggsProbs = ProbabilityTable<int>(params.mateNumEggsProbsFilePath);
    params.simPath = params.simParentFolderPath + "/" + params.simFolder;
    params.simSavePath = params.simPath + "/" + params.simSaveFolder;
    params.simSaveInfoPath = params.simPath + "/" + params.simSaveInfoFolder;
    std::filesystem::create_directory(params.simPath);
    std::filesystem::create_directory(params.simSavePath);
    std::filesystem::create_directory(params.simSaveInfoPath);

    std::cout << "\nreading movement probabilities from: " << params.movtDistanceProbsFilePath << "\n";
    params.movtDistanceProbs = ProbabilityTable<double>(params.movtDistanceProbsFilePath);
    std::filesystem::copy_file(params.movtDistanceProbsFilePath, params.simSaveInfoPath + "/moveDistanceProbs.csv");
    // params.deathAgeScores = ProbabilityTable<int>(params.deathAgeScoresFilePath);
    // params.deathCarryingCapacity = ProbabilityTable<int>(params.deathCarryingCapacityFilePath);

    //read in quadtree
    std::cout << "\nreading quadtree from: " << params.simQuadtreeFilePath << "\n";
    quadtree = Quadtree::readQuadtree(params.simQuadtreeFilePath);
    std::filesystem::copy_file(params.simQuadtreeFilePath, params.simSaveInfoPath + "/quadtree.qtree");

    if(params.simSaveRectanglesFilePath != ""){
        std::cout << "\nreading save rectangles from: " << params.simSaveRectanglesFilePath << "\n";
        saveRectangles = Rectangle::readRectanglesFromCSV(params.simSaveRectanglesFilePath);
        std::filesystem::copy_file(params.simSaveRectanglesFilePath, params.simSaveInfoPath + "/save_rectangles.csv");
        if(params.simSaveAllFrequency > 0){
            params.simSaveAllPath = params.simPath + "/" + params.simSaveAllFolder;
            std::filesystem::create_directory(params.simSaveAllPath);
        }
    }

    if(params.simSaveBackupFrequency > 0){
        params.simSaveBackupPath = params.simPath + "/" + params.simSaveBackupFolder;
        std::filesystem::create_directory(params.simSaveBackupPath);
    }

    if(params.simCulvertRectanglesFilePath != ""){
        std::cout << "\nreading culvert rectangles from: " << params.simCulvertRectanglesFilePath << "\n";
        culvertRectangles = Rectangle::readRectanglesFromCSV(params.simCulvertRectanglesFilePath);
        std::filesystem::copy_file(params.simCulvertRectanglesFilePath, params.simSaveInfoPath + "/culvert_rectangles.csv");
    }
    //initializePointGrid(); //initialize pointGrid (using the extent of 'quadtree')
    movtHist = std::vector<std::tuple<int,int,int,int,Point>>(0);
    // if(params.initAgentFilePath.size() == 0){
    //     std::cout << "no agent file provided, creating agents\n";
    //     initializeAgents();
    // } else {
    std::cout << "\nreading initial agents agents from " << params.initAgentFilePath << "\n";
    //readAgents(params.initAgentFilePath);
    readAgentsKen(params.initAgentFilePath);
    // }
    initializeRandomGenerator(params.simRandomSeed);
    iterInfo = std::vector<IterationInfo>(params.simNIterations + 1); //add one for recording the initial state
    //iterInfo.at(0) = IterationInfo{-1,params.initNAgents,0,0};
    //#thisIter = IterationInfo{0,0,0,0};
    writeParams(params.simSaveInfoPath + std::string("/params.csv"), params);
}

// !!!!! IMPORTANT NOTE ABOUT RESTARTING A SIM !!!!!
// The Agent class keeps track of its parents via pointers (momPtr and dadPtr). When the
// agents are written out it outputs the id, x, and y coords of both parents by accessing
// the pointers. However, when the agents are read in when being restarted, the parent 
// pointers are not "re-attached". So the restarted agents will all have empty pointers to
// their parents. This doesn't really matter - the simulation runs fine. The pointers are purely
// informational and aren't required for any of the simulation functionality. But when the 
// agents are output after being restarted, all agents born before the restart will have -1's in
// the parent columns.
// We could retrieve some of the parents if we wanted to - if an agent's parent is still alive, 
// it will be in the CSV, so we could search through the agents after reading them and 
// re-attach any pointers. However, if the parents have died (and therefore aren't in the CSV)
// there's nothing we can do - that info is gone. The exception would be if we saved every 
// agent from every year - but then to retrieve the parents you'd have to read through the old
// CSVs to find the parents.
// If I ever decided that that info was really important, the easiest thing to do would probably
// be to just add properties to the Agent class for storing the ID, x and y coords of the
// parents rather than relying on pointers. Then I could access that info without needing 
// pointers.
void Simulation::initializeFromParamsRestart(std::string agentsPath, std::string iterInfoPath){
    // params.mateNumEggsProbs = ProbabilityTable<int>(params.mateNumEggsProbsFilePath);
    params.simPath = params.simParentFolderPath + "/" + params.simFolder;
    params.simSavePath = params.simPath + "/" + params.simSaveFolder;
    params.simSaveInfoPath = params.simPath + "/" + params.simSaveInfoFolder;
    // std::filesystem::create_directory(params.simPath);
    // std::filesystem::create_directory(params.simSavePath);
    // std::filesystem::create_directory(params.simSaveInfoPath);

    std::cout << "\nreading movement probabilities from: " << params.movtDistanceProbsFilePath << "\n";
    params.movtDistanceProbs = ProbabilityTable<double>(params.movtDistanceProbsFilePath);
    // std::filesystem::copy_file(params.movtDistanceProbsFilePath, params.simSaveInfoPath + "/moveDistanceProbs.csv");
    // params.deathAgeScores = ProbabilityTable<int>(params.deathAgeScoresFilePath);
    // params.deathCarryingCapacity = ProbabilityTable<int>(params.deathCarryingCapacityFilePath);

    //read in quadtree
    std::cout << "\nreading quadtree from: " << params.simQuadtreeFilePath << "\n";
    quadtree = Quadtree::readQuadtree(params.simQuadtreeFilePath);
    // std::filesystem::copy_file(params.simQuadtreeFilePath, params.simSaveInfoPath + "/quadtree.qtree");

    if(params.simSaveRectanglesFilePath != ""){
        std::cout << "\nreading save rectangles from: " << params.simSaveRectanglesFilePath << "\n";
        saveRectangles = Rectangle::readRectanglesFromCSV(params.simSaveRectanglesFilePath);
        // std::filesystem::copy_file(params.simSaveRectanglesFilePath, params.simSaveInfoPath + "/save_rectangles.csv");
        if(params.simSaveAllFrequency > 0){
            params.simSaveAllPath = params.simPath + "/" + params.simSaveAllFolder;
            // std::filesystem::create_directory(params.simSaveAllPath);
        }
    }

    if(params.simSaveBackupFrequency > 0){
        params.simSaveBackupPath = params.simPath + "/" + params.simSaveBackupFolder;
        // std::filesystem::create_directory(params.simSaveBackupPath);
    } 
    if(params.simCulvertRectanglesFilePath != ""){
        std::cout << "\nreading culvert rectangles from: " << params.simCulvertRectanglesFilePath << "\n";
        culvertRectangles = Rectangle::readRectanglesFromCSV(params.simCulvertRectanglesFilePath);
        // std::filesystem::copy_file(params.simCulvertRectanglesFilePath, params.simSaveInfoPath + "/culvert_rectangles.csv");
    }
    //initializePointGrid(); //initialize pointGrid (using the extent of 'quadtree')
    movtHist = std::vector<std::tuple<int,int,int,int,Point>>(0);
    // if(params.initAgentFilePath.size() == 0){
    //     std::cout << "no agent file provided, creating agents\n";
    //     initializeAgents();
    // } else {
    std::cout << "\nreading agents from " << agentsPath << "\n";
    //readAgents(params.initAgentFilePath);
    readAgentsKen(agentsPath, false);
    auto maxIdIterator = std::max_element( agents.begin(), agents.end(),
                             []( const std::shared_ptr<Agent> &a, const std::shared_ptr<Agent> &b )
                             {
                                 return a->id < b->id;
                             } ); 
    nextId = (*maxIdIterator)->id + 1;

    std::cout << "\nreading iteration info from " << iterInfoPath << "\n";
    iterInfo = std::vector<IterationInfo>(params.simNIterations + 1); //add one for recording the initial state
    readIterInfo(iterInfoPath);
    auto maxIterIterator = std::max_element( iterInfo.begin(), iterInfo.end(),
                            []( const IterationInfo &a, const IterationInfo &b )
                            {
                                return a.iteration < b.iteration;
                            } ); 
    counter = (*maxIterIterator).iteration + 1;
    // }
    initializeRandomGenerator(params.simRandomSeed);
    
    //iterInfo.at(0) = IterationInfo{-1,params.initNAgents,0,0};
    //#thisIter = IterationInfo{0,0,0,0};
    // writeParams(params.simSaveInfoPath + std::string("/params.csv"), params);
}


void Simulation::initializeRandomGenerator(int seed){
    randomGenerator = std::mt19937(seed);
}

//creates a pointgrid based on the 'initPointGridCellSize' parameter and uses the extent of the quadtree
// void Simulation::initializePointGrid(){
//     //get number of cells in each direction
//     int nX = std::ceil((quadtree->root->xMax - quadtree->root->xMin)/params.initPointGridCellSize);
//     int nY = std::ceil((quadtree->root->yMax - quadtree->root->yMin)/params.initPointGridCellSize);

//     //use cell size to determine pointGrid extent
//     double xMin = quadtree->root->xMin;
//     double xMax = xMin + nX*params.initPointGridCellSize;
//     double yMin = quadtree->root->yMin;
//     double yMax = yMin + nY*params.initPointGridCellSize;

//     //create the empty pointGrid and assign a point to 'pointGrid'
//     PointGrid* pg = new PointGrid(nX, nY, xMin, xMax, yMin, yMax);
//     pointGrid = std::shared_ptr<PointGrid>(pg);
// }

// //initializes agents - this includes their life history info (age,sex) and their point location
// //this results in populating the PointGrid
// void Simulation::initializeAgents(){
//     // agents = std::vector<std::shared_ptr<Agent>>(params.initNAgents); // 2/19/2021 d
//     agents = std::list<std::shared_ptr<Agent>>(); // 2/19/2021 i

//     //https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
//     //std::random_device rd;  //Will be used to obtain a seed for the random number engine
//     //std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//     //std::mt19937 gen(time(NULL)); //Standard mersenne_twister_engine seeded with rd()
//     //std::mt19937 gen(1234); //Standard mersenne_twister_engine seeded with rd()
//     std::uniform_int_distribution<> ageDist(0,params.deathMaxAge); //CHANGE EVENTUALLY - might be good to allow the generated numbers to follow a distribution
//     std::uniform_int_distribution<> sexDist(0,1); 
//     std::uniform_real_distribution<> xDist(quadtree->root->xMin, quadtree->root->xMax);
//     std::uniform_real_distribution<> yDist(quadtree->root->yMin, quadtree->root->yMax);
//     for(int i = 0; i < params.initNAgents; ++i){
//         //create Agent and AgentPoint
        
//         Point agentLoc = Point(xDist(randomGenerator), yDist(randomGenerator));

//         //generate random genetic data
//         std::vector<std::vector<int>> loci(params.initNLoci);
//         std::uniform_int_distribution<> alleleDist(params.initAlleleMin,params.initAlleleMax); 
//         for(int i = 0; i < params.initNLoci; ++i){
//             //loci.at(i) = std::vector<int>(params.initNPerLocus);
//             loci.at(i) = std::vector<int>(2); //each locus has two alleles
//             //for(int j = 0; j < params.initNPerLocus; ++j){
//             for(int j = 0; j < 2; ++j){
//                 loci.at(i).at(j) = alleleDist(randomGenerator);
//             }
//         }
//         // std::shared_ptr<Agent> agent = makeAgent(getNextId(), ageDist(randomGenerator), sexDist(randomGenerator), 1, agentLoc.x, agentLoc.y, nullptr, nullptr, loci);
//         std::shared_ptr<Agent> agent = std::make_shared<Agent>(getNextId(), ageDist(randomGenerator), sexDist(randomGenerator), 1, agentLoc.x, agentLoc.y, nullptr, nullptr, loci);
//         //agent->point.setCoords(xDist(randomGenerator), yDist(randomGenerator));
//         //std::shared_ptr<AgentPoint> agentPoint = makeAgentPoint(xDist(randomGenerator), yDist(randomGenerator));
        
//         //link 'agent' and 'agentPoint' via their pointers
//         //agent->agentPoint = agentPoint;
//         //agentPoint->agent = agent;
//         //add the agent to the list of live agents
//         //agents[i] = agent; // 2/19/2021
//         //agents.push_back(agent);
//         agents.emplace_back(agent);
//         // //add the point to the PointGrid and add the pointer to the PointCell to 'agentPoint'

//     }
//     //reconstructPointGrid();
// }

// void Simulation::reconstructPointGrid(){
//     //first we need to extract the "AgentPoints" from the "Agent" objects so that we get a list of "AgentPoints"
//     std::cout << "check1" << std::endl;
//     std::vector<std::shared_ptr<AgentPoint>> points(agents.size());
//     for(int i = 0; i < points.size(); ++i){
//         points[i] = agents[i]->agentPoint;
//     }
//     std::cout << "check2" << std::endl;
//     PointGrid* pg = new PointGrid(points, pointGrid->nX, pointGrid->nY, pointGrid->xMin, pointGrid->xMax, pointGrid->yMin, pointGrid->yMax);
//     std::cout << "check3" << std::endl;
//     pointGrid = std::shared_ptr<PointGrid>(pg); //Potential memory leak since I'm reassigning 'pointGrid'?
//     std::cout << "check4" << std::endl;
//     // for(int i = 0; i < agents.size(); ++i){
//     //     //add the point to the PointGrid and add the pointer to the PointCell to 'agentPoint'
//     //     std::shared_ptr<PointCell> pointCell = pointGrid->addPoint(agents[i]->agentPoint);
//     //     agents[i]->agentPoint->pointCell = pointCell;
//     // }
// }

void Simulation::beginIteration(){
    thisIter.iteration = counter;
    thisIter.nAgents = 0;
    thisIter.nBorn = 0;
    thisIter.nDied = 0;
}


// void Simulation::move(bool debug){
//     //for(size_t i = 0; i < agents.size(); ++i){ // 2/19/2021 d
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         Point attractPt = PointUtilities::getRandomPointOnCircle(*iAgent, params.movtAttractDist, randomGenerator); // 2/19/2021 i
//         double maxStraightDist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         //std::cout << attractPt.x - agents[i]->point.x << ", " << attractPt.y - agents[i]->point.y << "\n";
//         //std::vector<Point> points = SimUtilities::moveAgent(   
//         std::list<Point> points = SimUtilities::moveAgent(   
//             *quadtree, 
//             //*agents[i], // 2/19/2021 d
//             *iAgent, // 2/19/2021 i
//             attractPt,
//             params.movtNPoints,
//             params.movtStepSize,
//             params.movtMaxTotalDist,
//             maxStraightDist,
//             params.movtMaxTotalDistSubstep,
//             params.movtResistWeight1,
//             params.movtAttractWeight1,
//             params.movtDirectionWeight1,
//             params.movtResistWeight2,
//             params.movtAttractWeight2,
//             params.movtDirectionWeight2,
//             randomGenerator,
//             debug);

//         // 2/19/2021 d {
//         //save points to movtHist - probably will want to remove this when I start running the simulation for real
//         // for(size_t j = 0; j < points.size(); ++j){ // 2/19/2021 d
//         //     std::tuple<int,int,int,Point> tup{counter, j, agents[i]->id, points[j]};
//         //     movtHist.push_back(tup);
//         // }
//         // 2/19/2021 d }

//         Point finalPoint = points.back(); //get the last element in the returned list of points

//         //DEBUGGING {
//         // std::cout << "*******************************************\n";
//         // std::cout << "finalPoint:"<< finalPoint.x << ", " << finalPoint.y <<"\n";
//         // std::cout << "*******************************************\n";
//         //DEBUGGING }

//         // agents[i]->setCoords(finalPoint.x, finalPoint.y); //set the coordinates of the agent to the new point // 2/19/2021 d
//         iAgent->x = finalPoint.x;
//         iAgent->y = finalPoint.y; //set the coordinates of the agent to the new point // 2/19/2021 d
//         // std::shared_ptr<PointCell> oldPointCell = agents[i]->agentPoint->pointCell;
//         // std::shared_ptr<PointCell> newPointCell = pointGrid->getCell(finalPoint.x, finalPoint.y); //get a reference to the PointCell that contains this point
//         // if(newPointCell->id != oldPointCell->id){ //if the new point is in a different PointCell than the previous location, reassign the point to a new PointCell
//         //     //std::find(oldPointCell->points.begin(), oldPointCell->points.end(), agents[i]->agentPoint);
//         //     //https://stackoverflow.com/questions/3385229/c-erase-vector-element-by-value-rather-than-by-position
//         //     //remove the old point from its PointCell
//         //     oldPointCell->points.erase(std::remove(oldPointCell->points.begin(), oldPointCell->points.end(), agents[i]->agentPoint), oldPointCell->points.end());
//         //     //change the pointer of the AgentPoint to the new PointCell
//         //     agents[i]->agentPoint->pointCell = newPointCell;
//         //     newPointCell->points.push_back(agents[i]->agentPoint);
//         // }
//     }
//     //counter++;
// }

// moves each tortoise by selecting a random point some distance away and then getting the LCP to that point.
// If the cumulative resistance hits a pre-specified threshold, the path is cut off. Here's the procedure I'm using
// right now.
// First calculate the cumulative resistance for each point, and find the cell centroids whose resistance is closest 
// to the threshold. There are three possible scenarios I'm considering:
//   1. The final centroid is in the same cell that the original destination point was in. In this case, set the
//       agent's new location to be the original destination point
//   2. The final centroid is not in the same cell as the original destination point or the starting point. In this
//       case, randomly place the agent's final location somewhere in this cell 
//   3. The final centroid is in the same cell as the starting point. In this case, look at the line from the starting
//       point to the original destination point and select the point along this line where the cumulative resistance 
//       equals the threshold resistance. This is the agent's final location.
//
// One of the tricky aspects of using LCP on a quadtree is figuring out where to put the end point. LCP works 
// using the cell centroids. This is less problematic when the cells are all the same size. However, if we use 
// the centroid of a cell as the agent's final spot, that can cause weird things to happen. For example, if the agent ends
// up in a big cell, and the cell is bigger than the mating radius, the agent will only be able to mate with 
// agents in the same cell. To avoid this, I could jitter the final point. However, if the cell is large, this could
// result in an agent moving an unrealistically long distance.
// Also, I think it's possible that agents in big cells may become 'trapped' in those cells. In a big cell, case
// 3 (above) is the most likely scenario, so the agent will most likely move somewhere within the cell. However, the
// LCP is always calculated from the cell centroid, so for the next LCP it'll start at the cell centroid regardless
// of where the point actually is. This will greatly reduce the chance of the agent getting out of this cell.
void Simulation::moveLCP(){
    for(auto iAgent : agents){ 
        double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
        iAgent->dist = dist;
        iAgent->dir = -1;
        if(dist > 0){
            Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
            iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
            double value = quadtree->getValue(destPt);
            if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
            // if(!std::isnan(destNode->value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
                //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
                if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
                //std::shared_ptr<Node> agentNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
                // if(destNode->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
                //if(destNode->id == agentNode->id){
                    double cost = dist + dist * quadtree->getValue(Point(iAgent->x, iAgent->y));
                    //double cost = dist + dist * agentNode->value;
                    if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
                        double pct = dist/cost; //get the ratio of dist and cost

                        //now find the point along the line between these two points where we hit the maximum cost
                        double x = iAgent->x + (destPt.x - iAgent->x)*pct;
                        double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
                        iAgent->x = x;
                        iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
                    }
                } else {
                    std::vector<Point> newPoints{*iAgent, destPt};
                    LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, newPoints, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
                    
                    auto path = spf.getLcp(destPt); //get the shortest path
                    
                    if(path.size() > 0){ // only proceed if a path was found
                        //now go through the path and check if any of the values are above the threshold that determines which values are considered complete barriers to movement
                        int index = 0;
                        int prevCost = 0;
                        for(size_t i = 0; i < path.size(); ++i){
                            index = i;
                            double totCost = path[i]->cost + path[i]->dist; // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
                            if(totCost > dist){
                                if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
                                    index=i-1;
                                } else {
                                    index=i;
                                }
                                break;
                            }
                            prevCost = totCost;
                            // if(path.at(i)->node.lock()->value >= params.movtBarrierThreshold){ //if we've hit a barrier, we'll cut the path off.
                            //     break;
                            // } else{
                            //     //double totCost = std::get<1>(path.at(i)) + std::get<2>(path.at(i)); // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
                            //     double totCost = path[i]->cost + path[i]->dist; // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
                            //     if(totCost > dist){
                            //         if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
                            //             index=i-1;
                            //         } else {
                            //             index=i;
                            //         }
                            //         break;
                            //     }
                            //     prevCost = totCost;
                            // }
                        }
                        auto finalNode = path[index]->node.lock();
                        
                        //if 'finalNode' is the same node as the original destination point fell in, set the final point to be the destination point
                        if(index == (int)path.size()-1){
                            iAgent->x = destPt.x;
                            iAgent->y = destPt.y;
                        } else { // if finalNode does NOT contain the original destination point, randomly put the final point somewhere inside this cell
                            
                            //Point nodeCentroid = Point((finalNode->xMax + finalNode->xMin)/2, (finalNode->yMax + finalNode->yMin)/2); //set the final point to be the cell centroid.  CHANGE THIS!!!! because we're using a quadtree this would result in all the agents being congregated at the middle
                            
                            //jitter the point so it's not at the centroid - NOTE!!!!!!! - CHANGE THIS!!!! - this is a very simplistic way to do this, and could cause problems because of the variable cell size of the quadtree cells
                            std::uniform_real_distribution<> disX(finalNode->xMin, finalNode->xMax); 
                            std::uniform_real_distribution<> disY(finalNode->yMin, finalNode->yMax); 
                            //double angle = dis(randomGenerator);//generate a random number between 0 and 2*pi
                            // Point finalPoint(disX(randomGenerator), disY(randomGenerator));
                            // iAgent->x = finalPoint.x;
                            // iAgent->y = finalPoint.y; //set the coordinates of the agent to the new point // 2/19/2021 d
                            // Point finalPoint(, );
                            iAgent->x = disX(randomGenerator);
                            iAgent->y = disY(randomGenerator); //set the coordinates of the agent to the new point // 2/19/2021 d
                                    
                        }
                        //save points to movtHist - probably will want to remove this when I start running the simulation for real
                        if(params.movtSaveAllMovt){
                            if(dist >= params.movtSaveThresholdDist){
                                for(size_t j = 0; j < path.size(); ++j){ // 2/19/2021 d
                                    // auto node = std::get<0>(path.at(j));
                                    auto node = path[j]->node.lock();
                                    Point point = Point((node->xMin + node->xMax)/2, (node->yMin + node->yMax)/2);
                                    std::tuple<int,int,int,int,Point> tup{counter, j, iAgent->id, j <= index, point};
                                    movtHist.push_back(tup);
                                }
                            }
                        }
                    }   
                }

            }
            // 2/19/2021 d }
        }
    }
}


Point Simulation::moveAgentToNeighbor(const Point &pt, const std::shared_ptr<Node> &node, const Point &ptNb, const std::shared_ptr<Node> &nodeNb, double maxCost){
    std::vector<double> difs{
        std::abs(node->xMin - nodeNb->xMax), // left side
        std::abs(node->xMax - nodeNb->xMin), // right side
        std::abs(node->yMin - nodeNb->yMax), // bottom
        std::abs(node->yMax - nodeNb->yMin) // top
    }; 
    int minIndex = 0;
    for(int i = 1; i < 4; ++i){
        if(difs[i] < difs[minIndex]){
            minIndex = i;
        }
    }

    // NOTE: my guess is that there is a much more concise way of doing these calculations (probably using matrices or something). Right now it's kind of cumbersome - there's a lot of code replication going on. But now that I finally got it working I'm not feeling especially inspired to make it as elegant as possible - I doubt it would improve performance at all. So I'm not going to. Maybe someday (let's be honest, that means never).
    double mid{0}; // this is the coordinate (x or y, depending on which side they are adjacent on) that represents the line along which the two nodes are adjacent
    bool isX = true; // tells us whether mid coordinate is an x-coordinate or a y-coordinate

    if(minIndex == 0){ // left side
        mid = node->xMin;
    } else if(minIndex == 1) { // right side
        mid = node->xMax;
    } else if(minIndex == 2) { // bottom
        mid = node->yMin;
        isX = false;
    } else if(minIndex == 3) { // top
        mid = node->yMax;
        isX = false;
    }
    
    // get the two "corners" where the two rectangles meet - these two coordinates define the segment of the "shared edge"
    Point corner1(std::max(node->xMin, nodeNb->xMin), std::max(node->yMin, nodeNb->yMin));
    Point corner2(std::min(node->xMax, nodeNb->xMax), std::min(node->yMax, nodeNb->yMax));
    
    // get the difference in the x and y coordinates
    double deltaX = ptNb.x - pt.x;
    double deltaY = ptNb.y - pt.y;
    double ratio{0}; // this will be the proportion of the line that falls in the first node (as long as the line falls entirely within the two nodes)
    Point midPoint; // this'll be point at which the line intersects the (vertical or horizontal) line represented by 'mid'
    bool inside = true; // tells us whether the segment lies entirely within the two neighboring cells
    // now get the ratio between: {the difference between the known (x or y) mid-coordinate and the (x or y) coordinate of the starting point} and {the difference in the (x or y) coordinates of the two centroids}
    if(isX){
        double x = mid;
        ratio = (x - pt.x) / deltaX;
        double y = pt.y + ratio * deltaY; // get the y coordinate of the intersection point
        // x and y now define the place where the line intersects 'mid'. Now check if this intersection point lies on the 'shared edge' of the two nodes represented by the two 'corner' points. If not, set the new midpoint to be the closest corner point
        if(y < corner1.y){
            inside = false;
            midPoint = Point(x, corner1.y);
        } else if(y > corner2.y){
            inside = false;
            midPoint = Point(x, corner2.y);
        } else {
            midPoint = Point(x, y);
        }
    } else {
        double y = mid;
        ratio = (y - pt.y) / deltaY;
        double x = pt.x + ratio * deltaX;
        if(x < corner1.x){
            inside = false;
            midPoint = Point(corner1.x, y);
        } else if (x > corner2.x){
            inside = false;
            midPoint = Point(corner2.x, y);
        } else {
            midPoint = Point(x, y);
        }
    }

    // calculate the length of the segment in each node
    double dist1;
    double dist2;

    if(inside){
        // if the line falls w/in the two nodes, use the ratio we calculated to get the length of the segment in each cell
        double dist = std::sqrt(std::pow(pt.x - ptNb.x, 2) + std::pow(pt.y - ptNb.y, 2));
        dist1 = dist * ratio;
        dist2 = dist - dist1;
    } else {
        // if the line goes outside the two nodes, calculate the distance from point 1 to the midpoint, and from the midpoint to point 2
        dist1 = std::sqrt(std::pow(pt.x - midPoint.x, 2) + std::pow(pt.y - midPoint.y, 2));
        dist2 = std::sqrt(std::pow(ptNb.x - midPoint.x, 2) + std::pow(ptNb.y - midPoint.y, 2));
    }

    double cost1 = dist1 + dist1 * node->value;
    double cost2 = dist2 + dist2 * nodeNb->value;

    double x;
    double y;
    if(cost1 > maxCost){
        double pct = maxCost / cost1; // get the ratio of dist and cost

        // now find the point along the line between these two points where we hit the maximum cost
        x = pt.x + (midPoint.x - pt.x)*pct;
        y = pt.y + (midPoint.y - pt.y)*pct;
    } else {
        double remainingCost = maxCost - cost1;
        double pct = remainingCost/cost2; // get the ratio of dist and cost

        // now find the point along the line between these two points where we hit the maximum cost
        x = midPoint.x + (ptNb.x - midPoint.x) * pct;
        y = midPoint.y + (ptNb.y - midPoint.y) * pct;
    }
    return Point(x,y);                
}

// modified version of 'moveLCP()' that I think is superior.
// changes:
//  * checks if destination point is in a neighbor - if so, moves it there, taking the cost of the cells into account. 
//      Basically it picks the point along the segment where the cumulative cost is equal to the maximum allowed cost.
//      Note that doing this check could actually slow things down if the vast majority of the destination points don't
//      fall in neighboring cells (as movement distance increases, this check become less effective and will eventually 
//      cause it to be slower)
//  * changed how I cut off the path. Before I would find the cell whose cumulative cost-distance is closest to the 
//      max cost allowed, and if it wasn't the destination point, then I'd pick a random point in that cell. Now I find 
//      the first cell whose cumulative cost-distance is greater than the max value. If this cell is not the one with 
//      the destination point, I pick a random point in the cell (this is because the destination was already chosen 
//      in continuous space - i.e. it's not the cell centroid. But if it's a different cell the point will be the 
//      centroid, so I pick a random point to make the agent's placement more random). Then I calculate the point
//      along this last segment where the cumulative cost-distance is equal to the maximum allowed cost, and that's the 
//      agent's end point.
void Simulation::moveLCP2(){
    for(auto iAgent : agents){ 
        double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
        iAgent->dist = dist;
        iAgent->dir = -1;
        if(dist > 0){
            Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
            iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
            //double value = quadtree->getValue(destPt);
            std::shared_ptr<Node> destNode = quadtree->getNode(destPt);
            
            if(destNode){
                // if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
                if(!std::isnan(destNode->value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
                    //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
                    // if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
                    std::shared_ptr<Node> agentNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
                    // if(destNode->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
                    if(destNode->id == agentNode->id){
                        // double cost = dist + dist * quadtree->getValue(Point(iAgent->x, iAgent->y));
                        double cost = dist + dist * agentNode->value;
                        if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
                            double pct = dist/cost; //get the ratio of dist and cost

                            //now find the point along the line between these two points where we hit the maximum cost
                            double x = iAgent->x + (destPt.x - iAgent->x) * pct;
                            double y = iAgent->y + (destPt.y - iAgent->y) * pct;

                            iAgent->x = x;
                            iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
                        }
                    } else {
                        // check if the destination point falls in a neighboring cell
                        int destNodeId = destNode->id;
                        auto it = std::find_if(agentNode->neighbors.begin(), agentNode->neighbors.end(), [&destNodeId](const std::weak_ptr<Node> nodeNb) {return nodeNb.lock()->id == destNodeId;});
                        if(it != agentNode->neighbors.end()){
                            Point finalPoint = moveAgentToNeighbor(*iAgent, agentNode, destPt, destNode, dist);
                            iAgent->x = finalPoint.x;
                            iAgent->y = finalPoint.y;
                            
                        } else {
                            std::vector<Point> newPoints{*iAgent, destPt};
                            LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, newPoints, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
                            
                            auto path = spf.getLcp(destPt); //get the shortest path
                            //std::cout << "path.size(): " << path.size() << "\n";
                            if(path.size() > 0){ // only proceed if a path was found
                                int index{0};
                                double prevCost{0};
                                double remainingCost{0};
                                // find the first point where the total cost is MORE than the max cost
                                // if(path.size() == 4){
                                //     int x = 1;
                                // }
                                for(size_t i = 0; i < path.size(); ++i){
                                    index = i;
                                    double totCost = path[i]->cost + path[i]->dist;
                                    if(totCost > dist){
                                        remainingCost = dist - prevCost;
                                        break;
                                    }
                                    prevCost = totCost;
                                }
                                
                                // if the final node ISN'T the same as the destination node, we'll randomize that point and then use 'moveAgentToNeighbor' to find the point between the penultimate point and that point where the max cost is met
                                Point finalPoint = path[index]->pt;
                                std::shared_ptr<Node> finalNode = path[index]->node.lock();
                                std::shared_ptr<LcpFinder::NodeEdge> nodeEdgeParent = path[index]->parent.lock();
                                if(index != (int)path.size()-1){
                                    std::uniform_real_distribution<> disX(finalNode->xMin, finalNode->xMax); 
                                    std::uniform_real_distribution<> disY(finalNode->yMin, finalNode->yMax); 
                                    finalPoint.x = disX(randomGenerator);
                                    finalPoint.y = disY(randomGenerator); 
                                }
                                std::shared_ptr<Node> parentNode = nodeEdgeParent->node.lock();
                                Point chosenPoint = moveAgentToNeighbor(nodeEdgeParent->pt, parentNode, finalPoint, finalNode, remainingCost);
                                // there's a small chance that 'moveAgentToNeighbor' could actually select a point in a different cell. This can happen if the randomly chosen point is closer to the penultimate point than the centroid was. If the total cost remaining is actually more than the total cost of the new edge formed by the randomly selected point, the end point will actually be *beyond* the randomly chosen point. This isn't a problem in and of itself, but it's possible that it goes into a different cell. Because of that, I've included this check - if the chosen point isn't in the penultimate cell or the final cell, then use the randomly selected point as the end point.
                                std::shared_ptr<Node> chosenNode = quadtree->getNode(chosenPoint);
                                if(chosenNode){
                                    if(chosenNode->id != parentNode->id && chosenNode->id != finalNode->id){
                                        chosenPoint = finalPoint;
                                    }
                                } else {
                                    chosenPoint = finalPoint;
                                }
                                
                                iAgent->x = chosenPoint.x;
                                iAgent->y = chosenPoint.y;
                                
                                //save points to movtHist - probably will want to remove this when I start running the simulation for real
                                // if(params.movtSaveAllMovt){
                                //     if(dist >= params.movtSaveThresholdDist){
                                //         for(size_t j = 0; j < path.size(); ++j){ // 2/19/2021 d
                                //             // auto node = std::get<0>(path.at(j));
                                //             auto node = path.at(j)->node.lock();
                                //             Point point = Point((node->xMin + node->xMax)/2, (node->yMin + node->yMax)/2);
                                //             std::tuple<int,int,int,int,Point> tup{counter, j, iAgent->id, j <= index, point};
                                //             movtHist.push_back(tup);
                                //         }
                                //     }
                                // }
                            }   
                        }
                    }
                }
            }
            // 2/19/2021 d }
        }
    }
}

// void Simulation::moveLCPTest(){
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         // std::cout << "check1\n";
//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         // double dist = 500; // FOR DEBUGGING ONLY!
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         // std::cout << "check2\n";
//         if(dist > 0){
//             //std::cout << "test\n";
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             //LcpFinder(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);
//             if(!std::isnan(quadtree->getValue(destPt))){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 iAgent->x = destPt.x;
//                 iAgent->y = destPt.y;
//                 // // std::cout << "check3\n";

//                 // //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 // if(quadtree->getNode(destPt.x, destPt.y)->id == quadtree->getNode(iAgent->x, iAgent->y)->id){
                    
//                 //     //CHANGE THIS LINE - can just use 'dist' instead
//                 //     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                 //     double cost = dist + dist*quadtree->getValue(iAgent->x, iAgent->y);
//                 //     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                 //         double pct = dist/cost; //get the ratio of dist and cost

//                 //         //now find the point along the line between these two points where we hit the maximum cost
//                 //         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                 //         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                 //         iAgent->setCoords(x, y); //set the coordinates of the agent to the new point // 2/19/2021 d
//                 //     }
//                 // } else {
//                 //     LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
//                 //     auto path = spf.getShortestPath(destPt); //get the shortest path
//                 //     // std::cout << "check4\n";
                    
//                 //     //now go through the path and check if any of the values are above the threshold that determines which values are considered complete barriers to movement
//                 //     int index = 0;
//                 //     int prevCost = 0;
//                 //     for(size_t i = 0; i < path.size(); ++i){
//                 //         index = i;
//                 //         if(std::get<0>(path.at(i))->value >= params.movtBarrierThreshold){ //if we've hit a barrier, we'll cut the path off.
//                 //             break;
//                 //         } else{
//                 //             double totCost = std::get<1>(path.at(i)) + std::get<2>(path.at(i)); // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
//                 //             if(totCost > dist){
//                 //                 if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
//                 //                     index=i-1;
//                 //                 } else {
//                 //                     index=i;
//                 //                 }
//                 //                 break;
//                 //             }
//                 //             prevCost = totCost;
//                 //         }
//                 //     }
//                 //     auto finalNode = std::get<0>(path.at(index));
                    
//                 //     //if 'finalNode' is the same node as the original destination point fell in, set the final point to be the destination point
//                 //     if(index == (int)path.size()-1){
//                 //         iAgent->setCoords(destPt.x, destPt.y);
//                 //     } else { // if finalNode does NOT contain the original destination point, randomly put the final point somewhere inside this cell
                        
//                 //         //Point nodeCentroid = Point((finalNode->xMax + finalNode->xMin)/2, (finalNode->yMax + finalNode->yMin)/2); //set the final point to be the cell centroid.  CHANGE THIS!!!! because we're using a quadtree this would result in all the agents being congregated at the middle
                        
//                 //         //jitter the point so it's not at the centroid - NOTE!!!!!!! - CHANGE THIS!!!! - this is a very simplistic way to do this, and could cause problems because of the variable cell size of the quadtree cells
//                 //         std::uniform_real_distribution<> disX(finalNode->xMin, finalNode->xMax); 
//                 //         std::uniform_real_distribution<> disY(finalNode->yMin, finalNode->yMax); 
//                 //         //double angle = dis(randomGenerator);//generate a random number between 0 and 2*pi
//                 //         Point finalPoint(disX(randomGenerator), disY(randomGenerator));
//                 //         iAgent->setCoords(finalPoint.x, finalPoint.y); //set the coordinates of the agent to the new point // 2/19/2021 d
                                
//                 //     }
//                 // //save points to movtHist - probably will want to remove this when I start running the simulation for real
//                 //     // if(params.movtSaveAllMovt){
//                 //     //     if(dist >= params.movtSaveThresholdDist){
//                 //     //         for(size_t j = 0; j < path.size(); ++j){ // 2/19/2021 d
//                 //     //             auto node = std::get<0>(path.at(j));
//                 //     //             Point point = Point((node->xMin + node->xMax)/2, (node->yMin + node->yMax)/2);
//                 //     //             std::tuple<int,int,int,int,Point> tup{counter, j, iAgent->id, j <= index, point};
//                 //     //             movtHist.push_back(tup);
//                 //     //         }
//                 //     //     }
//                 //     // }
//                 // }

//             }
//             // 2/19/2021 d }
//         }
//     }
// }


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// I realized that this doesn't do cost + dist when calculating the cost of paths - just costs. Which means that it can move
// farther than the chosen value because the resistance values are less than one
// The solution to this is to add one to all the cell values. I might have to do that when first starting the sim
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
void Simulation::moveLCPConstrained(){
    for(auto iAgent : agents){
        double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
        if(dist > 0){

            //make the LCP network, constraining it so that all of the paths have a cost less than 'dist'
            LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
            spf.makeNetworkCostDist(dist);
            
            //now figure out the probabilities for selecting each cell, based on cell size
            //first check to see how many nodes have LCPs - we'll use that to determine the size of the vectors
            int nPaths = 0;
            double areaSum = 0; //we'll eventually divide all the areas by 'areaSum' so that the probabilities sum to 1
            for(size_t i = 0; i < spf.nodeEdges.size(); ++i){
                if(spf.nodeEdges[i]->parent.lock()){
                    nPaths++;
                    std::shared_ptr<Node> node = spf.nodeEdges[i]->node.lock();
                    areaSum += (node->xMax - node->xMin) * (node->yMax - node->yMin);
                }
            }
            
            //probs stores the probabilities for selecting each cell, while 'indices' stores the corresponding index in 'spf.nodeEdges'
            std::vector<double> probs(nPaths);
            std::vector<int> indices(nPaths); //using parallel arrays... not great, but I need 'probs' to be it's own vector for using with the 'discrete_distribution<int>'
            int counter = 0;
            for(size_t i = 0; i < spf.nodeEdges.size(); ++i){
                if(spf.nodeEdges[i]->parent.lock()){
                    std::shared_ptr<Node> node = spf.nodeEdges[i]->node.lock();
                    double area = (node->xMax - node->xMin) * (node->yMax - node->yMin);
                    probs.at(counter) = area/areaSum;
                    indices.at(counter) = i;
                    counter++;
                }
            }

            //now use the probabilities to select which cell to move to
            std::discrete_distribution<int> distribution(probs.begin(), probs.end());
            int index = distribution(randomGenerator); //this is the index (in 'indices'!) of the chosen path
            int pathIndex = indices.at(index);

            //now pick a point within the chosen cell
            std::shared_ptr<Node> node = spf.nodeEdges.at(pathIndex)->node.lock();
            std::uniform_real_distribution<double> xUnif(node->xMin,node->xMax); 
            std::uniform_real_distribution<double> yUnif(node->yMin,node->yMax); 
            Point finalPoint = Point(xUnif(randomGenerator), yUnif(randomGenerator));
            iAgent->x = finalPoint.x;
            iAgent->y = finalPoint.y;
        }
    }
}

// uses 'params.movtDistanceProbs' to pick a random distance. Then picks a
// a random angle, and moves the agent to the corresponding point.
void Simulation::moveSimple(){
    for(auto iAgent : agents){ // 2/19/2021 i
        double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
        iAgent->dist = dist;
        iAgent->dir = -1;
        if(dist > 0){
            Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
            iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
            double value = quadtree->getValue(destPt);
            if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
                iAgent->x = destPt.x;
                iAgent->y = destPt.y;
            }
        }
    }
}

// similar to moveSimple, but doesn't allow an agent to move to a quadtree cell with 
// a different value than the value of its current cell. Also it "redraws" destination
// points that are unreachable - this helps avoid the edge-clumping problem
void Simulation::moveCulverts(){
    std::vector<std::tuple<std::shared_ptr<Agent>, Point>> crossingAgents;
    for(auto iAgent : agents){
        bool keepGoing{true};
        bool addedToCrossingAgents{false};
        int counter{0}; // if the chosen destination point isn't valid, we'll try picking another point - this variable keeps track of how many times we've tried so we can cut it off after a certain number of tries - this will prevent an infinite loop
        while(keepGoing && counter < 20){
            double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
            iAgent->dist = dist;
            iAgent->dir = -1;
            double currentValue = quadtree->getValue(*iAgent);
            if(dist > 0){
                Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
                iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
                double newValue = quadtree->getValue(destPt); // get the value of the current cell
                
                if(!std::isnan(newValue)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
                    double dif = abs(currentValue - newValue);
                    if(dif < .001) { // check if it switched to a cell with a different value (i.e. moved to a different "zone"). I'm making the assumption that the quadtree has integer values - but because the quadtree uses doubles I need to account for small amounts of error
                        iAgent->x = destPt.x;
                        iAgent->y = destPt.y;
                        keepGoing = false;
                    } else {
                        if(!addedToCrossingAgents && iAgent->age >= params.mateAge){
                            std::string rectId1 = Rectangle::isPointInRectangles(*iAgent, culvertRectangles);
                            std::string rectId2 = Rectangle::isPointInRectangles(destPt, culvertRectangles);
                            if(rectId1 != "" && rectId1 == rectId2){
                                crossingAgents.emplace_back(std::make_tuple(iAgent, destPt));
                                addedToCrossingAgents = true;
                            }
                        }
                    }
                }
            } else {
                keepGoing = false;
            }
            counter++;
        }
    }
    if(params.movtNPerCulvertPerYear > 0 && crossingAgents.size() > 0){
        std::shuffle(crossingAgents.begin(), crossingAgents.end(), randomGenerator);
        int n = params.movtNPerCulvertPerYear;
        if(crossingAgents.size() < n){
            n = crossingAgents.size();
        }
        for(int i = 0; i < n; i++){
            std::shared_ptr<Agent> agent_i = std::get<0>(crossingAgents[i]);
            Point point_i = std::get<1>(crossingAgents[i]);
            agent_i->x = point_i.x;
            agent_i->y = point_i.y;
        }
    }
}
// void Simulation::moveSimple2(){
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         // std::cout << "check1\n";
//         // std::cout << iAgent->id << "\n";
//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         // std::cout << "dist: " << dist << "\n";
//         if(dist > 0){
//             //std::cout << "test\n";
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             // std::cout << "dir: " << iAgent->dir << "\n";
//             //LcpFinder(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);
//             double value = quadtree->getValue(destPt);
//             // std::cout << "value: " << value << "\n";
//             if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 // std::cout << "check3\n";

//                 //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
//                     // std::cout << "same cell\n";
//                     //CHANGE THIS LINE - can just use 'dist' instead
//                     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                     double cost = dist + dist*quadtree->getValue(Point(iAgent->x, iAgent->y));
//                     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                         double pct = dist/cost; //get the ratio of dist and cost

//                         //now find the point along the line between these two points where we hit the maximum cost
//                         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                         iAgent->x = x;
//                         iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                     }
//                 } else {
//                     iAgent->x = destPt.x;
//                     iAgent->y = destPt.y;                
//                 }

//             }
//             // 2/19/2021 d }
//         }
//     }
// }


// void Simulation::moveSimple3(){
//     for(auto iAgent : agents){ 
//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         if(dist > 0){
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             double value = quadtree->getValue(destPt);
//             if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
//                     // std::cout << "same cell\n";
//                     //CHANGE THIS LINE - can just use 'dist' instead
//                     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                     double cost = dist + dist*quadtree->getValue(Point(iAgent->x, iAgent->y));
//                     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                         double pct = dist/cost; //get the ratio of dist and cost

//                         //now find the point along the line between these two points where we hit the maximum cost
//                         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                         iAgent->x = x;
//                         iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                     }
//                 } else {
//                     // std::cout << "different cell\n";
//                     LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
//                     auto path = spf.getLcp(destPt); //get the shortest path
//                     // std::cout << "check4\n";
                    
//                     if(path.size() > 0){
//                         iAgent->x = destPt.x;
//                         iAgent->y = destPt.y;
//                     }
                    
//                 }

//             }
//             // 2/19/2021 d }
//         }
//     }
// }

// void Simulation::moveSimple4(){
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         // std::cout << "check1\n";
//         // std::cout << iAgent->id << "\n";
//         // if(iAgent->id == 2267512 && counter == 7){
//         //     int x{1};
//         // }


//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         // std::cout << "dist: " << dist << "\n";
//         if(dist > 0){
//             //std::cout << "test\n";
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             // std::cout << "dir: " << iAgent->dir << "\n";
//             //LcpFinder(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);
//             double value = quadtree->getValue(destPt);
//             // std::cout << "value: " << value << "\n";
//             if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 // std::cout << "check3\n";

//                 //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
//                     // std::cout << "same cell\n";
//                     //CHANGE THIS LINE - can just use 'dist' instead
//                     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                     double cost = dist + dist*quadtree->getValue(Point(iAgent->x, iAgent->y));
//                     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                         double pct = dist/cost; //get the ratio of dist and cost

//                         //now find the point along the line between these two points where we hit the maximum cost
//                         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                         iAgent->x = x;
//                         iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                     }
//                 } else {
//                     // std::cout << "different cell\n";
//                     LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
//                     auto path = spf.getLcp(destPt); //get the shortest path
//                     // std::cout << "check4\n";
                    
//                     //now go through the path and check if any of the values are above the threshold that determines which values are considered complete barriers to movement
//                     int index = 0;
//                     int prevCost = 0;
//                     for(size_t i = 0; i < path.size(); ++i){
//                         index = i;
//                         if(std::get<0>(path.at(i))->value >= params.movtBarrierThreshold){ //if we've hit a barrier, we'll cut the path off.
//                             break;
//                         } else{
//                             double totCost = std::get<1>(path.at(i)) + std::get<2>(path.at(i)); // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
//                             if(totCost > dist){
//                                 if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
//                                     index=i-1;
//                                 } else {
//                                     index=i;
//                                 }
//                                 break;
//                             }
//                             prevCost = totCost;
//                         }
//                     }
//                     auto finalNode = std::get<0>(path.at(index));
                    
//                     iAgent->x = (finalNode->xMin + finalNode->xMax)/2;
//                     iAgent->y = (finalNode->yMin + finalNode->yMax)/2;

//                     // if(iAgent->id == 2267512 && counter == 7 && finalNode->id == 5630){
//                     //     int x{1};
//                     //     throw std::runtime_error("uh-oh");
//                     // }

//                     if(params.movtSaveAllMovt){
//                         if(dist >= params.movtSaveThresholdDist){
//                             for(size_t j = 0; j < path.size(); ++j){ // 2/19/2021 d
//                                 auto node = std::get<0>(path.at(j));
//                                 Point point = Point((node->xMin + node->xMax)/2, (node->yMin + node->yMax)/2);
//                                 std::tuple<int,int,int,int,Point> tup{counter, j, iAgent->id, j <= index, point};
//                                 movtHist.push_back(tup);
//                             }
//                         }
//                     }
//                 }
                

//             }
//             // 2/19/2021 d }
//         }
//     }
// }


// void Simulation::moveSimple5(){
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         // std::cout << "check1\n";
//         // std::cout << iAgent->id << "\n";
//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         // std::cout << "dist: " << dist << "\n";
//         if(dist > 0){
//             //std::cout << "test\n";
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             // std::cout << "dir: " << iAgent->dir << "\n";
//             //LcpFinder(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);
//             double value = quadtree->getValue(destPt);
//             // std::cout << "value: " << value << "\n";
//             if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 // std::cout << "check3\n";

//                 //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
//                     // std::cout << "same cell\n";
//                     //CHANGE THIS LINE - can just use 'dist' instead
//                     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                     double cost = dist + dist*quadtree->getValue(Point(iAgent->x, iAgent->y));
//                     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                         double pct = dist/cost; //get the ratio of dist and cost

//                         //now find the point along the line between these two points where we hit the maximum cost
//                         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                         iAgent->x = x;
//                         iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                     }
//                 } else {
//                     int n{10};
//                     std::vector<Point> pts(n);
//                     for(int i = 0; i < n; ++i){
//                         // std::cout << "different cell\n";
//                         LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
//                         auto path = spf.getLcp(destPt); //get the shortest path
//                         // std::cout << "check4\n";
                        
//                         //now go through the path and check if any of the values are above the threshold that determines which values are considered complete barriers to movement
//                         int index = 0;
//                         int prevCost = 0;
//                         for(size_t i = 0; i < path.size(); ++i){
//                             // if(std::isnan(std::get<0>(path.at(i))->value) ||
//                             //     std::get<0>(path.at(i))->value > 1 || 
//                             //     std::get<0>(path.at(i))->value < 0){
//                             //     int x{1};
//                             // }
//                             // if(std::get<1>(path.at(i)) < 0 || std::get<1>(path.at(i)) > 10000){
//                             //     int x{1};
//                             // }
//                             // if(std::get<2>(path.at(i)) < 0 || std::get<2>(path.at(i)) > 10000){
//                             //     int x{1};
//                             // }
//                             index = i;
//                             if(std::get<0>(path.at(i))->value >= params.movtBarrierThreshold){ //if we've hit a barrier, we'll cut the path off.
//                                 break;
//                             } else{
//                                 double totCost = std::get<1>(path.at(i)) + std::get<2>(path.at(i)); // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
//                                 if(totCost > dist){
//                                     if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
//                                         index=i-1;
//                                     } else {
//                                         index=i;
//                                     }
//                                     break;
//                                 }
//                                 prevCost = totCost;
//                             }
//                         }
//                         auto finalNode = std::get<0>(path.at(index));
                        
//                         Point finalPoint((finalNode->xMin + finalNode->xMax)/2, (finalNode->yMin + finalNode->yMax)/2);
//                         pts[i] = finalPoint;
//                         if(i > 0){
//                             if(pts[i-1].x != pts[i].x || 
//                                 pts[i-1].y != pts[i].y){
//                                     int x{1};
//                                     throw std::runtime_error("uh-oh");
//                                 }
//                         }
                        
//                     }
//                     iAgent->x = pts[0].x;
//                     iAgent->y = pts[0].y;
//                 }

//             }
//             // 2/19/2021 d }
//         }
//     }
// }

// void Simulation::moveLCP2(){
//     //for(auto &iAgent : agents){ // 2/19/2021 i
//     for(auto iAgent : agents){ // 2/19/2021 i
//         // Point attractPt = MathUtilities::getRandomPointOnCircle(*agents[i], params.movtAttractDist, randomGenerator); // 2/19/2021 d
//         // std::cout << "check1\n";
//         // std::cout << iAgent->id << "\n";
//         double dist = params.movtDistanceProbs.getRandomValue(randomGenerator);
//         iAgent->dist = dist;
//         iAgent->dir = -1;
//         // std::cout << "dist: " << dist << "\n";
//         if(dist > 0){
//             //std::cout << "test\n";
//             Point destPt = PointUtilities::getRandomPointOnCircle(*iAgent, dist, randomGenerator); // 2/19/2021 i
//             iAgent->dir = PointUtilities::getAngle(*iAgent, destPt);
//             // std::cout << "dir: " << iAgent->dir << "\n";
//             //LcpFinder(std::shared_ptr<Quadtree> _quadtree, Point startPoint, double _xMin, double _xMax, double _yMin, double _yMax);
//             double value = quadtree->getValue(destPt);
//             // std::cout << "value: " << value << "\n";
//             if(!std::isnan(value)){ //only proceed if the destination point isn't NaN - NOTE - this may not be ideal. This means agents on the edges will be less likely to move.
//                 // std::cout << "check3\n";

//                 //check if the destination point falls in the same cell we're currently in. If so, move directly to it.
//                 if(quadtree->getNode(destPt)->id == quadtree->getNode(Point(iAgent->x, iAgent->y))->id){
//                     // std::cout << "same cell\n";
//                     //CHANGE THIS LINE - can just use 'dist' instead
//                     //double dist = PointUtilities::distBtwPoints(*iAgent,destPt);
//                     double cost = dist + dist*quadtree->getValue(Point(iAgent->x, iAgent->y));
//                     if(cost > dist){ //probably don't need this check. cost will always be greater than dist unless the cost of the cell is 0
//                         double pct = dist/cost; //get the ratio of dist and cost

//                         //now find the point along the line between these two points where we hit the maximum cost
//                         double x = iAgent->x + (destPt.x - iAgent->x)*pct;
//                         double y = iAgent->y + (destPt.y - iAgent->y)*pct;
                        
//                         iAgent->x = x;
//                         iAgent->y = y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                     }
//                 } else {
//                     // std::cout << "different cell\n";
//                     LcpFinder spf(quadtree, *iAgent, iAgent->x - dist*2, iAgent->x + dist*2, iAgent->y - dist*2, iAgent->y + dist*2, false); //since we're limiting the max distance moved to 'dist', we can limit the search area to the square area with sides of 2*dist
//                     auto path = spf.getLcp(destPt); //get the shortest path
//                     // std::cout << "check4\n";
                    
//                     if(path.size() > 0){
//                         //now go through the path and check if any of the values are above the threshold that determines which values are considered complete barriers to movement
//                         int index = 0;
//                         int prevCost = 0;
//                         for(size_t i = 0; i < path.size(); ++i){
//                             index = i;
//                             if(std::get<0>(path.at(i))->value >= params.movtBarrierThreshold){ //if we've hit a barrier, we'll cut the path off.
//                                 break;
//                             } else{
//                                 double totCost = std::get<1>(path.at(i)) + std::get<2>(path.at(i)); // get the total cost. The values of the resistance surface go from 0 to 1 - this means the total cost will be less than or equal to the distance. But we want the cost to be greater than the distance (ideally, in a no resistance scenario, the cost would equal the distance.) This basically amounts to the values being from 1-2 rather than 0-1. Since we have both the distance and the cost, we can just add the two together (*should* be the same thing)
//                                 if(totCost > dist){
//                                     if((i > 0) && (dist-prevCost < totCost-dist)){ //we want to end at the point where the resistance value is closest to the distance - to do that we'll check how close the previous distance was
//                                         index=i-1;
//                                     } else {
//                                         index=i;
//                                     }
//                                     break;
//                                 }
//                                 prevCost = totCost;
//                             }
//                         }
//                         auto finalNode = std::get<0>(path.at(index));
                        
//                         //if 'finalNode' is the same node as the original destination point fell in, set the final point to be the destination point
//                         if(index == (int)path.size()-1){
//                             iAgent->x = destPt.x;
//                             iAgent->y = destPt.y;
//                         } else { // if finalNode does NOT contain the original destination point, randomly put the final point somewhere inside this cell
                            
//                             //Point nodeCentroid = Point((finalNode->xMax + finalNode->xMin)/2, (finalNode->yMax + finalNode->yMin)/2); //set the final point to be the cell centroid.  CHANGE THIS!!!! because we're using a quadtree this would result in all the agents being congregated at the middle
                            
//                             //jitter the point so it's not at the centroid - NOTE!!!!!!! - CHANGE THIS!!!! - this is a very simplistic way to do this, and could cause problems because of the variable cell size of the quadtree cells
//                             std::uniform_real_distribution<> disX(finalNode->xMin, finalNode->xMax); 
//                             std::uniform_real_distribution<> disY(finalNode->yMin, finalNode->yMax); 
//                             //double angle = dis(randomGenerator);//generate a random number between 0 and 2*pi
//                             // Point finalPoint(disX(randomGenerator), disY(randomGenerator));
//                             // iAgent->x = finalPoint.x;
//                             // iAgent->y = finalPoint.y; //set the coordinates of the agent to the new point // 2/19/2021 d
//                             // Point finalPoint(, );
//                             iAgent->x = disX(randomGenerator);
//                             iAgent->y = disY(randomGenerator); //set the coordinates of the agent to the new point // 2/19/2021 d
                                    
//                         }
//                         //save points to movtHist - probably will want to remove this when I start running the simulation for real
//                         if(params.movtSaveAllMovt){
//                             if(dist >= params.movtSaveThresholdDist){
//                                 for(size_t j = 0; j < path.size(); ++j){ // 2/19/2021 d
//                                     auto node = std::get<0>(path.at(j));
//                                     Point point = Point((node->xMin + node->xMax)/2, (node->yMin + node->yMax)/2);
//                                     std::tuple<int,int,int,int,Point> tup{counter, j, iAgent->id, j <= index, point};
//                                     movtHist.push_back(tup);
//                                 }
//                             }
//                         }
//                     }
//                 }

//             }
//             // 2/19/2021 d }
//         }
//     }
// }


// //note - it'd be nice to try using the pointgrid approach and seeing if that speeds things up
// void Simulation::reproduce(){
//     double sqMatingDist = pow(params.maxMatingDist, 2); //using squared distances cuts down on computation time
//     std::vector<std::shared_ptr<Agent>> babyAgents(0); //keep the babies separate so they're not included in the mating calculations
//     for(size_t i = 0; i < agents.size(); ++i){ // loop over each agent;
//         if(i % 1000 == 0){
//             std::cout << "i: " << i << "\n";
//         }
//         if( (agents[i]->isMale == 0) & (agents[i]->age >= params.matingAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age
//             std::uniform_real_distribution<> mateDstr(0, 1); //create the distribution we'll draw a random number from
//             double randomNumber = mateDstr(randomGenerator); //draw a random number from the distribution
//             if(randomNumber < params.matingProb){ //if it's less than 'matingProb', then this agent will mate (or at least attempt to mate - it's possible that there aren't any males close enough)
//                 std::vector<std::shared_ptr<Agent>> closeMales(0); //initialize a vector for storing the close males
//                 for(size_t j = 0; j < agents.size(); ++j){ //now we'll figure out which ones are within mating distance
//                     //std::cout << "j: " << j << "\n";
//                     if( (agents[j]->isMale == 1) & (agents[j]->age >= params.matingAge) ){
//                         double sqDist = MathUtilities::sqDistBtwPoints(*agents[i], *agents[j]);
//                         if(sqDist <= sqMatingDist){
//                             closeMales.push_back(agents[j]);
//                         }
//                     }
//                 }
//                 if(closeMales.size() > 0){
//                     for(int k = 0; k < params.clutchSize; ++k){
//                         //std::cout << "k: " << k << "\n";
//                         std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
//                         std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
//                         int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
//                         Point babyPoint = Point(agents[i]->x, agents[i]->y); //make the baby start in the same place as it's mother
//                         babyAgents.push_back(makeAgent(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y,closeMales.at(maleIndex),agents[i]));
//                     }
//                 }
//             }
//         }
//     }
//     for(size_t i = 0; i < babyAgents.size(); ++i){
//         agents.push_back(babyAgents[i]);
//     }
// }





std::shared_ptr<PointGrid> Simulation::makePointGrid(){
    int nX = std::ceil((quadtree->root->xMax - quadtree->root->xMin)/params.simPointGridCellSize);
    int nY = std::ceil((quadtree->root->yMax - quadtree->root->yMin)/params.simPointGridCellSize);
    double xMin = quadtree->root->xMin;
    double yMin = quadtree->root->yMin;    
    double xMax = xMin + nX*params.simPointGridCellSize;
    double yMax = yMin + nX*params.simPointGridCellSize;

    //PointGrid *pg = new PointGrid(agents,nX, nY, xMin, xMax, yMin, yMax); // do I need to make this a pointer?
    // std::shared_ptr<PointGrid> pg_ptr = std::shared_ptr<PointGrid>(pg); //should this really be a shared pointer? I'm just using it because I'm familiar with shared_ptrs
    std::shared_ptr<PointGrid> pg_ptr = std::shared_ptr<PointGrid>(new PointGrid(agents, nX, nY, xMin, xMax, yMin, yMax)); //should this really be a shared pointer? I'm just using it because I'm familiar with shared_ptrs
    return pg_ptr;
}


std::shared_ptr<PointGrid> Simulation::makePointGridAdultMales(){
    int nX = std::ceil((quadtree->root->xMax - quadtree->root->xMin)/params.simPointGridCellSize);
    int nY = std::ceil((quadtree->root->yMax - quadtree->root->yMin)/params.simPointGridCellSize);
    double xMin = quadtree->root->xMin;
    double yMin = quadtree->root->yMin;    
    double xMax = xMin + nX*params.simPointGridCellSize;
    double yMax = yMin + nX*params.simPointGridCellSize;

    //PointGrid *pg = new PointGrid(agents,nX, nY, xMin, xMax, yMin, yMax); // do I need to make this a pointer?
    // std::shared_ptr<PointGrid> pg_ptr = std::shared_ptr<PointGrid>(pg); //should this really be a shared pointer? I'm just using it because I'm familiar with shared_ptrs
    std::shared_ptr<PointGrid> pg_ptr = std::shared_ptr<PointGrid>(new PointGrid(nX, nY, xMin, xMax, yMin, yMax)); //should this really be a shared pointer? I'm just using it because I'm familiar with shared_ptrs
    pg_ptr->initializePointsAdultMales(agents, params.mateAge);
    return pg_ptr;
}

std::vector<std::vector<int>> Simulation::breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, double mutateProb, std::mt19937 &randomGenerator){
// std::vector<std::vector<int>> SimUtilities::breed(std::vector<std::vector<int>> &alleles1, std::vector<std::vector<int>> &alleles2, std::mt19937 &randomGenerator){
    if(alleles1.size() != alleles2.size()) throw std::runtime_error("Error in 'SimUtilities::breed() - 'alleles1' and 'alleles2' have different lengths");

    std::uniform_int_distribution<> indexDist(0,1); 
    std::vector<std::vector<int>> newAlleles(alleles1.size());

    for(size_t i = 0; i < alleles1.size(); ++i){
        if((alleles1[i].size() != 2) || (alleles2[i].size() != 2)) { //throw std::runtime_error("Error in 'SimUtilities::breed() - 'alleles1.at(" + std::to_string(i) + ")' and 'alleles2.at(" + std::to_string(i) + ")' have different lengths");
            throw std::runtime_error("Error in 'Simulation::breed() -  'alleles1.at(" + std::to_string(i) + ")' had size " + std::to_string(alleles1[i].size()) + " and 'alleles2.at(" + std::to_string(i) + ")' had size " + std::to_string(alleles2[i].size()) + ". Both must have size 2.");
        }
        newAlleles[i] = std::vector<int>(2);
        newAlleles[i][0] = (indexDist(randomGenerator) == 0) ? alleles1[i][0] : alleles1[i][1];
        newAlleles[i][1] = (indexDist(randomGenerator) == 0) ? alleles2[i][0] : alleles2[i][1];

        // newAlleles[i] = std::vector<int>(alleles1[i].size());    
        // for(size_t j = 0; j < alleles1[i].size(); ++j){
        //     newAlleles[i][j] = (indexDist(randomGenerator) == 0) ? alleles1[i][j] : alleles2[i][j];
        // }
    }
    if(mutateProb > 0){
        std::uniform_real_distribution<> mutateProbDist(0.0,1.0);
        std::uniform_int_distribution<> binaryDist(0,1); 
        for(size_t i = 0; i < newAlleles.size(); ++i){
            if(mutateProbDist(randomGenerator) < mutateProb){
                int ind = binaryDist(randomGenerator);
                newAlleles[i][ind] = (binaryDist(randomGenerator) == 0) ? newAlleles[i][ind] - 1 : newAlleles[i][ind] + 1;
                if(newAlleles[i][ind] < 0){
                    newAlleles[i][ind] = 0;
                }
            }
            // for(size_t j = 0; j < 2; ++j){
            //     if(mutateProbDist(randomGenerator) < mutateProb){
            //         newAlleles[i][j] = (mutateTypeDist(randomGenerator) == 0) ? newAlleles[i][j] - 1 : newAlleles[i][j] + 1;
            //         if(newAlleles[i][j] < 0){
            //             newAlleles[i][j] = 0;
            //         }
            //     }
            // }
        }
    }
    return newAlleles;
}

void Simulation::reproducePointGrid(){
    // //TEMPORARY {
    // int nX = std::ceil((quadtree->root->xMax - quadtree->root->xMin)/params.simPointGridCellSize);
    // int nY = std::ceil((quadtree->root->yMax - quadtree->root->yMin)/params.simPointGridCellSize);
    // double xMin = quadtree->root->xMin;
    // double yMin = quadtree->root->yMin;    
    // double xMax = xMin + nX*params.simPointGridCellSize;
    // double yMax = yMin + nX*params.simPointGridCellSize;
    // //TEMPORARY }

    // //RESTORE THIS CHUNK {
    // // int nX = std::ceil((quadtree->root->xMax - quadtree->root->xMin)/params.maxMatingDist);
    // // int nY = std::ceil((quadtree->root->yMax - quadtree->root->yMin)/params.maxMatingDist);
    // // double xMin = quadtree->root->xMin;
    // // double yMin = quadtree->root->yMin;
    // // double xMax = xMin + nX*params.maxMatingDist;
    // // double yMax = yMin + nX*params.maxMatingDist;
    // //RESTORE THIS CHUNK }


    // // std::cout << "reproducePointGrid: test1\n";
    // PointGrid *pg = new PointGrid(agents,nX, nY, xMin, xMax, yMin, yMax); // do I need to make this a pointer?
    // std::shared_ptr<PointGrid> pg_ptr = std::shared_ptr<PointGrid>(pg); //should this really be a shared pointer? I'm just using it because I'm familiar with shared_ptrs
    std::shared_ptr<PointGrid> pg_ptr = makePointGrid();
    // std::cout << "reproducePointGrid: test2\n";
    //std::cout << pg_ptr->toString();
    //double sqMatingDist = pow(params.maxMatingDist, 2); //using squared distances cuts down on computation time
    // std::vector<std::shared_ptr<Agent>> babyAgents(0); //keep the babies separate so they're not included in the mating calculations // 2/19/2021 d 
    std::list<std::shared_ptr<Agent>> babyAgents; //keep the babies separate so they're not included in the mating calculations // 2/19/2021 i
    //std::cout << "test3\n";
    // for(size_t i = 0; i < agents.size(); ++i){ // loop over each agent; // 2/19/2021 d
    // std::cout << "reproducePointGrid: test3\n";
    //int nBorn{0};
    //for(auto &iAgent : agents){ // loop over each agent; // 2/19/2021 i
    for(auto iAgent : agents){ // loop over each agent; // 2/19/2021 i
        // if(iAgent->id == 1794830 && counter == 2){
        //     int x{1};
        // }
        // std::cout << "reproducePointGrid: test4\n";
        // if(i % 1000 == 0){
        //     std::cout << "i: " << i << "\n";
        // }
        // if( (agents[i]->isMale == 0) & (agents[i]->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 d
        //if( (iAgent->isMale == 0) & (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
        if( (iAgent->isMale == 0) && (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
            //std::uniform_real_distribution<> mateDstr(0, 1); //create the distribution we'll draw a random number from
            //std::cout << "reproducePointGrid: test5\n";
            //double randomNumber = mateDstr(randomGenerator); //draw a random number from the distribution
            
            //int nEggs = std::lrint(params.mateNumEggsProbs.getRandomValue(randomGenerator)); //since the value returned is actually a double, use 'lrint' to round to an integer to avoid any floating point error
            std::poisson_distribution<int> pois(params.mateNumBabiesPois);
            int nEggs = pois(randomGenerator);
            if(nEggs > 0){ //if the number of eggs is greater than 0, reproduce.
            //std::cout << "reproducePointGrid: test6\n";
                // std::vector<std::shared_ptr<Agent>> closeAgents = pg_ptr->getPointsWithin(agents[i]->x, agents[i]->y, params.mateMaxDist); // 2/19/2021 d
                std::list<std::shared_ptr<Agent>> closeAgents = pg_ptr->getPointsWithin(iAgent->x, iAgent->y, params.mateMaxDist); // 2/19/2021 i
                //std::vector<std::shared_ptr<Agent>> closeAgents{ std::begin(closeAgentsList), std::end(closeAgentsList) }; //convert it to a vector so we can do random access more conveniently
                std::vector<std::shared_ptr<Agent>> closeMales; //initialize a vector for storing the close males
                //for(size_t j = 0; j < closeAgents.size(); ++j){ // 2/19/2021 d
                //std::cout << "reproducePointGrid: test7\n";
                std::unique_ptr<LcpFinder> lcpf;
                if(!closeAgents.empty()){
                    //for(auto &jAgent : closeAgents){ // 2/19/2021 i
                    std::shared_ptr<Node> startNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
                    for(auto jAgent : closeAgents){ // 2/19/2021 i
                        //std::cout << "reproducePointGrid: test8\n";
                        // 2/19/2021 d {
                        // if((agents[j]->isMale == 1) & (agents[j]->age >= params.mateAge)){
                        //     closeMales.push_back(agents[j]);
                        // }
                        // 2/19/2021 d }

                        // 2/19/2021 i {
                        // LcpFinder spf(quadtree, *iAgent, iAgent->x-params.mateMaxDist, iAgent->x+params.mateMaxDist, iAgent->y-params.mateMaxDist, iAgent->y+params.mateMaxDist);
                        // if((jAgent->isMale == 1) & (jAgent->age >= params.mateAge)){
                        
                        //closeMales.emplace_back(jAgent); //TEMPORARY!!!!!!!!!!!!!!!
                        // TEMPORARY COMMENTED OUT {
                        if((jAgent->isMale == 1) && (jAgent->age >= params.mateAge)){
                            // auto path = spf.getShortestPath(*jAgent);
                            // if(path.size() > 0){
                            //     closeMales.push_back(jAgent);
                            // }
                            
                            
                            std::shared_ptr<Node> endNode = quadtree->getNode(Point(jAgent->x, jAgent->y));

                            //bool isReachable{false};
                            bool isReachable = startNode->id == endNode->id &&
                                                !std::isnan(startNode->value) &&
                                                !std::isnan(endNode->value);
                            if(!isReachable){
                                for(size_t i = 0; i < endNode->neighbors.size(); ++i){ //so long as the neighbors have been assigned correctly I could do this for either startNode or endNode
                                    if(endNode->neighbors[i].lock()->id == startNode->id){
                                        isReachable = true;
                                        break;
                                    };
                                }
                                if(!isReachable){
                                    if(!lcpf){
                                        lcpf = std::make_unique<LcpFinder>(quadtree, *iAgent, iAgent->x-params.mateMaxDist, iAgent->x+params.mateMaxDist, iAgent->y-params.mateMaxDist, iAgent->y+params.mateMaxDist, false);
                                    }
                                    auto path = lcpf->getLcp(*jAgent);
                                    if(path.size() > 0){
                                        isReachable = true;
                                    }
                                }
                            }
                            
                            if(isReachable) closeMales.emplace_back(jAgent);
                            
                            // if(MathUtilities::isReachable(*quadtree, iAgent->x, iAgent->y, jAgent->x, jAgent->y, params.mateMaxDist)){
                            //     //std::cout << "reproducePointGrid: test9\n";
                            //     closeMales.emplace_back(jAgent);
                            // }
                        }
                        // TEMPORARY COMMENTED OUT }

                        //std::cout << "reproducePointGrid: test10\n";
                        // 2/19/2021 i }
                    }
                }
                
                //std::cout << "reproducePointGrid: test11\n";
                if(closeMales.size() > 0){
                    // 2/19/2021 i {
                    //std::cout << "reproducePointGrid: test12\n";
                    for(int k = 0; k < nEggs; ++k){

                        
                        //std::cout << "reproducePointGrid: test13\n";
                        //std::cout << "k: " << k << "\n";
                        std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
                        std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
                        int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
                        // Point babyPoint = Point(agents[i]->x, agents[i]->y); //make the baby start in the same place as it's mother // 2/19/2021 d
                        Point babyPoint = Point(iAgent->x, iAgent->y); //make the baby start in the same place as it's mother // 2/19/2021 i
                        // babyAgents.push_back(makeAgent(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y,closeMales.at(maleIndex),agents[i])); // 2/19/2021 d
                        //std::cout << "reproducePointGrid: test14\n";
                        // std::vector<std::vector<int>> loci = SimUtilities::breed(closeMales.at(maleIndex)->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                        std::vector<std::vector<int>> loci = Simulation::breed(closeMales[maleIndex]->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                        //babyAgents.push_back(makeAgent(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y,closeMales.at(maleIndex),iAgent, loci)); // 2/19/2021 i
                        // std::shared_ptr<Agent> makeAgent(int id, int age, int isMale, int isAlive, double x, double y, std::shared_ptr<Agent> dadPtr, std::shared_ptr<Agent> momPtr, std::vector<std::vector<int>> loci){
                        babyAgents.emplace_back(std::make_shared<Agent>(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y, closeMales[maleIndex],iAgent, loci)); // 2/19/2021 i
                        //std::cout << "reproducePointGrid: test15\n";

                        // // DEBUGGGING {
                        // if(iAgent->id == 2450569 && 
                        //     counter == 32 &&
                        //     k == 0 &&
                        //     closeMales.at(maleIndex)->id != 2316379 ){
                        //         int x{1};
                        //     }

                        // // DEBUGGGING }
                    }
                    thisIter.nBorn += nEggs;
                    // 2/19/2021 i }

                    // 2/19/2021 d {
                    // for(int k = 0; k < nEggs; ++k){
                    //     //std::cout << "k: " << k << "\n";
                    //     std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
                    //     std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
                    //     int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
                    //     Point babyPoint = Point(agents[i]->x, agents[i]->y); //make the baby start in the same place as it's mother 
                    //     babyAgents.push_back(makeAgent(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y,closeMales.at(maleIndex),agents[i]));
                    // }
                    // 2/19/2021 d }
                }
            }
        }
    }
    //std::cout << "test4\n";
    
    // 2/19/2021 d {
    // for(size_t i = 0; i < babyAgents.size(); ++i){
    //     agents.push_back(babyAgents[i]);
    // }
    // 2/19/2021 d }

    // 2/19/2021 i {
    //for(auto &iAgent : babyAgents){
    for(auto iAgent : babyAgents){
        agents.emplace_back(iAgent);
    }
    //thisIter.nBorn = nBorn;
    // 2/19/2021 i }
    //std::cout << "test5\n";
}

// void Simulation::reproducePointGridNoLCP(){
//     // std::cout << "make point grid\n";
//     // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
//     std::shared_ptr<PointGrid> pg_ptr = makePointGrid();
//     // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
//     std::list<std::shared_ptr<Agent>> babyAgents; //keep the babies separate so they're not included in the mating calculations // 2/19/2021 i
    
//     for(auto iAgent : agents){ // loop over each agent; // 2/19/2021 i
    
//         if( (iAgent->isMale == 0) && (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
//             std::poisson_distribution<int> pois(params.mateNumBabiesPois);
//             int nEggs = pois(randomGenerator);
//             if(nEggs > 0){ //if the number of eggs is greater than 0, reproduce.
//                 std::list<std::shared_ptr<Agent>> closeAgents = pg_ptr->getPointsWithin(iAgent->x, iAgent->y, params.mateMaxDist); // 2/19/2021 i
//                 std::vector<std::shared_ptr<Agent>> closeMales; //initialize a vector for storing the close males
//                 std::unique_ptr<LcpFinder> lcpf;
//                 if(!closeAgents.empty()){
//                     std::shared_ptr<Node> startNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
//                     for(auto jAgent : closeAgents){ // 2/19/2021 i
//                         if((jAgent->isMale == 1) && (jAgent->age >= params.mateAge)){
//                             closeMales.emplace_back(jAgent);
//                         }
//                     }
//                 }
                
//                 if(closeMales.size() > 0){
//                     for(int k = 0; k < nEggs; ++k){
//                         std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
//                         std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
//                         int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
//                         Point babyPoint = Point(iAgent->x, iAgent->y); //make the baby start in the same place as it's mother // 2/19/2021 i
//                         std::vector<std::vector<int>> loci = Simulation::breed(closeMales.at(maleIndex)->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
//                         babyAgents.emplace_back(std::make_shared<Agent>(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y, closeMales[maleIndex],iAgent, loci)); // 2/19/2021 i

//                     }
//                     thisIter.nBorn += nEggs;
//                 }
//             }
//         }
//     }
//     for(auto iAgent : babyAgents){
//         agents.emplace_back(iAgent);
//     }
// }

// uses 'makePointGridAdultMales()' (previous version - the commented one above - used 'makePointGrid()', which
// makes a PointGrid using all agents. Turns out that doing it for just adult males is considerably faster).
void Simulation::reproducePointGridNoLCP(){
    // std::cout << "make point grid\n";
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
    std::shared_ptr<PointGrid> pg_ptr = makePointGridAdultMales();
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
    std::list<std::shared_ptr<Agent>> babyAgents; //keep the babies separate so they're not included in the mating calculations // 2/19/2021 i
    
    for(auto iAgent : agents){ // loop over each agent; // 2/19/2021 i
    
        if( (iAgent->isMale == 0) && (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
            std::poisson_distribution<int> pois(params.mateNumBabiesPois);
            int nEggs = pois(randomGenerator);
            if(nEggs > 0){ //if the number of eggs is greater than 0, reproduce.
                std::vector<std::shared_ptr<Agent>> closeMales = pg_ptr->getPointsWithin_Vector(iAgent->x, iAgent->y, params.mateMaxDist); // 2/19/2021 i
                // std::vector<std::shared_ptr<Agent>> closeMales; //initialize a vector for storing the close males
                // std::unique_ptr<LcpFinder> lcpf;
                // if(!closeAgents.empty()){
                //     std::shared_ptr<Node> startNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
                //     for(auto jAgent : closeAgents){ // 2/19/2021 i
                //         if((jAgent->isMale == 1) && (jAgent->age >= params.mateAge)){
                //             closeMales.emplace_back(jAgent);
                //         }
                //     }
                // }
                
                if(closeMales.size() > 0){
                    for(int k = 0; k < nEggs; ++k){
                        std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
                        std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
                        int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
                        Point babyPoint = Point(iAgent->x, iAgent->y); //make the baby start in the same place as it's mother // 2/19/2021 i
                        std::vector<std::vector<int>> loci = Simulation::breed(closeMales.at(maleIndex)->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                        babyAgents.emplace_back(std::make_shared<Agent>(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y, closeMales[maleIndex],iAgent, loci)); // 2/19/2021 i
                    }
                    thisIter.nBorn += nEggs;
                }
            }
        }
    }
    for(auto iAgent : babyAgents){
        agents.emplace_back(iAgent);
    }
}


void Simulation::reproducePointGridCulverts(){
    std::shared_ptr<PointGrid> pg_ptr = makePointGrid();
    std::list<std::shared_ptr<Agent>> babyAgents; //keep the babies separate so they're not included in the mating calculations // 2/19/2021 i
    
    for(auto iAgent : agents){ // loop over each agent; // 2/19/2021 i
        if( (iAgent->isMale == 0) && (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
            double iAgentValue = quadtree->getValue(*iAgent);
            std::poisson_distribution<int> pois(params.mateNumBabiesPois);
            int nEggs = pois(randomGenerator);
            if(nEggs > 0){ //if the number of eggs is greater than 0, reproduce.
                std::list<std::shared_ptr<Agent>> closeAgents = pg_ptr->getPointsWithin(iAgent->x, iAgent->y, params.mateMaxDist); // 2/19/2021 i
                std::vector<std::shared_ptr<Agent>> closeMales; //initialize a vector for storing the close males
                std::unique_ptr<LcpFinder> lcpf;
                if(!closeAgents.empty()){
                    std::shared_ptr<Node> startNode = quadtree->getNode(Point(iAgent->x, iAgent->y));
                    for(auto jAgent : closeAgents){ // 2/19/2021 i
                        double jAgentValue = quadtree->getValue(*jAgent);
                        double valueDif = std::abs(iAgentValue - jAgentValue);
                        if((jAgent->isMale == 1) && (jAgent->age >= params.mateAge) && valueDif < .001){
                            closeMales.emplace_back(jAgent);
                        }
                    }
                }
                
                if(closeMales.size() > 0){
                    for(int k = 0; k < nEggs; ++k){
                        std::uniform_int_distribution<> maleDstr(0,closeMales.size()-1); //we'll randomly select one of the males as the mate
                        std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
                        int maleIndex = maleDstr(randomGenerator); //get the index of the male we'll use for the father
                        Point babyPoint = Point(iAgent->x, iAgent->y); //make the baby start in the same place as it's mother // 2/19/2021 i
                        std::vector<std::vector<int>> loci = Simulation::breed(closeMales[maleIndex]->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                        babyAgents.emplace_back(std::make_shared<Agent>(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y, closeMales[maleIndex],iAgent, loci)); // 2/19/2021 i

                    }
                    thisIter.nBorn += nEggs;
                }
            }
        }
    }
    for(auto iAgent : babyAgents){
        agents.emplace_back(iAgent);
    }
}

//simple reproduction algorithm - FOR TESTING ONLY!!!!!
//each mature female has a set # of offspring, regardless of whether there's a nearby male
void Simulation::reproduceSimple(){
    std::list<std::shared_ptr<Agent>> babyAgents; //keep the babies separate so they're not included in the mating calculations // 2/19/2021 i
    for(auto iAgent : agents){ 
        if( (iAgent->isMale == 0) && (iAgent->age >= params.mateAge) ){ //only females reproduce, so only continue if the agent is female and above the mating age // 2/19/2021 i
            int nEggs = 4;
            
            for(int k = 0; k < nEggs; ++k){
                std::uniform_int_distribution<> sexDstr(0,1); //this is for determining the sex of the new tortoise
                Point babyPoint = Point(iAgent->x, iAgent->y); //make the baby start in the same place as it's mother // 2/19/2021 i

                // std::vector<std::vector<int>> loci = SimUtilities::breed(iAgent->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                std::vector<std::vector<int>> loci = Simulation::breed(iAgent->loci, iAgent->loci, params.mateMutateProb, randomGenerator);
                babyAgents.emplace_back(std::make_shared<Agent>(getNextId(),0,sexDstr(randomGenerator),1, babyPoint.x, babyPoint.y,iAgent,iAgent, loci)); // 2/19/2021 i
            }
            thisIter.nBorn += nEggs;
        }
    }
    for(auto iAgent : babyAgents){
        agents.emplace_back(iAgent);
    }
}

void Simulation::age(){
    // 2/19/2021 d {
    // for(size_t i = 0; i < agents.size(); ++i){
    //     agents[i]->age = agents[i]->age + 1;
    // }
    // 2/19/2021 d }

    // 2/19/2021 i {
    //for(auto &iAgent : agents){
    for(auto iAgent : agents){
        iAgent->age = iAgent->age + 1;
    }
    // 2/19/2021 i }
    //counter++;
}

// void Simulation::die(){
//     // for(size_t i = 0; i < agents.size(); ++i){
//     //     if(agents[i]->age > params.deathMaxAge){
//     //         agents[i]->isAlive = false;
//     //         deadAgents.push_back(agents[i]); //add the agent to 'deadAgents'
//     //         agents.erase(agents.begin() + i); //remove agent from 'agents'
//     //     }
//     // }
//     Raster quadCount = MathUtilities::quadratCount(agents,params.deathDist,quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
//     double minDens = MathUtilities::min(*quadCount.values);
//     double maxDens = MathUtilities::max(*quadCount.values);
//     int minAge = MathUtilities::min(params.deathAgeScores.vals);
//     int maxAge = MathUtilities::max(params.deathAgeScores.vals);
//     std::vector<double> riskScores(agents.size());
//     //for(size_t i = 0; i < agents.size(); ++i){ // 2/19/2021 d
//     int i = 0;
//     for(auto &iAgent : agents){ // 2/19/2021 i
//         double densityScore = quadCount.getValue(iAgent->x, iAgent->y);
//         double ageScore = params.deathAgeScores.getProb(iAgent->age);
//         double habitatScore = quadtree->getValue(iAgent->x, iAgent->y);

//         double densityScoreScaled = MathUtilities::scaleNum(densityScore, minDens, maxDens, 0,1);
//         double ageScoreScaled = MathUtilities::scaleNum(ageScore, minAge, maxAge, 0,1);
//         //habitatScore will already be between 0 and 1 so IN THEORY I can just leave it as is. But I'll want to change that, because that's making a lot of assumptions.
//         double risk = densityScoreScaled + ageScoreScaled + habitatScore;
//         //agents.at(i)->deathRisk = risk;
//         iAgent->deathRisk = risk;
//         riskScores.at(i) = risk;
//         i++;
//     }

//     std::vector<double> riskScoresSort = riskScores;
//     std::sort(riskScoresSort.begin(), riskScoresSort.end());
//     int quantIndex = std::floor(riskScoresSort.size() - (params.deathPercentage * riskScoresSort.size()));
//     if(quantIndex >= 0){
//         double deathScoreCutoff = riskScoresSort.at(quantIndex);
//         //for(size_t i = 0; i < agents.size(); ++i){
//         for(auto &iAgent : agents){
//             if(iAgent->deathRisk > deathScoreCutoff | iAgent->age >= params.deathMaxAge-1){
//                 iAgent->isAlive = false;
//                 iAgent->yearDied = counter;
//                 deadAgents.push_back(iAgent); //add the agent to 'deadAgents'
//                 agents.erase(agents.begin() + i); //remove agent from 'agents'
//             }
//         }
//     }
// }

void Simulation::killAgent(std::list<std::shared_ptr<Agent>>::iterator &i){
    (*i)->isAlive = false; // NOTE - I should make these 4 lines a function
    (*i)->yearDied = counter;
    // (*i)->dadPtr = nullptr; // this "releases" the shared pointer to the parent - this way an agent will be removed from memory once all it's children are dead
    // (*i)->momPtr = nullptr; // same as above
    //deadAgents.push_back(*i);
    //agents.erase(i++);
    thisIter.nDied += 1;
}

// void Simulation::die2(){
//     // for(size_t i = 0; i < agents.size(); ++i){
//     //     if(agents[i]->age > params.deathMaxAge){
//     //         agents[i]->isAlive = false;
//     //         deadAgents.push_back(agents[i]); //add the agent to 'deadAgents'
//     //         agents.erase(agents.begin() + i); //remove agent from 'agents'
//     //     }
//     // }

//     //!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CHANGE THIS PART!! this is hacky way to avoid a problem I was having. If we query a raster exactly the right or top edge (for example, if xMax is 300, we might query (300,120)) it breaks because the query assumes that any point exactly on a border go to the right or to the top, so for the right and top edges it just returns NA. I should probably fix this.
//     // double pad = (quadtree->root->xMax - quadtree->root->xMin)*.001;
//     // Raster quadCount = MathUtilities::quadratCount(agents,params.deathDist,quadtree->root->xMin-pad, quadtree->root->xMax+pad, quadtree->root->yMin-pad, quadtree->root->yMax+pad);
//     // Raster quadCount = PointUtilities::quadratCount(agents,params.deathDist,quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
//     std::shared_ptr<PointGrid> pg_ptr = makePointGrid();
//     std::vector<double> riskScores(agents.size());
//     //for(size_t i = 0; i < agents.size(); ++i){
//     std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
//     //https://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it/596180#596180
//     //int debugCounter{0};
//     while (i != agents.end()) {
//         //std::cout << debugCounter++ << "\n";
//         if((*i)->age < params.deathMaxAge){
//             //std::cout << "x: " << (*i)->x << " | y: " << (*i) << "\n";
//             double densityScore = pg_ptr->nPointsWithin((*i)->x, (*i)->y, params.deathDist);
//             //double densityScore = quadCount.getValue((*i)->x, (*i)->y);
//             double ageScore = params.deathAgeScores.getProb((*i)->age);
//             double resistScore = quadtree->getValue(Point((*i)->x, (*i)->y));
            
//             if(std::isnan(resistScore)){ //if the resist score is NAN, it dies
//                 killAgent(i);
//                 // (*i)->isAlive = false; //NOTE - I should make these 4 lines a function
//                 // (*i)->yearDied = counter;
//                 // deadAgents.push_back(*i);
//                 // agents.erase(i++);
//                 // thisIter.nDied += 1;
//             } else {
//                 int resistScoreAdj = std::lrint(std::floor(resistScore*10));
//                 double carryingCapacity = params.deathCarryingCapacity.getProb(resistScoreAdj);
//                 double dieProb = 1-(carryingCapacity/densityScore);
//                 if(dieProb < 0){
//                     dieProb = 0;
//                 }
//                 dieProb = dieProb + ageScore/100; //divide by 100 because the age stuff is from 0-100
                
//                 std::uniform_real_distribution<double> distribution(0, 1);
//                 double randNum = distribution(randomGenerator);
//                 if(randNum < dieProb){
//                     killAgent(i);
//                     // (*i)->isAlive = false;
//                     // (*i)->yearDied = counter;
//                     // deadAgents.push_back(*i);
//                     // agents.erase(i++);
//                     // thisIter.nDied += 1;
//                 } else {
//                     i++;
//                 }
//             }
//         } else {
//             killAgent(i);
//             // (*i)->isAlive = false;
//             // (*i)->yearDied = counter;
//             // deadAgents.push_back(*i);
//             // agents.erase(i++);
//             // thisIter.nDied += 1;
//         }
//     }
// }


double Simulation::riskScore(double val, double slope, double thresh){
    return (1 / (1 + exp(slope * (val - thresh))));
}
// double Simulation::riskScoreHabitat(double habitat, double slope, double thresh){
//     return (1 / (1 + exp(slope * (habitat - thresh)))) * 10;
// }

// double Simulation::riskScoreDensity(double density, double slope, double thresh){
//     return (1 / (1 + exp(-1 * slope * (density - thresh)))) * 10;
// }

// double Simulation::riskScoreAge(int age, double a1, double mid1, double a2, double mid2, double inflectionAge, double juvMortality, double adultMortality, double baselineMortality){
//     if(age < inflectionAge){
//         return (1 - 1/(1+exp(-1*a1*(age-mid1)))) * juvMortality + baselineMortality;
//     } else {
//         return (1/(1+exp(-1*a2*(age-mid2))))*adultMortality +baselineMortality;
//     }
// }


// OLD FUNCTION!!! I now use 'dieRiskScoresQuadrat()' because it's way faster
void Simulation::dieRiskScores(){
    //Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 128, 128, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // std::cout << "point density\n";
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
    // Raster density = AgentUtilities::quadratCount(agents, 1000, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 1000, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";

    std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
    while (i != agents.end()) {
        // double habitatRisk = riskScoreHabitat(1-quadtree->getValue((*i)->x, (*i)->y)); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
        // double densityRisk = riskScoreDensity(density.getValue((*i)->x, (*i)->y)*1000000);
        // double ageRisk = riskScoreAge((*i)->age);
        // double risk = habitatRisk + densityRisk + ageRisk;
        (*i)->resistance = quadtree->getValue(Point((*i)->x, (*i)->y));
        if(std::isnan((*i)->resistance)){
            killAgent(i);
        } else {
            (*i)->habitatRisk = 10 * riskScore(1 - (*i)->resistance, params.deathHabSlope, params.deathHabThresh); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
            (*i)->densityRisk = 10 * riskScore(density.getValue((*i)->x, (*i)->y) * 1000000, params.deathDensitySlope * -1, params.deathDensityThresh);
            // (*i)->densityRisk = 10 * riskScore(density.getValue((*i)->x, (*i)->y), params.deathDensitySlope * -1, params.deathDensityThresh);
            if((*i)->age < params.mateAge){
                (*i)->ageRisk = params.deathAgeJuvMortality * (1 - riskScore((*i)->age, -1 * params.deathAgeJuvSlope, params.deathAgeJuvThresh));
            } else {
                (*i)->ageRisk = params.deathAgeAdultMortality * riskScore((*i)->age, -1 * params.deathAgeAdultSlope, params.deathAgeAdultThresh);
            }
            // (*i)->ageRisk = riskScoreAge((*i)->age, );
            // (*i)->deathRisk = (*i)->habitatRisk + (*i)->densityRisk + (*i)->ageRisk;
            (*i)->deathRisk = params.deathBaseMortality + (*i)->habitatRisk + (*i)->densityRisk + (*i)->ageRisk;

            std::poisson_distribution<int> pois((*i)->deathRisk);
            std::uniform_real_distribution<double> unif(0,1); 
            double dieProb = (double)pois(randomGenerator)/(double)100;
            double dieRoll = unif(randomGenerator);
            if(dieRoll <= dieProb){
                killAgent(i);
            } 
        }
        i++;
    }
}

// basically the same as 'dieRiskScores()' but uses a quadrat count instead of density
void Simulation::dieRiskScoresQuadrat(){
    //Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 128, 128, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // std::cout << "point density\n";
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
    Raster density = AgentUtilities::quadratCount(agents, 1000, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 1000, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";

    std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
    while (i != agents.end()) {
        if((*i)->age > params.deathMaxAge){
            killAgent(i);
        } else {
            // double habitatRisk = riskScoreHabitat(1-quadtree->getValue((*i)->x, (*i)->y)); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
            // double densityRisk = riskScoreDensity(density.getValue((*i)->x, (*i)->y)*1000000);
            // double ageRisk = riskScoreAge((*i)->age);
            // double risk = habitatRisk + densityRisk + ageRisk;
            (*i)->resistance = quadtree->getValue(Point((*i)->x, (*i)->y));
            if(std::isnan((*i)->resistance)){
                killAgent(i);
            } else {
                (*i)->habitatRisk = params.deathHabMortality * riskScore(1 - (*i)->resistance, params.deathHabSlope, params.deathHabThresh); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
                // (*i)->densityRisk = 10 * riskScore(density.getValue((*i)->x, (*i)->y) * 1000000, params.deathDensitySlope * -1, params.deathDensityThresh);
                (*i)->densityRisk = params.deathDensityMortality * riskScore(density.getValue((*i)->x, (*i)->y), params.deathDensitySlope * -1, params.deathDensityThresh);
                if((*i)->age < params.mateAge){
                    (*i)->ageRisk = params.deathAgeJuvMortality * (1 - riskScore((*i)->age, -1 * params.deathAgeJuvSlope, params.deathAgeJuvThresh));
                } else {
                    (*i)->ageRisk = params.deathAgeAdultMortality * riskScore((*i)->age, -1 * params.deathAgeAdultSlope, params.deathAgeAdultThresh);
                }
                // (*i)->ageRisk = riskScoreAge((*i)->age, );
                // (*i)->deathRisk = (*i)->habitatRisk + (*i)->densityRisk + (*i)->ageRisk;
                (*i)->deathRisk = params.deathBaseMortality + (*i)->habitatRisk + (*i)->densityRisk + (*i)->ageRisk;

                std::poisson_distribution<int> pois((*i)->deathRisk);
                std::uniform_real_distribution<double> unif(0,1); 
                double dieProb = (double)pois(randomGenerator)/(double)100;
                double dieRoll = unif(randomGenerator);
                if(dieRoll <= dieProb){
                    killAgent(i);
                } 
            }
        }
        i++;
    }
}

// same as dieRiskScores() except doesn't include the habitat risk score
void Simulation::dieRiskScoresCulverts(){
    //Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 128, 128, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    // Raster density = AgentUtilities::pointDensity(agents, params.deathDensityBandwidth, 1000, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    Raster quadrat = AgentUtilities::quadratCount(agents, params.deathDensityBandwidth, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
    while (i != agents.end()) {
        // double habitatRisk = riskScoreHabitat(1-quadtree->getValue((*i)->x, (*i)->y)); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
        // double densityRisk = riskScoreDensity(density.getValue((*i)->x, (*i)->y)*1000000);
        // double ageRisk = riskScoreAge((*i)->age);
        // double risk = habitatRisk + densityRisk + ageRisk;
        // (*i)->resistance = quadtree->getValue(Point((*i)->x, (*i)->y));
        if(std::isnan(quadtree->getValue(**i))){
            killAgent(i);
        } else {
            //(*i)->habitatRisk = 10 * riskScore(1 - (*i)->resistance, params.deathHabSlope, params.deathHabThresh); //subtract from one because this function uses habitat suitability (1=good) while our quadtree has resistance values (1=bad)
            // (*i)->densityRisk = 10 * riskScore(density.getValue((*i)->x, (*i)->y) * 1000000, params.deathDensitySlope * -1, params.deathDensityThresh);
            (*i)->densityRisk = 10 * riskScore(quadrat.getValue((*i)->x, (*i)->y), params.deathDensitySlope * -1, params.deathDensityThresh);
            if((*i)->age < params.mateAge){
                (*i)->ageRisk = params.deathAgeJuvMortality * (1 - riskScore((*i)->age, -1 * params.deathAgeJuvSlope, params.deathAgeJuvThresh));
            } else {
                (*i)->ageRisk = params.deathAgeAdultMortality * riskScore((*i)->age, -1 * params.deathAgeAdultSlope, params.deathAgeAdultThresh);
            }
            // (*i)->ageRisk = riskScoreAge((*i)->age, );
            // (*i)->deathRisk = (*i)->habitatRisk + (*i)->densityRisk + (*i)->ageRisk;
            (*i)->deathRisk = params.deathBaseMortality + (*i)->densityRisk + (*i)->ageRisk;

            std::poisson_distribution<int> pois((*i)->deathRisk);
            std::uniform_real_distribution<double> unif(0,1); 
            double dieProb = (double)pois(randomGenerator)/(double)100;
            double dieRoll = unif(randomGenerator);
            if(dieRoll <= dieProb){
                killAgent(i);
            } 
        }
        i++;
    }
}

void Simulation::dieSimple(){
    //Raster density = AgentUtilities::pointDensity(agents, 2000, 128, 128, quadtree->root->xMin, quadtree->root->xMax, quadtree->root->yMin, quadtree->root->yMax);
    std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
    while (i != agents.end()) {
        (*i)->resistance = quadtree->getValue(Point((*i)->x, (*i)->y));
        if(std::isnan((*i)->resistance)){
            killAgent(i);
        } else {
            std::uniform_real_distribution<double> unif(0,1); 
            double dieRoll = unif(randomGenerator);
            if(dieRoll <= .16){
                killAgent(i);
            } 
        }
        i++;
    }
}

void Simulation::doMoveMethod(){
    if(params.movtMethod == "moveLCP"){
        moveLCP();
    } else if(params.movtMethod == "moveLCP2"){
        moveLCP2();
    } else if(params.movtMethod == "moveLCPConstrained"){
        moveLCPConstrained();
    } else if(params.movtMethod == "moveSimple"){
        moveSimple();
    } else if(params.movtMethod == "moveCulverts"){
        moveCulverts();
    } else if(params.movtMethod != "none"){
        throw std::runtime_error("Invalid method given for 'movtMethod'. Allowable values are 'moveLCP', 'moveLCPConstrained', or 'moveSimple'.");
    }
}

void Simulation::doReproduceMethod(){
    if(params.mateMethod == "reproducePointGrid"){
        reproducePointGrid();
    } else if(params.mateMethod == "reproducePointGridNoLCP"){
        reproducePointGridNoLCP();
    } else if(params.mateMethod == "reproduceSimple"){
        reproduceSimple();
    } else if(params.mateMethod == "reproducePointGridCulverts"){
        reproducePointGridCulverts();
    } else if(params.mateMethod != "none") {
        throw std::runtime_error("Invalid method given for 'mateMethod'. Allowable values are 'reproducePointGrid', 'reproducePointGridNoLCP', or 'reproduceSimple'.");
    }
}

void Simulation::doDieMethod(){
    if(params.deathMethod == "dieRiskScores"){
        dieRiskScores();
    } else if (params.deathMethod == "dieRiskScoresQuadrat"){
        dieRiskScoresQuadrat();
    } else if (params.deathMethod == "dieSimple"){
        dieSimple();
    } else if (params.deathMethod == "dieRiskScoresCulverts"){
        dieRiskScoresCulverts();
    } else if(params.deathMethod != "none"){
        throw std::runtime_error("Invalid method given for 'deathMethod'. Allowable values are 'dieRiskScores' or 'dieSimple'.");
    }
}
    



// void Simulation::endIteration(){
//     thisIter.nAgents = agents.size() - thisIter.nDied;
//     iterInfo[counter] = thisIter;
    
//     //counter++;
// }

void Simulation::addIterationInfo(){
    thisIter.nAgents = agents.size() - thisIter.nDied;
    iterInfo[counter] = thisIter;   
}

void Simulation::advanceCounter(){
    counter++;
}

void Simulation::clearDeadAgents(){
    std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
    while (i != agents.end()) {
        if((*i)->isAlive == 0){
            (*i)->dadPtr = nullptr; // this "releases" the shared pointer to the parent - this way an agent will be removed from memory once all it's children are dead
            (*i)->momPtr = nullptr; // same as above
            agents.erase(i++);
        } else {
            i++;
        }
    }
}
// void Simulation::killTort(int index){
//     agents.at(index)->isAlive = false;
//     agents.at(index)->yearDied = counter;
//     deadAgents.push_back(agents.at(index)); //add the agent to 'deadAgents'
//     agents.erase(agents.begin() + index); //remove agent from 'agents'
// }

//takes two vectors of alleles (from the parents) and makes a new vector of alleles by randomly selecting between the parent's alleles for each allele







// void Simulation::writeLiveAgents(std::string filePath){
//     std::ofstream csv(filePath, std::ofstream::trunc);
//     //csv.open(filePath);
//     csv << "id, x, y, age, sex\n";
//     for(int i = 0; i < agents.size(); ++i){
//         csv << agents[i]->id << "," << agents[i]->point.x << "," << agents[i]->point.y << "," << agents[i]->age << "," << agents[i]->isMale << "\n";
//     }
//     csv.close();
// }

//void Simulation::runSim(int outputFreq, bool writeDeadAgentsBool){
void Simulation::runSim(bool isRestart){

    bool useSaveRects = params.simSaveRectanglesFilePath != "";
    // bool doBackup = params.simSaveBackupFrequency > 0;
    //writeLiveAgents();
    if(!isRestart){
        if(useSaveRects && params.simSaveAllFrequency > 0){
            writeAgents(params.simSaveAllPath, false);
        }
        writeAgents(params.simSavePath, useSaveRects);
        //endIteration(); //since we just wrote out the files, "end" the iteration - this'll increment the counter from 0 to 1.
        addIterationInfo();
        advanceCounter();
    }
    //writeAgents(agents, params.simOutputFolder + std::string("agents"));//write the initial tortoises to a file
    
    auto start = std::chrono::system_clock::now();
    runInfo.startTime = date::format("%F %T", start);
    // for(int i = 0; i < params.simNIterations; ++i){
    while(counter <= params.simNIterations){
        if(params.simVerbose) std::cout << "ITERATION " << counter << "\n";
        if(params.simVerbose) std::cout << "# of live agents: " << agents.size() << "\n";
        beginIteration();
        if(params.simVerbose) std::cout << "move" << "\n";
        // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
    
        // move(false);
        // moveLCP();
        // moveSimple();
        // moveSimple2();
        // moveSimple3();
        // moveSimple4();
        // moveSimple5();
        // moveLCP2();
        // moveLCPTest();
        // moveLCPConstrained();
        // moveLCP();
        doMoveMethod();
        if(params.simVerbose) std::cout << "reproduce" << "\n";
        // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
        // reproducePointGrid();
        // reproducePointGridNoLcp();
        // reproduceSimple();
        doReproduceMethod();
        if(params.simVerbose) std::cout << "age" << "\n";
        // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
        age();
        if(params.simVerbose) std::cout << "die" << "\n";
        // std::cout << date::format("%F %T", std::chrono::system_clock::now()) << "\n";
        // dieRiskScores();
        //dieSimple();
        doDieMethod();
        
        if((params.simSaveFrequency > 0) && (counter % params.simSaveFrequency == 0)){
            if(params.simVerbose) std::cout << "write agents in rectangles" << "\n";
            //writeLiveAgents();
            writeAgents(params.simSavePath, useSaveRects);
        }

        if(useSaveRects) {
            if(params.simSaveAllFrequency > 0 && counter % params.simSaveAllFrequency == 0){ 
                if(params.simVerbose) std::cout << "write all agents" << "\n";
                writeAgents(params.simSaveAllPath, false);
            } 
            // else if(i == params.simNIterations - 1){ // always write out all the agents on the last iteration
            //     writeAgents(params.simSaveAllPath, false);
            // }
        }
        
        addIterationInfo();
        if(params.simSaveBackupFrequency > 0 && (counter % params.simSaveBackupFrequency == 0)){
            for (auto& path: std::filesystem::directory_iterator(params.simSaveBackupPath)) {
                std::filesystem::remove_all(path);
            }
            if(params.simVerbose) std::cout << "write backup info" << "\n";
            writeAgents(params.simSaveBackupPath, false);
            writeIterInfo(params.simSaveBackupPath + std::string("/iterInfo.csv"));
        }


        if(params.simVerbose) std::cout << "advance counter\n";
        // endIteration();
        advanceCounter();
        if(params.simVerbose) std::cout << "clear dead agents\n";
        clearDeadAgents();
    }
    // if(writeDeadAgentsBool){
    //     std::cout << "write dead agents\n";
    //     writeDeadAgents();
    // }  
    //std::string firstPartDead(outputFolder + std::string("deadAgents"));
    
    
    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start); 
    std::cout << "TIME: " << duration.count() << " seconds\n"; 
    
    runInfo.endTime = date::format("%F %T", stop);
    runInfo.totalTimeSec = duration.count();
    writeRunInfo(params.simSaveInfoPath + std::string("/runInfo.csv"));
    
    writeIterInfo(params.simSaveInfoPath + std::string("/iterInfo.csv"));
    //sim.writeAllMovtHist("C:\\Users\\derek\\Documents\\Programming\\cpp\\abm\\output\\movtHist.csv");
    if(params.movtSaveAllMovt){
        writeAllMovtHist(params.simSaveInfoPath + std::string("/allMovt.csv"));
    }
    //sim.writeAllMovtHist(outputFolder + "movtHist.csv");
    //std::cout << "\n";
}



// void Simulation::writeAgents(std::list<std::shared_ptr<Agent>> agentVec, std::string filePath){
//     std::ofstream csv(filePath, std::ofstream::trunc);
//     //csv.open(filePath);
//     csv << "iter,id, x, y, age, isMale, isAlive, yearDied, momID, momX, momY, dadID, dadX, dadY";

//     for(int i = 0; i < params.initNLoci; ++i){
//         //for(int j = 0; j < params.initNPerLocus; ++j){
//         for(int j = 0; j < 2; ++j){
//             csv << ", allele" << i+1 << BasicUtilities::intToChar(j+1);
//         }
//     }
//     csv << "\n";
//     //for(size_t i = 0; i < agentVec.size(); ++i){
//     int i{0};
//     //for(auto &iAgent : agentVec){
//     for(auto iAgent : agentVec){
//         //the first set of tortoises won't have parents, so we'll assign nan for the mom and dad ID's
//         int momId=-1, dadId=-1;
//         double momX=-1, momY=-1, dadX=-1, dadY=-1;
//         // auto momPtr = iAgent->momPtr.lock();
//         auto momPtr = iAgent->momPtr;
//         if(momPtr){
//             momId = momPtr->id;
//             momX = momPtr->x;
//             momY = momPtr->y;
//         }
//         // auto dadPtr = iAgent->dadPtr.lock();
//         auto dadPtr = iAgent->dadPtr;
//         if(dadPtr){
//             dadId = dadPtr->id;
//             dadX = dadPtr->x;
//             dadY = dadPtr->y;
//         }
//         csv << counter << "," << iAgent->id << "," << iAgent->x << "," << iAgent->y << "," << iAgent->age << "," << iAgent->isMale << "," << iAgent->isAlive << "," << iAgent->yearDied << "," << momId << "," << momX << "," << momY << "," << dadId << "," << dadX << "," << dadY;
//         // for(int i = 0; i < params.initNLoci; ++i){
//         //     csv << "," << iAgent->loci.at(i).a1 << "," << iAgent->loci.at(i).a2;
//         // }
//         for(int i = 0; i < params.initNLoci; ++i){
//             //for(int j = 0; j < params.initNPerLocus; ++j){
//             for(int j = 0; j < 2; ++j){
//                 csv << "," << iAgent->loci.at(i).at(j);
//             }
//         }

//         csv << "\n";
        
//         i++;
//     }
//     csv.close();
// }



// void Simulation::writeLiveAgents(){
//     std::string firstPartLive(params.simOutputFolder + std::string("agents"));
//     std::string num(std::to_string(counter));
//     std::string numPadded = std::string(5 - num.length(), '0') + num;
//     std::string csvPart(".csv");
//     writeAgents(agents, firstPartLive + numPadded + csvPart);
// }

// void Simulation::writeAgents(bool useSaveRects){
//     std::string firstPartLive(params.simOutputFolder + std::string("agents"));
//     std::string num(std::to_string(counter));
//     std::string numPadded = std::string(5 - num.length(), '0') + num;
//     std::string csvPart(".csv");
//     writeAgentsKen(agents, firstPartLive + numPadded + csvPart);
// }

// void Simulation::writeDeadAgents(){
//     std::string firstPartLive(params.simOutputFolder + std::string("deadAgents"));
//     std::string num(std::to_string(counter));
//     std::string numPadded = std::string(5 - num.length(), '0') + num;
//     std::string csvPart(".csv");
//     writeAgents(deadAgents, firstPartLive + numPadded + csvPart);
// }

void Simulation::writeAllMovtHist(std::string filePath){
    std::ofstream csv(filePath, std::ofstream::trunc);
    //csv.open(filePath);
    csv << "iter,step,id,lcp_flag,x,y\n";
    for(size_t i = 0; i < movtHist.size(); ++i){
        csv << std::get<0>(movtHist[i]) << "," << std::get<1>(movtHist[i]) << "," << std::get<2>(movtHist[i]) << "," << std::get<3>(movtHist[i]) << "," << std::get<4>(movtHist[i]).x << "," << std::get<4>(movtHist[i]).y << "\n";
    }
    csv.close();
}

void Simulation::writeParams(std::string filePath, SimulationParameters pars){
    std::ofstream csv(filePath, std::ofstream::trunc);
    csv << "param,val\n";
    csv << "simNIterations," << pars.simNIterations << "\n";
    csv << "simPointGridCellSize," << pars.simPointGridCellSize << "\n";
    csv << "simQuadtreeFilePath," << pars.simQuadtreeFilePath << "\n";
    csv << "simParentFolderPath," << pars.simParentFolderPath << "\n";
    csv << "simFolder," << pars.simFolder << "\n";
    csv << "simRandomSeed," << pars.simRandomSeed << "\n";
    csv << "simSaveFrequency," << pars.simSaveFrequency << "\n";
    csv << "simSaveRectanglesFilePath," << pars.simSaveRectanglesFilePath << "\n";
    csv << "simSaveAllFrequency," << pars.simSaveAllFrequency << "\n";
    // csv << "simSaveAllFolder," << pars.simSaveAllFolder << "\n";
    csv << "simSaveBackupFrequency," << pars.simSaveBackupFrequency << "\n";
    csv << "simVerbose," << pars.simVerbose << "\n";
    csv << "simCulvertRectanglesFilePath" << pars.simCulvertRectanglesFilePath << "\n";
    // csv << "simSaveBackupFolder," << pars.simSaveBackupFolder << "\n";
    csv << "initAgentFilePath," << pars.initAgentFilePath << "\n";
    // csv << "initNAgents," << pars.initNAgents << "\n";
    // csv << "initAlleleMin," << pars.initAlleleMin << "\n";
    // csv << "initAlleleMax," << pars.initAlleleMax << "\n";
    // csv << "initNLoci," << pars.initNLoci << "\n";
    // csv << "initNPerLocus," << pars.initNPerLocus << "\n";
    csv << "movtMethod," << pars.movtMethod << "\n";
    csv << "movtDistanceProbsFilePath," << pars.movtDistanceProbs.filePath << "\n";
    // csv << "movtNPoints," << pars.movtNPoints << "\n";
    // csv << "movtStepSize," << pars.movtStepSize << "\n";
    // csv << "movtMaxTotalDist," << pars.movtMaxTotalDist << "\n";
    // csv << "movtMaxTotalDistSubstep," << pars.movtMaxTotalDistSubstep << "\n";
    // csv << "movtAttractDist," << pars.movtAttractDist << "\n";
    // csv << "movtResistWeight1," << pars.movtResistWeight1 << "\n";
    // csv << "movtAttractWeight1," << pars.movtAttractWeight1 << "\n";
    // csv << "movtDirectionWeight1," << pars.movtDirectionWeight1 << "\n";
    // csv << "movtResistWeight2," << pars.movtResistWeight2 << "\n";
    // csv << "movtAttractWeight2," << pars.movtAttractWeight2 << "\n";
    // csv << "movtDirectionWeight2," << pars.movtDirectionWeight2 << "\n";
    // csv << "movtBarrierThreshold," << pars.movtBarrierThreshold << "\n";
    csv << "movtSaveAllMovt," << pars.movtSaveAllMovt << "\n";
    csv << "movtSaveThresholdDist," << pars.movtSaveThresholdDist << "\n";
    csv << "movtNPerCulvertPerYear," << pars.movtNPerCulvertPerYear << "\n"; 
    csv << "mateMethod," << pars.mateMethod << "\n";
    csv << "mateAge," << pars.mateAge << "\n";
    csv << "mateNumBabiesPois," << pars.mateNumBabiesPois << "\n";
    csv << "mateMaxDist," << pars.mateMaxDist << "\n";
    csv << "mateMutateProb," << pars.mateMutateProb << "\n";
    // csv << "mateNumEggsProbsFilePath," << pars.mateNumEggsProbs.filePath << "\n";
    csv << "deathMethod," << pars.deathMethod << "\n";
    csv << "deathMaxAge," << pars.deathMaxAge << "\n";
    // csv << "deathDist," << pars.deathDist << "\n";
    // csv << "deathAgeScoresFilePath," << pars.deathAgeScores.filePath << "\n";
    // csv << "deathCarryingCapacityFilePath," << pars.deathCarryingCapacity.filePath << "\n";
    
    csv << "deathBaseMortality," << pars.deathBaseMortality << "\n"; //base mortality rate. Between 0 and 100
    
    csv << "deathAgeAdultMortality," << pars.deathAgeAdultMortality << "\n";
    csv << "deathAgeAdultSlope," << pars.deathAgeAdultSlope << "\n";
    csv << "deathAgeAdultThresh," << pars.deathAgeAdultThresh << "\n";
    
    csv << "deathAgeJuvMortality," << pars.deathAgeJuvMortality << "\n";
    csv << "deathAgeJuvSlope," << pars.deathAgeJuvSlope << "\n";
    csv << "deathAgeJuvThresh," << pars.deathAgeJuvThresh << "\n";

    csv << "deathDensityBandwidth," << pars.deathDensityBandwidth << "\n";
    csv << "deathDensityMortality," << pars.deathDensityMortality << "\n";
    csv << "deathDensitySlope," << pars.deathDensitySlope << "\n";
    csv << "deathDensityThresh," << pars.deathDensityThresh << "\n";

    csv << "deathHabMortality," << pars.deathHabMortality << "\n";
    csv << "deathHabSlope," << pars.deathHabSlope << "\n";
    csv << "deathHabThresh," << pars.deathHabThresh << "\n";
    
    csv.close();
}
// void Simulation::initializePointGrid(){



SimulationParameters Simulation::readParams(std::string paramsFilePath){
    //https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    std::fstream fin;
    fin.open(paramsFilePath, std::fstream::in);

    if(!fin.is_open()) throw std::runtime_error("Could not open file");

    std::string line, colname;
    if(fin.good()){
        std::getline(fin, line);
        std::istringstream ss(line);

        int colCounter{0};
        //check column names, make sure they're valid
        while(std::getline(ss,colname, ',')){
            //std::cout << colname << "\n";
            std::string oldColname = colname; //for debugging only
            //colname = BasicUtilities::cleanString(colname);
            BasicUtilities::cleanString(colname);
            if(colCounter == 0){
                if((colname != "\"param\"") && (colname != "param") && (colname != "'param'")){
                    throw std::runtime_error("Error reading CSV (" + paramsFilePath + "): First column must be called 'param' (value read: " + colname + ")");
                }                
            } else if (colCounter == 1){
                if((colname != "\"val\"") && (colname != "val") && (colname != "'val'")){
                    throw std::runtime_error("Error reading CSV(" + paramsFilePath + "): Second column must be called 'val' (value read: " + colname + ")");
                }                    
            } else {
                std::cout << "Warning while reading CSV(" + paramsFilePath + "): Third column detected. Only first two columns will be read. All others will be ignored." + "\n";
                break;
            }
            colCounter++;
        }
    }

    //create a vector of vectors for reading in the data
    //std::vector<std::vector<double>> rows = std::vector<std::vector<double>>();
    //std::cout << "first while" << "\n";
    SimulationParameters pars;
    while(std::getline(fin, line)){
        //std::cout << rowIdx << "\n";
        std::string param;
        std::string val;
        std::string string;
        std::stringstream ss(line);
        int colIdx{0};
        while(std::getline(ss,string, ',')){
            //string = BasicUtilities::cleanString(string);
            BasicUtilities::cleanString(string);
            if(colIdx == 0){
                param = string;
            } else if (colIdx == 1){   
                val = string;
            } else {
                throw std::runtime_error("Error reading CSV (" + paramsFilePath + "): third column detected");
            }
            //tempRow.at(colIdx) = std::stod(value);
            colIdx++;
        }

//     int simNIterations{0}; // number of iterations to run the simulation
//     double simPointGridCellSize{-1}; // the cell size of the pointGrid data structure
//     std::string simQuadtreeFilePath{""}; // file path to the quadtree
//     std::string simOutputParentPath{""}; // the folder containing the results will be created within this folder
//     std::string simOutputFolder{""}; // folder where files will be saved - will be created within 'simOuputParentPath'
//     int simRandomSeed{-1}; //random seed to use
//     int simSaveFrequency{1}; // the frequency to save outputs (i.e. 1 means every iteration, 10 means every 10 iterations, etc.)
//     std::string simSaveRectanglesFilePath{""}; // optional - path to a CSV that specifies rectangles - only agents that fall in these rectangles will be saved. CSV must have 5 columns: id, xmin, xmax, ymin, and ymax
//     int simSaveAllFrequency{-1}; // only relevant if 'simSaveRectanglesPath' is specified. This is the frequency at which to save all the agents. For example, you could output the "rectangle" output every year but then also output all agents every 100 years
//     std::string simSaveAllFolder{""}; // folder in which to save all agents (only relevant if simSaveAllFrequency > 0.) - doesn't need to specified. Folder will be automatically created if not provided. It will be created within 'simOuputFolder'
//     int simSaveBackupFrequency{-1}; // optional - if specified, all agents will be outputted at the given frequency. However, only the most recent CSV will be kept - the old one will be deleted when a new one is written. This is for backup purposes - in case the simulation is stopped unexpectedly, it could be restarted using these agents as the starting agents
//     std::string simSaveBackupFolder{""}; // folder in which to save the backup agents. Only relevant if simSaveBackupFrequency > 0. Will be created within 'simOutputFolder'
    
        if(val.length() > 0){
            if(param == "simNIterations" ) pars.simNIterations = std::stoi(val);
            if(param == "simPointGridCellSize") pars.simPointGridCellSize = std::stod(val);
            if(param == "simQuadtreeFilePath") pars.simQuadtreeFilePath = val;
            if(param == "simParentFolderPath") pars.simParentFolderPath = val;
            if(param == "simFolder") pars.simFolder = val;
            if(param == "simRandomSeed") pars.simRandomSeed = std::stoi(val);
            if(param == "simSaveFrequency") pars.simSaveFrequency = std::stoi(val);
            if(param == "simSaveRectanglesFilePath") pars.simSaveRectanglesFilePath = val;
            if(param == "simSaveAllFrequency") pars.simSaveAllFrequency = std::stoi(val);
            // if(param == "simSaveAllFolder") pars.simSaveAllFolder = val;
            if(param == "simSaveBackupFrequency") pars.simSaveBackupFrequency = std::stoi(val);
            if(param == "simVerbose") pars.simVerbose = std::stoi(val);
            if(param == "simCulvertRectanglesFilePath") pars.simCulvertRectanglesFilePath = val;
            // if(param == "simSaveBackupFolder") pars.simSaveBackupFolder = val;
            if(param == "initAgentFilePath") pars.initAgentFilePath = val;
            // if(param == "initNAgents") pars.initNAgents = std::stoi(val);
            // if(param == "initAlleleMin") pars.initAlleleMin = std::stoi(val);
            // if(param == "initAlleleMax") pars.initAlleleMax = std::stoi(val);
            // if(param == "initNLoci") pars.initNLoci = std::stoi(val);
            //if(param == "initNPerLocus") pars.initNPerLocus = std::stoi(val);
            if(param == "movtMethod") pars.movtMethod = val;
            if(param == "movtDistanceProbsFilePath") pars.movtDistanceProbsFilePath = val;
            // if(param == "movtNPoints") pars.movtNPoints = std::stoi(val);
            // if(param == "movtStepSize") pars.movtStepSize = std::stod(val);
            // if(param == "movtMaxTotalDist") pars.movtMaxTotalDist = std::stod(val);
            // if(param == "movtMaxTotalDistSubstep") pars.movtMaxTotalDistSubstep = std::stod(val);
            // if(param == "movtAttractDist") pars.movtAttractDist = std::stod(val);
            // if(param == "movtResistWeight1") pars.movtResistWeight1 = std::stod(val);
            // if(param == "movtAttractWeight1") pars.movtAttractWeight1 = std::stod(val);
            // if(param == "movtDirectionWeight1") pars.movtDirectionWeight1 = std::stod(val);
            // if(param == "movtResistWeight2") pars.movtResistWeight2 = std::stod(val);
            // if(param == "movtAttractWeight2") pars.movtAttractWeight2 = std::stod(val);
            // if(param == "movtDirectionWeight2") pars.movtDirectionWeight2 = std::stod(val);
            // if(param == "movtBarrierThreshold") pars.movtBarrierThreshold = std::stod(val);
            if(param == "movtSaveAllMovt") pars.movtSaveAllMovt = std::stoi(val);
            if(param == "movtSaveThresholdDist") pars.movtSaveThresholdDist = std::stod(val);
            if(param == "movtNPerCulvertPerYear") pars.movtNPerCulvertPerYear = std::stoi(val);

            if(param == "mateMethod") pars.mateMethod = val;
            if(param == "mateAge") pars.mateAge = std::stoi(val);
            if(param == "mateNumBabiesPois") pars.mateNumBabiesPois = std::stoi(val);
            if(param == "mateMaxDist") pars.mateMaxDist = std::stod(val);
            if(param == "mateMutateProb") pars.mateMutateProb = std::stod(val);
            // if(param == "mateNumEggsProbsFilePath") pars.mateNumEggsProbsFilePath = val;
            if(param == "deathMethod") pars.deathMethod = val;
            if(param == "deathMaxAge") pars.deathMaxAge = std::stoi(val);
            // if(param == "deathDist") pars.deathDist = std::stoi(val);
            // if(param == "deathAgeScoresFilePath") pars.deathAgeScoresFilePath = val;
            // if(param == "deathCarryingCapacityFilePath") pars.deathCarryingCapacityFilePath = val;

            if(param == "deathBaseMortality") pars.deathBaseMortality = std::stod(val); //base mortality rate. Between 0 and 100
            
            if(param == "deathAgeAdultMortality") pars.deathAgeAdultMortality = std::stod(val);
            if(param == "deathAgeAdultSlope") pars.deathAgeAdultSlope = std::stod(val);
            if(param == "deathAgeAdultThresh") pars.deathAgeAdultThresh = std::stod(val);
            
            if(param == "deathAgeJuvMortality") pars.deathAgeJuvMortality = std::stod(val);
            if(param == "deathAgeJuvSlope") pars.deathAgeJuvSlope = std::stod(val);
            if(param == "deathAgeJuvThresh") pars.deathAgeJuvThresh = std::stod(val);

            if(param == "deathDensityBandwidth") pars.deathDensityBandwidth = std::stod(val);
            if(param == "deathDensityMortality") pars.deathDensityMortality = std::stod(val);
            if(param == "deathDensitySlope") pars.deathDensitySlope = std::stod(val);
            if(param == "deathDensityThresh") pars.deathDensityThresh = std::stod(val);
            
            if(param == "deathHabMortality") pars.deathHabMortality = std::stod(val);
            if(param == "deathHabSlope") pars.deathHabSlope = std::stod(val);
            if(param == "deathHabThresh") pars.deathHabThresh = std::stod(val);
           
        }

    }
    fin.close();
    return pars;
}

void Simulation::writeRunInfo(std::string filePath){
    std::ofstream csv(filePath, std::ofstream::trunc);
    csv << "variable,value\n";
    csv << "startTime," << runInfo.startTime << "\n";
    csv << "endTime," << runInfo.endTime << "\n";
    csv << "totalTimeSec," << runInfo.totalTimeSec << "\n";
    csv.close();
}

void Simulation::writeIterInfo(std::string filePath){
    std::ofstream csv(filePath, std::ofstream::trunc);
    csv << "iter,nAgents,nBorn,nDied\n";
    for(size_t i = 0; i < iterInfo.size(); ++i){
        csv << iterInfo[i].iteration << "," << iterInfo[i].nAgents << "," << iterInfo[i].nBorn << "," << iterInfo[i].nDied << "\n";
    }
    csv.close();
}
void Simulation::readIterInfo(std::string filePath){
    std::cout << "Reading file: " << filePath << "\n";
    auto start = std::chrono::system_clock::now();
    
    std::fstream fin;
    fin.open(filePath, std::fstream::in);

    if(!fin.is_open()) throw std::runtime_error("Could not open file (" + filePath + ")");

    std::string line, colName; //initialize variables that will be used to store input
    std::vector<std::string> colNames{"iter", "nAgents", "nBorn", "nDied"}; //these are the required column names
    std::map<int, std::string> colMap; //map the index to the column name
    //std::vector<IterationInfo> rects;
    if(fin.good()){
        std::getline(fin, line); //get the first row
        std::istringstream ss(line);

        
        std::vector<std::string> colNamesFound; //keeps track of which columns we've found
        //check column names, make sure they're valid
        int colNumber{0}; //keeps track of which column we're on
        
        std::cout << "checking format\n";
        while(std::getline(ss,colName, ',')){
            BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
            BasicUtilities::removeSubstrs(colName, "\"");
            BasicUtilities::removeSubstrs(colName, "\'");
            if(std::find(colNames.begin(), colNames.end(), colName) != colNames.end()){ //if this string is in the 'colNames' vector, add it to out 'colNamesFound' vector
                colNamesFound.push_back(colName);
            }
            //std::cout << "readAgents8\n";
            colMap.insert(std::pair<int, std::string>(colNumber,colName)); //add an entry to our map
            colNumber++;
        }
        //std::cout << "readAgents18\n";
        if(colNamesFound.size() != colNames.size()){
            std::string colsString = BasicUtilities::combineStrings(colNames);
            std::string colsFoundString = BasicUtilities::combineStrings(colNamesFound);
            throw std::runtime_error("Error in Simulation::readIterInfo() while reading " + filePath + ": not all required columns included. Required columns are: (" + colsString + "). Of these names, these were found: (" + colsFoundString + ").");
        }

        std::cout << "begin reading in data lines\n";
        //int i{0};
        while(std::getline(fin, line)){
            //In a CSV I edited in Excel there were a bunch of empty lines (just a bunch of commas), and that was breaking this function. So this part removes all commas and spaces and sees if there's anything left. If not, then this row is empty, and we'll skip it.
            std::string lineCopy = line;
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ','), lineCopy.end()); // remove all commas
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ' '), lineCopy.end()); // remove all spaces
            if(lineCopy.length() > 0){

                IterationInfo itInfo;
                std::string val;
                std::stringstream ss(line);
                int colIdx{0};
                
                while(std::getline(ss,val, ',')){
                    
                    BasicUtilities::cleanString(val);
                    
                    std::string column = colMap[colIdx];
                    if(val.length() > 0){ //skip this entry if there's no value;
                        std::smatch numMatch;
                        
                        if(column == "iter"){ itInfo.iteration = std::stoi(val); }
                        else if (column == "nAgents") { itInfo.nAgents = std::stoi(val); }
                        else if (column == "nBorn") { itInfo.nBorn = std::stoi(val); }
                        else if (column == "nDied") { itInfo.nDied = std::stoi(val); }
                    } else {
                        std::cout << "WARNING: Empty value found in column '" << column << "'\n";
                    }
                    colIdx++;
                }
                iterInfo[itInfo.iteration] = itInfo;   
            }
        }
    }
    fin.close();
    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
    std::cout << "Reading in file took: " << duration.count() << " ms \n"; 
    //return rects;
}

void Simulation::readAgentsKen(std::string filePath, bool loadDeadAgents){
    std::cout << "Reading file: " << filePath << "\n";
    auto start = std::chrono::system_clock::now();
    
    //https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    //std::cout << "readAgents1\n";
    //std::cout << filePath << "\n";

    //Now actually read in the data
    std::fstream fin;
    fin.open(filePath, std::fstream::in);
    //std::cout << "readAgents2\n";
    if(!fin.is_open()) throw std::runtime_error("Could not open file (" + filePath + ")");
    //std::cout << "readAgents3\n";
    std::string line, colName; //initialize variables that will be used to store input
    std::vector<std::string> colNames{"tid", "x", "y", "age", "sex"}; //these are the required column names
    std::map<int, std::string> colMap; //map the index to the column name
    std::map<int, std::vector<int>> lociNPerMap; //map the locus to how many different items it has
    std::map<int, std::pair<int,int>> lociIndicesMap;//makes a map for the allele columns - the pair contains the number of the allele and which column of the allele this column is (i.e. a,b, or c, but in integers)
    std::regex alleleRegex("^allele\\d+\\w"); //matches things like 'allele1a' and 'allele13b'
    //std::cout << "readAgents4\n";
    if(fin.good()){
        //std::cout << "readAgents5\n";
        std::getline(fin, line); //get the first row
        std::istringstream ss(line);

        
        std::vector<std::string> colNamesFound; //keeps track of which columns we've found
        //check column names, make sure they're valid
        int colNumber{0}; //keeps track of which column we're on
        //std::cout << "readAgents6\n";
        std::cout << "checking format\n";
        while(std::getline(ss,colName, ',')){
            //std::cout << "readAgents7\n";
            //std::cout << colName << "\n";
            ////std::cout << colname << "\n";
            //std::string oldColname = colname; //for debugging only
            //colName = BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
            BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
            BasicUtilities::removeSubstrs(colName, "\"");
            BasicUtilities::removeSubstrs(colName, "\'");
            if(std::find(colNames.begin(), colNames.end(), colName) != colNames.end()){ //if this string is in the 'colNames' vector, add it to out 'colNamesFound' vector
                colNamesFound.push_back(colName);
            }
            //std::cout << "readAgents8\n";
            colMap.insert(std::pair<int, std::string>(colNumber,colName)); //add an entry to our map
            //std::cout << "readAgents9\n";
            //check to see if this column has the word 'allele' in it
            if(std::regex_search(colName, alleleRegex)){
                //std::cout << "readAgents10\n";
                std::smatch numMatch;
                if(std::regex_search(colName, numMatch, std::regex("\\d+"))){ // find the number in the column name
                    //std::cout << "readAgents11\n";
                    if(numMatch.size() != 1) throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", either no number was found in the name or more than one number was found.");
                    int alleleNum = std::stoi(numMatch[0]); //get the number
                    if(alleleNum < 1){
                        throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": Error in column name '" + colName + "'. The number of the column must be 1 or higher.");
                    }
                    //std::cout << "alleleNum: " << alleleNum << "\n";
                    if(lociNPerMap.find(alleleNum) == lociNPerMap.end()){ //if we don't have an entry for this locus, create one and associate it with the number 1 (because so far we've found one column for this locus)
                        lociNPerMap.insert(std::pair<int, std::vector<int>>(alleleNum, std::vector<int>()));
                        //std::cout << "readAgents12\n";
                    } 
                    //std::cout << "readAgents13\n";
                    if(numMatch.suffix().length() > 1) throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", either no character was found after the number or else more than one character was found. ");
                    //std::cout << "readAgents14\n";
                    int suffixNum = BasicUtilities::charToInt(numMatch.suffix().str().at(0));

                    std::pair<int, int> indicesPair = std::pair<int,int>(alleleNum-1, suffixNum-1); //subtract one because the column numberings start with 1 but we're using 0-based indexing
                    lociIndicesMap.insert(std::pair<int, std::pair<int,int>>(colNumber, indicesPair));
                    //std::cout << "suffixNum: " << suffixNum << "\n";
                    //std::cout << "readAgents15\n";
                    lociNPerMap[alleleNum].push_back(suffixNum); // add the suffix (the letter) to the vector for this allele
                    //std::cout << "readAgents16\n";
                } else {
                    throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", no number was found in the column name for an 'allele' column.");    
                }
                //std::cout << "readAgents17\n";
            }
            colNumber++;
        }
        //std::cout << "readAgents18\n";
        if(colNamesFound.size() != colNames.size()){
            std::string colsString = BasicUtilities::combineStrings(colNames);
            std::string colsFoundString = BasicUtilities::combineStrings(colNamesFound);
            throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": not all required columns included. Required columns are: (" + colsString + "). Of these names, these were found: (" + colsFoundString + ").");
        }
        
        //this next part checks to make sure the allele* columns are correct 
        //std::cout << "readAgents19\n";
        int counter{0};
        std::vector<int> expectedNums = BasicUtilities::makeIntSequence(1,lociNPerMap.size());
        //std::cout << "readAgents20\n";
        //int nNumsPrev{0};
        for( auto const& x : lociNPerMap){
            //std::cout << "readAgents21\n";
            std::vector<int>::iterator it = std::find(expectedNums.begin(), expectedNums.end(), x.first);
            //std::cout << "readAgents22\n";
            if(it == expectedNums.end()){
                std::string colNamei = "allele" + std::to_string(x.first);
                throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". There are " + std::to_string(lociNPerMap.size()) + " distinct locus columns (i.e. allele1a and allele1b are only one locus), so it was expected to see columns allele1, allele2, ... , allele" + std::to_string(lociNPerMap.size()) + ". However, " + colNamei + " was found, which is not in the previously specified sequence");
            } else {
                //std::cout << "readAgents23\n";
                expectedNums.erase(std::remove(expectedNums.begin(), expectedNums.end(), x.first), expectedNums.end());
            }
            //std::cout << "readAgents24\n";
            //if(x.second.size() != nNumsPrev){
            if(x.second.size() != 2){
                //throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". A previous locus (a locus consists of n columns, for example, allele1a, allele1b,..., allele1n) had " + std::to_string(nNumsPrev) + " columns, but this locus (" + std::to_string(x.first) + ") has " + std::to_string(x.second.size()) + " columns. All loci must have the same number of columns, and they must be labeled by consecutive lowercase letters (i.e. a,b,c,...");
                throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". A locus (" + std::to_string(x.first) + ") was found that has " + std::to_string(x.second.size()) + " columns - however, all loci must have the 2 columns, and they must be labeled by lowercase 'a' and 'b' (for example: allele4a, allele4b)");
            }

            //std::cout << "readAgents26\n";

            std::vector<int> expectedNums2 = BasicUtilities::makeIntSequence(1,x.second.size()); 

            //std::cout << "readAgents27\n";
            for(size_t i = 0; i < x.second.size(); ++i){

                //std::cout << "readAgents28\n";
                std::vector<int>::iterator it2 = std::find(expectedNums2.begin(), expectedNums2.end(), x.second.at(i));

                //std::cout << "readAgents29\n";
                if(it2 == expectedNums2.end()){
                    std::string colNamei = "allele" + std::to_string(x.first);
                    throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". There are " + std::to_string(x.second.size()) + " columns for locus " + std::to_string(x.first) + " so it was expected that there be columns allele" + std::to_string(x.first) + "a through allele" +std::to_string(x.first) + BasicUtilities::intToChar(x.second.size()) + ". However, this column (allele" + std::to_string(x.first) + BasicUtilities::intToChar(x.second.at(i)) + ") is not in that range, or it is a duplicate column.");
                } else {

                    //std::cout << "readAgents30\n";
                    expectedNums2.erase(std::remove(expectedNums2.begin(), expectedNums2.end(), x.second.at(i)), expectedNums2.end());
                }

                //std::cout << "readAgents31\n";
            }
            counter++;

            //std::cout << "readAgents32\n";
        }
        
        //std::cout << "readAgents33\n";

        params.initNLoci = lociNPerMap.size();
        //params.initNPerLocus = nNumsPrev;

        //create a vector of vectors for reading in the data
        int maxId{0}; //we want to keep track of the max - we'll want to set nextID to be maxID +1
        //std::cout << "readAgents34\n";
        //int nAgents{0};

        std::cout << "begin reading in data lines\n";
        // int nDeadAgents{0};
        while(std::getline(fin, line)){
            //std::cout << "======newRow======\n";
            //std::cout << "readAgents35\n";

            //In a CSV I edited in Excel there were a bunch of empty lines (just a bunch of commas), and that was breaking this function. So this part removes all commas and spaces and sees if there's anything left. If not, then this row is empty, and we'll skip it.
            std::string lineCopy = line;
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ','), lineCopy.end()); // remove all commas
            lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ' '), lineCopy.end()); // remove all spaces
            if(lineCopy.length() > 0){
                //nAgents++;
                //std::string param;
                std::string val;
                std::stringstream ss(line);
                int colIdx{0};
                //std::shared_ptr<Agent> newAgent(new Agent());
                
                
                // std::shared_ptr<Agent> newAgent(std::make_shared<Agent>());
                // newAgent->isAlive = 1; 
                // newAgent->yearDied = -1;
                //newAgent->loci = std::vector<std::vector<int>>(params.initNLoci);

                int id{-1};
                int age{-1};
                int isAlive{1}; //we'll assume the agents are alive unless the data tells us otherwise
                int isMale{-1};
                double x{-1};
                double y{-1};
                auto loci = std::vector<std::vector<int>>(params.initNLoci); //+++++++++
                //int isAlive{1}
                //std::cout << "readAgents36\n";
                while(std::getline(ss,val, ',')){
                    //val = BasicUtilities::cleanString(val);
                    BasicUtilities::cleanString(val);
                    //std::cout << "val: " << val << "-----------------\n";
                    std::string column = colMap[colIdx];
                    //std::cout << "colIdx: " << colIdx << "-----------------\n";
                    //std::cout << "column: " << column << "-----------------\n";
                    //std::cout << "readAgents37\n";
                    if(val.length() > 0){ //skip this entry if there's no value;
                        std::smatch numMatch;
                        if(lociIndicesMap.find(colIdx) != lociIndicesMap.end()){ //check if this column index is one of the allele columns
                        //if(std::regex_search(column, numMatch, std::regex("\\d+"))){ // find the number in the column name
                            //if(std::regex_search(column, alleleRegex)){    
//                            if(numMatch.prefix().str() == "allele"){
                            //std::cout << "readAgents38\n";
                            
                            //std::cout << "readAgents39\n";      
                            //int alleleNum = std::stoi(numMatch[0])-1; //get the number of the allele - the lowest number allowed is 1, so subtract 1
                            //std::cout << "readAgents40\n";      
                            //int suffixNum = BasicUtilities::charToInt(numMatch.suffix().str().at(0))-1; //get the character after the letter and subtract 1 so that the lowest number is 0
                            //std::cout << "readAgents41\n";     
                            int alleleNum = lociIndicesMap[colIdx].first;
                            int suffixNum = lociIndicesMap[colIdx].second; 
                            // if(newAgent->loci.at(alleleNum).size() == 0){ //if the vector for this locus is empty, make a vector of the appropriate size
                            if(loci.at(alleleNum).size() == 0){ //++++++++ //if the vector for this locus is empty, make a vector of the appropriate size
                                //std::cout << "readAgents42\n";      
                                //newAgent->loci.at(alleleNum) = std::vector<int>(params.initNPerLocus);
                                //newAgent->loci.at(alleleNum) = std::vector<int>(2);
                                loci.at(alleleNum) = std::vector<int>(2); //+++++++++
                            }
                            //std::cout << "readAgents43\n";      
                            // newAgent->loci.at(alleleNum).at(suffixNum) = std::stoi(val); //add the allele value to the correct locus   
                            loci.at(alleleNum).at(suffixNum) = std::stoi(val); //+++++++++ //add the allele value to the correct locus                        
  //                          }
                        } else {
                            //std::cout << "readAgents44\n";      
                            //std::vector<std::string> colNames{"id", "x", "y", "age", "isMale"}; //these are the required column names
                            //std::cout << "readAgents45\n";      
                            if(column == "tid"){
                                // newAgent->id = std::stoi(val);
                                // if(newAgent->id > maxId) maxId = newAgent->id; //
                                id = std::stoi(val); //++++++++++
                                if(id > maxId) maxId = id; //+++++++++
                            }
                            //std::cout << "readAgents46\n";      
                            //else if (column == "x") { newAgent->x = std::stod(val); }
                            else if (column == "x") { x = std::stod(val); } //+++++++++
                            //std::cout << "readAgents47\n";
                            //else if (column == "y") { newAgent->y = std::stod(val); }
                            else if (column == "y") { y = std::stod(val); } //+++++++++
                            //std::cout << "readAgents48\n";
                            // else if (column == "age") { newAgent->age = std::stoi(val); }
                            else if (column == "age") { age = std::stoi(val); }
                            else if (column == "alive") { isAlive = std::stoi(val); }
                            //std::cout << "readAgents49\n";
                            else if (column == "sex") { 
                                BasicUtilities::removeSubstrs(val, "\"");
                                if(val == "M"){
                                    // newAgent->isMale = 1;
                                    isMale = 1; //+++++++++
                                } else {
                                    // newAgent->isMale = 0;
                                    isMale = 0; //+++++++++
                                }
                                //newAgent->isMale = std::stoi(val); 
                            }
                            //std::cout << "readAgents50\n";
                        }
                    } else {
                        std::cout << "WARNING: Empty value found in column '" << column << "'\n";
                    }
                    //std::cout << "readAgents51\n";
                    //tempRow.at(colIdx) = std::stod(value);
                    colIdx++;
                }
                // if(isAlive == 0) nDeadAgents++;
                //std::cout << "readAgents52\n";
                //agents.emplace_back(newAgent);
                //Agent(int _id, int _age, int _isMale, int _isAlive, double _x, double _y, std::shared_ptr<Agent> _dadPtr, std::shared_ptr<Agent> _momPtr, std::vector<std::vector<int> > _loci);
                if(isAlive == 1 || loadDeadAgents){
                    agents.emplace_back(std::make_shared<Agent>(id, age, isMale, isAlive, x, y, nullptr, nullptr, loci)); //+++++++++
                }
            }

        }
        //std::cout << "readAgents53\n";
        nextId = maxId + 1;
        // thisIter.nDied = nDeadAgents;
        // params.initNAgents = agents.size();
        // params.initAlleleMin = -1;
        // params.initAlleleMax = -1;
    }
    //std::cout << "readAgents54\n";
    fin.close();
    // clearDeadAgents();
    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
    std::cout << "Reading in file took: " << duration.count() << " ms \n"; 
}



// void Simulation::writeAgentsKen(std::list<std::shared_ptr<Agent>> agentVec, std::string filePath, bool useSaveRects){
void Simulation::writeAgents(std::string folder, bool useSaveRects){
    std::string firstPart(folder + std::string("/agents"));
    std::string num(std::to_string(counter));
    std::string numPadded = std::string(5 - num.length(), '0') + num;
    std::string csvPart(".csv");
    std::string filePath = firstPart + numPadded + csvPart;

    std::ofstream csv(filePath, std::ofstream::trunc);
    csv.precision(12);
    //csv.open(filePath);
    //csv << "tid, yr,  x, y, age, sex, alive, mom, momX, momY, dad, dadX, dadY";
    csv << "tid, sex, pop, age, adult, yr, dir, dist, alive, risk, mvt, zone, hab, mom, dad";
    for(int i = 0; i < params.initNLoci; ++i){
        //for(int j = 0; j < params.initNPerLocus; ++j){
        for(int j = 0; j < 2; ++j){
            csv << ", allele" << i+1 << BasicUtilities::intToChar(j+1);
        }
    }
    csv << ", x, y, momX, momY, dadX, dadY, ageRisk, densityRisk, habitatRisk, group\n";
    //for(size_t i = 0; i < agentVec.size(); ++i){
    std::vector<std::string> sex{"F", "M"};
    int i{0};
    // bool useSaveRects = params.simSaveRectanglesPath != "";
    //for(auto &iAgent : agentVec){
    for(auto iAgent : agents){
        bool saveAgent = true;
        std::string rectId{""};
        if(useSaveRects){
            rectId = Rectangle::isPointInRectangles(*iAgent, saveRectangles);
            if(rectId == ""){
                saveAgent = false;
            }
        }
        if(saveAgent){
            //the first set of tortoises won't have parents, so we'll assign nan for the mom and dad ID's
            int momId=-1, dadId=-1;
            double momX=-1, momY=-1, dadX=-1, dadY=-1;
            // auto momPtr = iAgent->momPtr.lock();
            auto momPtr = iAgent->momPtr;
            if(momPtr){
                momId = momPtr->id;
                momX = momPtr->x;
                momY = momPtr->y;
            }
            // auto dadPtr = iAgent->dadPtr.lock();
            auto dadPtr = iAgent->dadPtr;
            if(dadPtr){
                dadId = dadPtr->id;
                dadX = dadPtr->x;
                dadY = dadPtr->y;
            }
            int isAdult = iAgent->age >= params.mateAge ? 1 : 0;
            //double resistance = quadtree->getValue(iAgent->x, iAgent->y);
            csv << iAgent->id << "," << sex[iAgent->isMale] << ",IV," << iAgent->age << "," <<  isAdult << "," << counter << "," << iAgent->dir << "," << iAgent->dist << "," << iAgent->isAlive << "," << iAgent->deathRisk << "," << iAgent->resistance << ",-1," << 1-iAgent->resistance << "," << momId << "," << dadId;
            //csv << counter << "," << iAgent->id << "," << iAgent->x << "," << iAgent->y << "," << iAgent->age << "," << iAgent->isMale << "," << iAgent->isAlive << "," << iAgent->yearDied << "," << momId << "," << momX << "," << momY << "," << dadId << "," << dadX << "," << dadY;
            // for(int i = 0; i < params.initNLoci; ++i){
            //     csv << "," << iAgent->loci.at(i).a1 << "," << iAgent->loci.at(i).a2;
            // }
            for(int i = 0; i < params.initNLoci; ++i){
                //for(int j = 0; j < params.initNPerLocus; ++j){
                for(int j = 0; j < 2; ++j){
                    csv << "," << iAgent->loci.at(i).at(j);
                }
            }
            csv << "," << iAgent->x << "," << iAgent->y << "," << momX << "," << momY << "," << dadX << "," << dadY << "," << iAgent->ageRisk << "," << iAgent->densityRisk << "," << iAgent->habitatRisk << "," << rectId;
            csv << "\n";
            
            i++;
        }
    }
    csv.close();
}

// void Simulation::readAgents(std::string filePath){
//     std::cout << "Reading file: " << filePath << "\n";
//     auto start = std::chrono::system_clock::now();
    
//     //https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
//     //std::cout << "readAgents1\n";
//     //std::cout << filePath << "\n";

//     //Now actually read in the data
//     std::fstream fin;
//     fin.open(filePath, std::fstream::in);
//     //std::cout << "readAgents2\n";
//     if(!fin.is_open()) throw std::runtime_error("Could not open file (" + filePath + ")");
//     //std::cout << "readAgents3\n";
//     std::string line, colName; //initialize variables that will be used to store input
//     std::vector<std::string> colNames{"id", "x", "y", "age", "isMale"}; //these are the required column names
//     std::map<int, std::string> colMap; //map the index to the column name
//     std::map<int, std::vector<int>> lociNPerMap; //map the locus to how many different items it has
//     std::map<int, std::pair<int,int>> lociIndicesMap;//makes a map for the allele columns - the pair contains the number of the allele and which column of the allele this column is (i.e. a,b, or c, but in integers)
//     std::regex alleleRegex("^allele\\d+\\w"); //matches things like 'allele1a' and 'allele13b'
//     //std::cout << "readAgents4\n";
//     if(fin.good()){
//         //std::cout << "readAgents5\n";
//         std::getline(fin, line); //get the first row
//         std::istringstream ss(line);

        
//         std::vector<std::string> colNamesFound; //keeps track of which columns we've found
//         //check column names, make sure they're valid
//         int colNumber{0}; //keeps track of which column we're on
//         //std::cout << "readAgents6\n";
//         std::cout << "checking format\n";
//         while(std::getline(ss,colName, ',')){
//             //std::cout << "readAgents7\n";
//             //std::cout << colName << "\n";
//             ////std::cout << colname << "\n";
//             //std::string oldColname = colname; //for debugging only
//             //colName = BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
//             BasicUtilities::cleanString(colName); //remove any unknown or non-ASCII characters
//             BasicUtilities::removeSubstrs(colName, "\"");
//             BasicUtilities::removeSubstrs(colName, "\'");
//             if(std::find(colNames.begin(), colNames.end(), colName) != colNames.end()){ //if this string is in the 'colNames' vector, add it to out 'colNamesFound' vector
//                 colNamesFound.push_back(colName);
//             }
//             //std::cout << "readAgents8\n";
//             colMap.insert(std::pair<int, std::string>(colNumber,colName)); //add an entry to our map
//             //std::cout << "readAgents9\n";
//             //check to see if this column has the word 'allele' in it
//             if(std::regex_search(colName, alleleRegex)){
//                 //std::cout << "readAgents10\n";
//                 std::smatch numMatch;
//                 if(std::regex_search(colName, numMatch, std::regex("\\d+"))){ // find the number in the column name
//                     //std::cout << "readAgents11\n";
//                     if(numMatch.size() != 1) throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", either no number was found in the name or more than one number was found.");
//                     int alleleNum = std::stoi(numMatch[0]); //get the number
//                     if(alleleNum < 1){
//                         throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": Error in column name '" + colName + "'. The number of the column must be 1 or higher.");
//                     }
//                     //std::cout << "alleleNum: " << alleleNum << "\n";
//                     if(lociNPerMap.find(alleleNum) == lociNPerMap.end()){ //if we don't have an entry for this locus, create one and associate it with the number 1 (because so far we've found one column for this locus)
//                         lociNPerMap.insert(std::pair<int, std::vector<int>>(alleleNum, std::vector<int>()));
//                         //std::cout << "readAgents12\n";
//                     } 
//                     //std::cout << "readAgents13\n";
//                     if(numMatch.suffix().length() > 1) throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", either no character was found after the number or else more than one character was found. ");
//                     //std::cout << "readAgents14\n";
//                     int suffixNum = BasicUtilities::charToInt(numMatch.suffix().str().at(0));

//                     std::pair<int, int> indicesPair = std::pair<int,int>(alleleNum-1, suffixNum-1); //subtract one because the column numberings start with 1 but we're using 0-based indexing
//                     lociIndicesMap.insert(std::pair<int, std::pair<int,int>>(colNumber, indicesPair));
//                     //std::cout << "suffixNum: " << suffixNum << "\n";
//                     //std::cout << "readAgents15\n";
//                     lociNPerMap[alleleNum].push_back(suffixNum); // add the suffix (the letter) to the vector for this allele
//                     //std::cout << "readAgents16\n";
//                 } else {
//                     throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": for column " + colName +", no number was found in the column name for an 'allele' column.");    
//                 }
//                 //std::cout << "readAgents17\n";
//             }
//             colNumber++;
//         }
//         //std::cout << "readAgents18\n";
//         if(colNamesFound.size() != colNames.size()){
//             std::string colsString = BasicUtilities::combineStrings(colNames);
//             std::string colsFoundString = BasicUtilities::combineStrings(colNamesFound);
//             throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ": not all required columns included. Required columns are: (" + colsString + "). Of these names, these were found: (" + colsFoundString + ").");
//         }
        
//         //this next part checks to make sure the allele* columns are correct 
//         //std::cout << "readAgents19\n";
//         int counter{0};
//         std::vector<int> expectedNums = BasicUtilities::makeIntSequence(1,lociNPerMap.size());
//         //std::cout << "readAgents20\n";
//         //int nNumsPrev{0};
//         for( auto const& x : lociNPerMap){
//             //std::cout << "readAgents21\n";
//             std::vector<int>::iterator it = std::find(expectedNums.begin(), expectedNums.end(), x.first);
//             //std::cout << "readAgents22\n";
//             if(it == expectedNums.end()){
//                 std::string colNamei = "allele" + std::to_string(x.first);
//                 throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". There are " + std::to_string(lociNPerMap.size()) + " distinct locus columns (i.e. allele1a and allele1b are only one locus), so it was expected to see columns allele1, allele2, ... , allele" + std::to_string(lociNPerMap.size()) + ". However, " + colNamei + " was found, which is not in the previously specified sequence");
//             } else {
//                 //std::cout << "readAgents23\n";
//                 expectedNums.erase(std::remove(expectedNums.begin(), expectedNums.end(), x.first), expectedNums.end());
//             }
//             //std::cout << "readAgents24\n";
//             //if(x.second.size() != nNumsPrev){
//             if(x.second.size() != 2){
//                 //throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". A previous locus (a locus consists of n columns, for example, allele1a, allele1b,..., allele1n) had " + std::to_string(nNumsPrev) + " columns, but this locus (" + std::to_string(x.first) + ") has " + std::to_string(x.second.size()) + " columns. All loci must have the same number of columns, and they must be labeled by consecutive lowercase letters (i.e. a,b,c,...");
//                 throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". A locus (" + std::to_string(x.first) + ") was found that has " + std::to_string(x.second.size()) + " columns - however, all loci must have the 2 columns, and they must be labeled by lowercase 'a' and 'b' (for example: allele4a, allele4b)");
//             }

//             //std::cout << "readAgents26\n";

//             std::vector<int> expectedNums2 = BasicUtilities::makeIntSequence(1,x.second.size()); 

//             //std::cout << "readAgents27\n";
//             for(size_t i = 0; i < x.second.size(); ++i){

//                 //std::cout << "readAgents28\n";
//                 std::vector<int>::iterator it2 = std::find(expectedNums2.begin(), expectedNums2.end(), x.second.at(i));

//                 //std::cout << "readAgents29\n";
//                 if(it2 == expectedNums2.end()){
//                     std::string colNamei = "allele" + std::to_string(x.first);
//                     throw std::runtime_error("Error in Simulation::readAgents() while reading " + filePath + ". There are " + std::to_string(x.second.size()) + " columns for locus " + std::to_string(x.first) + " so it was expected that there be columns allele" + std::to_string(x.first) + "a through allele" +std::to_string(x.first) + BasicUtilities::intToChar(x.second.size()) + ". However, this column (allele" + std::to_string(x.first) + BasicUtilities::intToChar(x.second.at(i)) + ") is not in that range, or it is a duplicate column.");
//                 } else {

//                     //std::cout << "readAgents30\n";
//                     expectedNums2.erase(std::remove(expectedNums2.begin(), expectedNums2.end(), x.second.at(i)), expectedNums2.end());
//                 }

//                 //std::cout << "readAgents31\n";
//             }
//             counter++;

//             //std::cout << "readAgents32\n";
//         }
        
//         //std::cout << "readAgents33\n";

//         params.initNLoci = lociNPerMap.size();
//         //params.initNPerLocus = nNumsPrev;

//         //create a vector of vectors for reading in the data
//         int maxId{0}; //we want to keep track of the max - we'll want to set nextID to be maxID +1
//         //std::cout << "readAgents34\n";
//         //int nAgents{0};

//         std::cout << "begin reading in data lines\n";
//         while(std::getline(fin, line)){
//             //std::cout << "======newRow======\n";
//             //std::cout << "readAgents35\n";

//             //In a CSV I edited in Excel there were a bunch of empty lines (just a bunch of commas), and that was breaking this function. So this part removes all commas and spaces and sees if there's anything left. If not, then this row is empty, and we'll skip it.
//             std::string lineCopy = line;
//             lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ','), lineCopy.end()); // remove all commas
//             lineCopy.erase(std::remove(lineCopy.begin(), lineCopy.end(), ' '), lineCopy.end()); // remove all spaces
//             if(lineCopy.length() > 0){
//                 //nAgents++;
//                 //std::string param;
//                 std::string val;
//                 std::stringstream ss(line);
//                 int colIdx{0};
//                 std::shared_ptr<Agent> newAgent(new Agent());
//                 newAgent->isAlive = 1; 
//                 newAgent->yearDied = -1;
//                 newAgent->loci = std::vector<std::vector<int>>(params.initNLoci);
//                 //std::cout << "readAgents36\n";
//                 while(std::getline(ss,val, ',')){
//                     //val = BasicUtilities::cleanString(val);
//                     BasicUtilities::cleanString(val);
//                     //std::cout << "val: " << val << "-----------------\n";
//                     std::string column = colMap[colIdx];
//                     //std::cout << "colIdx: " << colIdx << "-----------------\n";
//                     //std::cout << "column: " << column << "-----------------\n";
//                     //std::cout << "readAgents37\n";
//                     if(val.length() > 0){ //skip this entry if there's no value;
//                         std::smatch numMatch;
//                         if(lociIndicesMap.find(colIdx) != lociIndicesMap.end()){ //check if this column index is one of the allele columns
//                         //if(std::regex_search(column, numMatch, std::regex("\\d+"))){ // find the number in the column name
//                             //if(std::regex_search(column, alleleRegex)){    
//                           // if(numMatch.prefix().str() == "allele"){
//                             //std::cout << "readAgents38\n";
                            
//                             //std::cout << "readAgents39\n";      
//                             //int alleleNum = std::stoi(numMatch[0])-1; //get the number of the allele - the lowest number allowed is 1, so subtract 1
//                             //std::cout << "readAgents40\n";      
//                             //int suffixNum = BasicUtilities::charToInt(numMatch.suffix().str().at(0))-1; //get the character after the letter and subtract 1 so that the lowest number is 0
//                             //std::cout << "readAgents41\n";     
//                             int alleleNum = lociIndicesMap[colIdx].first;
//                             int suffixNum = lociIndicesMap[colIdx].second; 
//                             if(newAgent->loci.at(alleleNum).size() == 0){ //if the vector for this locus is empty, make a vector of the appropriate size
//                                 //std::cout << "readAgents42\n";      
//                                 //newAgent->loci.at(alleleNum) = std::vector<int>(params.initNPerLocus);
//                                 newAgent->loci.at(alleleNum) = std::vector<int>(2);
//                             }
//                             //std::cout << "readAgents43\n";      
//                             newAgent->loci.at(alleleNum).at(suffixNum) = std::stoi(val); //add the allele value to the correct locus                        
//                             //}
//                         } else {
//                             //std::cout << "readAgents44\n";      
//                             //std::vector<std::string> colNames{"id", "x", "y", "age", "isMale"}; //these are the required column names
//                             //std::cout << "readAgents45\n";      
//                             if(column == "id"){
//                                 newAgent->id = std::stoi(val);
//                                 if(newAgent->id > maxId) maxId = newAgent->id; //
//                             }
//                             //std::cout << "readAgents46\n";      
//                             else if (column == "x") { newAgent->x = std::stod(val); }
//                             //std::cout << "readAgents47\n";
//                             else if (column == "y") { newAgent->y = std::stod(val); }
//                             //std::cout << "readAgents48\n";
//                             else if (column == "age") { newAgent->age = std::stoi(val); }
//                             //std::cout << "readAgents49\n";
//                             else if (column == "isMale") { newAgent->isMale = std::stoi(val); }
//                             //std::cout << "readAgents50\n";
//                         }
//                     } else {
//                         std::cout << "WARNING: Empty value found in column '" << column << "'\n";
//                     }
//                     //std::cout << "readAgents51\n";
//                     //tempRow.at(colIdx) = std::stod(value);
//                     colIdx++;
//                 }
//                 //std::cout << "readAgents52\n";
//                 agents.emplace_back(newAgent);
//             }

//         }
//         //std::cout << "readAgents53\n";
//         nextId = maxId + 1;
//         // params.initNAgents = agents.size();
//         // params.initAlleleMin = -1;
//         // params.initAlleleMax = -1;
//     }
//     //std::cout << "readAgents54\n";
//     fin.close();
//     auto stop = std::chrono::system_clock::now();
//     auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
//     std::cout << "Reading in file took: " << duration.count() << " ms \n"; 
// }
