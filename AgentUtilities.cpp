#include "AgentUtilities.h"
#include "Agent.h"
#include "MathUtilities.h"
#include "Raster.h"
#include "libs/simple_fft/fft.h"
#include <cmath>
#include <complex>
#include <list>
#include <memory>

Raster AgentUtilities::quadratCount(const std::list<std::shared_ptr<Agent>> &points, const int nX, const int nY, const double xMin, const double xMax, const double yMin, const double yMax){ // 2/19/2021 i
    Raster raster(nX, nY, xMin, xMax, yMin, yMax);
    // for(size_t i = 0; i < points.size(); ++i){ // 2/19/2021 d
    for(auto iPoint : points){ // 2/19/2021 i
        //int index = raster.getIndex(points[i]->x, points[i]->y); // 2/19/2021 d
        //std::cout << "QC: x: " << iPoint->x << " | y: " <<  iPoint->y << "\n";
        //std::cout << iPoint->toString() << "\n";
        int index = raster.getIndex(iPoint->x, iPoint->y); // 2/19/2021 d
        raster.values->at(index) += 1;
    }
    return raster;
}

Raster AgentUtilities::quadratCount(const std::list<std::shared_ptr<Agent>> &points, const double cellSize, const double xMin, const double xMax, const double yMin, const double yMax){
    int nX = std::ceil((xMax-xMin)/cellSize);
    int nY = std::ceil((yMax-yMin)/cellSize);
    // return quadratCount(points, nX, nY, xMin, xMax, yMin, yMax);
    return quadratCount(points, nX, nY, xMin, xMin + (nX * cellSize), yMin, yMin + (nY * cellSize));
}

Raster AgentUtilities::quadratCount(const std::list<std::shared_ptr<Agent>> &points, const double cellSize){
    double xMin = points.front()->x;
    double xMax = points.front()->x;
    double yMin = points.front()->y;
    double yMax = points.front()->y;
    for(auto iPoint : points){
        if(iPoint->x > xMax) xMax = iPoint->x;
        if(iPoint->x < xMin) xMin = iPoint->x;
        if(iPoint->y > yMax) yMax = iPoint->y;
        if(iPoint->y < yMin) yMin = iPoint->y;
    } 
    int nX = std::ceil((xMax-xMin)/cellSize);
    int nY = std::ceil((yMax-yMin)/cellSize);
    return quadratCount(points, nX, nY, xMin, xMax, yMin, yMax);
}



//Used the code from 'second.moment.engine()' from the 'spatstat' R package
//as a guide for writing this function
//https://github.com/spatstat/spatstat.core/blob/master/R/Kmeasure.R
Raster AgentUtilities::pointDensity(const std::list<std::shared_ptr<Agent>> &points, const double bandwidth, const int nX, const int nY, const double xMin, const double xMax, const double yMin, const double yMax){
    if((nX & (nX - 1)) != 0 || (nY & (nY - 1)) != 0){ //CHANGED - Added this check to ensure that the length of a is a power of 2
        throw std::runtime_error("Error in AgentUtilities::pointDensity() - the dimensions of the output raster must be a power of 2 (provided dimensions: nX=" + std::to_string(nX) + ", nY=" + std::to_string(nY) + ")");
    }
    //------------------------
    //do the quadrat count
    //An optimization I could make is to not use the quadratCount function and instead do a quadrat count that puts the values directly into a 2D vector of complex<double>
    Raster quadrat = quadratCount(points, nX, nY, xMin, xMax, yMin, yMax);
    
    int padnx = quadrat.nX*2;
    int padny = quadrat.nY*2;
    std::vector<std::vector<std::complex<double>>> vals(padny,std::vector<std::complex<double>>(padnx));
    for(size_t r = 0; r < vals.size(); ++r){
        //vals.at(r) = std::vector<std::complex<double>>(padnx);
        for(size_t c = 0; c < vals.at(r).size(); ++c){
        // for(size_t c = 0; c < vals[r].size(); ++c){
            if((int)r < quadrat.nY && (int)c < quadrat.nX){
            //if(r < quadrat.nY && c < quadrat.nX){
                vals.at(r).at(c) = std::complex<double>(quadrat.getValue((int)c,(int)r));
                // vals[r][c] = std::complex<double>(quadrat.getValue((int)c,(int)r));
            } else {
                vals.at(r).at(c) = std::complex<double>(0);
                // vals[r][c] = std::complex<double>(0);
            }
        }
    }

    //------------------------
    //set up the kernel
    std::vector<std::vector<std::complex<double>>> kernel(padny, std::vector<std::complex<double>>(padnx));
    double sum = 0;
    for(size_t r = 0; r < kernel.size(); ++r){
        //kernel.at(r) = std::vector<std::complex<double>>(padnx);
        for(size_t c = 0; c < kernel.at(r).size(); ++c){
        // for(int c = 0; c < kernel[r].size(); ++c){
            double x;//=-1.5;
            if((int)c < quadrat.nX){
                x = quadrat.xCellLength*c;
            } else {
                x = quadrat.xCellLength*((int)c-padnx);
            }
            double y;
            if((int)r < quadrat.nY){
                y = quadrat.yCellLength*r;
            } else {
                y = quadrat.yCellLength*((int)r-padny);
            }
            //std::cout << "(r:" << r << " c:" << c << ") " << x << " " << y << "\n";
            double xDensity = MathUtilities::gaussDens(x, 0, bandwidth);
            double yDensity = MathUtilities::gaussDens(y, 0, bandwidth);
            double density = xDensity*yDensity;
            kernel.at(r).at(c) = std::complex<double>(density);
            // kernel[r][c] = std::complex<double>(density);
            sum += density;
        }
    }

    //divide all numbers by the sum
    for(size_t r = 0; r < kernel.size(); ++r){
        for(size_t c = 0; c < kernel.at(r).size(); ++c){
        // for(size_t c = 0; c < kernel[r].size(); ++c){
            kernel.at(r).at(c) = std::complex<double>(kernel.at(r).at(c).real()/sum);
            // kernel[r][c] = std::complex<double>(kernel[r][c].real()/sum);
        }
    }

    ///////////// USING SIMPLE_FFT {

    std::vector<std::vector<std::complex<double>>> vals_fft(padny, std::vector<std::complex<double>>(padnx));
    std::vector<std::vector<std::complex<double>>> kernel_fft(padny, std::vector<std::complex<double>>(padnx));
    const char * error = NULL;
    simple_fft::FFT(vals,vals_fft,padny,padnx,error);
    simple_fft::FFT(kernel,kernel_fft,padny,padnx,error);

    for(size_t r = 0; r < vals_fft.size(); ++r){
        for(size_t c = 0; c < vals_fft.at(r).size(); ++c){
            vals_fft.at(r).at(c) = vals_fft.at(r).at(c)*kernel_fft.at(r).at(c);
        }
    }

    std::vector<std::vector<std::complex<double>>> dens(padny, std::vector<std::complex<double>>(padnx));
    simple_fft::IFFT(vals_fft,dens,padny,padnx,error);
    
    Raster densityRast(quadrat.nX, quadrat.nY, quadrat.xMin, quadrat.xMax, quadrat.yMin, quadrat.yMax);
    
    double cellArea = densityRast.xCellLength * densityRast.yCellLength;
    for(int r = 0; r < densityRast.nY; ++r){
        for(int c = 0; c < densityRast.nX; ++c){
            densityRast.setValue((int) c, (int) r, dens.at(r).at(c).real()/cellArea);
        }
    }

    return densityRast;

    ///////////// USING SIMPLE_FFT }




    ///////////// USING MY CODE {
    
    // // get the FFT of both the quadrat count and the kernel
    // MathUtilities::fft2d(vals,false);
    // MathUtilities::fft2d(kernel,false);

    // for(size_t r = 0; r < vals.size(); ++r){
    //     for(size_t c = 0; c < vals.at(r).size(); ++c){
    //         vals.at(r).at(c) = vals.at(r).at(c)*kernel.at(r).at(c);
    //     }
    // }

    // MathUtilities::fft2d(vals,true);
    
    // Raster densityRast(quadrat.nX, quadrat.nY, quadrat.xMin, quadrat.xMax, quadrat.yMin, quadrat.yMax);
    
    // double cellArea = densityRast.xCellLength * densityRast.yCellLength;
    // for(int r = 0; r < densityRast.nY; ++r){
    //     for(int c = 0; c < densityRast.nX; ++c){
    //         densityRast.setValue((int) c, (int) r, vals.at(r).at(c).real()/cellArea);
    //     }
    // }

    // return densityRast;

    ///////////// USING MY CODE }
}

//wrapper for pointDensity that allows for specification of cell size rather than nX and nY
Raster AgentUtilities::pointDensity(const std::list<std::shared_ptr<Agent>> &points, const double bandwidth, const int cellSize, const double xMin, const double xMax, const double yMin, const double yMax){
    int nX = std::ceil((xMax-xMin)/cellSize);
    int nY = std::ceil((yMax-yMin)/cellSize);
    nX = std::pow(2, std::ceil(std::log2(nX)));
    nY = std::pow(2, std::ceil(std::log2(nY)));
    // return quadratCount(points, nX, nY, xMin, xMax, yMin, yMax);
    return pointDensity(points, bandwidth, nX, nY, xMin, xMin + (nX * cellSize), yMin, yMin + (nY * cellSize));
}
