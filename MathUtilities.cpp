#include "MathUtilities.h"
#include "PointUtilities.h"
#include "Raster.h"
#include "PointGrid.h"
#include "Point.h"
#include "Quadtree.h"
#include "Node.h"
#include "constants.h"
#include <cmath>
#include <vector>
#include <map>
#include <set>
#include <random>
#include <iostream>
#include <list>
#include <cassert>
#include <algorithm>
#include <complex>

#define _USE_MATH_DEFINES
#include <math.h>
// double MathUtilities::sqDistBtwPoints(const Point& point1, const Point& point2){
//     double dist = pow(point1.x - point2.x,2) + pow(point1.y - point2.y,2);
//     return dist;
// }

// double MathUtilities::distBtwPoints(const Point& point1, const Point& point2){
//     double dist = std::sqrt(std::pow(point1.x - point2.x, 2) + std::pow(point1.y - point2.y, 2));
//     return dist;
// }

// //gets the angle between two points in radians
// double MathUtilities::getAngle(const Point& point1, const Point& point2){
//     //double angle{std::numeric_limits<double>::quiet_NaN()};
//     double angle = std::atan2(point2.y - point1.y, point2.x - point1.x); //http://www.cplusplus.com/reference/cmath/atan2/
//     return angle;
// }

// //given a point, generates 'n' evenly spaced points around the point at distance 'radius'. First point will always be at 0 radians.
// std::vector<Point> MathUtilities::getPointsAroundPoint(const Point& point, int n, double radius){
//     std::vector<Point> points(n);
//     for(int i = 0; i < n; ++i){
//         double angle = (2.0*M_PI/static_cast<double>(n)) * i; //given that we want 'n' points, to make them evenly spaced we'll increment the angles by 2*pi/n
//         points[i].setCoords(std::cos(angle)*radius + point.x, std::sin(angle)*radius + point.y); //do I need to instantiate (or construct or whatever the term is) a Point object before? It seems like it's working, though
//     }
// return points;
// }

// //take a point, an angle (relative to the x-axis), and another point. The first point is considered the 
// //'current point', the second point is the point we're considering moving to,
// //and the angle is the direction in which we want to bias the movement. This
// //function will return a value that is low if the angle from 'point' to 
// //'optionPoint' is not similar to 'angle' (say, in the opposite direction) and
// //high if this angle is close to 'angle'
// double MathUtilities::getAngleOffsetVal(const Point& point, double angle, const Point& optionPoint){
//     double optAngle = getAngle(point, optionPoint);
//     double offset = std::cos(optAngle-angle) + 1; //'cos' produces a wave-like line with y ranging from -1 to 1. By subtracting 'angle' from 'optAngle', we're essentially centering the wave on 'angle'. If optAngle == angle, then optAngle-angle will be 0. And cos(0) is the highest possible value of cos.
//     return offset;
// }


// //give a point (center) and a distance (radius), outputs a random point that is distance
// //'radius' away from 'center'.
// Point MathUtilities::getRandomPointOnCircle(const Point& center, double radius, std::mt19937& randomGenerator){
//     //set up random number generation - got these lines from https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
    
//     //std::mt19937 gen(seed); //Standard mersenne_twister_engine seeded with rd()
//     //<old method>
//     // std::random_device rd;  //Will be used to obtain a seed for the random number engine
//     // std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//     // std::uniform_real_distribution<> dis(0, 2*M_PI); 
//     // double angle = dis(gen);//generate a random number between 0 and 2*pi
//     // //</old method>
    
    
    
//     //<new method>
//     std::uniform_real_distribution<> dis(0, 2*M_PI); 
//     double angle = dis(randomGenerator);//generate a random number between 0 and 2*pi
//     //</new method>

//     // std::cout << "angle: " << angle << std::endl;
//     //std::cout << angle << ",";
//     Point point;
//     //using the angle we randomly generated, find out what the x coordinates are of a point at angle 'angle' and distance 'radius' from the center point
//     point.setCoords(center.x + cos(angle)*radius, center.y + sin(angle)*radius);
//     //std::cout << "point: " << point.x << "," << point.y << std::endl;
//     return point;
// }

//return M_1_SQRT_2PI * exp(-0.5 * x * x) / sigma;
const double SQRT_2PI =  sqrt(2*M_PI);
double MathUtilities::gaussDens(double x, double mean, double sd){
    double dens = 1/(sd*SQRT_2PI) * exp(-.5*pow((x-mean)/sd, 2));
    //double denom  = (x-mean)/sd;
    //double dens = 1/(sd*SQRT_2PI) * exp(-.5*denom*denom);
    return dens;
}

// Raster MathUtilities::pointDensity(std::vector<std::shared_ptr<Agent>> &points, double bandwidth, int nX, int nY){
//     //std::vector<double> densities(points.size());
//     PointGrid pg = PointGrid(points, bandwidth*4);
//     Raster raster(nX, nY, pg.xMin, pg.xMax, pg.yMin, pg.yMax);

//     for(size_t i = 0; i < raster.values->size(); ++i){

//         Point iCoords = raster.getCoords(i);
//         std::vector<std::shared_ptr<Agent>> pointsTemp = pg.getPointsWithin(iCoords.x, iCoords.y, bandwidth*4);    
//         double sum{0};
//         for(size_t j = 0; j < pointsTemp.size(); ++j){
//             //sum += gaussDens(points.at(i).x, points.at(j).x, bandwidth) * gaussDens(points.at(i).y, points.at(j).y, bandwidth);
//             sum += gaussDens(iCoords.x, pointsTemp[j]->x, bandwidth) * gaussDens(iCoords.y, pointsTemp[j]->y, bandwidth);
//         }
//         raster.setValue(i,sum);
//     }
//     return raster;
// }
std::vector<int> MathUtilities::nPointsWithin(std::list<std::shared_ptr<Agent>> &points, double distance){
    std::vector<int> nPoints(points.size());
    PointGrid pg = PointGrid(points, distance);
    //for(size_t i = 0; i < points.size(); ++i){
    int i = 0;
    for(auto &iPoint : points){
        double xi = iPoint->x;
        double yi = iPoint->y;
        std::list<std::shared_ptr<Agent>> pointsTemp = pg.getPointsWithin(xi, yi, distance);  
        nPoints.at(i) = pointsTemp.size();
        ++i;
    }
    return nPoints;
}
//double pointDensity(Point &point, double b)


// double gaussBivariateDens(double x, double xMean, double xSd, double y, double yMean, double ySd){
//     double dens = gaussDens(x,xMean,xSd) * gaussDens(y,yMean,ySd);
//     return dens;
// }


// Raster MathUtilities::quadratCount(std::list<std::shared_ptr<Agent>> &points, int nX, int nY, double xMin, double xMax, double yMin, double yMax){ // 2/19/2021 d

// 2/19/2021 i }

// std::vector<double> MathUtilities::scaleToSum(std::vector<double> nums, double sum){
//     double numsSum{0};
//     for(size_t i = 0; i < nums.size(); ++i){
//         numsSum
//     }
// }

double MathUtilities::scaleNum(double num, double oldMin, double oldMax, double newMin, double newMax){
    double oldRange = oldMax - oldMin;
    double newRange = newMax - newMin;
    double numScaled = ((num-oldMin) * newRange/oldRange) + newMin;
    return numScaled;
}
std::vector<double> MathUtilities::scaleNums(std::vector<double> nums, double oldMin, double oldMax, double newMin, double newMax){
    //double numsSum{0};
    std::vector<double> numsScaled(nums.size());   
    double oldRange = oldMax - oldMin;
    double newRange = newMax - newMin;
    for(size_t i = 0; i < nums.size(); ++i){
        numsScaled.at(i) = ((nums.at(i)-oldMin) * newRange/oldRange) + newMin;
    }
    return numsScaled;
}

double MathUtilities::max(std::vector<double> nums){
    double maxNum = nums.at(0);
    for(size_t i = 1; i < nums.size(); ++i){
        if(nums.at(i) > maxNum){
            maxNum = nums.at(i);
        }
    }
    return maxNum;
}

double MathUtilities::min(std::vector<double> nums){
    double minNum = nums.at(0);
    for(size_t i = 1; i < nums.size(); ++i){
        if(nums.at(i) < minNum){
            minNum = nums.at(i);
        }
    }
    return minNum;
}

int MathUtilities::max(std::vector<int> nums){
    int maxNum = nums.at(0);
    for(size_t i = 1; i < nums.size(); ++i){
        if(nums.at(i) > maxNum){
            maxNum = nums.at(i);
        }
    }
    return maxNum;
}

int MathUtilities::min(std::vector<int> nums){
    int minNum = nums.at(0);
    for(size_t i = 1; i < nums.size(); ++i){
        if(nums.at(i) < minNum){
            minNum = nums.at(i);
        }
    }
    return minNum;
}


//bool isReachable(Point startPoint, point endPoint, double distance) const{
bool MathUtilities::isReachable(Quadtree& qt, double x1, double y1, double x2, double y2, double distance){
    std::shared_ptr<Node> startNode = qt.getNode(Point(x1, y1));
    std::shared_ptr<Node> endNode = qt.getNode(Point(x2, y2));

    if(!(startNode && endNode)) return false; //if either of the pointers are null, return false
    if(std::isnan(startNode->value) || std::isnan(endNode->value)) return false; //if either of the nodes have the value NAN, return false
    if(PointUtilities::sqDistBtwPoints(Point(x1,y1), Point(x2,y2)) > std::pow(distance,2)) return false; //if the points are more than 'distance' away, return false
    if(startNode->id == endNode->id) return true; //in this case they're in the same cell

    //otherwise check if they're neighbors and return true if they are;
    for(size_t i = 0; i < endNode->neighbors.size(); ++i){ //so long as the neighbors have been assigned correctly I could do this for either startNode or endNode
        if(endNode->neighbors.at(i).lock()->id == startNode->id) return true;
    }

    // otherwise we've got some work to do - we'll recursively start checking the neighbors to see if there's a valid path between the two points - starting at the end point
    std::list<int> prevIds;
    return doesPathExist(endNode, startNode->id, x1-distance, x1+distance, y1-distance, y1+distance, prevIds); //I think this should be a separate function since we don't want to do the checks I did above each time. It should recursively traverse over all the valid neighbors
}

bool MathUtilities::doesPathExist(std::shared_ptr<Node> node, int endNodeId, double xMin, double xMax, double yMin, double yMax, std::list<int> &prevIds){
    //std::cout << "doesPathExist(): (" << (node->xMin + node->xMax)/2 << "," << (node->yMin + node->yMax)/2 << ") | id: " <<  node->id <<"\n";
    //bool foundNode = false;
    if(node->neighbors.size() > 0){
        for(size_t i = 0; i < node->neighbors.size(); ++i){
            auto nb_i = node->neighbors[i].lock();
            if(std::find(prevIds.begin(), prevIds.end(), nb_i->id) == prevIds.end()){ //check if we've already iterated over this node - https://www.techiedelight.com/check-vector-contains-given-element-cpp
                prevIds.push_back(nb_i->id); //add this ID to our list of IDs we've checked
                if(!std::isnan(nb_i->value)){ //if it's NAN, then skip it - we can't travel through NAN
                    if(nb_i->id == endNodeId){ //if this is the end node, we're done.
                        return true;
                    } else { //otherwise check this nodes neighbors
                        //make sure this cell overlaps with the square defined by our min and max coords

                        //NOTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        //Is this right? shouldn't I be using 'node->neighbors.at(i)->xMin' etc. instead of 'node->xMin'? When I'm just using 'node' I'm checking the boundaries of the node that was passed to this funciton - but shouldn't I be checking the limits of the neighbor cell that we're checking right now? The way this is written, this value will be the same every single loop
                        bool isXValid = !(xMax < node->xMin || xMin > node->xMax);
                        bool isYValid = !(yMax < node->yMin || yMin > node->yMax);
                        if(isXValid && isYValid){
                            //return doesPathExist(node->neighbors.at(i), endNodeId, xMin, xMax, yMin, yMax, prevIds);
                            bool foundNode = doesPathExist(nb_i, endNodeId, xMin, xMax, yMin, yMax, prevIds);
                            if(foundNode) return true;
                        }
                    }
                }
            } 
        }
        return false;
    } else {
        return false;
    }

}



    //-------------------------
    // getNearestNeighborDistance
    //-------------------------
    //get the distance to the nearest neighbor (using centroids)
    double MathUtilities::getNearestNeighborDistance(const std::shared_ptr<Node> node){
    double min{0};
    double xMean{(node->xMin + node->xMax)/2};
    double yMean{(node->yMin + node->yMax)/2};
    for(size_t i = 0; i < node->neighbors.size(); ++i){
        auto nb_i = node->neighbors[i].lock();
        double xMeanNb{(nb_i->xMin + nb_i->xMax)/2};
        double yMeanNb{(nb_i->yMin + nb_i->yMax)/2};
        double dist = std::pow(xMean - xMeanNb,2) + std::pow(yMean - yMeanNb,2);
        if( (dist < min) | (i == 0)){
            min = dist;
        }
    }
    return std::sqrt(min);
    }

    //find the minimum distance from a point to the centroids of the neighbors of this node
    double MathUtilities::getNearestNeighborDistance(const std::shared_ptr<Node> node, const Point& point){
    double min{0};
    for(size_t i = 0; i < node->neighbors.size(); ++i){
        auto nb_i = node->neighbors[i].lock();
        double xMeanNb{(nb_i->xMin + nb_i->xMax)/2};
        double yMeanNb{(nb_i->yMin + nb_i->yMax)/2};
        double dist = pow(point.x - xMeanNb,2) + pow(point.y - yMeanNb,2);
        if( (dist < min) | (i == 0) ){
            min = dist;
        }
    }
    return sqrt(min);
    }





//here's a library I could look into using: https://github.com/d1vanov/Simple-FFT
const double PI = acos(-1);
// This code is from: https://cp-algorithms.com/algebra/fft.html
// A few modifications made (see comments in the function for these changes)
// Another change I made is that I got rid of the 'using cd = std::complex<double>;' statement and 
// so I'm using 'std::complex<double>' instead of 'cd' - I wanted the code to be more verbose and explicit
// IMPORTANT NOTE - from comparison with the 'fft' function in R, it looks this function only
// produces correct results when the length of 'a' is a power of 2 - note that this is a characteristic
// of the FFT algorithm - see this StackOverflow Q&A - https://dsp.stackexchange.com/questions/10043/how-important-is-it-to-use-power-of-2-when-using-fft (I thought the "Jul 24 '13 at 16:24" answer was the most helpful)
void MathUtilities::fft(std::vector<std::complex<double>> & a, bool invert) {

    int n = a.size();

    //http://www.graphics.stanford.edu/~seander/bithacks.html#DetermineIfPowerOf2
    if((n & (n - 1)) != 0){ //CHANGED - Added this check to ensure that the length of a is a power of 2
        throw std::runtime_error("Error in MathUtilities::fft() - the length of 'a' is not a power of 2");
    }
    
    for (int i = 1, j = 0; i < n; i++) {
        int bit = n >> 1;
        for (; j & bit; bit >>= 1)
            j ^= bit;
        j ^= bit;

        if (i < j)
            swap(a[i], a[j]);
    }

    for (int len = 2; len <= n; len <<= 1) {
        double ang = 2 * PI / len * (invert ? 1 : -1); //CHANGED - switched the 1 and -1 since the results seemed flipped compared to the R results - I think this is a mistake in the code
        std::complex<double> wlen(cos(ang), sin(ang));
        for (int i = 0; i < n; i += len) {
            std::complex<double> w(1);
            for (int j = 0; j < len / 2; j++) {
                std::complex<double> u = a[i+j], v = a[i+j+len/2] * w;
                a[i+j] = u + v;
                a[i+j+len/2] = u - v;
                w *= wlen;
            }
        }
    }

    //CHANGED - commenting this out because the R version of FFT inverse doesn't divide by n, and I want my code to correspond as closely as possible to the R code
    // if (invert) {
    //     for (std::complex<double> & x : a)
    //         x /= n;
    // }
}

std::vector<std::vector<std::complex<double>>> MathUtilities::rotateVector(std::vector<std::vector<std::complex<double>>> &a){
    if(a.size() > 0){
        int nrow = a.size();
        int ncol = a.at(0).size();

        std::vector<std::vector<std::complex<double>>> out(ncol,std::vector<std::complex<double>>(nrow));

        for(int c = 0; c < ncol; c++){
            //assert(a.at(c).size() == ncol);
            //out.at(c) = std::vector<std::complex<double>>(nrow);
            for(int r = 0; r < nrow; r++){
                out.at(c).at(r) = a.at(r).at(c);
            }
        } 
        return out;
    } else {
        return std::vector<std::vector<std::complex<double>>>(0);
    }
}

// should I make it return a new object rather than modifying the original one?
void MathUtilities::fft2d(std::vector<std::vector<std::complex<double>>> &vals, bool invert){
    for(size_t i = 0; i < vals.size(); ++i){
        fft(vals.at(i), invert);
    }

    
    vals = rotateVector(vals);
    for(size_t i = 0; i < vals.size(); ++i){
        fft(vals.at(i), invert);
    }
    vals = rotateVector(vals);
}

std::vector<std::vector<std::complex<double>>> MathUtilities::doubleVecToComplex(std::vector<std::vector<double>> vals){
    std::vector<std::vector<std::complex<double>>> complexVec(vals.size());

    for(size_t i = 0; i < vals.size(); ++i){
        
        complexVec.at(i) = std::vector<std::complex<double>>(vals.at(i).begin(), vals.at(i).end());
    }
    return complexVec;
}












// //finds the shortest path in a network
// //adjMat - a adjacency matrix representing a network. Values should indicate the 
//     //distance (or cost) of travelling between two nodes. If an edge does not 
//     //exist between two nodes, then that value should be NAN. This matrix needs
//     //to be symmetrical.
// //startIndex - an integer representing the "origin" node. This corresponds to the
//     //adjacency matrix (so if 4 is provided the node represented by the fifth row
//     //and column (0-based indexing) is used as the origin)
// //RETURNS: 
//     //an object of class Matrix. This essentially represents a subset of the original
//     //network that only includes the edges that make up the shortest paths to each 
//     //node. This can be used to find the shortest path from the origin to any other node.
//     //A shortest path from the origin to another node can be found by tracing the 
//     //path backwards - start with the destination node, and then use the adjacency matrix
//     //to identify the edges that lead back to the origin (because the resulting network
//     //is a tree with the origin as the root, there will only be one path back to the 
//     //origin)
// // Matrix MathUtilities::getShortestPaths(Matrix &adjMat, int startIndex){
// //     assert(adjMat.nRow() == adjMat.nCol());
// //     assert(startIndex < adjMat.nRow());
// //     Matrix pathMat(NAN,adjMat.nRow(), adjMat.nCol());
// //     // std::vector<double> nodeCosts(adjMat.nRow(),NAN);
// //     std::vector<std::pair<int, double>> nodeCosts(adjMat.nRow());
// //     nodeCosts.at(0) = std::pair<int, double>(startIndex,0);
// //     int nodeCostsSize = 1;
// //     bool keepGoing = true;
// //     int counter = 0;
// //     while(keepGoing){
// //         // std::cout << "loop " << counter + 1 << "\n";
// //         //order is node 1 id, node 2 id, cost of edge, total cost from origin to node 2
// //         std::vector<std::tuple<int,int,double,double>> tempNodeCosts(adjMat.nRow()); //make a vector for storing the nodes (and total cost to reach those nodes) that we'll consider adding this iteration
// //         int tempNodeCostsSize = 0; //because I'm initializing 'tempNodeCosts' to have size 'adjMat.nRow()' (to avoid having to use 'push_back') I need to keep track of how many things we've added to it
// //         //for each node we've already added, find the other nodes that are "reachable" and calculate the total cost it takes to reach each of the 'reachable' nodes
// //         for(int i = 0; i < nodeCostsSize; ++i){
// //             // std::cout << "    subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
// //             //now we'll look at the column of 'adjMat' that tells us which nodes this node is connected to
// //             for(int j = 0; j < adjMat.nRow(); ++j){
// //                 // std::cout << "        subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
// //                 double edgeVal = adjMat.getValue(j,nodeCosts.at(i).first); //get the value for this 'edge'
// //                 if(!std::isnan(edgeVal)){ //if the value is NAN then this isn't actually an edge
// //                     //if the value isn't NAN then we need to check this edge
// //                     //first we'll check to see if we've already included this node
// //                     bool isAlreadyIncluded = false;
// //                     for(size_t k = 0; k < nodeCostsSize; ++k){
// //                         // std::cout << "            subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
// //                         if(nodeCosts.at(k).first == j){
// //                             // std::cout << "            k: " << k << "\n";
// //                             // std::cout << "            nodeCosts.at(k).first: " << nodeCosts.at(k).first << "\n";
// //                             // std::cout << "            nodeCosts.at(k).second: " << nodeCosts.at(k).second << "\n";
// //                             // std::cout << "THROW OUT NODE " << j << "!\n";
// //                             isAlreadyIncluded = true;
// //                             break;
// //                         }
// //                         // std::cout << "            END subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
// //                     }
// //                     if(!isAlreadyIncluded){ //if we haven't added this node yet, we'll consider this edge as a possibility
// //                         // std::cout << "INCLUDE NODE " << j << "!\n";
// //                         // std::cout << "            add a node possibility\n";
// //                         tempNodeCosts.at(tempNodeCostsSize) = std::tuple<int,int,double,double>(nodeCosts.at(i).first, j, edgeVal, nodeCosts.at(i).second + edgeVal);
// //                         tempNodeCostsSize++;
// //                         // std::cout << "            END add a node possibility\n";
// //                     }
// //                 }
// //                 // std::cout << "        END subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
// //             }
// //             // std::cout << "    END subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
// //         }
// //         if(tempNodeCostsSize == 0){ //this means there are no nodes to add, so we're done
// //             keepGoing = false;
// //         } else {
// //             //now we'll find which of the possible edges has the lowest total cost
// //             int minIndex = 0; //index of the lowest cost edge
// //             for(int i = 0; i < tempNodeCostsSize; ++i){
// //                 // std::cout << "    subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
// //                 double cost = std::get<3>(tempNodeCosts.at(i)); //get the total cost for adding this edge (stored at the 4th spot in the tuple (index 3))
// //                 if(cost < std::get<3>(tempNodeCosts.at(minIndex))){
// //                     minIndex = i;
// //                 }
// //                 // std::cout << "    END subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
// //             }

// //             // std::cout << "    add chosen node to nodeCost\n";
// //             //add the node to our list of nodes that we've reached (along with the cost to get there)
// //             nodeCosts.at(nodeCostsSize) = std::pair<int,double>(std::get<1>(tempNodeCosts.at(minIndex)), std::get<3>(tempNodeCosts.at(minIndex)));
// //             nodeCostsSize++;

// //             //add the edge to our matrix of edges
// //             // std::cout << "    add node to matrix\n";
// //             pathMat.setValue(std::get<2>(tempNodeCosts.at(minIndex)), std::get<1>(tempNodeCosts.at(minIndex)), std::get<0>(tempNodeCosts.at(minIndex)));
            
// //             // std::cout << "    evaluate if we need to keep going\n";
            
// //             if(nodeCostsSize > nodeCosts.size()){
// //                 // std::cout << "    STOP!\n";
// //                 keepGoing = false;
// //             }

// //             // std::cout << "END loop " << counter + 1 << "\n";
// //             counter++;

            
// //         }
// //         std::cout << "==================\nnodeCosts\n";
// //             for(int i = 0; i < nodeCostsSize; ++i){
// //                 std::cout << "(id: " << nodeCosts.at(i).first << ", cost: " << nodeCosts.at(i).second << ")\n";
// //             }
// //         std::cout << "==================\n";
// //     }
// //     return pathMat;
// // }

// //This is a modification of 'getShortestPaths' that finds only the path from
// //the origin to the destination. This allows it to (ideally) run faster because
// //because as soon as we reach the desired destination we can stop. Ideally I'd 
// //like to somehow integrate these two functions so I don't have two functions
// //that basically do the same thing... but I think it'd be tricky and I'm not 
// //sure it's worth the time. So for now at least I'm going to take the easy way out
// //and just have two separate functions.
// //adjMat - a adjacency matrix representing a network. Values should indicate the 
//     //distance (or cost) of travelling between two nodes. If an edge does not 
//     //exist between two nodes, then that value should be NAN. This matrix needs
//     //to be symmetrical.
// //startIndex - an integer representing the "origin" node. This corresponds to the
//     //adjacency matrix (so if 4 is provided the node represented by the fifth row
//     //and column (0-based indexing) is used as the origin)
// //RETURNS: 
//     //an object of class Matrix. This essentially represents a subset of the original
//     //network that only includes the edges that make up the shortest paths to each 
//     //node. This can be used to find the shortest path from the origin to any other node.
//     //A shortest path from the origin to another node can be found by tracing the 
//     //path backwards - start with the destination node, and then use the adjacency matrix
//     //to identify the edges that lead back to the origin (because the resulting network
//     //is a tree with the origin as the root, there will only be one path back to the 
//     //origin)
// Matrix MathUtilities::getShortestPath(Matrix &adjMat, int startIndex, int endIndex){
//     assert(adjMat.nRow() == adjMat.nCol());
//     assert(startIndex < adjMat.nRow());
//     Matrix pathMat(NAN,adjMat.nRow(), adjMat.nCol());
//     // std::vector<double> nodeCosts(adjMat.nRow(),NAN);
//     std::vector<std::pair<int, double>> nodeCosts(adjMat.nRow());
//     nodeCosts.at(0) = std::pair<int, double>(startIndex,0);
//     int nodeCostsSize = 1;
//     bool keepGoing = true;
//     int counter = 0;
//     while(keepGoing){
//         // std::cout << "loop " << counter + 1 << "\n";
//         //order is node 1 id, node 2 id, cost of edge, total cost from origin to node 2
//         std::vector<std::tuple<int,int,double,double>> tempNodeCosts(adjMat.nRow()); //make a vector for storing the nodes (and total cost to reach those nodes) that we'll consider adding this iteration
//         int tempNodeCostsSize = 0; //because I'm initializing 'tempNodeCosts' to have size 'adjMat.nRow()' (to avoid having to use 'push_back') I need to keep track of how many things we've added to it
//         //for each node we've already added, find the other nodes that are "reachable" and calculate the total cost it takes to reach each of the 'reachable' nodes
//         for(int i = 0; i < nodeCostsSize; ++i){
//             // std::cout << "    subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//             //now we'll look at the column of 'adjMat' that tells us which nodes this node is connected to
//             for(int j = 0; j < adjMat.nRow(); ++j){
//                 // std::cout << "        subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//                 double edgeVal = adjMat.getValue(j,nodeCosts.at(i).first); //get the value for this 'edge'
//                 if(!std::isnan(edgeVal)){ //if the value is NAN then this isn't actually an edge
//                     //if the value isn't NAN then we need to check this edge
//                     //first we'll check to see if we've already included this node
//                     bool isAlreadyIncluded = false;
//                     for(size_t k = 0; k < nodeCostsSize; ++k){
//                         // std::cout << "            subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                         if(nodeCosts.at(k).first == j){
//                             // std::cout << "            k: " << k << "\n";
//                             // std::cout << "            nodeCosts.at(k).first: " << nodeCosts.at(k).first << "\n";
//                             // std::cout << "            nodeCosts.at(k).second: " << nodeCosts.at(k).second << "\n";
//                             // std::cout << "THROW OUT NODE " << j << "!\n";
//                             isAlreadyIncluded = true;
//                             break;
//                         }
//                         // std::cout << "            END subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                     }
//                     if(!isAlreadyIncluded){ //if we haven't added this node yet, we'll consider this edge as a possibility
//                         // std::cout << "INCLUDE NODE " << j << "!\n";
//                         // std::cout << "            add a node possibility\n";
//                         tempNodeCosts.at(tempNodeCostsSize) = std::tuple<int,int,double,double>(nodeCosts.at(i).first, j, edgeVal, nodeCosts.at(i).second + edgeVal);
//                         tempNodeCostsSize++;
//                         // std::cout << "            END add a node possibility\n";
//                     }
//                 }
//                 // std::cout << "        END subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//             }
//             // std::cout << "    END subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//         }
//         if(tempNodeCostsSize == 0){ //this means there are no nodes to add, so we're done
//             keepGoing = false;
//         } else {
//             //now we'll find which of the possible edges has the lowest total cost
//             int minIndex = 0; //index of the lowest cost edge
//             for(int i = 0; i < tempNodeCostsSize; ++i){
//                 // std::cout << "    subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//                 double cost = std::get<3>(tempNodeCosts.at(i)); //get the total cost for adding this edge (stored at the 4th spot in the tuple (index 3))
//                 if(cost < std::get<3>(tempNodeCosts.at(minIndex))){
//                     minIndex = i;
//                 }
//                 // std::cout << "    END subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//             }

//             // std::cout << "    add chosen node to nodeCost\n";
//             //add the node to our list of nodes that we've reached (along with the cost to get there)
//             nodeCosts.at(nodeCostsSize) = std::pair<int,double>(std::get<1>(tempNodeCosts.at(minIndex)), std::get<3>(tempNodeCosts.at(minIndex)));
//             nodeCostsSize++;

//             //add the edge to our matrix of edges
//             // std::cout << "    add node to matrix\n";
//             pathMat.setValue(std::get<2>(tempNodeCosts.at(minIndex)), std::get<1>(tempNodeCosts.at(minIndex)), std::get<0>(tempNodeCosts.at(minIndex)));
            
//             // std::cout << "    evaluate if we need to keep going\n";
            
//             if(nodeCostsSize > nodeCosts.size()){
//                 // std::cout << "    STOP!\n";
//                 keepGoing = false;
//             }

//             // std::cout << "END loop " << counter + 1 << "\n";
//             counter++;

            
//         }
//         std::cout << "==================\nnodeCosts\n";
//             for(int i = 0; i < nodeCostsSize; ++i){
//                 std::cout << "(id: " << nodeCosts.at(i).first << ", cost: " << nodeCosts.at(i).second << ")\n";
//             }
//         std::cout << "==================\n";
//     }
//     return pathMat;
// }







// int MathUtilities::addNextEdge(Matrix &adjMat, std::vector<std::pair<int,double>> &nodeCosts, int &nodeCostsSize, Matrix &pathMat){
    
//     // std::cout << "loop " << counter + 1 << "\n";
//     //order is node 1 id, node 2 id, cost of edge, total cost from origin to node 2
//     std::vector<std::tuple<int,int,double,double>> tempNodeCosts(adjMat.nRow()); //make a vector for storing the nodes (and total cost to reach those nodes) that we'll consider adding this iteration
//     int tempNodeCostsSize = 0; //because I'm initializing 'tempNodeCosts' to have size 'adjMat.nRow()' (to avoid having to use 'push_back') I need to keep track of how many things we've added to it
//     //for each node we've already added, find the other nodes that are "reachable" and calculate the total cost it takes to reach each of the 'reachable' nodes
//     for(int i = 0; i < nodeCostsSize; ++i){
//         // std::cout << "    subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//         //now we'll look at the column of 'adjMat' that tells us which nodes this node is connected to
//         for(int j = 0; j < adjMat.nRow(); ++j){
//             // std::cout << "        subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//             double edgeVal = adjMat.getValue(j,nodeCosts.at(i).first); //get the value for this 'edge'
//             if(!std::isnan(edgeVal)){ //if the value is NAN then this isn't actually an edge
//                 //if the value isn't NAN then we need to check this edge
//                 //first we'll check to see if we've already included this node
//                 bool isAlreadyIncluded = false;
//                 for(size_t k = 0; k < nodeCostsSize; ++k){
//                     // std::cout << "            subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                     if(nodeCosts.at(k).first == j){
//                         // std::cout << "            k: " << k << "\n";
//                         // std::cout << "            nodeCosts.at(k).first: " << nodeCosts.at(k).first << "\n";
//                         // std::cout << "            nodeCosts.at(k).second: " << nodeCosts.at(k).second << "\n";
//                         // std::cout << "THROW OUT NODE " << j << "!\n";
//                         isAlreadyIncluded = true;
//                         break;
//                     }
//                     // std::cout << "            END subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                 }
//                 if(!isAlreadyIncluded){ //if we haven't added this node yet, we'll consider this edge as a possibility
//                     // std::cout << "INCLUDE NODE " << j << "!\n";
//                     // std::cout << "            add a node possibility\n";
//                     tempNodeCosts.at(tempNodeCostsSize) = std::tuple<int,int,double,double>(nodeCosts.at(i).first, j, edgeVal, nodeCosts.at(i).second + edgeVal);
//                     tempNodeCostsSize++;
//                     // std::cout << "            END add a node possibility\n";
//                 }
//             }
//             // std::cout << "        END subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//         }
//         // std::cout << "    END subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//     }
//     if(tempNodeCostsSize == 0){ //this means there are no nodes to add, so we're done
//         return NAN;
//     } else {
//         //now we'll find which of the possible edges has the lowest total cost
//         int minIndex = 0; //index of the lowest cost edge
//         for(int i = 0; i < tempNodeCostsSize; ++i){
//             // std::cout << "    subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//             double cost = std::get<3>(tempNodeCosts.at(i)); //get the total cost for adding this edge (stored at the 4th spot in the tuple (index 3))
//             if(cost < std::get<3>(tempNodeCosts.at(minIndex))){
//                 minIndex = i;
//             }
//             // std::cout << "    END subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//         }

//         // std::cout << "    add chosen node to nodeCost\n";
//         //add the node to our list of nodes that we've reached (along with the cost to get there)
//         nodeCosts.at(nodeCostsSize) = std::pair<int,double>(std::get<1>(tempNodeCosts.at(minIndex)), std::get<3>(tempNodeCosts.at(minIndex)));
//         nodeCostsSize++;

//         //add the edge to our matrix of edges
//         // std::cout << "    add node to matrix\n";
//         pathMat.setValue(std::get<2>(tempNodeCosts.at(minIndex)), std::get<1>(tempNodeCosts.at(minIndex)), std::get<0>(tempNodeCosts.at(minIndex)));
        
//         return minIndex;
//     }
// }









// MathUtilities::ShortestPathFinder::ShortestPathFinder(Matrix &_adjMat, int _startIndex)
//     : fullySearched{false}, previouslyAddedNode{-1}, startIndex{_startIndex}{
//     //adjMat = std::shared_ptr<Matrix>(&_adjMat);
//     adjMat = &_adjMat;
//     pathMat = Matrix(NAN,adjMat->nRow(), adjMat->nCol());
//     // std::vector<double> nodeCosts(adjMat.nRow(),NAN);
//     nodeCosts = std::vector<std::pair<int, double>>(adjMat->nRow());
//     nodeCosts.at(0) = std::pair<int, double>(startIndex,0);
//     nodeCostsSize = 1;
//     // fullySearched = false;
//     // previouslyAddedNode = -1;

// }
// Matrix MathUtilities::ShortestPathFinder::getShortestPathNetwork(){
//     while(!fullySearched){
//         addNextEdge();
//     }
//     return pathMat;
// }
// std::vector<int> MathUtilities::ShortestPathFinder::getShortestPath(int endIndex){
//     int counter0{0};
//     while(!fullySearched){
//         if(counter0%100 == 0){
//             std::cout << "counter0: " << counter0 << "\n";
//         }
//         counter0++;
//         // std::cout << "---------\n";
//         // std::cout << "previouslyAddedNode: " << previouslyAddedNode << "\n";
//         // std::cout << "endIndex: " << endIndex << "\n";
//         if(previouslyAddedNode == endIndex){
//             break;
//             //std::cout << "break!\n";
//         }
//         previouslyAddedNode = addNextEdge();
//     }
//     int thisNode = endIndex;
//     int maxPathLength = pathMat.nRow();
//     //std::vector<int> shortestPath(maxPathLength);
//     std::vector<int> shortestPath(1);
//     shortestPath.at(0) = endIndex;
//     int counter{0};
//     //int nAdded{0};
//     while(thisNode != startIndex && counter <= maxPathLength){

//         for(int i = 0; i < pathMat.nCol(); ++i){
//             double val = pathMat.getValue(thisNode, i);
//             if(!isnan(val)){
//                 //std::cout << val << "\n";
//                 shortestPath.emplace_back(i);
//                 thisNode = i;
//                 //nAdded++;
//                 break;
//             }
//         }
//         counter++;
//     }
//     //std::reverse(shortestPath.begin(), shortestPath.end());
//     return shortestPath;
// }
// int MathUtilities::ShortestPathFinder::addNextEdge(){
//     // std::cout << "loop " << counter + 1 << "\n";
//     //order is node 1 id, node 2 id, cost of edge, total cost from origin to node 2
//     std::vector<std::tuple<int,int,double,double>> tempNodeCosts(adjMat->nRow()); //make a vector for storing the nodes (and total cost to reach those nodes) that we'll consider adding this iteration
//     int tempNodeCostsSize = 0; //because I'm initializing 'tempNodeCosts' to have size 'adjMat.nRow()' (to avoid having to use 'push_back') I need to keep track of how many things we've added to it
//     //for each node we've already added, find the other nodes that are "reachable" and calculate the total cost it takes to reach each of the 'reachable' nodes
//     for(int i = 0; i < nodeCostsSize; ++i){
//         // std::cout << "    subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//         //now we'll look at the column of 'adjMat' that tells us which nodes this node is connected to
//         for(int j = 0; j < adjMat->nRow(); ++j){
//             // std::cout << "        subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//             double edgeVal = adjMat->getValue(j,nodeCosts.at(i).first); //get the value for this 'edge'
//             if(!std::isnan(edgeVal)){ //if the value is NAN then this isn't actually an edge
//                 //if the value isn't NAN then we need to check this edge
//                 //first we'll check to see if we've already included this node
//                 bool isAlreadyIncluded = false;
//                 for(size_t k = 0; k < nodeCostsSize; ++k){
//                     // std::cout << "            subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                     if(nodeCosts.at(k).first == j){
//                         // std::cout << "            k: " << k << "\n";
//                         // std::cout << "            nodeCosts.at(k).first: " << nodeCosts.at(k).first << "\n";
//                         // std::cout << "            nodeCosts.at(k).second: " << nodeCosts.at(k).second << "\n";
//                         // std::cout << "THROW OUT NODE " << j << "!\n";
//                         isAlreadyIncluded = true;
//                         break;
//                     }
//                     // std::cout << "            END subsubsubloop " << k + 1 << " of " << nodeCosts.size() << "\n";
//                 }
//                 if(!isAlreadyIncluded){ //if we haven't added this node yet, we'll consider this edge as a possibility
//                     // std::cout << "INCLUDE NODE " << j << "!\n";
//                     // std::cout << "            add a node possibility\n";
//                     tempNodeCosts.at(tempNodeCostsSize) = std::tuple<int,int,double,double>(nodeCosts.at(i).first, j, edgeVal, nodeCosts.at(i).second + edgeVal);
//                     tempNodeCostsSize++;
//                     // std::cout << "            END add a node possibility\n";
//                 }
//             }
//             // std::cout << "        END subsubloop " << j + 1 << " of " << adjMat.nRow() << "\n";
//         }
//         // std::cout << "    END subloop " << i + 1 << " of " << nodeCostsSize << "(subloop1)\n";
//     }
//     if(tempNodeCostsSize == 0){ //this means there are no nodes to add, so we're done
//         fullySearched = true;
//         return -1;
//     } else {
//         //now we'll find which of the possible edges has the lowest total cost
//         int minIndex = 0; //index of the lowest cost edge
//         for(int i = 0; i < tempNodeCostsSize; ++i){
//             // std::cout << "    subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//             double cost = std::get<3>(tempNodeCosts.at(i)); //get the total cost for adding this edge (stored at the 4th spot in the tuple (index 3))
//             if(cost < std::get<3>(tempNodeCosts.at(minIndex))){
//                 minIndex = i;
//             }
//             // std::cout << "    END subloop " << i + 1 << " of " << tempNodeCostsSize << "(subloop2)\n";
//         }

//         // std::cout << "    add chosen node to nodeCost\n";
//         //add the node to our list of nodes that we've reached (along with the cost to get there)
//         nodeCosts.at(nodeCostsSize) = std::pair<int,double>(std::get<1>(tempNodeCosts.at(minIndex)), std::get<3>(tempNodeCosts.at(minIndex)));
//         nodeCostsSize++;

//         //add the edge to our matrix of edges
//         // std::cout << "    add node to matrix\n";
//         pathMat.setValue(std::get<2>(tempNodeCosts.at(minIndex)), std::get<1>(tempNodeCosts.at(minIndex)), std::get<0>(tempNodeCosts.at(minIndex)));
        
//         //return minIndex;
//         return std::get<1>(tempNodeCosts.at(minIndex));
//     }
// }






// std::vector<std::shared_ptr<Node>> getShortestPathQuadtree(std::shared_ptr<Node> startNode, std::shared_ptr<Node> endNode, double xMin, double xMax, double yMin, double yMax){

// }


// struct NodeEdge{
//     int id;
//     std::shared_ptr<Node> node;
//     std::shared_ptr<NodeEdge> parent;
//     std::shared_ptr<NodeEdge> self;
//     double cost;
//     int nNodesFromOrigin;
//     //std::vector<std::shared_ptr<NodeEdge>> possibleEdges;
// };

// std::shared_ptr<NodeEdge> makeNodeEdge(int id, std::shared_ptr<Node> node, std::shared_ptr<NodeEdge> parent){
//     //std::vector<std::shared_ptr<NodeEdge>> possibleEdges;
//     //NodeEdge *ne = new NodeEdge{node, parent,nullptr, possibleEdges};
//     NodeEdge *ne = new NodeEdge{id, node, parent, nullptr,0,0};
//     ne->self = std::shared_ptr<NodeEdge>(ne);
//     return ne->self;
// }
//     // std::list<std::shared_ptr<Agent>>::iterator i = agents.begin();
//     // //https://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it/596180#596180
//     // //int debugCounter{0};
//     // while (i != agents.end()) {
//     // for(auto &iAgent : agents){
//     //     iAgent->age = iAgent->age + 1;
//     // }
// std::vector<std::shared_ptr<Node>> MathUtilities::getShortestPathQuadtree(Quadtree &qt, Point startPoint, Point endPoint, double xMin, double xMax, double yMin, double yMax){
//     std::shared_ptr<Node> startNode = qt.getNode(startPoint.x, startPoint.y);
//     std::shared_ptr<Node> endNode = qt.getNode(endPoint.x, endPoint.y);
    
//     std::list<std::shared_ptr<Node>> nodes = qt.getNodesInBox(xMin, xMax, yMin, yMax);
//     std::vector<std::shared_ptr<NodeEdge>> nodeEdges(nodes.size());
//     //std::list<std::shared_ptr<Node>>::iterator itr = nodes.begin();
//     //for(size_t i = 0; i < nodes.size(); ++i){
//     int counter{0};
//     for(auto &iNode : nodes){
//         nodeEdges.at(counter) = makeNodeEdge(counter, iNode, nullptr);
//         //nodeEdges.emplace_back(makeNodeEdge(iNode, nullptr));
//         counter++;
//     }
    
//     std::map<int, int> dict; //dictionary with Node ID's as the key and the index of the corresponding 'NodeEdge' in 'nodeEdges'
//     //int counter{0};
//     for(size_t i = 0; i < nodeEdges.size(); ++i){
//         //if(!std::isnan(nodeEdges.at(i)->node->value)){
//             dict[nodeEdges.at(i)->node->id] = i; 
//         //}
//     }

//     // std::set<std::tuple<int,int,double>> possibleEdges; //order of tuple is nodeEdge ID 1, nodeEdge ID 2, dist between the nodes
//     //https://stackoverflow.com/questions/2620862/using-custom-stdset-comparator
//     auto cmp = [](std::tuple<int,int,double> a, std::tuple<int,int,double> b) { return std::get<2>(a) < std::get<2>(b); };
//     std::multiset<std::tuple<int,int,double>, decltype(cmp)> possibleEdges(cmp);
//     //getShortestPathRecurse(startNodeEdge);

//     //std::shared_ptr<NodeEdge> startNodeEdge = nodeEdges.at(dict[startNode->id]);
//     //std::shared_ptr<NodeEdge> nodeEdge = startNodeEdge;


//     possibleEdges.insert(std::make_tuple(dict[startNode->id],dict[startNode->id], 0)); //initialize our set with the start node
//     //////////////////////////////////////////
//     //bool keepGoing = true;
//     while(possibleEdges.size() != 0){ //if possibleEdges is 0 then we've added all the edges possible and we're done
//         //std::tuple<int,int,double> edgeToAdd = possibleEdges.
//         auto beginItr = possibleEdges.begin();
//         std::shared_ptr<NodeEdge> nodeEdge = nodeEdges.at(std::get<1>(*beginItr)); //get the edge that's at the front of the set
//         if(nodeEdge->parent){
//             possibleEdges.erase(beginItr); 
//         } else { //if the destination node already has a pointer for parent, it's already been included, so we'll skip this one - otherwise we'll set the 'parent' property of the destination node and then add the additional edge possibilities that result    
//             nodeEdge->parent = nodeEdges.at(std::get<0>(*beginItr)); //set the parent of the destination node to be the source node
//             nodeEdge->cost = std::get<2>(*beginItr); //assign the distance to the NodeEdge
//             nodeEdge->nNodesFromOrigin = nodeEdge->parent->nNodesFromOrigin + 1; //add 1 to the number of nodes from the origin of the parent
//             possibleEdges.erase(beginItr); //remove this edge from the list of possibilities

//             if(nodeEdge->node->id == endNode->id){ //if the NodeEdge we just modified is the endNode, then we're done, and we can stop
//                 break;
//             }
//             //now we'll add the edges corresponding to this nodes neighbors
//             Point nodePoint = Point((nodeEdge->node->xMin + nodeEdge->node->xMax)/2, (nodeEdge->node->yMin + nodeEdge->node->yMax)/2); //make the point for the node by getting its centroid
//             for(size_t i = 0; i < nodeEdge->node->neighbors.size(); ++i){ //loop over each of its neighbors
//                 //if(nodeEdge)
//                 std::map<int,int>::iterator itr = dict.find(nodeEdge->node->neighbors.at(i)->id); //see if this neighbor is included in our dictionary - if not, then it must fall outside the extent
//                 if(itr != dict.end()){
//                     //int nodeEdgeIndex = itr->second;
//                     std::shared_ptr<NodeEdge> nodeEdgeNb = nodeEdges.at(itr->second);
//                     if(!(nodeEdgeNb->parent) && !std::isnan(nodeEdge->node->value)){ //check if this node already has a parent assigned i.e. has already been included in the network, or if this node is NAN
//                         Point nbPoint = Point((nodeEdgeNb->node->xMin + nodeEdgeNb->node->xMax)/2, (nodeEdgeNb->node->yMin + nodeEdgeNb->node->yMax)/2);
//                         //get cost for path - to do that we need to know the length of the segment in each cell

//                         //first, figure out which side the two cells are adjacent on - this'll give us one coordinate for the intersection point (whether we know the x or y depends on which side they're adjacent on)
//                         double mid{0};
//                         bool isX = true; //tells us whether mid coordinate is an x-coordinate or a y-coordinate
//                         if(nodeEdge->node->xMin == nodeEdgeNb->node->xMax){ //left side
//                             mid = nodeEdge->node->xMin;
//                         } else if(nodeEdge->node->xMax == nodeEdgeNb->node->xMin) { //right side
//                             mid = nodeEdge->node->xMax;
//                         } else if(nodeEdge->node->yMin == nodeEdgeNb->node->yMax) { //bottom
//                             mid = nodeEdge->node->yMin;
//                             isX = false;
//                         } else if(nodeEdge->node->yMax == nodeEdgeNb->node->yMin) { //top
//                             mid = nodeEdge->node->yMax;
//                             isX = false;
//                         }


//                         double dist = MathUtilities::distBtwPoints(nodePoint, nbPoint); //get the distance between the two centroids
//                         double ratio{0};
//                         //now get the ratio between {the difference of the known (x or y) mid-coordinate and the (x or y) coordinate of the starting point} and {the difference in the (x or y) coordinates of the two centroids}
//                         if(isX){
//                             double deltaX = nodePoint.x - nbPoint.x; //get the difference of the x coords
//                             ratio = (mid-nodePoint.x)/deltaX;
//                         } else {
//                             double deltaY = nodePoint.y - nbPoint.y; //get the difference of the y coords
//                             ratio = (mid-nodePoint.y)/deltaY;
//                         }

//                         //use the ratio we just calculated to get the length of the segment in each cell
//                         double dist1 = dist*ratio;
//                         double dist2 = dist - dist1;

//                         //use those distances to get the cost, weighted by the length of the segment in each cell. Add this to the cost to get to 'nodeEdge' to get the total cost from the origin
//                         double cost = dist1*(nodeEdge->node->value) + dist2*(nodeEdgeNb->node->value) + nodeEdge->cost; 
                        
//                         possibleEdges.insert(std::make_tuple(nodeEdge->id, nodeEdgeNb->id, cost));
//                     }
//                 }
//             }
//         }
//     }

//     //now we just need to return the path to the end node as a vector
//     std::shared_ptr<NodeEdge> currentNodeEdge = nodeEdges.at(dict[endNode->id]);
//     std::vector<std::shared_ptr<Node>> nodePath(currentNodeEdge->nNodesFromOrigin+1);
//     //for(size_t i = (nodePath.size()-1); i >= 0; --i){
//     for(size_t i = 1; i <= nodePath.size(); ++i){
//         //nodePath.at(i) = currentNodeEdge->node;
//         nodePath.at(nodePath.size()-i) = currentNodeEdge->node;
//         currentNodeEdge = currentNodeEdge->parent;
//     }
//     return nodePath;


// }

// std::vector<std::shared_ptr<Node>> MathUtilities::getShortestPathQuadtree(Quadtree &qt, Point startPoint, Point endPoint){
//     return getShortestPathQuadtree(qt,startPoint, endPoint, qt.root->xMin, qt.root->xMax, qt.root->yMin, qt.root->yMax);
// }





































// void getShortestPathRecurse(std::shared_ptr<NodeEdge> nodeEdge, std::vector<std::shared_ptr<NodeEdge>> &nodeEdgeVec, std::map<int, int> &dict, std::multiset<std::tuple<int,int,double>> possibleEdges){
    
//     Point nodePoint = Point((nodeEdge->node->xMin + nodeEdge->node->xMax)/2, (nodeEdge->node->yMin + nodeEdge->node->yMax)/2);
//     for(size_t i = 0; i < nodeEdge->node->neighbors.size(); ++i){
//         std::map<int,int>::iterator itr = dict.find(nodeEdge->node->neighbors.at(i)->id);
//         if(itr != dict.end()){
//             //int nodeEdgeIndex = itr->second;
//             std::shared_ptr<NodeEdge> nodeEdgeNb = nodeEdgeVec.at(itr->second);
//             if(!(nodeEdgeNb->parent)){
//                 Point nbPoint = Point((nodeEdgeNb->node->xMin + nodeEdgeNb->node->xMax)/2, (nodeEdgeNb->node->yMin + nodeEdgeNb->node->yMax)/2);
//                 double dist = MathUtilities::distBtwPoints(nodePoint, nbPoint);
//                 possibleEdges.insert(std::make_tuple(nodeEdge->id, nodeEdgeNb->id, dist));
//             }
//         }
//     }
// }
// void addNextEdge(std::list<std::shared_ptr<NodeEdge>> nodeOptions){
//     std::shared_ptr<NodeEdge> newEdge = nodeOptions.front();
//     nodeOptions.pop_front();

// }

// void NodeNetwork::getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>>> &networkNodes){




// Matrix MathUtilities::getAdjMatFromQuadtree(Quadtree& qt, double xMin, double xMax, double yMin, double yMax){
//     //check the bounds and correct if necessary - I don't think this'll work if the provided boundaries don't overlap at all with the quadtree extent...
//     if(xMin < qt.root->xMin) xMin = qt.root->xMin;
//     if(xMax > qt.root->xMax) xMax = qt.root->xMax;
//     if(yMin < qt.root->yMin) yMin = qt.root->yMin;
//     if(yMax > qt.root->yMax) yMax = qt.root->yMax;

//     //this might be the craziest nested object I've ever made
//     std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>*>> networkNodes;
//     std::shared_ptr<Node> startNode = qt.getNode((xMin+xMax)/2, (yMin+yMax)/2);
//     getConnectedNodes(startNode, xMin, xMax, yMin, yMax, networkNodes);

//     //make a map so that we can link the node ID's to the IDs of the network
//     std::map<int, int> nodeDict;
//     for(size_t i = 0; i < networkNodes.size(); ++i){
//         nodeDict[networkNodes.at(i).first->id] = i; 
//     }

//     //Loop through 'networkNodes', and use the neighbors and distances we calculated to populate 'adjMat'
//     Matrix adjMat(NAN,networkNodes.size(), networkNodes.size());
//     for(size_t i = 0; i < networkNodes.size(); ++i){
//         std::vector<std::pair<std::shared_ptr<Node>,double>>* nbs = networkNodes.at(i).second; //for code readability
//         for(size_t j = 0; j < nbs->size(); ++j){
//             adjMat.setValue(nbs->at(j).second, nodeDict[networkNodes.at(i).first->id], nodeDict[nbs->at(j).first->id]);
//             adjMat.setValue(nbs->at(j).second, nodeDict[nbs->at(j).first->id], nodeDict[networkNodes.at(i).first->id]);
//         }
//     }


//     //qt.getNode()
// }
// void MathUtilities::getConnectedNodes(std::shared_ptr<Node> node, double xMin, double xMax, double yMin, double yMax, std::vector<std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>*>> &networkNodes){
//     if(node->neighbors.size() > 0){
//         std::vector<std::pair<std::shared_ptr<Node>,double>> *nodeDists;
//         Point nodePoint = Point((node->xMax + node->xMin)/2, (node->yMax + node->yMin)/2); //get the point representing the center of 'node'
//         for(size_t i = 0; i < node->neighbors.size(); ++i){
//             std::shared_ptr<Node> nb = node->neighbors.at(i); //this is to make the code more readable
//             bool isAlreadyIncluded = false;
//             for(size_t j = 0; j < networkNodes.size(); ++j){ //check to see if we've already added this node
//                 if(networkNodes.at(j).first->id == nb->id){
//                     isAlreadyIncluded = true;
//                     break;
//                 }
//             }
//             //if(std::find(networkNodes.begin(), networkNodes.end(), nb) == networkNodes.end()){ //check if this node is already included
//             if(!isAlreadyIncluded){ //check if this node is already included
//                 bool isXValid = !(xMax < nb->xMin || xMin > nb->xMax);
//                 bool isYValid = !(yMax < nb->yMin || yMin > nb->yMax);
//                 if(isXValid && isYValid){
//                     Point nbPoint = Point((nb->xMax + nb->xMin)/2, (nb->yMax + nb->yMin)/2); //get the point representing the center of 'node'
//                     double distBtwCells = distBtwPoints(nodePoint, nbPoint); //get the distance btw the nodes
//                     nodeDists->emplace_back(std::pair<std::shared_ptr<Node>,double>(nb,distBtwCells)); //add the distance to the 
//                     // networkNodes.emplace_back(node->neighbors.at(i));
//                     getConnectedNodes(node->neighbors.at(i), xMin, xMax, yMin, yMax, networkNodes);
//                 }
//             }
//         }
//         networkNodes.emplace_back(std::pair<std::shared_ptr<Node>,std::vector<std::pair<std::shared_ptr<Node>,double>>*>(node,nodeDists));
//     }
// }








































