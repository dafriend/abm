#include "BasicUtilities.h"

#include <string>
#include <algorithm>
#include <cctype>
#include <locale>
#include <filesystem>
#include <iostream>
#include <date/date.h>
#include <vector>

//NOTE: all of the trim functions are from this StackOverflow answer:
//https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
// trim from start (in place)
void BasicUtilities::ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void BasicUtilities::rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void BasicUtilities::trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// // trim from start (copying)
// static inline std::string BasicUtilities::ltrim_copy(std::string s) {
//     ltrim(s);
//     return s;
// }

// // trim from end (copying)
// static inline std::string BasicUtilities::rtrim_copy(std::string s) {
//     rtrim(s);
//     return s;
// }

// // trim from both ends (copying)
// static inline std::string BasicUtilities::trim_copy(std::string s) {
//     trim(s);
//     return s;
// }


bool BasicUtilities::invalidChar (char c) 
{  
    return !(c>=0 && c <128);   
} 
void BasicUtilities::stripUnicode(std::string & str) 
{ 
    str.erase(remove_if(str.begin(),str.end(), invalidChar), str.end());  
}


//std::string BasicUtilities::cleanString(std::string & str){
void BasicUtilities::cleanString(std::string & str){
    //http://www.cplusplus.com/forum/general/76900/ - remove any unknown characters
    //str.resize(remove_if(str.begin(), str.end(),[](char x){return !isalnum(x) && !isspace(x);})-str.begin());
    //https://stackoverflow.com/questions/2528995/remove-r-from-a-string-in-c
    //str.erase( std::remove(str.begin(), str.end(), '\r'), str.end() );
    stripUnicode(str);
    trim(str);
    //return str;
}



//given a folder, sees if there are any folders in there with the current date in a specific format.
//if not, it creates a folder with the date (year_month_day_1) and the number 1 at the end. If there
//are already folders with that date, it produces a name in the same format but where the final number
//is one higher than the highest number among the other folders for that date (for example, 2021_03_01_6,
//meaning it's the sixth simulation run on March 1.)
std::string BasicUtilities::getNewFolderName(std::string folderName){
    namespace fs = std::filesystem;
    //https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c

    auto today = date::year_month_day{date::floor<date::days>(std::chrono::system_clock::now())};
    std::string year = date::format("%Y", today);
    std::string month = date::format("%m", today);
    std::string day = date::format("%d", today);
    //std::string path = "/Users/dfriend/Documents/cpp/abm/output/sim_results";
    std::string path = folderName;
    std::vector<std::string> ymd{year,month,day};
    //std::cout << year << "\n";
    //std::cout << month << "\n";
    //std::cout << day << "\n";

    int dateNumber{0};
    for (const auto & entry : fs::directory_iterator(path)){
        //std::string s = "scott>=tiger>=mushroom";
        // std::cout << "==============\n";
        std::string s = entry.path().stem().string();
        // std::cout << s << std::endl;
        std::string delimiter = "_";

        size_t pos = 0;
        std::string token;
        int counter{0};
        bool previousValueWasValid = true;
        //bool hasFolderWithDate = false; //will be true if a folder already exists with the current date.
        
        while ((pos = s.find(delimiter)) != std::string::npos) {
            // std::cout << "----------\n";
            // std::cout << "counter: " << counter << "\n";
            token = s.substr(0, pos);
            if((token == ymd.at(counter)) && previousValueWasValid){
                previousValueWasValid = true;
                // std::cout << "valid!\n";
            } else {
                previousValueWasValid = false;
            } 
            
            // std::cout << token << std::endl;

            s.erase(0, pos + delimiter.length());
            counter++;
        }
        if(counter == 3 && previousValueWasValid){
            // std::cout << "dateNumber check \n";
            if(std::stoi(s) > dateNumber){
                // std::cout << "s > dateNumber\n";
                // std::cout << "s: " << std::stoi(s) << "\n";
                dateNumber = std::stoi(s);
            }
        }
        // std::cout << "----------\n";
        // std::cout << entry.path() << std::endl;
    }
    dateNumber++;
    std::string num(std::to_string(dateNumber));
    std::string numPadded = std::string(3 - num.length(), '0') + num;
    std::string newFolderName = year + "_" + month + "_" + day + "_" + numPadded;
    //std::cout << folder << "\n";
    return newFolderName;
}

std::string BasicUtilities::combineStrings(std::vector<std::string> strings){
    std::string s;
    if(strings.size() > 0){
        s += strings.at(0);
        for (size_t i = 1; i < strings.size(); ++i){
            s += ", " + strings.at(i);
        }   
    }
    return s;
}

//returns 1 for a, 2 for b, etc.
int BasicUtilities::charToInt(char c){
    return (int)c-96; //97 is "a" in ASCII
}

//returns a for 1, b for 2, etc.
char BasicUtilities::intToChar(int num){
    return static_cast<char>(num+96);
}
// modified from https://www.oreilly.com/library/view/c-cookbook/0596007612/ch04s12.html
void BasicUtilities::removeSubstrs(std::string& s, const std::string& p) {
    size_t n = p.length();

    for (size_t i = s.find(p); i != std::string::npos; i = s.find(p)){
        s.erase(i, n);
    }
}


std::vector<int> BasicUtilities::makeIntSequence(int min, int max){
    std::vector<int> intVec(max - min + 1);
    int index{0};
    for(int i = min; i <= max; ++i){
        intVec.at(index) = i;
        index++;
    }
    return intVec;
}