#include "Raster.h"
#include <memory>
#include <string>
#include <cassert>
#include <sstream>
#include <iomanip>
//#include <thread>
#include <iostream>
#include <cmath>

//#include <Rcpp.h> //only needed for debugging purposes - can be removed eventually

//using namespace Rcpp; //only needed for debugging purposes - can be removed eventually

Raster::Raster()
  :nX{0}, nY{0}, xMin{0}, xMax{0}, yMin{0}, yMax{0}, xCellLength{0}, yCellLength{0}{
    //if(DEBUG) cout << "Raster()"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    values = std::shared_ptr<std::vector<double>>(new std::vector<double>(0,0));
}

Raster::Raster(int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax) 
  : nX{_nX}, nY{_nY}, xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax} {      
    //if(DEBUG) cout << "Raster(int _nX, int _nY)"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    xCellLength = (xMax-xMin)/nX;
    yCellLength = (yMax-yMin)/nY;
    values = std::shared_ptr<std::vector<double>>(new std::vector<double>(nX*nY, 0));
}

// Raster::Raster(std::vector<std::shared_ptr<Point>> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)
//   : nX{_nX}, nY{_nY}, xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax}  {
Raster::Raster(std::vector<double> _values, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)
  : Raster(_nX, _nY, _xMin, _xMax, _yMin, _yMax) {

    assert((int)_values.size() == nX*nY);
    //if(DEBUG) cout << "Raster(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));

    //calculate some of the basic info about the Raster we're going to make
    xCellLength = (xMax-xMin)/nX;
    yCellLength = (yMax-yMin)/nY;

    values = std::shared_ptr<std::vector<double>>(new std::vector<double>(nX*nY, 0));
    for(size_t i = 0; i < values->size(); ++i){
        values->at(i) = _values.at(i);
    }
}

Raster::Raster(std::shared_ptr<std::vector<double>> _values, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)
  : Raster(_nX, _nY, _xMin, _xMax, _yMin, _yMax) {

    assert((int)_values->size() == nX*nY);
    //if(DEBUG) cout << "Raster(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));

    //calculate some of the basic info about the Raster we're going to make
    xCellLength = (xMax-xMin)/nX;
    yCellLength = (yMax-yMin)/nY;
    values = _values;
}
//gets the index based on the ROW/COL (NOT coordinates)
int Raster::getIndex(int x, int y) const{
    if(x >= 0 && x < nX && y >= 0 && y < nY){ //check to make sure the indices are valid
      int index = y*nX + x; 
      return index;
    } else {
      std::cout << "Raster::getIndex(int x, int y) -> invalid index (x:" << x << ", y:" << y << "). Raster size is " << nX <<" by "<<nY << " (nX by nY). Returning -1.";
      return -1;
  }
}

//gets the index based on the COORDINATES (NOT row/col)
int Raster::getIndex(double x, double y) const{
  int xIndex = floor((x-xMin)/xCellLength);
  int yIndex = floor((y-yMin)/yCellLength);
  if(x == xMax) xIndex = nX-1;
  if(y == yMax) yIndex = nY-1;
  return getIndex(xIndex, yIndex);
}

//gets the cell based on the ROW/COL (NOT coordinates)
double Raster::getValue(int x, int y) const{
  int index = getIndex(x,y);
  if(index != -1){
    return values->at(index);
  } else {
    throw std::runtime_error(std::string("in 'Raster::getValue(int x, int y) const': index value of ") + std::to_string(index) + std::string(" is invalid"));
  }
}

//gets the cell based on the COORDINATES (NOT row/col)
double Raster::getValue(double x, double y) const{
  int index = getIndex(x,y);
  if(index != -1){
    return values->at(index);
  } else {
    throw std::runtime_error(std::string("in 'Raster::getValue(double x, double y) const': index value of ") + std::to_string(index) + std::string(" is invalid"));
  }
  //if(DEBUG) cout << "getCell(double x, double y)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  // //figure out the indices of the cell that contains the point
  // int xIndex = floor((x-xMin)/xCellLength);
  // int yIndex = floor((y-yMin)/yCellLength);
  // return getCell(xIndex, yIndex); //use those indices to get the cell
}

Point Raster::getCoords(int x, int y) const{
  double xCoord = xMin + x*xCellLength + xCellLength/2;
  double yCoord = yMin + y*yCellLength + yCellLength/2;
  return Point(xCoord, yCoord);
}

Point Raster::getCoords(int index) const{
  int x = index % nX;
  int y = std::floor(index/nX);
  return getCoords(x,y);
}

std::vector<Point> Raster::getPoints() const {
  std::vector<Point> points(values->size());
  for(size_t i = 0; i < values->size(); ++i){
    points.at(i) = getCoords(i);
  }
  return points;
}

void Raster::setValue(int index, double value){
    values->at(index) = value;
}

void Raster::setValue(int x, int y, double value){
    int index = getIndex(x,y);
    values->at(index) = value;
}

void Raster::setValue(double x, double y, double value){
    int index = getIndex(x,y);
    values->at(index) = value;
}

std::string Raster::toString(int precision) const{
    
    int max_size{0};

    for(size_t i = 0; i < values->size(); ++i){
      std::stringstream stream;
      stream << std::fixed << std::setprecision(precision) << values->at(i);
      std::string s = stream.str();
      if((int)s.length() > max_size){
          max_size = s.length();
      }
    }

    std::string str("");
    // str = str + 
    //   "nX: " + std::to_string(nX) + "\n" +
    //   "nY: " + std::to_string(nY) + "\n" +
    //   "xMin: " + std::to_string(xMin) + "\n" +
    //   "xMax: " + std::to_string(xMax) + "\n" +
    //   "yMin: " + std::to_string(yMin) + "\n" +
    //   "yMax: " + std::to_string(yMax) + "\n" +
    //   "xCellLength: " + std::to_string(xCellLength) + "\n" +
    //   "yCellLength: " + std::to_string(yCellLength) + "\n" +
    //   "nPoints: " + std::to_string(nPoints) + "\n";
    std::string hline(max_size*nX + nX + 1, '-');
    for(int i_y = nY-1; i_y >= 0; --i_y){
        str = str + hline + "\n" + "|";
        //s.append(20 - s.length(), 'X');
        for(int i_x = 0; i_x < nX; ++i_x){
            double val_i = getValue(i_x,i_y);
            std::stringstream stream;
            stream << std::fixed << std::setprecision(precision) << val_i;
            std::string s = stream.str();
            s.insert(s.begin(), max_size - s.length(), ' ');
            str = str + s + "|";
        }
        str = str + "\n";
    }
    str = str + hline + "\n";
    return str;
}

// +--+--+--+
// |45| 0|20|
// +--+--+--+
// |34| 1|30|
// +--+--+--+
// | 0|98|11| 
// +--+--+--+