#ifndef POINTGRID_H
#define POINTGRID_H

//#include "Point.h"
//#include "PointCell.h"
#include <memory>
#include <vector>
#include <string>
#include <list>

// class Point;

//class AgentPoint;

class Agent;

class PointCell;

class PointGrid{
  public:                                          // 5 6 7 8 9
    std::vector<std::shared_ptr<PointCell> > cells; // 0 1 2 3 4   
    
    int nX;
    int nY;
    double xMin;
    double xMax;
    double yMin;
    double yMax;
    double xCellLength;
    double yCellLength;
    //int nPoints;
    
    PointGrid();
    PointGrid(int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    PointGrid(std::list<std::shared_ptr<Agent>> &points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    PointGrid(std::list<std::shared_ptr<Agent>> &points, double cellSize);
    PointGrid(std::vector<std::shared_ptr<PointCell>> &_cells, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax, int _nPoints);
    
    //void addPoint(std::shared_ptr<AgentPoint> point);
    std::shared_ptr<PointCell> addPoint(std::shared_ptr<Agent> point);
    void addPoints(std::vector<std::shared_ptr<Agent> > &points);
    void initializePoints(std::list<std::shared_ptr<Agent> > &points);
    void initializePointsAdultMales(std::list<std::shared_ptr<Agent> > &points, int adultAge);
    //void initializePoints2(std::vector<std::shared_ptr<AgentPoint>> points);
    //void addPoints2(std::vector<std::shared_ptr<AgentPoint>> points);

    int getIndex(int x, int y) const;
    int getIndex(double x, double y) const;
    std::shared_ptr<PointCell> getCell(int x, int y) const;
    std::shared_ptr<PointCell> getCell(double x, double y) const;
    PointGrid getCellsByIndex(int _xMin, int _xMax, int _yMin, int _yMax) const;
    PointGrid getCells(double _xMin, double _xMax, double _yMin, double _yMax) const;
    
    int nPointsWithin(double x, double y, double dist) const;
    
    // std::vector<std::shared_ptr<Agent> > getPointsWithin(double x, double y, double dist) const; // 2/19/2021 d
    std::list<std::shared_ptr<Agent> > getPointsWithin(double x, double y, double dist) const; // 2/19/2021 i
    std::vector<std::shared_ptr<Agent> > getPointsWithin_Vector(double x, double y, double dist) const; // 2/19/2021 i
    std::string toString() const;
};


#endif
