#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Point.h"

#include <string>
#include <vector>

class Rectangle
{ 
public:
    double xMin{0};
    double xMax{0};
    double yMin{0};
    double yMax{0};
    std::string id{""};

    Rectangle();
    Rectangle(double _xMin, double _xMax, double _yMin, double _yMax, std::string _id);
    std::string toString();
    // bool containsPoint(const Point &pt);

    // static bool isPointInRectangles(const Point &pt, const std::vector<Rectangle> &rects);
    static std::string isPointInRectangles(const Point &pt, const std::vector<Rectangle> &rects);
    static std::vector<Rectangle> readRectanglesFromCSV(std::string filePath);
};

// class Rectangles
// {
// public:
//     std::vector<Rectangle> rectangles;
// }
#endif
