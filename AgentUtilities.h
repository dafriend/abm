#ifndef AGENTUTILITIES_H
#define AGENTUTILITIES_H

#include "Agent.h"
#include "Raster.h"
#include <list>
#include <memory>


namespace AgentUtilities{
    Raster quadratCount(const std::list<std::shared_ptr<Agent>> &points, const int nX, const int nY, const double xMin, const double xMax, const double yMin, const double yMax);
    Raster quadratCount(const std::list<std::shared_ptr<Agent>> &points, const double cellSize, const double xMin, const double xMax, const double yMin, const double yMax);
    Raster quadratCount(const std::list<std::shared_ptr<Agent>> &points, const double cellSize);

    Raster pointDensity(const std::list<std::shared_ptr<Agent>> &points, const double bandwidth, const int nX, const int nY, const double xMin, const double xMax, const double yMin, const double yMax);
    Raster pointDensity(const std::list<std::shared_ptr<Agent>> &points, const double bandwidth, const int cellSize, const double xMin, const double xMax, const double yMin, const double yMax);
}
#endif