#include "Quadtree.h"
#include "Node.h"
#include "PointGrid.h"
#include "PointCell.h"
#include "Simulation.h"
#include "MathUtilities.h"
#include "PointUtilities.h"
#include "AgentUtilities.h"
#include "Matrix.h"
#include "Point.h"
#include "Agent.h"
#include "ProbabilityTable.h"
#include "Rectangle.h"
// #include "SimUtilities.h"
//#include "ProbabilityTable.cpp"
#include "Raster.h"
//#include "AgentPoint.h"
//#include "quadtree/matrix.h"
#include "BasicUtilities.h"
//#include "NodeNetwork.h"
#include "LcpFinder.h"
#include <iostream>
#include <ostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <string>
#include <chrono>
#include <random>
#include <filesystem>
#include <limits>

#include <cereal/archives/portable_binary.hpp>
#include <cereal/archives/xml.hpp>
#include <cereal/types/memory.hpp>
#include <date/date.h>

namespace fs = std::filesystem;
int main(int argc, char* argv[]){
// int mainMain(int argc, char* argv[]){
    
    if(argc <= 1){
        std::cout << "a filepath must be provided to a CSV containing the simulation parameters\n";
        return 0;
    } 
    std::string path = argv[1];
    std::string simName;

    std::cout << "\nreading parameters from: " << path << "\n";
    SimulationParameters sp = Simulation::readParams(path);
    if(argc >= 3){
        if(strcmp(argv[2], "-restart") == 0){
            if(argc < 4){
                std::cout << "two arguments must follow '-restart', in this order:\n* the path to the starting agents CSV\n* the path to the starting iteration info CSV\n";
                return 0;
            }
            std::string agentsPath(argv[3]);
            std::string iterInfoPath(argv[4]);
            Simulation sim(sp, agentsPath, iterInfoPath);
            sim.runSim(true);
            return 0;
        } else {
            sp.simFolder = argv[2];
        }
    } else if(sp.simFolder == ""){
        sp.simFolder = BasicUtilities::getNewFolderName(sp.simParentFolderPath);
    }
    // std::string newFolder = sp.simParentFolderPath + folderName;
    // std::cout << "\noutput folder: " << newFolder << "\n";
    // fs::create_directory(newFolder);
    // std::string outputFolder = folderName + "/output_data";
    // fs::create_directory(outputFolder);
    // sp.simOutputFolder = folderName + "/";
    
    Simulation sim(sp);
    //sim.runSim(1,true);
    //sim.runSim(1);
    sim.runSim();
    return 0;
}

int moveNeighborMain(){
// int main(){
    std::string filePath = "/Users/dfriend/OneDrive/Documents/Dissertation/Chapters/Ch2/data/resist_surfaces/mojave_quadtrees/mojave_range_.1_cpp.qtree";
    std::shared_ptr<Quadtree> qt = Quadtree::readQuadtree(filePath);
    std::cout << "finished reading quadtree\n";

    Point pt1(472498.775176062, 3863088.48442858);
    Point pt2(473944.024761548, 3864533.73401407);

    // Point pt1(472009.164007343, 3862170.51972112);
    // Point pt2(474525.986951565, 3862209.04252129);

    std::shared_ptr<Node> node1 = qt->getNode(pt1);
    std::shared_ptr<Node> node2 = qt->getNode(pt2);
    double maxCost = 500;
    Point finalPt = Simulation::moveAgentToNeighbor(pt1, node1, pt2, node2, maxCost);
    std::cout << finalPt.toString() << "\n";
    return 0;
}

// int main(){
int rectangleMain(){
    std::string filePath = "/Users/dfriend/OneDrive/Documents/Dissertation/Chapters/Ch2/data/connectivity_plots/csv_all_plots.csv";
    auto rects = Rectangle::readRectanglesFromCSV(filePath);
    for(size_t i = 0; i < rects.size(); i++){
        std::cout << rects[i].toString() << "\n";
    }

    Point pt1(0,0);
    Point pt2(654520, 3953900);
    Point pt3(665700, 3958500);
    std::cout << pt1.toString() << "\n";
    std::cout << Rectangle::isPointInRectangles(pt1, rects) << "\n\n"; // should be false
    
    std::cout << pt2.toString() << "\n";
    std::cout << Rectangle::isPointInRectangles(pt2, rects) << "\n\n"; // should be true
    
    std::cout << pt3.toString() << "\n";
    std::cout << Rectangle::isPointInRectangles(pt3, rects) << "\n\n"; // should be false
    return 0;
}

//tests the 'AgentUtilities::pointDensity' function
int pointDensityMain(){
//int main(){    
    //std::string filePath = "/Users/dfriend/Documents/cpp/abm/scratch_data/bei500k.csv";
    std::string filePath = "/Users/dfriend/Documents/cpp/abm/scratch_data/bei.csv";
    //std::string filePath = "/Users/dfriend/Documents/abm/scratch_data/bei.csv";
    //std::string filePath = "/Users/dfriend/Documents/cpp/abm/scratch_data/points.csv";
    std::fstream fin;
    fin.open(filePath, std::fstream::in);

    if(!fin.is_open()) throw std::runtime_error("Could not open file");

    std::string line, colname;

    //std::vector<std::vector<double>> rows = std::vector<std::vector<double>>();
    std::list<std::shared_ptr<Agent>> points;
    int rowIdx{0};
    //std::cout << "first while" << "\n";
    while(std::getline(fin, line)){
        //std::cout << rowIdx << "\n";
        std::string value;
        std::stringstream ss(line);
        int colIdx{0};
        //std::vector<double> tempRow(2);
        std::shared_ptr<Agent> pt(new Agent());
        //rows.push_back(std::vector<double>(2));
        //while(ss >> val){
        while(std::getline(ss,value, ',')){
            if(colIdx == 0){
                pt->x = std::stod(value);
            } else if (colIdx == 1){   
                pt->y = std::stod(value);
            }
            colIdx++;
        }
        points.push_back(pt);
        rowIdx++;
    }

    fin.close();

    auto start = std::chrono::system_clock::now();
    
    Raster dens = AgentUtilities::pointDensity(points,20,128,128,0,1000,0,500);
    //Raster dens = AgentUtilities::pointDensity(points,1000,128,128,632027,671308,3908100,3974280);


    

    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
    std::cout << "TIME: " << duration.count() << "\n";

    std::string outFile = "/Users/dfriend/Documents/cpp/abm/scratch_data/dens_cpp_6_23_21.csv";
    //std::string outFile = "/Users/dfriend/Documents/cpp/abm/scratch_data/cppBeiDensity.csv";
    std::ofstream csv(outFile, std::ofstream::trunc);
    csv.precision(std::numeric_limits<double>::max_digits10);
    for(int y = 0; y < dens.nY; ++y){
        for(int x = 0; x < dens.nX; ++x){
            csv << dens.getValue((int) x, (int) (dens.nY-1)-y);
            if(x != dens.nX-1){
                csv << ",";
            }
        }
        csv << "\n";
    }
    csv.close();
    return 0;
}










// int mainShortestPathQT(){
//     std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken.qtree";
//     std::shared_ptr<Quadtree> newQt = Quadtree::readQuadtree(filePath);
//     std::cout << "finished reading quadtree\n";
//     Point pt1(649000,3946250);
//     Point pt2(648800,3947250);

//     //Point pt1(645000,3930500);
//     //Point pt2(655000,3959000);
    
//     //Point pt1(624000,3942000);
//     //Point pt2(680000,3963000);
    
//     auto start = std::chrono::system_clock::now();
//     // auto sp = MathUtilities::getShortestPathQuadtree(*newQt, pt1, pt2, 640000,670000, 3930000, 3960000);
//     auto sp = MathUtilities::getShortestPathQuadtree(*newQt, pt1, pt2, 648500,649900,3946150,3947600);
//     // auto sp = MathUtilities::getShortestPathQuadtree(*newQt, pt1, pt2);
//     auto stop = std::chrono::system_clock::now();
//     auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start); 
//     std::cout << "TIME: " << duration.count() << "\n";

//     std::cout << "vals = c(";
//     for(size_t i = 0; i < sp.size(); ++i){  
//         Point pt((sp.at(i)->xMin + sp.at(i)->xMax)/2,(sp.at(i)->yMin + sp.at(i)->yMax)/2);
//         std::cout << pt.x << "," << pt.y;
//         if(i != sp.size()-1){
//             std::cout << ",";
//         }
//     }
//     std::cout << ")\n";

//     return 0;
// }

int mainQuadtreeLCPDebug(){
// int main(){
    // std::string filePath = "/Users/dfriend/Documents/clark_county_project/quadtree_lcp_debug/qt.qtree";
    std::string filePath = "/Users/dfriend/Documents/clark_county_project/quadtree_lcp_debug/qt2.qtree";
    std::shared_ptr<Quadtree> newQt = Quadtree::readQuadtree(filePath);
    std::cout << "finished reading quadtree\n";

    // Point pt1(683218.3, 3924514);
    // Point pt2(683271.7, 3923083);
    Point pt1(8488.439, 25842.65);
    Point pt2(14750.149, 27929.89);
    
    double buf{7112};
    // auto xvals = std::vector<double>{pt1.x, pt2.x};
    // auto yvals = std::vector<double>{pt1.y, pt2.y};
    auto xvals = std::vector<double>{pt1.x};
    auto yvals = std::vector<double>{pt1.y};
    double xmin = MathUtilities::min(xvals) - buf;
    double xmax = MathUtilities::max(xvals) + buf;
    double ymin = MathUtilities::min(yvals) - buf;
    double ymax = MathUtilities::max(yvals) + buf;

    LcpFinder lcpf(newQt, pt1, xmin, xmax, ymin, ymax, false);
    lcpf.makeNetworkAll();
    auto lcp = lcpf.getLcp(pt2);
    // std::cout << "vals2 = c(";
    std::cout << "pt1: " << pt1.x << ", " << pt1.y << "\n";
    std::cout << "pt2: " << pt2.x << ", " << pt2.y << "\n";
    std::cout << "lims: " << xmin << ", " << xmax << ", " << ymin << ", " << ymax << "\n";
    for(size_t i = 0; i < lcp.size(); ++i){  
        auto node = lcp.at(i)->node.lock();
        std::cout << i + 1 << ": (" << lcp.at(i)->pt.x << ", " << lcp.at(i)->pt.y << ") | " << lcp.at(i)->cost << " | " << lcp.at(i)->dist << "\n";
        // if(i != lcp.size()-1){
        //     std::cout << ",";
        // }
    }
    // std::cout << ")\n";
    return 0;
}

int mainShortestPathQT2(){
// int main(){
    std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken2.qtree";
    //std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_grid5.qtree";
    std::shared_ptr<Quadtree> newQt = Quadtree::readQuadtree(filePath);
    std::cout << "finished reading quadtree\n";

    // Point pt1(624000,3942000);
    // Point pt2(680000,3963000);
    Point pt1(640000,3970000);
    Point pt2(660000,3920000);
    //Point pt2(630000,3950000);
    
    //Point pt1(630000,3942000);
    //Point pt2(680000,3910000);



    //trial 1 - find the entire shortest network
    auto start = std::chrono::system_clock::now();
    
    LcpFinder spf(newQt, pt1,-100,-50,-100,-50, false);
    spf.makeNetworkAll();
    //auto sp = spf.getShortestPath(pt2);

    auto stop = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
    std::cout << "TIME1: " << duration.count() << "\n";

    //trial 2 - use the network constructed in trial 1 to get the shortest path to a specific node
    auto start2 = std::chrono::system_clock::now();

    auto sp2 = spf.getLcp(pt2);

    auto stop2 = std::chrono::system_clock::now();
    auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(stop2 - start2); 
    std::cout << "TIME2: " << duration2.count() << "\n";

    std::cout << "vals2 = c(";
    for(size_t i = 0; i < sp2.size(); ++i){  
        // Point pt((std::get<0>(sp2.at(i))->xMin + std::get<0>(sp2.at(i))->xMax)/2,(std::get<0>(sp2.at(i))->yMin + std::get<0>(sp2.at(i))->yMax)/2);
        std::cout << sp2.at(i)->pt.x << "," << sp2.at(i)->pt.y;
        if(i != sp2.size()-1){
            std::cout << ",";
        }
    }
    std::cout << ")\n";


    //trial 3 - find the shortest path without constructing the entire network
    auto start3 = std::chrono::system_clock::now();
    
    LcpFinder spf3(newQt, pt1);
    //spf2.makeShortestPathNetwork();
    auto sp3 = spf3.getLcp(pt2);

    auto stop3 = std::chrono::system_clock::now();
    auto duration3 = std::chrono::duration_cast<std::chrono::milliseconds>(stop3 - start3); 
    std::cout << "TIME3: " << duration3.count() << "\n";

    std::cout << "vals3 = c(";
    for(size_t i = 0; i < sp3.size(); ++i){  
        // Point pt((sp3.at(i)->xMin + sp3.at(i)->xMax)/2,(sp3.at(i)->yMin + sp3.at(i)->yMax)/2);
        // Point pt((std::get<0>(sp3.at(i))->xMin + std::get<0>(sp3.at(i))->xMax)/2,(std::get<0>(sp3.at(i))->yMin + std::get<0>(sp3.at(i))->yMax)/2);
        std::cout << sp3.at(i)->pt.x << "," << sp3.at(i)->pt.y;
        if(i != sp3.size()-1){
            std::cout << ",";
        }
    }
    std::cout << ")\n";


    return 0;
}


int mainQuadtreeNodesInBox(){
    std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken.qtree";
    std::shared_ptr<Quadtree> newQt = Quadtree::readQuadtree(filePath);
    std::cout << "finished reading quadtree\n";
    std::list<std::shared_ptr<Node>> nodes = newQt->getNodesInBox(648500,649900,3946150,3947600);
    std::cout << nodes.size() << "\n";

    return 0;
}

// int mainQuadtreeShortestPath(){
// //int main(){
//     std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken.qtree";
//     std::shared_ptr<Quadtree> newQt = Quadtree::readQuadtree(filePath);
//     std::shared_ptr<Node> node1 = newQt->getNode(655000,3959000);
//     std::shared_ptr<Node> node2 = newQt->getNode(645000,3930500);
//     NodeNetwork nn = NodeNetwork(*newQt, 640000,670000, 3930000, 3960000);
//     //std::cout << nn.adjMat.toString() << "\n";
//     std::vector<std::shared_ptr<Node>> nnSp = nn.getShortestPath(node1->id, node2->id);

//     std::cout << "vals = c(";
//     for(size_t i = 0; i < nnSp.size(); ++i){
//         Point pt((nnSp.at(i)->xMin + nnSp.at(i)->xMax)/2,(nnSp.at(i)->yMin + nnSp.at(i)->yMax)/2);
//         std::cout << pt.x << "," << pt.y;
//         if(i != nnSp.size()-1){
//             std::cout << ",";
//         }
//     }
//     std::cout << ")\n";
//     return 0;
// }


// //tests the shortest path function
// int mainShortestPath(){
// //     std::vector<double> nums1 { NAN, 7.3, 8.2, NAN, 2.5,
// //                                 7.3, NAN, 2.4, 3.4, NAN,
// //                                 8.2, 2.4, NAN, 3.2, 9.2,
// //                                 NAN, 3.4, 3.2, NAN, NAN,
// //                                 2.5, NAN, 9.2, NAN, NAN};
//     std::vector<double> nums1 {NAN, NAN, NAN, NAN, 2.6, NAN, NAN, NAN, 2.5, NAN, 2.9, NAN, NAN, NAN, NAN, 
// NAN, NAN, 1.5, NAN, 0.6, NAN, NAN, 1.9, 1.6, NAN, NAN, NAN, NAN, NAN, 2.2, NAN, NAN, 
// NAN, NAN, NAN, NAN, 1.4, NAN, NAN, NAN, NAN, 1.9, NAN, NAN, NAN, NAN, NAN, NAN, NAN, 3.5, NAN, 
// NAN, NAN, 2.2, 2.5, NAN, 2.3, NAN, NAN, NAN, NAN, 1.6, NAN, NAN, NAN, 1.2, NAN, NAN, 
// NAN, 1.2, NAN, NAN, NAN, NAN, NAN, 5, 2.8, NAN, NAN, NAN, 2.6, NAN, NAN, NAN, NAN, NAN, 3.9, 
// NAN, 2.6, NAN, NAN, 3.3, 8.4, NAN, NAN, NAN, NAN, NAN, NAN, 2.8, NAN, NAN, NAN, 1.2, 
// NAN, NAN, NAN, NAN, NAN, 0.3, NAN, NAN, NAN, NAN, NAN, 6.2, NAN, NAN, NAN, NAN, NAN, NAN, NAN, 
// NAN, 3.9, NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, 2.2, NAN, 0.3, NAN, 1.2, NAN, 
// NAN, NAN, NAN, NAN, NAN, NAN, NAN, 1.7, NAN, 1.1, 1.3, NAN, NAN, 1.9, NAN, NAN, NAN, 3.1, 
// NAN, 2.5, NAN, NAN, NAN, 2.6, NAN, NAN, 1.7, NAN, NAN, 2.3, 0.8, NAN, NAN, NAN, NAN, 
// NAN, NAN, NAN, NAN, NAN, 2.2, 3.5, 1.2, NAN, 0.3, NAN, NAN, NAN, NAN, NAN, NAN, 9, 5.5, 
// NAN, NAN, NAN, NAN, NAN, NAN, 2.9, NAN, NAN, NAN, NAN, NAN, NAN, 1.1, 2.3, NAN, NAN, NAN, NAN, 
// NAN, 1.4, 4.3, 3.1, 3.8, NAN, NAN, NAN, NAN, NAN, NAN, 3.3, NAN, NAN, 1.3, 0.8, 
// NAN, NAN, NAN, 5.1, NAN, NAN, NAN, NAN, NAN, 3.5, NAN, NAN, NAN, NAN, NAN, 8.4, NAN, NAN, NAN, 
// NAN, 9, NAN, 5.1, NAN, 3.4, NAN, NAN, NAN, NAN, 1.6, NAN, NAN, NAN, 2.2, NAN, NAN, NAN, 
// NAN, NAN, NAN, 5.5, NAN, NAN, 3.4, NAN, 2.7, NAN, NAN, NAN, 3, NAN, NAN, NAN, 2.5, NAN, 
// NAN, NAN, NAN, 1.9, NAN, NAN, 1.4, NAN, NAN, 2.7, NAN, NAN, 2.5, NAN, 3.5, NAN, NAN, 
// NAN, NAN, 5, NAN, 6.2, 2.2, NAN, NAN, NAN, 4.3, NAN, NAN, NAN, NAN, NAN, 3.5, 2.3, 
// NAN, NAN, NAN, 1.4, 2.3, 2.8, NAN, NAN, NAN, NAN, NAN, NAN, 3.1, NAN, NAN, NAN, 2.5, 
// 3.5, NAN, NAN, NAN, NAN, 1.5, NAN, NAN, NAN, NAN, NAN, 0.3, NAN, NAN, NAN, 3.8, NAN, NAN, 
// NAN, NAN, 2.3, NAN, NAN, NAN, 0.9, NAN, NAN, NAN, NAN, NAN, NAN, NAN, 3.1, NAN, NAN, NAN, 3.5, 
// 1.6, 3, 3.5, NAN, NAN, NAN, NAN, NAN, 0.6, NAN, NAN, NAN, 2.8, NAN, 1.2, NAN, NAN, 
// NAN, NAN, NAN, NAN, NAN, NAN, NAN, NAN, 0.9, NAN, 0};
//     Matrix mat1 = Matrix(nums1,20,20);

//     std::cout << mat1.toString() << "\n";
//     std::cout << "value at row 2, col 0: " << mat1.getValue(2,0) << "\n";
//     MathUtilities::ShortestPathFinder spf(mat1, 4);
//     Matrix sp = spf.getShortestPathNetwork();
//     std::vector<int> path1 = spf.getShortestPath(10);
//     std::cout << "\nshortest path network:\n";
//     //Matrix sp = MathUtilities::getShortestPaths(mat1,4);
    
//     std::cout << sp.toString() << "\n";
//     std::cout << "\npath1:\n";
//     for(size_t i = 0; i < path1.size(); ++i){
//         std::cout << path1.at(i) << " ";
//     }
//     std::cout << "\n";
    
//     MathUtilities::ShortestPathFinder spf2(mat1, 4);
//     std::vector<int> path2 = spf2.getShortestPath(10);
//     std::cout << "\npath2:\n";
//     for(size_t i = 0; i < path2.size(); ++i){
//         std::cout << path2.at(i) << " ";
//     }

//     std::cout << "\npath2 adj mat:\n";
//     std::cout << spf2.pathMat.toString() << "\n";
// }





//int mainReadAgents(){
int mainOld(){
    SimulationParameters sp = Simulation::readParams("/Users/dfriend/Documents/cpp/abm/input/params.csv");
    std::string folderName = BasicUtilities::getNewFolderName(sp.simParentFolderPath);
    std::cout << folderName << "\n";
    fs::create_directory(folderName);
    sp.simFolder = folderName + "/";
    Simulation sim(sp);
    //sim.readAgents(sp.initAgentFilePath);
    std::cout << "DONE WITH READING\n";
    //sim.writeLiveAgents();
    sim.runSim();
    return 0;
}



//isReachable() testing
int mainIsReachable(){
// int main(){
    std::vector<double> nums1 { .0, .1,NAN,NAN, .1, .2, .1, .0,
                                .2, .2,NAN,NAN, .1, .2, .1, .0,
                                .5, .6, .2, .1, .5, .6, .7, .6,
                                .2, .4,NAN, .0, .2, .6, .7, .5,
                                .4, .5,NAN, .1, .3, .3, .2, .2,
                                .0,NAN,NAN, .3, .2, .2, .2, .2,
                                .0,NAN, .7, .7, .3, .2, .2, .3,
                                .0,NAN, .9, .9, .2, .3, .3, .3};
    
    Matrix mat1 = Matrix(nums1,8,8);

    std::shared_ptr<Quadtree> qt = std::shared_ptr<Quadtree>(new Quadtree(0,8,0,8));
    
    auto splitFun = [](const Matrix& mat) -> bool{
        return Quadtree::splitRange(mat, .22);
    };
    auto combineFun = [](const Matrix& mat) -> double { 
        return Quadtree::combineMedian(mat);
    };
    qt->makeTree(mat1, splitFun, combineFun);
    qt->assignNeighbors();

    std::cout << mat1.toString() << "\n";
    std::cout << qt->toString() << "\n";

    std::cout << "pt1: (7.5,7.5) | pt2: (7.6,7.6) | dist: 2.0 | true\n"; //Should be TRUE
    std::cout << MathUtilities::isReachable(*qt,7.5,7.5,7.6,7.6,2) << "\n\n";

    std::cout << "pt1: (7.5,7.5) | pt2: (6.5,6.5) | dist: 2.0 | true\n";
    std::cout << MathUtilities::isReachable(*qt,7.5,7.5,6.5,6.5,2) << "\n\n";

    std::cout << "pt1: (7.5,7.5) | pt2: (4.5,4.5) | dist: 2.0 | false\n";
    std::cout << MathUtilities::isReachable(*qt,7.5,7.5,4.5,4.5,2) << "\n\n";

    std::cout << "pt1: (0.5,0.5) | pt2: (2.5,0.5) | dist: 3.0 | false\n";
    std::cout << MathUtilities::isReachable(*qt,0.5,0.5,2.5,0.5,3) << "\n\n";

    std::cout << "pt1: (1.5,0.5) | pt2: (2.5,0.5) | dist: 3.0 | false\n";
    std::cout << MathUtilities::isReachable(*qt,1.5,0.5,2.5,0.5,3) << "\n\n";

    std::cout << "pt1: (1.5,3.5) | pt2: (3.5,3.5) | dist: 3.0 | true\n";
    std::cout << MathUtilities::isReachable(*qt,1.5,3.5,3.5,3.5,3) << "\n\n";
    
    std::cout << "pt1: (.5,.5) | pt2: (7.5,7.5) | dist: 10.0 | true\n";
    std::cout << MathUtilities::isReachable(*qt,0.5,0.5,7.5,7.5,10) << "\n\n";

    std::cout << "pt1: (7.5,7.5) | pt2: (.5,.5) | dist: 10.0 | true\n";
    std::cout << MathUtilities::isReachable(*qt,7.5,7.5,0.5,0.5,10) << "\n\n";
    
    return 0;
}




int mainSim2(){
    Simulation sim("/Users/dfriend/Documents/cpp/abm/output/sim_test/params1.csv");
    sim.runSim();
    return 0;
}

//int main(){
int mainTestMatrix(){
    std::vector<double> nums1 { 0,.1, NAN, NAN,.1,.2,.1, 0,
                               .2,.2, NAN, NAN,.1,.2,.1, 0,
                               .5,.6, NAN,.1,.5,.6,.7,.6,
                               .2,.4, 0, 0,.2,.6,.7,.5,
                               .4,.5,.1,.1,.3,.3,.2,.2,
                               .0,.0,.3,.3,.2,.2,.2,.2,
                                NAN,NAN,.7,.7,.3,.2,.2,.3,
                                0, NAN,.9,.9,.2,.3,.3,.3};
    //std::vector<double> nums2 {.4,.5,.1,.2,.6,.4,.5,.3,.2,.1,.3,.0,.0,.3,.2,.3};
    Matrix mat1 = Matrix(nums1, 8, 8);
    std::cout << mat1.toString() << "\n";
    double mean = mat1.mean();
    double med = mat1.median();
    std::cout << "mean: " << mean << "\n";
    std::cout << "median: " << med << "\n";
    return 0;
}

// //Quadtree read/write testing
// int mainQuadtreeIO(){
//     std::vector<double> nums1 { 0,.1, NAN, NAN,.1,.2,.1, 0,
//                                .2,.2, NAN, NAN,.1,.2,.1, 0,
//                                .5,.6, NAN,.1,.5,.6,.7,.6,
//                                .2,.4, 0, 0,.2,.6,.7,.5,
//                                .4,.5,.1,.1,.3,.3,.2,.2,
//                                .0,.0,.3,.3,.2,.2,.2,.2,
//                                 NAN,NAN,.7,.7,.3,.2,.2,.3,
//                                 0, NAN,.9,.9,.2,.3,.3,.3};
//     //std::vector<double> nums2 {.4,.5,.1,.2,.6,.4,.5,.3,.2,.1,.3,.0,.0,.3,.2,.3};
//     Matrix mat1 = Matrix(nums1, 8, 8);
//     //std::cout << mat1.toString() << std::endl;
//     std::shared_ptr<Quadtree> qt = std::shared_ptr<Quadtree>(new Quadtree(0,8,0,8,.001));
//     qt->makeTree(mat1);
//     qt->assignNeighbors();
//     std::string filePath = "quadtreeBinary2.qtree";
//     Quadtree::writeQuadtree(*qt, filePath);
//     Quadtree newQt = Quadtree::readQuadtree(filePath);
//     std::cout << newQt.toString() << "\n";

//     // {
//     //     std::ofstream os(filePath);
//     //     cereal::XMLOutputArchive oarchive(os);
//     //     oarchive(qt);
//     //     //oarchive(*(qt.root));
//     // }

//     // {
//     //     std::ifstream is(filePath);
//     //     cereal::XMLInputArchive iarchive(is);
//     //     // std::shared_ptr<Node> node;
//     //     // Node node;
//     //     // iarchive(node);
//     //     // std::cout << node.toString() << "\n";
//     //     Quadtree qt;
//     //     iarchive(qt);
//     //     std::cout << qt.toString() << "\n";
//     // }

// }

// // this code is for testing to make sure the random draw feature of the 'ProbabilityTable' class is working as expected
int mainProbabilityTable(){
    std::string filePath = "/Users/dfriend/Documents/cpp/abm/input/n_egg_probs.csv";
    ProbabilityTable<int> pt(filePath);
    //std::cout << "testing1\n";
    std::cout << pt.toString() << "\n";
    //std::cout << "testing2\n";
    std::cout << "prob for 5: " << pt.getProb(5) << "\n";


    std::string filePath2 = "/Users/dfriend/Documents/cpp/abm/input/age_survival_probs.csv";
    ProbabilityTable<int> pt2(filePath2);
    std::cout << pt2.toString() << "\n";
    std::cout << "prob for 12: " << pt2.getProb(12) << "\n";
    std::cout << "prob for 67: " << pt2.getProb(67) << "\n";


    std::string filePath3 = "/Users/dfriend/Documents/cpp/abm/input/move_distance_probs.csv";
    ProbabilityTable<double> pt3(filePath3);
    std::cout << pt3.toString() << "\n";

    std::vector<double> nums{4,2,1,3,7,9,5,3,4,8};
    std::vector<double> numsScaled = MathUtilities::scaleNums(nums,1,9,-10,47);
    for(size_t i = 0; i < numsScaled.size(); ++i){
        std::cout << numsScaled.at(i) << "\n";
    }
    std::cout << "7.5 scaled: " << MathUtilities::scaleNum(7.5,1,9,-10,47) << "\n";
    std::cout << "max: " << MathUtilities::max(nums) << "\n";
    std::cout << "min: " << MathUtilities::min(nums) << "\n";
    // std::mt19937 randomGenerator = std::mt19937();
    // std::cout << "c(";
    // int n_times = 10000;
    // for(int i = 0; i < n_times; ++i){
    //     std::cout << pt.getRandomValue(randomGenerator);
    //     if(i == n_times-1){
    //         std::cout << ")" << "\n";
    //     } else {
    //         std::cout << ",";
    //     }
    // }
    return 0;
}

//test Raster
int mainRaster(){
    std::vector<double>* vec_pt = new std::vector<double>{.300,.497,.103,.140,
                                                          .555,.534,.987,.002,
                                                          .637,.201,.891,.337,
                                                          .678,.196,.589,.797};
    std::shared_ptr<std::vector<double>> vec = std::shared_ptr<std::vector<double>>(vec_pt);
    Raster rast(vec,4,4,0,4,0,4);
    std::cout << rast.toString() << "\n";
    std::cout << "(0.3, 0.1) -> " << rast.getValue(.3,.1) << "\n";
    std::cout << "(0.0, 0.0) -> " << rast.getValue(0.0,0.0) << "\n";
    //std::cout << "(4,4) -> " << rast.getValue(4.0,4.0) << "\n"; //this breaks it
    std::cout << "(3.9, 0.1) -> " << rast.getValue(3.9,0.1) << "\n";
    std::cout << "(0.1, 3.9) -> " << rast.getValue(.1,3.9) << "\n";
    std::cout << "(1.3, 2.4) -> " << rast.getValue(1.3,2.4) << "\n";
    std::cout << "(2.0, 2.0) -> " << rast.getValue(2.0,2.0) << "\n";
    return 0;
}

// int mainDensity(){
//     std::vector<Point> points = readPoints("/Users/dfriend/Documents/cpp/abm/scratch_data/bei100k.csv");
//     std::vector<std::shared_ptr<Agent>> agents(points.size());
//     std::cout << "check1\n";
//     for(size_t i = 0; i < points.size(); ++i){
//         std::shared_ptr<Agent> a(new Agent());
//         a->x = points.at(i).x;
//         a->y = points.at(i).y;
//         agents.at(i) = a;
//     }
//     std::cout << "check2\n";

//     //double bw = 31;
//     //PointGrid pg(agents, bw);
//     //std::cout << "check3\n";
//     //std::cout << pg.toString() << "\n";


//     auto start = std::chrono::high_resolution_clock::now();

//     //Raster dens = MathUtilities::pointDensity(agents,50,128,128);
//     //Raster qc = MathUtilities::quadratCount(agents,120,-200,1200,-200,700);
//     std::vector<int> np = MathUtilities::nPointsWithin(agents, 10);
    
//     auto stop = std::chrono::high_resolution_clock::now();
//     auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
//     std::cout << "TIME: " << duration.count() << "\n"; 
    
//     //std::cout << dens.toString(4) << "\n";
//     //std::cout << qc.toString(0) << "\n";
//     // for(size_t i = 0; i < dens.size(); ++i){
//     //     if(i%4 == 0){
//     //         std::cout << "\n[" << i+1 << "] ";
//     //     }
//     //     std::cout << dens.at(i) << " ";
//     // }
//     // std::cout << "\n";
// }

//tests the mutate function
// int mainMutate(){
// int mainMutate(){
//     int nLoci = 2;
//     int nPerLocus = 2;
//     std::vector<std::vector<int>> loci(nLoci);
//     std::mt19937 r = std::mt19937(10);
    
//     for(int i = 0; i < nLoci; ++i){
//         loci.at(i) = std::vector<int>(nPerLocus);
//         for(int j = 0; j < nPerLocus; ++j){
//             loci.at(i).at(j) = 50;
//         }
//     }
//     for(int i = 0; i < 20; ++i ){
//         SimUtilities::mutate(loci, r);
//         std::string str;
//         for(size_t j = 0; j < loci.size(); ++j){
//             for(int k = 0; k < nPerLocus; ++k){
//                 str = str + std::to_string(loci.at(j).at(k)) + " ";
//             }
//             str += "| ";
//         }
//         std::cout << str << "\n";
//     }    
//     return 0;
// }

//test the breeding function
int mainBreed(){
// int main(){
    std::uniform_int_distribution<> alleleDist(100,400); 
    std::mt19937 r = std::mt19937(10);
    int nLoci = 5;
    double mutateProb = .5;

    //TEST 1 - make sure it's randomly selecting between the two - 0's and 1's make it easier to see
    {
        std::vector<std::vector<int>> loci1(nLoci);
        std::vector<std::vector<int>> loci2(nLoci);
        for(int i = 0; i < nLoci; ++i){
            loci1.at(i) = std::vector<int>(2);
            loci2.at(i) = std::vector<int>(2);
            for(int j = 0; j < 2; ++j){
                loci1.at(i).at(j) = 0;
                loci2.at(i).at(j) = 1;
            }
        }

        // std::vector<std::vector<int>> loci3 = SimUtilities::breed(loci1, loci2, mutateProb, r);
        std::vector<std::vector<int>> loci3 = Simulation::breed(loci1, loci2, mutateProb, r);

        std::string l1,l2,l3;
        for(size_t i = 0; i < loci1.size(); ++i){
            for(int j = 0; j < 2; ++j){
                l1 = l1 + std::to_string(loci1.at(i).at(j)) + " ";
                l2 = l2 + std::to_string(loci2.at(i).at(j)) + " ";
                l3 = l3 + std::to_string(loci3.at(i).at(j)) + " ";
            }
            l1 += "| ";
            l2 += "| ";
            l3 += "| ";
        }
        std::cout << l1 << "\n" << l2 << "\n" << l3 << "\n";
        
    }
    std::cout << "\n\n";
    //TEST 2 - use more realistic-looking #s
    for(int counter = 0; counter < 10; ++counter){
        std::vector<std::vector<int>> loci1(nLoci);
        std::vector<std::vector<int>> loci2(nLoci);
        for(int i = 0; i < nLoci; ++i){
            loci1.at(i) = std::vector<int>(2);
            loci2.at(i) = std::vector<int>(2);
            for(int j = 0; j < 2; ++j){
                loci1.at(i).at(j) = alleleDist(r);
                loci2.at(i).at(j) = alleleDist(r);
            }
        }

        // std::vector<std::vector<int>> loci3 = SimUtilities::breed(loci1, loci2, mutateProb, r);
        std::vector<std::vector<int>> loci3 = Simulation::breed(loci1, loci2, mutateProb, r);

        std::string l1,l2,l3;
        for(size_t i = 0; i < loci1.size(); ++i){
            for(int j = 0; j < 2; ++j){
                l1 = l1 + std::to_string(loci1.at(i).at(j)) + " ";
                l2 = l2 + std::to_string(loci2.at(i).at(j)) + " ";
                l3 = l3 + std::to_string(loci3.at(i).at(j)) + " ";
            }
            l1 += "| ";
            l2 += "| ";
            l3 += "| ";
        }
        std::cout << l1 << "\n" << l2 << "\n" << l3 << "\n";
        std::cout << "=============================================\n";
    }
    return 0;
}


// int mainSaveParams(){


//     //Quadtree quadtree0 = Quadtree::readQuadtree(filePath);
//     //std::shared_ptr<Quadtree> quadtree = std::shared_ptr<Quadtree>(&quadtree0);
//     //std::cout << quadtree->toString() << "\n";
    
//     SimulationParameters params;

//     params.simOutputFolder = "/Users/dfriend/Documents/cpp/abm/output/sim_test/2_27_21(1)/";
//     params.simQuadtreeFilePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_grid5_gaps.qtree";
//     params.movtDistanceProbsFilePath = "/Users/dfriend/Documents/cpp/abm/input/move_distance_probs.csv";
//     params.mateNumEggsProbsFilePath = "/Users/dfriend/Documents/cpp/abm/input/n_egg_probs.csv";
//     params.deathAgeScoresFilePath = "/Users/dfriend/Documents/cpp/abm/input/age_survival_probs.csv";
//     params.deathCarryingCapacityFilePath = "/Users/dfriend/Documents/cpp/abm/input/resist_carrying_capacity.csv";
    


//     params.simNIterations=10;
//     params.simPointGridCellSize=1000;
//     params.simRandomSeed=33;

//     params.initNAgents=1000; //IT WAS BREAKING WHEN params.initNAgents = 50000
//     params.initNLoci = 4;
//     params.initAlleleMin = 200;
//     params.initAlleleMax = 206;
//     //params.initNAgents=10000;
    

//     params.movtNPoints = 16;
//     params.movtStepSize = 500;
//     params.movtAttractDist = 5000;
//     //params.movtMaxStraightDist = 1000;
//     params.movtMaxTotalDist = 5000;
//     params.movtMaxTotalDistSubstep = 1500;
//     params.movtResistWeight1 = 2;
//     params.movtAttractWeight1 = 1;
//     params.movtDirectionWeight1 = 1;
//     params.movtResistWeight2 = 2;
//     params.movtAttractWeight2 = 1;
//     params.movtDirectionWeight2 = 1;
    
//     //params.mateAge = 17; //NOTE - make it so that the males have to be above 'matingAge' as well!!!
//     params.mateAge = 17; //NOTE - make it so that the males have to be above 'matingAge' as well!!!
//     //params.matingProb = .6;
//     params.mateMaxDist = 500;
    
//     //params.clutchSize = 3;
//     //params.deathMaxAge=70;
//     params.deathMaxAge=70;
//     //params.deathPercentage=.2;
//     params.deathDist=1000; //if I change this the ProbTable for carrying capacity won't be used as expected because those numbers are for 1km2 cells
    
//     Simulation sim(params);
//     std::string outputPath = params.simOutputFolder + "params1.csv";
//     std::cout << "test1\n";
//     sim.writeParams(outputPath,sim.params);
//     std::cout << "test2\n";
//     Simulation sim2(outputPath);
//     std::cout << "test3\n";
//     std::string outputPath2 = params.simOutputFolder + "params2.csv";
//     std::cout << "test4\n";
//     sim2.writeParams(outputPath2,sim.params);
//     std::cout << "test5\n";
//     return 0;
// }
// //run the simulation
// int mainSim(){



//     // std::vector<double> nums1 { 0,.1, NAN, NAN,.1,.2,.1, 0,
//     //                            .2,.2, NAN, NAN,.1,.2,.1, 0,
//     //                            .5,.6, NAN,.1,.5,.6,.7,.6,
//     //                            .2,.4, 0, 0,.2,.6,.7,.5,
//     //                            .4,.5,.1,.1,.3,.3,.2,.2,
//     //                            .0,.0,.3,.3,.2,.2,.2,.2,
//     //                             NAN,NAN,.7,.7,.3,.2,.2,.3,
//     //                             0, NAN,.9,.9,.2,.3,.3,.3};
//     // std::vector<double> nums2 {.5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5,
//     //                            .5,.5,.5,.5,.5,.5,.5,.5};
//     // //std::vector<double> nums2 {.4,.5,.1,.2,.6,.4,.5,.3,.2,.1,.3,.0,.0,.3,.2,.3};
//     // Matrix mat1 = Matrix(nums1, 8, 8);
//     // std::cout << mat1.toString() << std::endl;
//     // Quadtree* qt = new Quadtree(0,8,0,8,.001);
//     // std::shared_ptr<Quadtree> quadtree = std::shared_ptr<Quadtree>(qt);
//     // quadtree->makeTree(mat1);
//     // quadtree->assignNeighbors();
//     //std::string filePath = "/Users/dfriend/Documents/cpp/abm/other_files/habQuadtree.qtree";
//     // std::string filePath = "/Users/dfriend/Documents/cpp/abm/other_files/empty.qtree";


    
//     // std::string quadtreePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah.qtree";
//     // std::string quadtreePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_grid5.qtree";
//     std::string quadtreePath = "/Users/dfriend/Documents/cpp/abm/input/ivanpah_grid5_gaps.qtree";
//     std::string movtDistPath = "/Users/dfriend/Documents/cpp/abm/input/move_distance_probs.csv";
//     std::string nEggsPath = "/Users/dfriend/Documents/cpp/abm/input/n_egg_probs.csv";
//     std::string ageScoresPath = "/Users/dfriend/Documents/cpp/abm/input/age_survival_probs.csv";
//     std::string carryingCapPath = "/Users/dfriend/Documents/cpp/abm/input/resist_carrying_capacity.csv";
//     std::string outputFolder = "/Users/dfriend/Documents/cpp/abm/output/sim_test/";



//     //Quadtree quadtree0 = Quadtree::readQuadtree(filePath);
//     //std::shared_ptr<Quadtree> quadtree = std::shared_ptr<Quadtree>(&quadtree0);
//     std::shared_ptr<Quadtree> quadtree = Quadtree::readQuadtree(quadtreePath);
//     quadtree->assignNeighbors();
//     //std::cout << quadtree->toString() << "\n";
    
//     SimulationParameters params;

//     params.simOutputFolder = outputFolder;

//     params.simNIterations=100;
//     params.simPointGridCellSize=1000;
    
//     params.initNAgents=50000; //IT WAS BREAKING WHEN params.initNAgents = 50000
//     params.initNLoci = 4;
//     params.initAlleleMin = 200;
//     params.initAlleleMax = 206;
//     //params.initNAgents=10000;
    

//     params.movtNPoints = 16;
//     params.movtStepSize = 500;
//     params.movtAttractDist = 5000;
//     //params.movtMaxStraightDist = 1000;
//     params.movtMaxTotalDist = 5000;
//     params.movtMaxTotalDistSubstep = 1500;
//     params.movtResistWeight1 = 2;
//     params.movtAttractWeight1 = 1;
//     params.movtDirectionWeight1 = 1;
//     params.movtResistWeight2 = 2;
//     params.movtAttractWeight2 = 1;
//     params.movtDirectionWeight2 = 1;
//     params.movtDistanceProbs = ProbabilityTable<double>(movtDistPath);
    
//     //params.mateAge = 17; //NOTE - make it so that the males have to be above 'matingAge' as well!!!
//     params.mateAge = 17; //NOTE - make it so that the males have to be above 'matingAge' as well!!!
//     //params.matingProb = .6;
//     params.mateMaxDist = 500;
//     params.mateNumEggsProbs = ProbabilityTable<int>(nEggsPath);
    
//     //params.clutchSize = 3;
//     //params.deathMaxAge=70;
//     params.deathMaxAge=70;
//     //params.deathPercentage=.2;
//     params.deathAgeScores = ProbabilityTable<int>(ageScoresPath);
//     params.deathDist=1000; //if I change this the ProbTable for carrying capacity won't be used as expected because those numbers are for 1km2 cells
//     params.deathCarryingCapacity = ProbabilityTable<int>(carryingCapPath);


//     std::cout << params.deathCarryingCapacity.toString() << "\n";
//     //std::cout << "start" << std::endl;
//     //Simulation sim = Simulation(params, quadtree, 2);
//     Simulation sim = Simulation(params);
//     //std::string outputFolder = "C:/Users/derek/Documents/Programming/cpp/abm/output/";
//     //std::string firstFile = "liveAgents0000.csv";
//     //sim.writeLiveAgents("C:\\Users\\derek\\Documents\\Programming\\cpp\\abm\\output\\liveAgents0000.csv");
//     std::cout << "test1\n";
//     sim.writeAgents(sim.agents, outputFolder + std::string("liveAgents0000.csv"));
//     std::cout << "test2\n";
//     auto start = std::chrono::high_resolution_clock::now();
//     for(int i = 1; i < 100; ++i){
//         std::cout << "ITERATION " << i << "\n";
//         std::cout << "# of live agents: " << sim.agents.size() << "\n";
//         std::cout << "move" << "\n";
//         sim.move(false);
//         std::cout << "reproduce" << "\n";
//         sim.reproducePointGrid();
//         std::cout << "age" << "\n";
//         sim.age();
//         std::cout << "die" << "\n";
//         sim.die2();
//         std::cout << "advance counter" << "\n";
//         sim.advanceCounter();
//         std::cout << "write results" << "\n";
//         std::string firstPartLive(outputFolder + std::string("agents"));
        
//         std::string num(std::to_string(i));
//         std::string numPadded = std::string(4 - num.length(), '0') + num;
//         std::string csvPart(".csv");
//         sim.writeAgents(sim.agents, firstPartLive + numPadded + csvPart);
        
//     }
//     //std::string firstPartDead(outputFolder + std::string("deadAgents"));
//     std::cout << "write dead agents" << "\n";
//     sim.writeAgents(sim.deadAgents, outputFolder + std::string("deadAgents.csv"));
//     auto stop = std::chrono::high_resolution_clock::now();
//     auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start); 
//     std::cout << "TIME: " << duration.count() << "\n"; 
//     //sim.writeAllMovtHist("C:\\Users\\derek\\Documents\\Programming\\cpp\\abm\\output\\movtHist.csv");
//     sim.writeAllMovtHist(outputFolder + "movtHist.csv");
//     std::cout << "\n";
//     //1 ends at 22 
//     //2 ends at 7
//     //3 ends at 190
//     //4 ends at 42
//     //5 ends at 65
//     //6 ends at 25
//     //7 doesn't end (within 200 iterations)
//     //8 ends at 32
//     //9 ends at 6
//     //10 ends at 125
//     //18 ends at 48
//     //std::cout << "end" << std::endl;

//     //std::cout << sim.quadtree->toString() << std::endl;
//     //std::cout << sim.pointGrid->toString() << std::endl;
    
//     // for(int i = 0; i < 8; ++i){
//     //     std::cout << "==== " << i << " ====" << std::endl;
//     //     std::cout << "Move agents" << std::endl;
//     //     sim.moveAgents(true);

//     //     for(int j = 0; j < sim.agents.size(); ++j){
//     //         std::cout << j << " | " << sim.agents[j]->agentPoint->x << ", " << sim.agents[j]->agentPoint->y << "\n";
//     //     }

//     //     std::cout << "Construct PointGrid" << std::endl;
//     //     sim.reconstructPointGrid();
//     //     // std::cout << "hi there1" << std::endl;
//     //     std::cout << sim.pointGrid->toString() << std::endl;
//     //     // std::cout << "hi there2" << std::endl;
//     // }


//     // for(int i = 0; i < sim.agents.size(); ++i){
//     //     std::cout << sim.agents[i]->toString() << std::endl;
//     // }
//     // std::cout << qt.toString() << std::endl;
//     // double nnDist = qt.root->children[1]->getNearestNeighborDistance();
//     // std::cout << qt.root->children[1]->toString() << std::endl;
//     // std::cout << "nearest neighbor distance: " << nnDist << std::endl;


//     // PointGrid pg1(3,3,0,6.1,0,6.1);
//     // //PointGrid pg2(7,7,0,6.1,0,6.1);
//     // std::vector<std::shared_ptr<Point>> points(nums1.size());
//     // for(int i = 0; i < nums1.size(); ++i){
//     //     //Point* point_i = new AgentPoint(nums1[i], nums2[i],i);
//     //     std::shared_ptr<AgentPoint> ap = makeAgentPoint(nums1[i], nums2[i], i);
//     //     //points[i] = std::shared_ptr<Point>(point_i);
//     //     pg1.addPoint(ap);
//     //     std::cout << ap->toString() << std::endl;
//     // }
//     // //pg2.addPoints(points);
//     // //PointGrid(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
//     // //PointGrid pg3(points, 7, 7, 0,6.1,0,6.1);
//     // std::cout << pg1.toString() << std::endl;
//     // //std::cout << pg2.toString() << std::endl;
//     // //std::cout << pg3.toString() << std::endl;

//     // std::cout << pg1.cells[0]->toString() << std::endl;

//     return 0;
// }


// //================================================================================
// //CODE FOR TESTING SPEED OF POINTGRID FOR REPRODUCING
// //================================================================================
// int main(){
//     std::string filePath = "/Users/dfriend/Documents/cpp/abm/other_files/ivanpah.qtree";
//     std::shared_ptr<Quadtree> quadtree = Quadtree::readQuadtree(filePath);
//     quadtree->assignNeighbors();
//     //std::cout << quadtree->toString() << "\n";
    
//     SimulationParameters params;
//     params.initNAgents=5000;
//     params.initPointGridCellSize=1;

//     params.movtNPoints = 16;
//     params.movtStepSize = 500;
//     params.movtAttractDist = 5000;
//     params.movtMaxStraightDist = 1000;
//     params.movtMaxTotalDist = 3000;
//     params.movtMaxTotalDistSubstep = 1500;
//     params.movtResistWeight1 = 2;
//     params.movtAttractWeight1 = 1;
//     params.movtDirectionWeight1 = 1;
//     params.movtResistWeight2 = 2;
//     params.movtAttractWeight2 = 1;
//     params.movtDirectionWeight2 = 1;

//     params.mateMaxAge=60;
//     params.mateAge = 17; //NOTE - make it so that the males have to be above 'matingAge' as well!!!
//     params.mateProb = .6;
//     params.mateMaxDist = 500;
//     params.clutchSize = 3;
    

//     //x limits: 620842.8 684842.8
//     //y limits: 3899220 3963220
//     //difference for both x and y is 64000
//     std::vector<int> initNAgents{100,1000,5000,10000,25000};
//     std::vector<double> initPointGridCellSize{100,500,1000,5000,10000};
//     std::vector<double> maxMatingDist{100,500,1000,5000,10000};
    
//     // std::vector<int> initNAgents{100, 200};
//     // std::vector<double> initPointGridCellSize{100,500};
//     // std::vector<double> maxMatingDist{500,1000};

//     std::string outputPath = "/Users/dfriend/Documents/cpp/abm/output/pointGrid_speed_testing/results.csv";
//     std::ofstream csv(outputPath, std::ofstream::trunc);
//     //std::ofstream csv;
//     //csv.open(outputPath);
//     csv << "type,n_agents,max_mating_dist,pg_cell_size,time_ms,n_agents_after\n";
//     for(int i_n_agents = 0; i_n_agents < initNAgents.size(); ++i_n_agents){
//         params.initNAgents = initNAgents.at(i_n_agents);

//         for(int i_mate_dist = 0; i_mate_dist < maxMatingDist.size(); ++i_mate_dist){
//             params.maxMatingDist = maxMatingDist.at(i_mate_dist);

//             //-------
//             //Do 'reproduce' without the point grid and time it
//             //-------
//             std::cout << "REGULAR: initNAgents: " << params.initNAgents << " | maxMatingDist: " << params.maxMatingDist << "\n"; 
//             Simulation sim1 = Simulation(params, quadtree, 2);
//             auto start1 = std::chrono::high_resolution_clock::now(); //get the start time
//             sim1.reproduce(); 
//             auto stop1 = std::chrono::high_resolution_clock::now(); //get the end time
//             auto duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(stop1 - start1); //get the duration
//             std::cout << "REGULAR: " << duration1.count() << "ms\n"; 
//             csv << "regular," << params.initNAgents << "," << params.maxMatingDist << ",NA," << duration1.count() << "," << sim1.agents.size() << "\n";
//             std::cout << "\n";
//             for(int i_pg_cell_size = 0; i_pg_cell_size < initPointGridCellSize.size(); ++i_pg_cell_size){
//                 params.initPointGridCellSize = initPointGridCellSize.at(i_pg_cell_size);

//                 //std::cout << "start" << std::endl;
//                 std::cout << "POINTGRID: initNAgents: " << params.initNAgents << " | maxMatingDist: " << params.maxMatingDist << " | initPointGridCellSize: " << params.initPointGridCellSize << "\n"; 
//                 Simulation sim2 = Simulation(params, quadtree, 2);
//                 auto start2 = std::chrono::high_resolution_clock::now(); //get the start time
//                 sim2.reproducePointGrid(); 
//                 auto stop2 = std::chrono::high_resolution_clock::now(); //get the end time
//                 auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(stop2 - start2); //get the duration
//                 std::cout << "POINTGRID: " << duration2.count() << "ms\n"; 

//                 csv << "pointgrid," << params.initNAgents << "," << params.maxMatingDist << "," << params.initPointGridCellSize << "," << duration2.count() << "," << sim2.agents.size() << "\n";
//                 std::cout << "\n";
//             }
//         }
//     }
//     csv.close();
//     return 0;
// }