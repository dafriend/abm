//#include "Point.h"
//#include "AgentPoint.h"
#include "Agent.h"
#include "PointCell.h"
#include <vector>
#include <memory>

PointCell::PointCell()
  : PointCell{-1,0}{}

PointCell::PointCell(int _id)
  : PointCell{_id, 0}{}

PointCell::PointCell(int _id, int size)
  : id{_id}, points{std::vector<std::shared_ptr<Agent>>(size)}{}

PointCell::PointCell(int _id, std::vector<std::shared_ptr<Agent>> &_points)
  : id{_id}, points{_points}{}

std::string PointCell::toString() const{
  std::string str = "points.size(): " + std::to_string(points.size());
  return str;
}

// std::shared_ptr<PointCell> makePointCell(int id){
//   //if(DEBUG) Rcout << "makePointCell()"<< std::endl;
//   //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
//   // PointCell* pointCell = new PointCell();
//   std::shared_ptr<PointCell> pointCell = std::make_shared<PointCell>();
//   pointCell->id = id;
//   // pointCell->ptr = std::shared_ptr<PointCell>(pointCell);
//   pointCell->points = std::vector<std::shared_ptr<Agent>>(0);
  
//   // return pointCell->ptr;
//   return pointCell;
// }

// std::shared_ptr<PointCell> makePointCell(int id, int size){
//   //if(DEBUG) Rcout << "makePointCell()"<< std::endl;
//   //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
//   PointCell* pointCell = new PointCell();
//   pointCell->id = id;
//   pointCell->ptr = std::shared_ptr<PointCell>(pointCell);
//   pointCell->points = std::vector<std::shared_ptr<Agent>>(size);
  
//   return pointCell->ptr;
// }

// std::shared_ptr<PointCell> makePointCell(int id, std::vector<std::shared_ptr<Agent>> &points){
//   //if(DEBUG) Rcout << "makePointCell(std::vector<Point> points)"<< std::endl;
//   //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
//   PointCell* pointCell = new PointCell();
//   pointCell->id = id;
//   pointCell->ptr = std::shared_ptr<PointCell>(pointCell);
//   pointCell->points = points;
  
//   return pointCell->ptr;
// }

