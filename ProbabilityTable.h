#ifndef PROBABILITYTABLE_H
#define PROBABILITYTABLE_H


#include <vector>
#include <tuple>
#include <string>
#include <random>


template <class T>
class ProbabilityTable{
public: 
    std::string filePath;
    //std::vector<Row> rows;  
    //std::vector<double> vals;
    std::vector<T> vals;
    std::vector<double> probs;

    ProbabilityTable();
    ProbabilityTable(std::string _filePath);

    void readCSV();
    void readCSV(std::string _filePath);
    
    // std::vector<double> getValues();
    // std::vector<double> getProbabilities();
    double getProb(T value);
    T getRandomValue(std::mt19937 &randomGenerator);

    std::string toString();
};

#include "ProbabilityTable.cpp"

#endif