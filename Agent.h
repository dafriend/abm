#ifndef AGENT_H
#define AGENT_H

#include "Point.h"
//#include "AgentPoint.h"
#include <memory>
#include <string>
#include <vector>

//class AgentPoint;

// struct Locus {
//     //Locus(int a1, int a2) : a1(a1), a2(a2) {}
//     std::vector<int> alleles;
//     int a1{0};
//     int a2{0};
// };

class Agent : public Point {
    
    //std::shared_ptr<AgentPoint> agentPoint; //pointer to point
public:
    int id{-1}; //agent ID
    int age{-1}; //agent age
    int isMale{-1}; //is agent a male
    int isAlive{-1}; //is agent alive
    int yearDied{-1}; //year the tort died
    double habitatRisk{0};
    double densityRisk{0};
    double ageRisk{0};
    double deathRisk{0};
    double resistance{0};
    double dist{0};
    double dir{0};
    //std::vector<Locus> loci;
    std::vector<std::vector<int> > loci;
    //int momId; //ID of mother
    //int dadId; //ID of father
    //Point point;
    std::shared_ptr<Agent> dadPtr; //pointer to dad
    std::shared_ptr<Agent> momPtr; //pointer to mom
    // std::weak_ptr<Agent> dadPtr; //pointer to dad
    // std::weak_ptr<Agent> momPtr; //pointer to mom
    // int dadId{-1};
    // double dadX{-1};
    // double dadY{-1};
    // int momId{-1};
    // double momX{-1};
    // double momY{-1};

    //std::shared_ptr<Agent> ptr; //pointer to self
    Agent();
    //Agent(double x, double y, int id);
    Agent(int _id, int _age, int _isMale, int _isAlive, double _x, double _y, std::shared_ptr<Agent> _dadPtr, std::shared_ptr<Agent> _momPtr, std::vector<std::vector<int> > _loci);
    
    //void setCoords(double _x, double _y); 
    void advanceAge(); //not yet implemented
    void kill(); //not yet implemented
    //void changePoint(double x, double y); //not yet implemented

    std::string toString() const;

    //friend std::shared_ptr<Agent> makeAgent(int id, int age, int isAlive);
};

// std::shared_ptr<Agent> makeAgent(int id, int age, int isMale, int isAlive, double x, double y, std::shared_ptr<Agent> dadPtr, std::shared_ptr<Agent> momPtr, std::vector<std::vector<int> > _loci);
//std::shared_ptr<Agent> makeAgent();

#endif