#dat = read.csv("~/Downloads/liveAgents00156.csv")
#dim(dat)
#head(dat)

#plot(dat$maxptx, dat$maxpty, asp=1, cex=.2, pch=16)


#test = read.csv("~/Documents/abm/input/thefirsttortsv4.csv")
#head(test)

library(spatstat)
library(quadtree)

qt = qt_read("/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken2.qtree")
#qt = qt_read("/Users/dfriend/Documents/abm/input/ivanpah_ken2.qtree")
#test = read.csv("/Users/dfriend/Documents/abm/output/sim_results/2021_06_10_023/output_data/liveAgents00020.csv")
test = read.csv("/Users/dfriend/Documents/cpp/abm/output/sim_results/2021_06_23_003/output_data/liveAgents00020.csv")
#png("~/Desktop/plot.png", width = 5, height = 5)
ext = qt_extent_orig(qt)
pt_ppp = ppp(test$maxptx, test$maxpty, window=owin(xrange = ext[1:2], yrange = ext[3:4]))
dens = density(pt_ppp, sigma=1000,edge=FALSE)
plot(raster(dens))
      #dev.off()

qt_plot(qt, crop=TRUE, border_col="transparent")
points(test$maxptx, test$maxpty, cex=.1, pch=16)

df = data.frame(x=test$maxptx,y=test$maxpty)
write.table(df, row.names=FALSE, col.names=FALSE,"/Users/dfriend/Documents/cpp/abm/scratch_data/points.csv", sep=",")

range(df$x)
range(df$y)
