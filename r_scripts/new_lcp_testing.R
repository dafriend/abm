library(quadtree)
data(habitat)
qt <- quadtree(habitat, .15)
start_point <- c(6989, 20142)
end_point <- c(33015, 38162)

plot(qt, crop = TRUE, border_lwd = .1)
points(rbind(start_point))
find_lcp(qt, start_point, start_point+1, use_orig_points = TRUE)
find_lcp(qt, start_point, start_point+1, use_orig_points = FALSE)

find_lcp(qt, start_point, c(-90, -90))

find_lcp(qt, start_point, start_point+2000, use_orig_points = TRUE)
find_lcp(qt, start_point, start_point+2000, use_orig_points = FALSE)

lcpf <- lcp_finder(qt, c(-90,-90))
find_lcp(lcpf, start_point + 1)
find_lcp(lcpf, start_point + 1, allow_same_cell_path = TRUE)

lcpf <- lcp_finder(qt, start_point, new_points = rbind(c(-90,-90)))
find_lcp(lcpf, start_point + 1)
find_lcp(lcpf, start_point + 1, allow_same_cell_path = TRUE)


end_point = start_point + 2000

lcpf <- lcp_finder(qt, start_point, new_points = rbind(start_point, end_point))
find_lcp(lcpf, start_point + 1)
find_lcp(lcpf, end_point)
find_lcp(lcpf, start_point)



find_lcp(qt, start_point, start_point+1, same_cell_path = TRUE, use_orig_end_pts = TRUE)
find_lcp(qt, start_point, start_point+1, same_cell_path = TRUE, use_orig_end_pts = FALSE)
find_lcp(qt, start_point, start_point+1, same_cell_path = FALSE, use_orig_end_pts = TRUE)
find_lcp(qt, start_point, start_point+1, same_cell_path = FALSE, use_orig_end_pts = FALSE)
test
start_point <- c(2168, 27836)
end_point <- c(35880, 28731)

quadtree::extract(qt, rbind(start_point, end_point))
lcpf <- lcp_finder(qt, start_point)
lcp <- find_lcp(lcpf, end_point)

find_lcp(lcpf, start_point+1)

plot(qt, border_lwd = .1, crop = TRUE)
points(rbind(start_point, end_point), col = "red", pch = 16)
lines(lcp)


lcpf2 <- lcp_finder(qt, start_point, new_pts = rbind(start_point, end_point))
lcp2 <- find_lcp2(lcpf2, end_point)
lcp2
plot(qt, border_lwd = .1, crop = TRUE)
points(rbind(start_point, end_point), col = "red", pch = 16)
lines(lcp)
lines(lcp2, col = "red")

#==========================================
plot(qt, border_lwd = .1, crop = TRUE)
points(rbind(start_point, end_point), col = "red", pch = 16)
lines(lcp)
points(lcp, pch = 16, col = "black", cex = .5)
lines(lcp2, col = "red")
points(lcp2, pch = 16, col = "red", cex = .5)

lims <- data.frame(locator(2))

plot(qt, border_lwd = .1, xlim = lims$x, ylim = lims$y)
points(rbind(start_point, end_point), col = "red", pch = 16)
lines(lcp)
points(lcp, pch = 16, col = "black", cex = .5)
lines(lcp2, col = "red")
points(lcp2, pch = 16, col = "red", cex = .5)

#==========================================

lims <- structure(list(x = c(1668.92243002385, 6494.82929584537), y = c(24165.2817873729, 28387.9502949667)), class = "data.frame", row.names = c(NA, -2L))
plot(qt, border_lwd = .1, xlim = lims$x, ylim = lims$y)

spt1 <- c(2037.186, 26036.2)
spt2 <- c(3969.109, 26021.89)
spt3 <- c(2960.216, 26980.69)
dest_pt <- c(2888.663, 25284.9)
points(rbind(spt1, spt2, spt3, dest_pt), col = 1:4, pch = 16)



lcpf1a <- lcp_finder(qt, spt1, new_pts = rbind(spt1, dest_pt))
lcpf2a <- lcp_finder(qt, spt2, new_pts = rbind(spt2, dest_pt))
lcpf3a <- lcp_finder(qt, spt3, new_pts = rbind(spt3, dest_pt))

lcp1a <- find_lcp(lcpf1a, dest_pt)
lcp2a <- find_lcp(lcpf2a, dest_pt)
lcp3a <- find_lcp(lcpf3a, dest_pt)

lines(lcp1a, col = 1, lwd = 2, lty = 1)
lines(lcp2a, col = 2, lwd = 2, lty = 1)
lines(lcp3a, col = 3, lwd = 2, lty = 1)

bench::mark(quadtree::extract(qt, rbind(spt1)))
#lcpf <- lcp_finder(qt, spt1, new_pts = rbind(spt1, spt2))
#find_lcp(lcpf, spt2)
#---------

lcpf1b <- lcp_finder(qt, spt1, new_pts = rbind(spt1, dest_pt))
lcpf2b <- lcp_finder(qt, spt2, new_pts = rbind(spt2, dest_pt))
lcpf3b <- lcp_finder(qt, spt3, new_pts = rbind(spt3, dest_pt))

lcp1b <- find_lcp2(lcpf1b, dest_pt)
lcp2b <- find_lcp2(lcpf2b, dest_pt)
lcp3b <- find_lcp2(lcpf3b, dest_pt)

lines(lcp1b, col = 1, lwd = 2, lty = 2)
lines(lcp2b, col = 2, lwd = 2, lty = 2)
lines(lcp3b, col = 3, lwd = 2, lty = 2)


lcpf1c <- lcp_finder(qt, spt1)
lcpf2c <- lcp_finder(qt, spt2)
lcpf3c <- lcp_finder(qt, spt3)

lcp1c <- find_lcp(lcpf1c, dest_pt, use_original_end_points = TRUE)
lcp2c <- find_lcp(lcpf2c, dest_pt, use_original_end_points = TRUE)
lcp3c <- find_lcp(lcpf3c, dest_pt, use_original_end_points = TRUE)

lines(lcp1c, col = 1, lwd = 2, lty = 3)
lines(lcp2c, col = 2, lwd = 2, lty = 3)
lines(lcp3c, col = 3, lwd = 2, lty = 3)

lcp1a
lcp1b

lcp2a
lcp2b

lcp3a
lcp3b

#==========================================
lcpf <- lcp_finder(qt, start_point, new_pts = rbind(start_point, end_point, c(0,0)))
lcp <- find_lcps(lcpf)
lcp
#==========================================
tail(lcp)
tail(lcp2)
library(raster)
library(quadtree)
rst <- raster("/Volumes/CC_Connectivity_Modeling/DerekCode/CC Habitat model 250m/EnsembleModelccBuffered.img")
qt2 <- quadtree(rst, .05, "sd")
qt

plot(qt2, crop = TRUE, border_lwd = .1)
start_point2 <- c(682694, 4000543)
points(rbind(start_point2))

f1 <- function(qt, pt){
  lcpf <- lcp_finder(qt, pt)
  lcps <- find_lcps(lcpf)
}

# f2 <- function(qt, pt){
#   lcpf <- lcp_finder(qt, pt, new_pts = rbind(pt))
#   lcps <- find_lcps2(lcpf)
# }

bench::mark(f1(qt, start_point),
  #          f2(qt, start_point), 
            check = FALSE)

bench::mark(f1(qt2, start_point2),
#            f2(qt2, start_point2), 
            check = FALSE)

# lims <- locator(2)
# df <- data.frame(lims)
df <- structure(list(x = c(735.881877710691, 6151.26583893188), y = c(24794.2086912152, 30413.9467641806)), class = "data.frame", row.names = c(NA, -2L))



start_point <- c(6989, 34007)
end_point <- c(33015, 38162)

plot(qt, border_lwd = .1, xlim = df$x, ylim = df$y)

pts <- locator(2)
pts <- data.frame(pts)

points(rbind(start_point, end_point), col = "red", pch = 16)
lines(lcp)

quadtree::extract(qt, rbind(start_point, end_point))
lcpf <- lcp_finder(qt, as.numeric(pts[1,]), new_pts = as.matrix(pts))
lcp <- find_lcp(lcpf, as.numeric(pts[2,]))
lcp

plot(qt, border_lwd = .1, xlim = df$x, ylim = df$y)
lines(lcp)


# write.csv(lcp,"lcps/qt_find_lcp_data.csv", row.names=FALSE)
#lcp_prev <- read.csv("lcps/qt_find_lcp_data.csv")
#expect_equal(lcp, as.matrix(lcp_prev))