library(ggplot2)
library(quadtree)
library(data.table)

sim_dir <- "/Users/dfriend/Documents/cpp/abm/output/sim_results/"
all_dirs <- dir(sim_dir)
first_sim <- "2021_11_26_081$"
last_sim <- "2021_11_26_107$"

first_ind <- which(grepl(first_sim, all_dirs))
last_ind <- which(grepl(last_sim, all_dirs))
sims <- all_dirs[first_ind:last_ind]
sims

out_paths <- paste0(sim_dir, sims, "/output_data")

qt_path <- "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken2_r.qtree"
qt <- read_quadtree(qt_path)


compare_df_list <- function(dfs){
  combns <- combn(length(dfs), 2)
  mat <- matrix(NA, nrow = length(dfs), ncol = length(dfs))
  bool <- logical(ncol(combns))
  for(i in 1:ncol(combns)){
    ind1 <- combns[1,i]
    ind2 <- combns[2,i]
    
    if(nrow(dfs[[ind1]]) != nrow(dfs[[ind2]])){
      bool[i] <- FALSE
    } else {
      bool[i] <- all(dfs[[ind1]] == dfs[[ind2]])
    }
    mat[ind1, ind2] <- bool[i]
    mat[ind2, ind1] <- bool[i]
  }
  df <- cbind(t(combns), data.frame(bool))
  return(list(all_same = all(bool), mat = mat, df = df))
}

# gets clusters in a binary adjacency matrix
get_clusters <- function(adj_mat){
  diag(adj_mat) <- TRUE
  eq <- apply(adj_mat, 2, which, simplify = FALSE)
  clusts <- unique(eq)
  names(clusts) <- 1:length(clusts)
  return(clusts)
}

# convenience functions for calculating the distance between points
pythag = function(a,b){ sqrt(a^2 + b^2) }
dist_btw_points = function(pt1, pt2){ pythag(pt1[1]-pt2[1], pt1[2]-pt2[2]) }
dist_btw_points_mat = function(pts1, pts2){ pythag(pts1[,1]-pts2[,1], pts1[,2]-pts2[,2]) }



# ======================
# check iteration info
iter_paths <- paste0(out_paths, "/iterInfo.csv")
iter_paths
it <- lapply(iter_paths, read.csv)
all_it <- do.call(cbind, lapply(it, `[[`, "nAgents"))
all_it

it_comp <- compare_df_list(it)
it_comp$all_same
it_comp$mat
it_comp$df

rows_dif <- apply(all_it, 1, function(x){
  all(x == x[1])
})

# first different row
first_dif <- min(which(!rows_dif))
first_dif
# ---- 
# make plot
all <- do.call(rbind, it)
xlim <- range(all$iter)
ylim <- range(all$nAgents)
plot(1,1, xlim = xlim, ylim = ylim, type = "n")
cols <- rainbow(length(it))
for(i in 1:length(it)){
  with(it[[i]], lines(iter, nAgents, col = cols[i]))  
}

#tail(it5)

#===================
# check movements
movt_paths <- paste0(out_paths, "/allMovt.csv")
movt <- lapply(movt_paths, fread)
sapply(movt, nrow)

movt_comp <- compare_df_list(movt)
movt_comp$all_same
movt_comp$mat
#movt_comp$df

#===================
# check final CSV

csv_paths <- paste0(out_paths, "/liveAgents00050.csv")
csvs <- lapply(csv_paths, fread)
cbind(sapply(csvs, nrow))

csv_compare <- compare_df_list(csvs)
csv_compare$all_same
csv_compare$mat
get_clusters(csv_compare$mat)
#===================
# check all CSVs
csv_paths2 <- dir(out_paths[1], "liveAgents\\d+[.]csv$")
csv_paths2
all_csvs <- lapply(1:length(csv_paths2), function(i){
  print(i)
  csv_paths_i <- paste0(out_paths, "/", csv_paths2[i])
  return(lapply(csv_paths_i, fread))
})

all_comp <- lapply(1:length(all_csvs), function(i){
  print(i)
  dfs_i <- lapply(all_csvs[[i]], function(x){
    for (i in names(x)) x[is.na(get(i)), (i):= -1]
    return(x)
  })
  comp <- compare_df_list(dfs_i)
  return(comp)
})
all_same <- sapply(all_comp, `[[`, "all_same")
all_same
mats <- lapply(all_comp, `[[`, "mat")

t(data.frame(iter = 1:length(all_same), all_same))


clusts <- lapply(mats, get_clusters)
n_clusts <- sapply(clusts, length)
rbind(iter = 1:length(n_clusts), n_clusts)

clusts[[length(clusts)]]
#=================================
# Scratch area
#=================================

mv1 <- movt[[1]]
mv2 <- movt[[4]]

min_nrow <- min(nrow(mv1), nrow(mv2))

mv1a <- mv1[1:min_nrow,]
mv2a <- mv2[1:min_nrow,]

mv_eq <- mv1a == mv2a

mv1a[18266:18276,]
mv2a[18266:18276,]

rows <- 18266:18276
rows <- 18266:18269
rows <- 18269:18276
cbind(mv1a[rows, ], mv2a[rows, ], mv_eq[rows, 5])


#rows1 <- 
xlim <- range(mv1a$x[rows], mv2a$x[rows])
ylim <- range(mv1a$y[rows], mv2a$y[rows])

plot(qt)
plot(qt, xlim = xlim, ylim = ylim)

apply(mv_eq, 2, function(x) min(which(!x)))


clusts[[1]]
clusts[[51]]
clusts[[8]]
clusts[[14]]
clusts[[17]]
clusts[[19]]
clusts[[20]]
clusts[[21]]
clusts[[25]]
clusts[[29]]
clusts[[31]]
clusts[[32]]
clusts[[33]]
clusts[[36]]
clusts[[38]]
clusts[[42]]


test <- compare_df_list(all_csvs[[2]])
test$mat
test <- compare_df_list(all_csvs[[1]])
test$mat

lst <- all_csvs[[1]]
#==================
# check difs in more detail

# the two sims to compare
inds <- c(1,4)
iter_compare <- 8
iter_dfs <- all_csvs[[iter_compare]]
dat1 <- iter_dfs[[inds[1]]]
dat2 <- iter_dfs[[inds[2]]]

min_nrow <- min(nrow(dat1), nrow(dat2))

dat1a <- dat1[1:min_nrow,]
dat2a <- dat2[1:min_nrow,]

a_cols <- grepl("allele", names(dat1a))

comp <- dat1a == dat2a
all(comp)
head(comp)
sum(!comp)

dif_inds_all <- apply(comp, 2, function(x) which(!x), simplify = FALSE)
dif_inds_all
dif_inds <- apply(comp, 2, function(x) min(which(!x)))
dif_inds
min(dif_inds)
min(dif_inds[dif_inds > 1])

min_ind <- 8464
dat1a[min_ind, !..a_cols]
dat2a[min_ind, !..a_cols]
pt1 <- dat1a[min_ind, .(x,y) ]
pt2 <- dat2a[min_ind, .(x,y) ]
pt1
pt2

id1 <- dat1a$tid[min_ind]
id2 <- dat2a$tid[min_ind]
id1 == id2

iter_dfs <- all_csvs[[iter_compare]]
prev_dat1 <- all_csvs[[iter_compare - 1]][[inds[1]]]
prev_dat2 <- all_csvs[[iter_compare - 1]][[inds[2]]]
prev_pt1 <- prev_dat1[prev_dat1$tid == id1, .(x, y)]
prev_pt2 <- prev_dat2[prev_dat2$tid == id1, .(x, y)]

prev_pt1
prev_pt2
plot(qt, crop = TRUE, border_col = "transparent")
points(rbind(pt1, pt2), col = c("red", "blue"), pch = 16)
points(rbind(prev_pt1))

dist_btw_points_mat(prev_pt1, pt1)
dist_btw_points_mat(prev_pt1, pt2)

# as.numeric(pt2)
# qt@ptr$getCell(as.numeric(pt2))
# extract(qt, pt2, extents = TRUE)
lims = locator(2)
plot(qt, xlim = lims$x, ylim = lims$y)
points(pt2)
points(rbind(pt1, pt2), col = c("red", "blue"), pch = 16)
points(rbind(prev_pt1))
             
dest_pt <- c(644967.64481537428, 3942295.3016565316)
path <- rbind(
  c(645112.22939999995, 645362.22939999995, 3942301.9229000001, 3942551.9229000001),
  c(644862.22939999995, 645112.22939999995, 3942301.9229000001, 3942551.9229000001),
  c(644862.22939999995, 645112.22939999995, 3942051.9229000001, 3942301.9229000001))
path <- data.frame(path)
names(path) <- c("x0", "x1", "y0", "y1")


points(rbind(dest_pt), col = "purple", pch = 17)
with(path, rect(x0, y0, x1, y1, border = "red", lwd = 2))

dest_pt2 <- c(644967.64481537428, 3942295.3016565316)
path2 <- rbind(
  c(645112.22939999995, 645362.22939999995, 3942301.9229000001, 3942551.9229000001),
  c(645112.22939999995, 645362.22939999995, 3942051.9229000001, 3942301.9229000001),
  c(644862.22939999995, 645112.22939999995, 3942051.9229000001, 3942301.9229000001))
path2 <- data.frame(path2)
names(path2) <- c("x0", "x1", "y0", "y1")
with(path2, rect(x0, y0, x1, y1, border = "blue", lwd = 2))
#-----------

pt1 <- dat1a[2756-1, .(x,y) ]
pt2 <- dat2a[2756-1, .(x,y) ]
pt1
pt2
id1 <- dat1a$tid[2756-1]
id2 <- dat2a$tid[2756-1]
id1 == id2

iter_dfs <- all_csvs[[iter_compare]]
prev_dat1 <- all_csvs[[iter_compare - 1]][[inds[1]]]
prev_dat2 <- all_csvs[[iter_compare - 1]][[inds[2]]]
prev_pt1 <- prev_dat1[prev_dat1$tid == id1, .(x, y)]
prev_pt2 <- prev_dat2[prev_dat2$tid == id1, .(x, y)]

prev_pt1
prev_pt2
plot(qt, crop = TRUE, border_col = "transparent")
points(rbind(pt1, pt2), col = c("red", "blue"), pch = 16)
points(rbind(prev_pt1))

dist_btw_points_mat(prev_pt1, pt1)
dist_btw_points_mat(prev_pt1, pt2)

lims = locator(2)
plot(qt, xlim = lims$x, ylim = lims$y)
points(rbind(pt1, pt2), col = c("red", "blue"), pch = 16)
points(rbind(prev_pt1))








unq_dif_inds <- sort(unique(dif_inds))
unq_dif_inds
for(i in 1:length(unq_dif_inds)){
  print(unq_dif_inds[i])
  dif_cols <- names(dat1a)[which(!comp[unq_dif_inds[i],])]
  print(dif_cols)
  if(is.infinite(unq_dif_inds[i])){
    print("infinite")
  } else {
    inds_i <- (unq_dif_inds[i] - 1):(unq_dif_inds[i] + 1)
    inds_i <- inds_i[inds_i > 0 & inds_i <= min_nrow]
    print(rbind(dat1a[inds_i], NA, dat2a[inds_i], fill = TRUE))
  }
  input <- readline("enter to continue, q to quit:\n")
  if(input == "q"){
    break()
  }
}

# check coords of agent with different density risk

lims <- locator(2)

plot(qt, crop = TRUE, xlim = lims$x, ylim = lims$y)
points(649841.2, 3973047.6, pch = 16, col = "blue")
points(650015.6, 3972949.7, pch = 16, col = "red")
points(dat1a[319, .(x,y)], pch = 16, col = "red")
max()

#locator(2)
xlim <- c(669005.8, 673195.6)
ylim <- c(3960410, 3965198)
plot(qt, xlim = xlim, ylim = ylim, col = hcl.colors(100))
points(dat1a[319, .(x,y)], pch = 16, col = "red")




dat1a[319, .(x,y)]
xlim <- quadtree::extent(qt)[1:2]
ylim <- quadtree::extent(qt)[3:4]
xseq <- seq(xlim[1], xlim[2], length.out = 129)
yseq <- seq(ylim[1], ylim[2], length.out = 129)
xseq
yseq








