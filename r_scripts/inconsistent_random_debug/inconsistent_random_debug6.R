library(ggplot2)

sim_dir <- "/Users/dfriend/Documents/cpp/abm/output/sim_results/"
sims <- c("2021_11_23_012",
          "2021_11_23_013",
          "2021_11_23_014",
          "2021_11_23_015",
          "2021_11_23_016")
          

out_paths <- paste0(sim_dir, sims, "/output_data")

qt_path <- "/Users/dfriend/Documents/cpp/abm/input/ivanpah_ken2_r.qtree"
qt <- read_quadtree(qt_path)

# ======================
# check iteration info
iter_paths <- paste0(out_paths, "/iterInfo.csv")
iter_paths
it <- lapply(iter_paths, read.csv)
all_it <- do.call(cbind, lapply(it, `[[`, "nAgents"))
all_it

combns <- combn(length(it), 2)
bool <- sapply(1:ncol(combns), function(i){
  return(all(it[[ combns[1,i] ]] == it[[ combns[2,i] ]]))
})
all(bool)
bool
cbind(t(combns), data.frame(bool))

rows_dif <- apply(all_it, 1, function(x){
  all(x == x[1])
})

# first different row
first_dif <- min(which(!rows_dif))
first_dif
# ---- 
# make plot
all <- do.call(rbind, it)
xlim <- range(all$iter)
ylim <- range(all$nAgents)
plot(1,1, xlim = xlim, ylim = ylim, type = "n")
cols <- rainbow(length(it))
for(i in 1:length(it)){
  with(it[[i]], lines(iter, nAgents, col = cols[i]))  
}

#tail(it5)

#===================
# check movements
movt_paths <- paste0(out_paths, "/allMovt.csv")
movt <- lapply(movt_paths, read.csv)
cbind(sapply(movt, nrow))

combns_m <- combn(length(movt), 2)
bool_m <- sapply(1:ncol(combns_m), function(i){
  # return(all(movt[[ combns_m[1,i] ]] == movt[[ combns_m[2,i] ]]))
  movt1 <- movt[[ combns_m[1,i] ]]
  movt2 <- movt[[ combns_m[2,i] ]]
  if(nrow(movt1) != nrow(movt2)) return(FALSE)
  return(all( movt1 == movt2 ))
})
all(bool_m)
bool_m
cbind(t(combns_m), data.frame(bool_m))

#===================
# check final CSV

csv_paths <- paste0(out_paths, "/liveAgents00050.csv")
csvs <- lapply(csv_paths, fread)
cbind(sapply(csvs, nrow))

combns_c <- combn(length(movt), 2)
bool_c <- sapply(1:ncol(combns_c), function(i){
  csv1 <- csvs[[ combns_c[1,i] ]]
  csv2 <- csvs[[ combns_c[2,i] ]]
  if(nrow(csv1) != nrow(csv2)) return(FALSE)
  return(all( csv1 == csv2 ))
})
all(bool_c)
bool_c
cbind(t(combns_c), data.frame(bool_c))

#==================
# check difs in more detail

# the two sims to compare
inds <- c(1,3)
csv_compare <- paste0(out_paths[inds], "/liveAgents00001.csv")
dat1 <- fread(csv_compare[1])
dat2 <- fread(csv_compare[2])
min_nrow <- min(nrow(dat1), nrow(dat2))

dat1a <- dat1[1:min_nrow,]
dat2a <- dat2[1:min_nrow,]

a_cols <- grepl("allele", names(dat1a))

comp <- dat1a == dat2a
all(comp)
head(comp)

dif_inds <- apply(comp, 2, function(x) min(which(!x)))
dif_inds
min(dif_inds)
min(dif_inds[dif_inds > 1])

unq_dif_inds <- sort(unique(dif_inds))
unq_dif_inds
for(i in 1:length(unq_dif_inds)){
  print(unq_dif_inds[i])
  dif_cols <- names(dat1a)[which(!comp[unq_dif_inds[i],])]
  print(dif_cols)
  if(is.infinite(unq_dif_inds[i])){
    print("infinite")
  } else {
    inds_i <- (unq_dif_inds[i] - 1):(unq_dif_inds[i] + 1)
    inds_i <- inds_i[inds_i > 0 & inds_i <= min_nrow]
    print(rbind(dat1a[inds_i], NA, dat2a[inds_i], fill = TRUE))
  }
  input <- readline("enter to continue, q to quit:\n")
  if(input == "q"){
    break()
  }
}

# check coords of agent with different density risk

lims <- locator(2)

plot(qt, crop = TRUE, xlim = lims$x, ylim = lims$y)
points(649841.2, 3973047.6, pch = 16, col = "blue")
points(650015.6, 3972949.7, pch = 16, col = "red")
points(dat1a[319, .(x,y)], pch = 16, col = "red")
max()

#locator(2)
xlim <- c(669005.8, 673195.6)
ylim <- c(3960410, 3965198)
plot(qt, xlim = xlim, ylim = ylim, col = hcl.colors(100))
points(dat1a[319, .(x,y)], pch = 16, col = "red")




dat1a[319, .(x,y)]
xlim <- quadtree::extent(qt)[1:2]
ylim <- quadtree::extent(qt)[3:4]
xseq <- seq(xlim[1], xlim[2], length.out = 129)
yseq <- seq(ylim[1], ylim[2], length.out = 129)
xseq
yseq









