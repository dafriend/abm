# IV_init.R must be run first 

#libraries
library(stringr)  #wrappers for string operators(like extracting and replacing vectors - ex. csv year numbers)
library(R.utils)  #to count lines in files, etc.
library(RColorBrewer)
library(rgdal)    #to read shapefiles
library(raster)
library(splitstackshape) #to stratify, etc.
library(adegenet)
library(rgeos)

dir.create(OUT.DIR)
dir.create(SAMPLE.DIR)
dir.create(SAMPLE.ALL.DIR)
dir.create(SAMPLE.OLD.DIR)
dir.create(SAMPLE.YOUNG.DIR)

# Bring in needed csv files for each landscape scenario
files <- dir(TORT.DIR, pattern=paste0(FILES.BASENAME, "\\d*\\.csv$"))
files_full <- paste0(TORT.DIR, "/", files)
#files_nums <- gsub("\\D", "", files)
#files_nums  #to create names for writing files

dats <- lapply(files_full, fread)
dat <- rbindlist(dats)  


zones_path <- "/Volumes/CC_Connectivity_Modeling/CompletedRuns/Outputs_IVvanpah _StaticHabitatModelRuns_V9/Zone File for IV BaseRuns/IVBaseRunNoBarrierV9zones.img"
zones_path <- "/Users/dfriend/Documents/cpp/abm/input/tort_plots/tort_plots.shp"
zns0 <- readOGR(zones_path)
zns <- spTransform(zns0, quadtree::projection(qt))
#zns0 <- vect(zones_path)
#zns <- terra::project(zns0, quadtree::projection(qt))

# get the zone that each point falls in
# finding which polygon a point falls in (if any) takes a while - to be more
# efficient I'm using a raster representing the zones to get the zones that
# an agent falls in. This will be slightly inaccurate (the amount of error
# will depend on the size of the cell), but for my purposes that tiny bit of 
# error is inconsequential
lims <- dat[, .(xlim = range(x), ylim = range(y))]
bb <- bbox(zns)
ext <- raster::extent(c(bb[1,], bb[2,]))
template <- raster(ext, res = 20, crs = crs(zns))
zns_rst <- raster::rasterize(zns, template)
#plot(zns_rst)
# ext <- terra::ext(c(lims$xlim, lims$ylim))
# template <- rast(ext, res = 10, crs = crs(zns))
# zns_rst <- raster::rasterize(zns, template)

dat[, zone :=  raster::extract(zns_rst, dat[,.(x,y)])]
get_samp <- function(dat, n){
  if(nrow(dat) < n){
    return(dat)
  } else {
    dat[sample(1:nrow(dat), n)]
  }
}

n_samp <- 50
samp <- dat[!is.na(zone), get_samp(.SD, ..n_samp), keyby = .(yr, zone)]
samp_sum <- samp[, .N, keyby = .(yr, zone)]
head(samp_sum, 50)

#test <- over(pts, zns)
#int <- rgeos::gIntersects(pts, zns, byid = TRUE)
int <- rgeos::gIntersects(zns, pts, byid = TRUE)
int_inds <- apply(int, 2, which)
head(test)
system.time(intersect(pts, zns))

test <- data.table(int)
apply()
test[any(.SD)]

pts <- vect(dat[,.(x,y)], geom = c("x", "y"), crs = proj4string(zns))
pts2 <- vect(pts)

terra::extract(zns, pts)
zn_pts <- intersect(pts, zns)

gUnaryUnion(zns)

system.time(over(pts, zns))
over_df <- over(zn_pts, zns)
zn_pts$zone <- over_df$id
as.data.table(zn_pts)
test <- SpatialPoints(dat[,.(x,y)], proj4string = crs(proj4string(zns)))
test[zns]
pts[zns,]

test <- over(zns, pts)

table(test$id)
test <- readOGR("/Users/dfriend/Downloads/rv16my07/rv16my07.shp")
plot(test)
plot(zns)
plot(r)
# lapply(filez1.full, FUN=read.csv, sep=',') #lapply = apply list with fun to read csv's
# head(files.raw1[[1]])
# 
# n_df_list <- lapply(1:length(files.raw1), function(i){
#   n_all <- nrow(files.raw1[[i]])
#   n_alive <- sum(files.raw1[[i]]$alive == 1)
#   n_dead <- sum(files.raw1[[i]]$alive == 0)
#   return(data.frame(N_tot = n_all, N_alive = n_alive, N_dead = n_dead))
# })
# n_df <- do.call(rbind, n_df_list)
# write.csv(n_df, paste0(OUT.DIR, "table_n_per_year.csv"))

# Check zones with points
mypal <- brewer.pal(8, "Pastel1")
#zone_map <- readOGR(ZONES.PATH) 
zone_map <- raster(ZONES.PATH)
# nd_map <- raster(DEM.PATH)
# nd_map_sp <- raster::crop(nd_map, zone_map)

dev2(paste0(OUT.DIR, "plot_zones"))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = "Zones")
dev.off2()

# dev2(paste0(OUT.DIR, "plot_zones_dem"))
# plot(nd_map_sp, axes=FALSE, xlab="", ylab="", col = grey.colors(10, start=0, end=1))
# plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, add = TRUE)
# dev.off2()

#dev2(paste0(OUT.DIR, "plot_zones_labelled"))
#plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, border = "transparent", main = "Zones")
#zone_cents <- gCentroid(zone_map, byid = TRUE)
#text(zone_cents, labels = zone_map$Layer_1)
#dev.off2()

dev2(paste0(OUT.DIR, "plot_torts_yr_1"))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = "Year 1")
points(c(files.raw1[[1]]$x), c(files.raw1[[1]]$y), pch = 16, cex = .3)
dev.off2()

dev2(paste0(OUT.DIR, "plot_torts_yr_", length(files.raw1)))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = paste0("Year ", length(files.raw1)))
points(c(files.raw1[[length(files.raw1)]]$x), c(files.raw1[[length(files.raw1)]]$y), pch = 16, cex = .3)
dev.off2()


# Make subsets for young and old tortoises
files.raw1_y <- lapply(files.raw1, function(x) x[x$age %in% AGE.YOUNG,])
files.raw1_o <- lapply(files.raw1, function(x) x[x$age %in% AGE.OLD,])

# check subsets
head(files.raw1_y[[1]])
head(files.raw1_o[[1]])

# cut dataset down to a subsample
files.raw1.smpl <- lapply(files.raw1, function(x) stratified(indt=x, group=c("zone"), size=round(N.SAMPLE), select=list(zone=ZONES)))
files.raw1.smpl_y <- lapply(files.raw1_y, function(x) stratified(indt=x, group=c("zone"), size=round(N.SAMPLE), select=list(zone=ZONES)))
files.raw1.smpl_o <- lapply(files.raw1_o, function(x) stratified(indt=x, group=c("zone"), size=round(N.SAMPLE), select=list(zone=ZONES)))

# check sampled data
head(files.raw1.smpl[[1]])
head(files.raw1.smpl_y[[1]])
head(files.raw1.smpl_o[[1]])

# plot sampled data 
dev2(paste0(OUT.DIR, "plot_samples_yr1_all"))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = "Sampled tortoises | all ages | year 1")
points(c(files.raw1.smpl[[1]]$x), c(files.raw1.smpl[[1]]$y), pch = 20)
dev.off2()

dev2(paste0(OUT.DIR, "plot_samples_yr1_young"))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = "Sampled tortoises | young | year 1")
points(c(files.raw1.smpl_y[[1]]$x), c(files.raw1.smpl_y[[1]]$y), pch = 20)
dev.off2()

dev2(paste0(OUT.DIR, "plot_samples_yr1_old"))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, main = "Sampled tortoises | old | year 1")
points(c(files.raw1.smpl_o[[1]]$x), c(files.raw1.smpl_o[[1]]$y), pch = 20)
dev.off2()

# save sampled data
for(i in 1: length(files.raw1.smpl)){
  write.csv(files.raw1.smpl[[i]], paste0(SAMPLE.ALL.DIR, "/", files.ll[i],".csv"))
  write.csv(files.raw1.smpl_y[[i]], paste0(SAMPLE.YOUNG.DIR, "/", files.ll[i],"_y.csv"))
  write.csv(files.raw1.smpl_o[[i]], paste0(SAMPLE.OLD.DIR, "/", files.ll[i],"_o.csv"))
}

# create subsample map for report using nd map and specified zones
dev2(paste0(OUT.DIR, "plot_samples_yr1_dem_zones"))
plot(nd_map_sp, axes=FALSE, xlab="", ylab="", col = grey.colors(10, start=0, end=1))
plot(zone_map, axes=FALSE, xlab="", ylab="", col=mypal, add = TRUE)
points(c(files.raw1.smpl[[1]]$x), c(files.raw1.smpl[[1]]$y), pch = 20)
dev.off2()

dev2(paste0(OUT.DIR, "plot_samples_yr1_dem"))
plot(nd_map_sp, axes=FALSE, xlab="", ylab="", col = grey.colors(10, start=0, end=1))
points(c(files.raw1.smpl[[1]]$x), c(files.raw1.smpl[[1]]$y), pch = 20)
dev.off2()