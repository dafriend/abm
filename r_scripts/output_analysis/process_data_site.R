#run one of the site init files before running this files

# zone_dir <- paste0(SITE_DIR, "Zone File for IV BaseRuns/")

# #barrier_file <- "/Volumes/CC_Connectivity_Modeling/DerekCode/IV_barrier_rast/IV_lin_barriers.img"
# barrier_file <- paste0(OUT_SITE_DATA_DIR, "barriers_rast.img")
# zone_file <- paste0(OUT_SITE_DATA_DIR, "zones_rast.img")

# zones_regex <- c(zone_dir, "\\.img$")
# zones_path <- dir_cust(zones_regex[1], zones_regex[2], recursive = FALSE, full.names = TRUE)

zones_rast <- raster::raster(ZONES_PATH)
zones_rast <- raster::trim(zones_rast)
zones_coords <- centroids_from_zone_rast(zones_rast)

save_sep(zones_rast, zones_coords,
         dir = OUT_SITE_DATA_DIR)

#=========================================
# make barriers rast
if(FALSE){ #disable for now
  nad83z11 <- CRS("+init=epsg:26911")
  ll <- CRS("+init=epsg:4326")
  aea <- CRS("+init=epsg:2163")
  riaea <- '+proj=aea +lat_0=23 +lon_0=-96 +lat_1=29.5 +lat_2=45.5 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs'
  proj <- CRS("+proj=utm +zone=11 +ellps=GRS80 +units=m +no_defs")
  
  
  ## rescale function to set raster to 0-1 ##
  r.rescale <- function(x, x.min = -3.131791, x.max = 3.387701, new.min = 0, new.max = 255) {
    if(is.null(x.min)) {x.min = min(x)}
    if(is.null(x.max)) {x.max = max(x)}
    new.min + (x - x.min) * ((new.max - new.min) / (x.max - x.min))
  }
  
  # Boundary for the Ivanpah area
  wshd.bnd <- readOGR(WATERSHED_PATH)
  
  # reproject to project projection used
  wshd.bnd <- spTransform(wshd.bnd, proj)
  
  # bring in buffered cc to crop the unused part off
  cc.buff <- readOGR(paste0(GIS_DIR, "base_layers/CC_20km_buffer/CC_20km_buffer.shp"))
  
  
  runyears <- 2020 # number of years to run
  #for (i in 1:length(runyears)){
  ### initial habitat model read in ####
  dt <- raster(HABITAT_PATH)
  dt.utm <- projectRaster(dt, crs =proj)
  ## trim habitat to iv and cc
  dt.utm <- mask(dt.utm, wshd.bnd)
  dt.utm <- mask(dt.utm, cc.buff)
  
  
  ## code to square up the cell sizes if needed  ####
  # dt2 <- dt
  # res(dt2) <- c(250,250)
  # dt2 <- resample(dt, dt2)
  # plot(dt2)
  # writeRaster(dt2, 'DTIvanpah250.img', driver='HFA')
  # 
  # ## lat long and utm hab model projections
  # dt.ll <- projectRaster(dt, crs=ll)
  # dt.utm <- projectRaster(dt, crs = proj)
  
  
  
  ## using this raster for the 30m base layer ####
  thisurban <- paste0(GIS_DIR, "urban_growth/ssp5_clark_county/year",runyears,".img")
  urban.r <- raster(thisurban)
  ## reproject boundary to urban layer (faster than the reverse)
  wshd.tmp <- spTransform(wshd.bnd, proj4string(urban.r))
  # crop to our boundary
  urban.c.r <- crop(urban.r, wshd.tmp)
  # project the cropped image back to our project CRS
  urban.c.r <- projectRaster(urban.c.r, crs=proj)
  # finally mask to our boundary edges
  
  urban.m.r <- mask(urban.c.r, wshd.bnd)
  urban.m.r.sr <- terra::rast(urban.m.r)
  #urban.m.r <- clusterR(x=urban.c.r, cl=cl, fun= mask, mask=wshd.bnd)
  #endCluster(cl)
  # plot(urban.c.r)
  # plot(urban.m.r)
  # invert the urban layer so that urban is 1
  urban.r <- raster(urban.m.r.sr)
  ## resample habitat to match the urban base layer
  # note this downscales using a bilinear by default
  dt.utm.sr <- terra::rast(dt.utm)
  #class(dt.utm.sr)
  dt.utm.sr <- terra::resample(dt.utm.sr, urban.m.r.sr, method = 'cubicspline')
  dt.utm <- raster(dt.utm.sr)
  # roads ####
  roads <-  readOGR(paste0(GIS_DIR, "roads/nv_maj_road_traffic.shp"))
  
  ## reproject
  roads.utm <- spTransform(roads, proj)
  ## select roads only in study area
  roads.utm <- roads.utm[wshd.bnd,]
  
  ## separate the three categories - note IV only has 2 of the cats
  roads3 <- roads.utm[roads.utm$Road_Cat ==3,]
  roads2 <- roads.utm[roads.utm$Road_Cat ==2,]
  roads1 <- roads.utm[roads.utm$Road_Cat ==1,]
  # plot(dt.utm)
  # lines(roads3)
  # lines(roads1)
  # 
  
  ## rasterize the roads to the 30 m grid ####
  if(dim(roads3)[1] >0){
    roads.maj <- rasterize(roads3, urban.r, field = 'Road_Cat')
  }
  #plot(roads.maj)
  if(dim(roads2)[1] >0){
    roads.med <-  rasterize(roads2, urban.r, field = 'Road_Cat')
  }
  #writeRaster(roads.maj, 'roadsmaj.img', driver ='HFA')
  if(dim(roads1)[1] >0){
    roads.min <-  rasterize(roads1, urban.r, field = 'Road_Cat')
  }
  
  
  
  
  ## railroad  ####
  rr <- readOGR(paste0(GIS_DIR, "railroads/rail_road_center_lines.shp"))
  # reproject
  rr <- spTransform(rr,proj)
  # select in study area
  rr <- rr[wshd.bnd,]
  #plot(rr)
  # rasterize
  rr.r <- rasterize(rr, dt.utm)
  
  ## Fencing
  fence <- readOGR(paste0(GIS_DIR, "fencing/Tortoise_Fencing_2019.shp"))
  # reproject
  fence <- spTransform(fence, proj)
  # select within area
  fence <- fence[wshd.bnd,]
  #lines(fence, col='blue')
  # rasterize
  fence.r <- rasterize(fence, dt.utm)
  
  barriers_rast <- dt.utm
  barriers_rast[] <- NA
  if(exists("roads.maj")) barriers_rast[roads.maj > 0] <- 1
  if(exists("roads.med")) barriers_rast[roads.med > 0] <- 1
  if(exists("roads.min")) barriers_rast[roads.min > 0] <- 1
  barriers_rast[rr.r > 0] <- 1
  #barriers_rast[solar.r > 0] <- 999
  barriers_rast[fence.r > 0] <- 1
  barriers_rast[urban.m.r > 0] <- 1
  #barriers_rast[barriers_rast <= 0.2] <- 999
  # barriers_rast[barriers_rast > 900] <- NA
  barriers_rast <- mask(barriers_rast, dt.utm)
  barriers_rast <- crop(barriers_rast, zones_rast)
  save_sep(barriers_rast, dir = OUT_SITE_DATA_DIR)
}
  #plot(barriers_rast)
  
  #writeRaster(barriers_rast, paste0(OUT_SITE_DATA_DIR, "barriers_rast.img"), overwrite = TRUE)
  #writeRaster(barriers_rast, "/Users/dfriend/Documents/clark_county_project/IV_lin_barriers.img", overwrite = TRUE)
  # dt.zones.p <- rasterToPolygons(dt.zones.c)
  # dt.zones.p2 <- gUnaryUnion(dt.zones.p, id=dt.zones.p@data$clumps)
  # dt.zones.p2 <- as(dt.zones.p2, 'SpatialPolygonsDataFrame')
  # dt.zones.p2$zone <-1:length(dt.zones.p2)
  # dt.zones.p2 <- dt.zones.p2[,-1]
  # 
  # writeOGR(dt.zones.p2, paste0(outdir,'/',simname,'zones.shp'), layer='zones', driver='ESRI Shapefile', overwrite=TRUE)
  
  # get rid of un-needed files to clear ram
  
#}