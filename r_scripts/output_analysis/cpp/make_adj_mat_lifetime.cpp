#include <Rcpp.h>
#include <string>
#include <vector>
#include <map>
using namespace Rcpp;

// given a data frame, add each agents current zone to its vector of visited zones
// if it isnt already in the vector (each agents vector is an element of agentLocs)
void addAgentZones(DataFrame &df, std::map<std::string, IntegerVector> &agentLocs, std::string zoneColName, std::string idColName){
  // get the zone and ID columns
  // IntegerVector zoneCol = df["zone"];
  // IntegerVector idCol = df["tid"];
  // 
  IntegerVector zoneCol = df[zoneColName];
  IntegerVector idCol = df[idColName];
  // iterate over the data frame
  for(int i = 0; i < df.nrow(); ++i){
    //std::string zone_str_i = as<std::string>(zoneCol[i]);
    int zone_i = zoneCol[i];
    std::string id_i = std::to_string(idCol[i]);
    
    // if there isnt an entry for this agent in agentLocs, create one 
    if(agentLocs.find(id_i) == agentLocs.end()){
      agentLocs[id_i] = zoneCol[i];
    } else { // otherwise add to the existing vector
      // check if the current zone is already in the vector - if not, add it
      if(std::find(agentLocs[id_i].begin(), agentLocs[id_i].end(), zone_i) == agentLocs[id_i].end()){
        agentLocs[id_i].push_back(zone_i);
      }
    }
  }
}

// [[Rcpp::export]]
// IntegerMatrix make_adj_mat_lifetime(const List &dfs, std::string zoneColName, std::string idColName){
List make_adj_mat_lifetime(const List &dfs, std::string zoneColName, std::string idColName){
  // Rcout << "test1\n";
  auto dfVec = as<std::vector<DataFrame>>(dfs);
  std::map<std::string, IntegerVector> agentLocs;
  // Rcout << "test2\n";
  //List agentLocs;
  IntegerVector uniqueZones;
  for(size_t i = 0; i < dfVec.size(); ++i){
    addAgentZones(dfVec[i], agentLocs, zoneColName, idColName);
    // IntegerVector dfZones = dfVec[i]["zone"];
    IntegerVector dfZones = dfVec[i][zoneColName];
    uniqueZones = union_(uniqueZones, unique(dfZones));
  }
  // Rcout << "test3\n";
  uniqueZones = na_omit(uniqueZones);
  IntegerMatrix adjMat(uniqueZones.length(), uniqueZones.length());
  CharacterVector uniqueZonesChar = as<CharacterVector>(uniqueZones);
  IntegerVector matInds = seq(0,uniqueZones.length()-1);
  matInds.names() = uniqueZonesChar;
  colnames(adjMat) = uniqueZonesChar;
  rownames(adjMat) = uniqueZonesChar;
  // 
  // Rcout << "uniqueZones: " << uniqueZones << "\n";
  // Rcout << "uniqueZonesChar: " << uniqueZonesChar << "\n";
  // Rcout << "matInds: " << matInds << "\n";
  //Rcout << "adjMat:\\n" << adjMat << "\\n";
  // Rcout << "test4\n";
  for(auto it = agentLocs.begin(); it != agentLocs.end(); it++){
    IntegerVector zones = na_omit(it->second);
    if(zones.length() > 1){
      for(int i = 0; i < (zones.length() - 1); ++i){
        for(int j = i + 1; j < zones.length(); ++j){
          int zoneInd1 = matInds[std::to_string(zones[i])];
          int zoneInd2 = matInds[std::to_string(zones[j])];
          adjMat(zoneInd1, zoneInd2)++;
          adjMat(zoneInd2, zoneInd1)++;
        }
      }
    }
  }
  // Rcout << "test5\n";
  List agentLocsList = wrap(agentLocs);
  // Rcout << "test6\n";
  List returnList = List::create(Named("agent_zones") = agentLocsList, Named("adj_mat") = adjMat);
  return returnList;
  // Rcout << "test7\n";
  // List agentLocsList(agentLocs.size());
  // for (auto it = agentLocs.begin(); it != agentLocs.end(); ++it) {
  //   agentLocsList[it->first] = it->second;
  // }
  
  // return adjMat;
}