#ifndef BASICUTILITIES_H
#define BASICUTILITIES_H

#include <string>
#include <vector>
namespace BasicUtilities{

    
    //std::string cleanString(std::string str); //removes special characters
    void cleanString(std::string & str);
    //https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    void ltrim(std::string &s); // trim from start (in place)
    void rtrim(std::string &s); // trim from end (in place)
    void trim(std::string &s); // trim from both ends (in place)
    // static inline std::string ltrim_copy(std::string s); // trim from start (copying)
    // static inline std::string rtrim_copy(std::string s); // trim from end (copying)
    // static inline std::string trim_copy(std::string s); // trim from both ends (copying)

    // https://stackoverflow.com/questions/10178700/c-strip-non-ascii-characters-from-string
    bool invalidChar (char c); // given a character, returns 'true' if it's a standard ASCII character
    void stripUnicode(std::string & str); // removes all non-ASCII characters from a string


    std::string getNewFolderName(std::string folderName); //given a "parent folder" creates a unique folder based on the date and a number that distinguishes it from other folders made on that date

    std::string combineStrings(std::vector<std::string> strings);

    int charToInt(char c);
    char intToChar(int num);

    // modified from: https://www.oreilly.com/library/view/c-cookbook/0596007612/ch04s12.html
    void removeSubstrs(std::string& s, const std::string& p);

    std::vector<int> makeIntSequence(int min, int max);
}



#endif