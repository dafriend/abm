#ifndef RASTER_H
#define RASTER_H

#include "Point.h"
#include <memory>
#include <vector>
#include <string>

class Raster{
  public:                                          // 5 6 7 8 9
    std::shared_ptr<std::vector<double>> values; // 0 1 2 3 4   
    
    int nX;
    int nY;
    double xMin;
    double xMax;
    double yMin;
    double yMax;
    double xCellLength;
    double yCellLength;
    
    Raster();
    Raster(int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    Raster(std::vector<double> _values, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    Raster(std::shared_ptr<std::vector<double>> _values, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    // PointGrid(std::vector<std::shared_ptr<Agent>> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax);
    // PointGrid(std::vector<std::shared_ptr<Agent>> points, double cellSize);
    
    
    //void addPoint(std::shared_ptr<AgentPoint> point);
    // std::shared_ptr<PointCell> addPoint(std::shared_ptr<Agent> point);
    // void addPoints(std::vector<std::shared_ptr<Agent> > points);
    // void initializePoints(std::vector<std::shared_ptr<Agent> > points);
    //void initializePoints2(std::vector<std::shared_ptr<AgentPoint>> points);
    //void addPoints2(std::vector<std::shared_ptr<AgentPoint>> points);

    int getIndex(int x, int y) const;
    int getIndex(double x, double y) const;
    double getValue(int x, int y) const;
    double getValue(double x, double y) const;
    Point getCoords(int x, int y) const;
    Point getCoords(int index) const;
    std::vector<Point> getPoints() const;

    void setValue(int index, double value);
    void setValue(int x, int y, double value);
    void setValue(double x, double y, double value);
    // PointGrid getCellsByIndex(int _xMin, int _xMax, int _yMin, int _yMax) const;
    // PointGrid getCells(double _xMin, double _xMax, double _yMin, double _yMax) const;
    
    // std::vector<std::shared_ptr<Agent> > getPointsWithin(double x, double y, double dist) const;
    std::string toString(int precision = 2) const;
};

#endif