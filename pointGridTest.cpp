// #include "Quadtree.h"
// #include "Node.h"
// #include "PointGrid.h"
// #include "PointCell.h"
// #include "Simulation.h"
// #include "Matrix.h"
// #include "Point.h"
// #include "Agent.h"
// #include "AgentPoint.h"
// //#include "quadtree/matrix.h"
// #include <iostream>
// #include <vector>
// #include <random>
// #include <ctime>
// #include <chrono>

// //This file is for testing the speed of initializing a PointGrid. One method is to pre-allocate the size of each of the 
// //PointCell vectors. The other just uses 'push_back'. This script compares the speeds of the two and is also a check to 
// //make sure the two methods produce the same results.

// int main(){

//     double xMin = 0;
//     double xMax = 10;
//     double yMin = 0;
//     double yMax = 10;
//     int nX = 200;
//     int nY = 200;
//     int nPoints = 10000000;

//     std::vector<std::shared_ptr<AgentPoint>> points = std::vector<std::shared_ptr<AgentPoint>>(nPoints);
//     //https://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
//     std::mt19937 gen(time(NULL)); //Standard mersenne_twister_engine seeded with rd()
//     std::uniform_real_distribution<> xDist(xMin, xMax);
//     std::uniform_real_distribution<> yDist(yMin, yMax);
//     for(int i = 0; i < nPoints; ++i){
//         //create Agent and AgentPoint
//         points[i] = makeAgentPoint(xDist(gen), yDist(gen));
//     }

//     PointGrid pg1 = PointGrid(nX, nY, xMin, xMax, yMin, yMax);
//     PointGrid pg2 = PointGrid(nX, nY, xMin, xMax, yMin, yMax);

//     std::cout << "\nMethod 1" << std::endl;
//     auto start1 = std::chrono::high_resolution_clock::now();
//     pg1.initializePoints(points);
//     auto end1 = std::chrono::high_resolution_clock::now();
//     auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>(end1 - start1); 
//     std::cout << duration1.count() << " microseconds" << std::endl; 


//     std::cout << "\nMethod 2" << std::endl;
//     auto start2 = std::chrono::high_resolution_clock::now();
//     pg2.initializePoints2(points);
//     auto end2 = std::chrono::high_resolution_clock::now();
//     auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(end2 - start2); 
//     std::cout << duration2.count() << " microseconds"<< std::endl; 
    
//     // std::cout << "\nMethod 1 results" << std::endl;
//     // std::cout << pg1.toString() << std::endl;

//     // std::cout << "\nMethod 2 results:" << std::endl;
//     // std::cout << pg2.toString() << std::endl;
//     return 0;
// }