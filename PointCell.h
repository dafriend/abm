#ifndef POINTCELL_H
#define POINTCELL_H

//#include "Point.h"
#include <vector>
#include <memory>
#include <string>

class Agent;

class PointCell{
public:
  int id;
  std::vector<std::shared_ptr<Agent>> points;
  //std::shared_ptr<PointCell> ptr;
  
  PointCell();
  PointCell(int _id);
  PointCell(int _id, int size);
  PointCell(int _id, std::vector<std::shared_ptr<Agent>> &_points);

  std::string toString() const;
};

// std::shared_ptr<PointCell> makePointCell(int id);
// std::shared_ptr<PointCell> makePointCell(int id, int size);
// std::shared_ptr<PointCell> makePointCell(int id, std::vector<std::shared_ptr<Agent>> &_points);

#endif