#include "PointGrid.h"
#include "PointCell.h"
//#include "Point.h"
#include "Agent.h"
#include "constants.h"
#include <memory>
#include <thread>
#include <iostream>
#include <cmath>
#include <list>

//#include <Rcpp.h> //only needed for debugging purposes - can be removed eventually

//using namespace Rcpp; //only needed for debugging purposes - can be removed eventually

PointGrid::PointGrid()
  :nX{0}, nY{0}, xMin{0}, xMax{0}, yMin{0}, yMax{0}, xCellLength{0}, yCellLength{0}{//, nPoints{0}{
    //if(DEBUG) cout << "PointGrid()"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    
  cells = std::vector<std::shared_ptr<PointCell>>(0);
}

PointGrid::PointGrid(int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax) 
  : nX{_nX}, nY{_nY}, xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax}{//, nPoints{0} {      
    //if(DEBUG) cout << "PointGrid(int _nX, int _nY)"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    xCellLength = (xMax-xMin)/nX;
    yCellLength = (yMax-yMin)/nY;
    cells = std::vector<std::shared_ptr<PointCell>>(nX*nY);
    // for(int xi = 0; xi < nX; ++xi){
    //     for(int yi = 0; yi < nY; ++yi){
    //         //if(DEBUG) cout << "xi: "<< xi << " | yi:" << yi << std::endl;
    //         //if(DEBUG) cout << "yi*nX + xi = "<< yi*nX + xi<< std::endl;
    //         //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    //         //cells[yi*nX + xi] = makePointCell(yi*nX + xi);  
    //         cells[yi*nX + xi] = std::make_shared<PointCell>(yi*nX + xi);
    //     }
    // }
}

// PointGrid::PointGrid(std::vector<std::shared_ptr<Point>> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)
//   : nX{_nX}, nY{_nY}, xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax}  {
PointGrid::PointGrid(std::list<std::shared_ptr<Agent>> &points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)
  : PointGrid(_nX, _nY, _xMin, _xMax, _yMin, _yMax) {
  
  //if(DEBUG) cout << "PointGrid(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  //calculate some of the basic info about the PointGrid we're going to make
  xCellLength = (xMax-xMin)/nX;
  yCellLength = (yMax-yMin)/nY;
  //nPoints = points.size();
  
  //initialize the vector that'll contain our points
  cells = std::vector<std::shared_ptr<PointCell>>(nX*nY);
  
  //initialize the PointCells
  // std::cout << "PointGrid constructor: before 'initializePoints();\n";
  initializePoints(points);
  // std::cout << "PointGrid constructor: after 'initializePoints();\n";
  //if(DEBUG) cout << "PointGrid(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax) 1"<< std::endl;
  //if(DEBUG) cout << "cells.size(): " << cells.size() << std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  //initialize the PointCells
  //initializePoints();

  // for(int xi = 0; xi < nX; ++xi){
  //   for(int yi = 0; yi < nY; ++yi){
  //     //if(DEBUG) cout << "xi: "<< xi << " | yi:" << yi << std::endl;
  //     //if(DEBUG) cout << "yi*nX + xi = "<< yi*nX + xi<< std::endl;
  //     //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  //     cells[yi*nX + xi] = makePointCell(yi*nX + xi);  
  //   }
  // }
  
  // //if(DEBUG) cout << "PointGrid(std::vector<Point> points, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax) 2"<< std::endl;
  
  // //add the points
  // addPoints(points);
  
  //if(DEBUG) cout << "nX: " << nX << std::endl;
  //if(DEBUG) cout << "nY: " << nY << std::endl;
  //if(DEBUG) cout << "nPoints: " << nPoints << std::endl;
  //if(DEBUG) cout << "xMin: " << xMin << std::endl;
  //if(DEBUG) cout << "xMax: " << xMax << std::endl;
  //if(DEBUG) cout << "yMin: " << yMin << std::endl;
  //if(DEBUG) cout << "yMax: " << yMax << std::endl;
  //if(DEBUG) cout << "xCellLength: "  << xCellLength << std::endl;
  //if(DEBUG) cout << "yCellLength: " << yCellLength << std::endl;
  
}
PointGrid::PointGrid(std::list<std::shared_ptr<Agent>> &points, double cellSize){
  // double xMin = quadtree->root->xMin;
  // double yMin = quadtree->root->yMin;    
  // double xMax = xMin + nX*params.initPointGridCellSize;
  // double yMax = yMin + nX*params.initPointGridCellSize;
  //std::cout << "pgcheck1\n";
  xMin = points.front()->x;
  xMax = points.front()->x;
  yMin = points.front()->y;
  yMax = points.front()->y;
  //std::cout << "pgcheck2\n";
  //for(size_t i = 1; i < points.size(); ++i){
  for(auto &iPoint : points){
    if(iPoint->x > xMax) xMax = iPoint->x;
    if(iPoint->x < xMin) xMin = iPoint->x;
    if(iPoint->y > yMax) yMax = iPoint->y;
    if(iPoint->y < yMin) yMin = iPoint->y;
  }
  //std::cout << "pgcheck3\n";

  nX = std::ceil((xMax - xMin)/cellSize);
  nY = std::ceil((yMax - yMin)/cellSize);
  xCellLength = cellSize;
  yCellLength = cellSize;
  // nPoints = points.size();
  //std::cout << "pgcheck4\n";
  cells = std::vector<std::shared_ptr<PointCell>>(nX*nY);
  //std::cout << "pgcheck5\n";
  initializePoints(points);
  //std::cout << "pgcheck6\n";
}

PointGrid::PointGrid(std::vector<std::shared_ptr<PointCell>> &_cells, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax, int _nPoints)
  : cells{_cells}, nX{_nX}, nY{_nY}, xMin{_xMin}, xMax{_xMax}, yMin{_yMin}, yMax{_yMax}{//, nPoints{_nPoints} {
    //if(DEBUG) cout << "PointGrid(std::vector<std::shared_ptr<PointCell>> _cells, int _nX, int _nY, double _xMin, double _xMax, double _yMin, double _yMax)"<< std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    
    xCellLength = (xMax-xMin)/nX;
    yCellLength = (yMax-yMin)/nY;
  }

//void PointGrid::addPoint(std::shared_ptr<AgentPoint> point){
// adds a point to the pointGrid and returns a pointer to the cell the point was added to
std::shared_ptr<PointCell> PointGrid::addPoint(std::shared_ptr<Agent> point){
  //if(DEBUG) cout << "addPoint(Point point)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  //get the cell we want based on the x and y coordinates and then add this point to the cell
  if(point->x >= xMin && point->x <= xMax && point->y >= yMin && point->y <= yMax){
    std::shared_ptr<PointCell> cell = getCell(point->x, point->y);
    cell->points.push_back(point);
    return cell;
  } else {
    return nullptr;
    //std::cout << "point (" << point.x << "," << point.y << ") falls outside the limits (x: (" << xMin << ":" << xMax << "), y: (" << yMin << ":" << yMax << "))" << std::endl;
  }
}

void PointGrid::addPoints(std::vector<std::shared_ptr<Agent>> &points){
  //if(DEBUG) cout << "addPoints(std::vector<Point> points)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  for(size_t i = 0; i < points.size(); ++i){
    //get the cell we want based on the x and y coordinates and then add this point to the cell
    //getCell(points[i].x, points[i].y)->points.push_back(points[i]);
    addPoint(points[i]);
  }
}

//compared this to 'initializePoints2' (see Evernote entry from 12/10/2020) - that one's faster
// void PointGrid::initializePoints(std::vector<std::shared_ptr<AgentPoint>> points){
//   //initialize the PointCells
//   for(int xi = 0; xi < nX; ++xi){
//     for(int yi = 0; yi < nY; ++yi){
//       //if(DEBUG) cout << "xi: "<< xi << " | yi:" << yi << std::endl;
//       //if(DEBUG) cout << "yi*nX + xi = "<< yi*nX + xi<< std::endl;
//       //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
//       cells[yi*nX + xi] = makePointCell(yi*nX + xi);  
//     }
//   }
//   addPoints(points);
// }

//testing to see if it's faster to do a pre-allocation step
void PointGrid::initializePoints(std::list<std::shared_ptr<Agent>> &points){
  //if(DEBUG) cout << "addPoints(std::vector<Point> points)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  //std::cout << "PointGrid::initializePoints()\n";
  //std::cout << "ip_check1" << std::endl;
  //figure out how many points fall in each cell
  std::vector<int> nPointsInCell(cells.size(), 0);
  //for(size_t i = 0; i < points.size(); ++i){
  for(auto &iAgent : points){
    int index = getIndex(iAgent->x, iAgent->y);
    nPointsInCell[index]++;
  }
  //std::cout << "ip_check2" << std::endl;
  //create each PointCell, allocating the size of each vector based on what we just calculated
  for(int xi = 0; xi < nX; ++xi){
    for(int yi = 0; yi < nY; ++yi){
      int index = getIndex(xi, yi);
      //cells[index] = makePointCell(yi*nX + xi, nPointsInCell[index]);  
      cells[index] = std::make_shared<PointCell>(yi*nX + xi, nPointsInCell[index]);
    }
  }
  //std::cout << "ip_check3" << std::endl;
  //we need a way of keeping track of how many points we've added to each PointCell - that's what this vector is for
  std::vector<int> nPointsTracker(cells.size(), 0);
  //add the points to the PointCells
  //for(size_t i = 0; i < points.size(); ++i){
  for(auto &iPoint : points){
    // std::cout << "ip for loop, i=" << i << "\n";
    //get the cell we want based on the x and y coordinates and then add this point to the cell
    //getCell(points[i].x, points[i].y)->points.push_back(points[i]);
    // std::cout << "ip for loop, check1\n";
    // std::cout << "points.at(i)->x: " << points.at(i)->x << "\n";
    // std::cout << "points.at(i)->y: " << points.at(i)->y << "\n";

    int pointGridIndex = getIndex(iPoint->x, iPoint->y);
    // std::cout << "ip for loop, check2\n";
    // std::cout << "nPointsTracker.size(): " << nPointsTracker.size() << "\n";
    // std::cout << "pointGridIndex:  " << pointGridIndex << "\n";
    int pointCellIndex = nPointsTracker[pointGridIndex];
    // std::cout << "ip for loop, check3\n";
    //cells.at(pointGridIndex)->points.at(pointCellIndex) = points.at(i);
    cells.at(pointGridIndex)->points[pointCellIndex] = iPoint;
    // std::cout << "ip for loop, check4\n";
    nPointsTracker.at(pointGridIndex)++;
    // std::cout << "ip for loop, check5\n";
  }
  // std::cout << "ip_check4" << std::endl;
}


// this is the same as the above function but for only adult males - this is for reproduction
// this is super hacky but it's crunch time and I don't have time to make my code pretty
void PointGrid::initializePointsAdultMales(std::list<std::shared_ptr<Agent>> &points, int adultAge){
  
  //figure out how many points fall in each cell
  std::vector<int> nPointsInCell(cells.size(), 0);
  for(auto &iAgent : points){
    if(iAgent->isMale == 1 && iAgent->age >= adultAge){
        int index = getIndex(iAgent->x, iAgent->y);
        nPointsInCell[index]++;
    }
  }
  //create each PointCell, allocating the size of each vector based on what we just calculated
  for(int xi = 0; xi < nX; ++xi){
    for(int yi = 0; yi < nY; ++yi){
      int index = getIndex(xi, yi);
      cells[index] = std::make_shared<PointCell>(yi*nX + xi, nPointsInCell[index]);
    }
  }
  //we need a way of keeping track of how many points we've added to each PointCell - that's what this vector is for
  std::vector<int> nPointsTracker(cells.size(), 0);
  //add the points to the PointCells
  for(auto &iAgent : points){
    if(iAgent->isMale == 1 && iAgent->age >= adultAge){
        //get the cell we want based on the x and y coordinates and then add this point to the cell
        int pointGridIndex = getIndex(iAgent->x, iAgent->y);
        int pointCellIndex = nPointsTracker[pointGridIndex];
        cells.at(pointGridIndex)->points[pointCellIndex] = iAgent;
        nPointsTracker.at(pointGridIndex)++;
    }
  }
}

//gets the index based on the ROW/COL (NOT coordinates)
int PointGrid::getIndex(int x, int y) const{
    if(x >= 0 || x < nX || y >= 0 || y < nY){ //check to make sure the indices are valid
      int index = y*nX + x; 
      return index;
    } else {
      return -1;
  }
}

//gets the index based on the COORDINATES (NOT row/col)
int PointGrid::getIndex(double x, double y) const{
  int xIndex = floor((x-xMin)/xCellLength);
  int yIndex = floor((y-yMin)/yCellLength);
  return getIndex(xIndex, yIndex);
}

//gets the cell based on the ROW/COL (NOT coordinates)
std::shared_ptr<PointCell> PointGrid::getCell(int x, int y) const{
  int index = getIndex(x,y);
  if(index != -1){
    return cells.at(index);
  } else {
    return nullptr;
  }

}

//gets the cell based on the COORDINATES (NOT row/col)
std::shared_ptr<PointCell> PointGrid::getCell(double x, double y) const{
  int index = getIndex(x,y);
  if(index != -1){
    return cells.at(index);
  } else {
    return nullptr;
  }
  //if(DEBUG) cout << "getCell(double x, double y)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  // //figure out the indices of the cell that contains the point
  // int xIndex = floor((x-xMin)/xCellLength);
  // int yIndex = floor((y-yMin)/yCellLength);
  // return getCell(xIndex, yIndex); //use those indices to get the cell
}

PointGrid PointGrid::getCellsByIndex(int xMinIndex, int xMaxIndex, int yMinIndex, int yMaxIndex) const{
  //if(DEBUG) cout << "getCellsByIndex(int xMin, int xMax, int yMin, int yMax)"<< std::endl;
  //if(DEBUG) cout << "int _xMin: " << _xMin << std::endl;
  //if(DEBUG) cout << "int _xMax: " << _xMax << std::endl;
  //if(DEBUG) cout << "int _yMin: " << _yMin << std::endl;
  //if(DEBUG) cout << "int _yMax: " << _yMax << std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  //There's got to be a better way than this to do range checking....
  
  if(xMinIndex < nX && xMaxIndex > 0 && yMinIndex < nY && yMaxIndex > 0){
    if(xMinIndex < 0) xMinIndex = 0;
    if(xMaxIndex >= nX) xMaxIndex = nX-1;
    if(yMinIndex < 0) yMinIndex = 0;
    if(yMaxIndex >= nY) yMaxIndex = nY-1;
    
    //if(DEBUG) cout << "int _xMin: " << _xMin << std::endl;
    //if(DEBUG) cout << "int _xMax: " << _xMax << std::endl;
    //if(DEBUG) cout << "int _yMin: " << _yMin << std::endl;
    //if(DEBUG) cout << "int _yMax: " << _yMax << std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    //get the number of cells in each direction
    int _nX = xMaxIndex - xMinIndex + 1;
    int _nY = yMaxIndex - yMinIndex + 1;
    
    //intialize a vector for storing pointers to PointCells
    std::vector<std::shared_ptr<PointCell>> cellsSub(_nX*_nY);
    int _npoints{0}; //this is for keeping track of the total number of points contained in the subset
    
    //if(DEBUG) cout << "_nX: " << _nX << std::endl;
    //if(DEBUG) cout << "_nY: " << _nY << std::endl;
    //if(DEBUG) cout << "cellsSub.size(): " << cellsSub.size() << std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    
    //retrieve each cell in the desired range
    for(int xi = 0; xi < _nX; xi++){
      for(int yi = 0; yi < _nY; yi++){
        //if(DEBUG) cout << "xi: "<< xi << " | yi:" << yi << std::endl;
        //if(DEBUG) cout << "yi*_nX + xi = "<< yi*_nX + xi<< std::endl;
        //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
        
        std::shared_ptr<PointCell> pc = getCell(xi+xMinIndex, yi+yMinIndex); //get a pointer to the cell we want
        cellsSub[yi*_nX + xi] = pc; //store this pointer at the correct index in our 'cellsSub' vector
        _npoints += pc->points.size(); //add the number of points in this cell to the total number of points
      }
    }
    //if(DEBUG) cout << "_npoints: " << _npoints << std::endl;
    //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
    
    //use what we've calculated to make a new PointGrid
    PointGrid pg(cellsSub, _nX, _nY, xMinIndex, xMaxIndex, yMinIndex, yMaxIndex, _npoints);
    //PointGridWrapper pgw(pg);
    ////if(DEBUG) cout << pgw.asMatrix() << std::endl;
    return pg;
  } else {
    int _nX = xMaxIndex - xMinIndex + 1;
    int _nY = yMaxIndex - yMinIndex + 1;
    auto vec = std::vector<std::shared_ptr<PointCell>>(0);
    PointGrid pg(vec, _nX, _nY, xMinIndex, xMaxIndex, yMinIndex, yMaxIndex, 0);
    return pg;
  }
}

PointGrid PointGrid::getCells(double _xMin, double _xMax, double _yMin, double _yMax) const{
  //if(DEBUG) cout << "getCells(double xMin, double xMax, double yMin, double yMax)"<< std::endl;
  //if(DEBUG) cout << "double xMin: " << _xMin << std::endl;
  //if(DEBUG) cout << "double xMax: " << _xMax << std::endl;
  //if(DEBUG) cout << "double yMin: " << _yMin << std::endl;
  //if(DEBUG) cout << "double yMax: " << _yMax << std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  
  //calculate the indices of the PointCells we need to retrieve in order to fully cover the range specified
  int xMinInt = floor((_xMin - xMin)/xCellLength);
  int xMaxInt = ceil((_xMax - xMin)/xCellLength) - 1;
  int yMinInt = floor((_yMin - yMin)/yCellLength);
  int yMaxInt = ceil((_yMax - yMin)/yCellLength) - 1;
  
  //use those indices to retrieve the cells we want
  return getCellsByIndex(xMinInt, xMaxInt, yMinInt, yMaxInt);
}

int PointGrid::nPointsWithin(double x, double y, double dist) const{
  //if(DEBUG) cout << "getPointsWithin(double x, double y, double dist)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  PointGrid subGrid = getCells(x-dist, x+dist, y-dist, y+dist);
  //if(DEBUG) cout << "subGrid.cells.size(): " << subGrid.cells.size() << std::endl;

  int nPoints{0}; //counter for how many points we've found
  //now go through all the points in each cell and check if they're within the distance 
  double distSquared = pow(dist, 2);
  // int counter{0};// 2/19/2021 d
  for(size_t i = 0; i < subGrid.cells.size(); ++i){
    for(size_t j = 0; j < subGrid.cells.at(i)->points.size(); ++j){
      double distBtwSquared = pow(x-subGrid.cells.at(i)->points.at(j)->x, 2) + pow(y-subGrid.cells.at(i)->points.at(j)->y, 2);
      //if(DEBUG) cout << distBtwSquared << std::endl;
      if(distBtwSquared < distSquared){
        nPoints++;
      }
    }
  }
  return nPoints;
}

// std::vector<std::shared_ptr<Agent>> PointGrid::getPointsWithin(double x, double y, double dist) const{ // 2/19/2021 d
std::list<std::shared_ptr<Agent>> PointGrid::getPointsWithin(double x, double y, double dist) const{ // 2/19/2021 d
  //if(DEBUG) cout << "getPointsWithin(double x, double y, double dist)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  PointGrid subGrid = getCells(x-dist, x+dist, y-dist, y+dist);
  //if(DEBUG) cout << "subGrid.cells.size(): " << subGrid.cells.size() << std::endl;
  //get the number of total points in these cells - I'll make the vector for storing the points this big
  int nPoints{0}; 
  for(size_t i = 0; i < subGrid.cells.size(); ++i){
    nPoints += subGrid.cells[i]->points.size();
  }
  
  //now go through all the points in each cell and check if they're within the distance
  // std::vector<std::shared_ptr<Agent>> points(nPoints); // 2/19/2021 d
  //std::list<std::shared_ptr<Agent>> points(nPoints); // 2/19/2021 i
  std::list<std::shared_ptr<Agent>> points; // 2/19/2021 i
  double distSquared = pow(dist, 2);
  // int counter{0};// 2/19/2021 d
  for(size_t i = 0; i < subGrid.cells.size(); ++i){
    for(size_t j = 0; j < subGrid.cells.at(i)->points.size(); ++j){
      double distBtwSquared = pow(x-subGrid.cells.at(i)->points.at(j)->x, 2) + pow(y-subGrid.cells.at(i)->points.at(j)->y, 2);
      //if(DEBUG) cout << distBtwSquared << std::endl;
      if(distBtwSquared < distSquared){
        // points[counter] = subGrid.cells[i]->points[j]; // 2/19/2021 d
        //points.push_back(subGrid.cells[i]->points[i]); // 2/19/2021 i
        points.push_back(subGrid.cells.at(i)->points.at(j)); // 2/19/2021 i
        //if(DEBUG) cout << points[counter].x << ", " << points[counter].y << std::endl;
        // counter += 1; // 2/19/2021 d
        
      }
    }
  }
  // points.resize(counter); // 2/19/2021 d
  return points;
}

// std::vector<std::shared_ptr<Agent>> PointGrid::getPointsWithin(double x, double y, double dist) const{ // 2/19/2021 d
std::vector<std::shared_ptr<Agent>> PointGrid::getPointsWithin_Vector(double x, double y, double dist) const{ // 2/19/2021 d
  //if(DEBUG) cout << "getPointsWithin(double x, double y, double dist)"<< std::endl;
  //if(DEBUG) std::this_thread::sleep_for(std::chrono::milliseconds(WAITTIME));
  PointGrid subGrid = getCells(x-dist, x+dist, y-dist, y+dist);
  //if(DEBUG) cout << "subGrid.cells.size(): " << subGrid.cells.size() << std::endl;
  //get the number of total points in these cells - I'll make the vector for storing the points this big
  int nPoints{0}; 
  for(size_t i = 0; i < subGrid.cells.size(); ++i){
    nPoints += subGrid.cells[i]->points.size();
  }
  
  //now go through all the points in each cell and check if they're within the distance
  // std::vector<std::shared_ptr<Agent>> points(nPoints); // 2/19/2021 d
  //std::list<std::shared_ptr<Agent>> points(nPoints); // 2/19/2021 i
  std::vector<std::shared_ptr<Agent>> points; // 2/19/2021 i
  double distSquared = pow(dist, 2);
  // int counter{0};// 2/19/2021 d
  for(size_t i = 0; i < subGrid.cells.size(); ++i){
    for(size_t j = 0; j < subGrid.cells.at(i)->points.size(); ++j){
      double distBtwSquared = pow(x-subGrid.cells.at(i)->points.at(j)->x, 2) + pow(y-subGrid.cells.at(i)->points.at(j)->y, 2);
      //if(DEBUG) cout << distBtwSquared << std::endl;
      if(distBtwSquared < distSquared){
        // points[counter] = subGrid.cells[i]->points[j]; // 2/19/2021 d
        //points.push_back(subGrid.cells[i]->points[i]); // 2/19/2021 i
        points.emplace_back(subGrid.cells.at(i)->points.at(j)); // 2/19/2021 i
        //if(DEBUG) cout << points[counter].x << ", " << points[counter].y << std::endl;
        // counter += 1; // 2/19/2021 d
        
      }
    }
  }
  // points.resize(counter); // 2/19/2021 d
  return points;
}

std::string PointGrid::toString() const{
  
  int max_size{0};
  for(int i_y = nY-1; i_y >= 0; --i_y){
    for(int i_x = 0; i_x < nX; ++i_x){
      int size_i = getCell(i_x,i_y)->points.size();
      if(size_i > max_size){
        max_size = size_i;
      }
    }
  }
  std::string max_size_str = std::to_string(max_size);
  int max_char = max_size_str.length();

  std::string str("");
  // str = str + 
  //   "nX: " + std::to_string(nX) + "\n" +
  //   "nY: " + std::to_string(nY) + "\n" +
  //   "xMin: " + std::to_string(xMin) + "\n" +
  //   "xMax: " + std::to_string(xMax) + "\n" +
  //   "yMin: " + std::to_string(yMin) + "\n" +
  //   "yMax: " + std::to_string(yMax) + "\n" +
  //   "xCellLength: " + std::to_string(xCellLength) + "\n" +
  //   "yCellLength: " + std::to_string(yCellLength) + "\n" +
  //   "nPoints: " + std::to_string(nPoints) + "\n";
  std::string hline(max_char*nX + nX + 1, '-');
  for(int i_y = nY-1; i_y >= 0; --i_y){
    str = str + hline + "\n" + "|";
    //s.append(20 - s.length(), 'X');
    for(int i_x = 0; i_x < nX; ++i_x){
      int size_i = getCell(i_x,i_y)->points.size();
      std::string size_i_str = std::to_string(size_i);
      size_i_str.insert(size_i_str.begin(), max_char - size_i_str.length(), ' ');
      str = str + size_i_str + "|";
    }
    str = str + "\n";
  }
  str = str + hline + "\n";
  return str;
}

// +--+--+--+
// |45| 0|20|
// +--+--+--+
// |34| 1|30|
// +--+--+--+
// | 0|98|11| 
// +--+--+--+