//header guard included becuase 'ProbabilityTable.h' includes this file. It has to since it uses template classes. See the following link:
//https://www.codeproject.com/Articles/48575/How-to-Define-a-Template-Class-in-a-h-File-and-Imp
//This link doesn't say to use a header guard but I found that it works
#ifndef PROBABILITYTABLE_CPP
#define PROBABILITYTABLE_CPP


#include "ProbabilityTable.h"
#include "BasicUtilities.h"
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept> // std::runtime_error
#include <vector>
#include <random>
#include <chrono>

template <class T>
class Row{
public:
    T value;
    double prob;
};


template <class T>
ProbabilityTable<T>::ProbabilityTable(){
    filePath = "";
    vals = std::vector<T>(0);
    probs = std::vector<double>(0);
}

template <class T>
ProbabilityTable<T>::ProbabilityTable(std::string _filePath) : filePath{_filePath}{
    readCSV();
}

template <class T>
void ProbabilityTable<T>::readCSV(){
    //https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    std::fstream fin;
    fin.open(filePath, std::fstream::in);

    if(!fin.is_open()) throw std::runtime_error("Could not open file");

    std::string line, colname;
    //double val;

    //read the column names
    //int nNames{0};

    // if(fin.good()){
    //     std::getline(fin, line);
    //     std::stringstream ss(line);

    //     int counter{0};
    //     //check column names, make sure they're valid
    //     while(std::getline(ss,colname, ',')){
    //         //std::cout << colname << "\n";
    //         if(counter == 0){
    //             if(colname != std::string("\"val\"")){
    //                 throw std::runtime_error("Error reading CSV (" + filePath + "): First column must be called 'val'");
    //             }                
    //         } else if (counter == 1){
    //             if(colname != "\"prob\""){
    //                 throw std::runtime_error("Error reading CSV(" + filePath + "): Second column must be called 'prob'");
    //             }                    
    //         } else {
    //             std::cout << "Warning while reading CSV(" + filePath + "): Third column detected. Only first two columns will be read. All others will be ignored." + "\n";
    //             break;
    //         }
    //         counter++;
    //     }
    // }
    if(fin.good()){
        std::getline(fin, line);
        std::istringstream ss(line);

        int colCounter{0};
        //check column names, make sure they're valid
        while(std::getline(ss,colname, ',')){
            //std::cout << colname << "\n";
            std::string oldColname = colname; //for debugging only
            //colname = BasicUtilities::cleanString(colname);
            BasicUtilities::cleanString(colname);
            if(colCounter == 0){
                if((colname != "\"val\"") && (colname != "val") && (colname != "'val'")){
                    throw std::runtime_error("Error reading CSV (" + filePath + "): First column must be called 'param' (value read: " + colname + ")");
                }                
            } else if (colCounter == 1){
                if((colname != "\"prob\"") && (colname != "prob") && (colname != "'prob'")){
                    throw std::runtime_error("Error reading CSV(" + filePath + "): Second column must be called 'val' (value read: " + colname + ")");
                }                    
            } else {
                std::cout << "Warning while reading CSV(" + filePath + "): Third column detected. Only first two columns will be read. All others will be ignored." + "\n";
                break;
            }
            colCounter++;
        }
    }
    //create a vector of vectors for reading in the data
    //std::vector<std::vector<double>> rows = std::vector<std::vector<double>>();
    std::vector<Row<T>> rows = std::vector<Row<T>>();
    int rowIdx{0};
    //std::cout << "first while" << "\n";
    while(std::getline(fin, line)){
        //std::cout << rowIdx << "\n";
        std::string value;
        std::stringstream ss(line);
        int colIdx{0};
        //std::vector<double> tempRow(2);
        Row<T> tempRow;
        //rows.push_back(std::vector<double>(2));
        //while(ss >> val){
        while(std::getline(ss,value, ',')){
            if(colIdx == 0){
                tempRow.value = std::stod(value);
            } else if (colIdx == 1){   
                tempRow.prob = std::stod(value);
            } else {
                throw std::runtime_error("Error reading CSV (" + filePath + "): third column detected");
            }
            //tempRow.at(colIdx) = std::stod(value);
            colIdx++;
        }
        rows.push_back(tempRow);
        rowIdx++;
    }

    fin.close();
    
    //I'm reading the data into the rows vector first, then using that length to set the size of the 'vals' and 'probs' vectors. Not sure this is necessary. But I'm paranoid about using parallel arrays - especially if I was filling them with 'push_back()', I think I could have the potential of the vectors being different lengths (say one row in the CSV was incomplete and only had 1 value). Doing it this way should guarantee that the values match up.
    vals = std::vector<T>(rows.size());
    probs = std::vector<double>(rows.size());
    //std::cout << "for loop" << "\n";
    for(size_t i = 0; i < rows.size(); ++i){
        //std::cout << i << "\n";
        vals.at(i) = rows.at(i).value;
        probs.at(i) = rows.at(i).prob;
    }
}

template <class T>
void ProbabilityTable<T>::readCSV(std::string _filePath){
    filePath = _filePath;
    readCSV();
}
   
template <class T>
std::string ProbabilityTable<T>::toString(){
    //std::cout << "toString()\n";
    //std::cout << "vals.size()" << vals.size() << "\n";
    //std::cout << "probs.size()" << probs.size() << "\n";
    std::string str = "";
    for(size_t i = 0; i < vals.size(); ++i){
        //std::cout << i << "\n";
        str = str + std::to_string(vals.at(i)) + "  |  " + std::to_string(probs.at(i)) + "\n";
    }
    return str;
}

template <class T>
T ProbabilityTable<T>::getRandomValue(std::mt19937 &randomGenerator){
    std::discrete_distribution<int> distribution(probs.begin(), probs.end());
    int index = distribution(randomGenerator);
    return vals.at(index);
}

template <class T>
double ProbabilityTable<T>::getProb(T value){
    int index{-1};
    for(size_t i = 0; i < vals.size(); ++i){
        if(vals.at(i) == value){
            if(index == -1){
                index = i;
            } else {
                throw std::runtime_error("Error in ProbabilityTable<T>::getProb(T value): multiple matches found for 'value'");
            }
        }
    }
    if(index == -1){
        throw std::runtime_error(std::string("Error in ProbabilityTable<T>::getProb(T value): 'value' (") + std::to_string(value) + std::string(") not found in 'vals'"));
    }
    return probs.at(index);
}

#endif